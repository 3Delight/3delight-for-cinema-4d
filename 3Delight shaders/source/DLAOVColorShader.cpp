#include "IDs.h"
#include "c4d.h"
#include "dl_aov_color.h"
#include "RegisterPrototypes.h"

class DL_AOV_COLOR : public ShaderData
{
public:
	virtual Bool Init(GeListNode* node);
	virtual Bool GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
	virtual Vector Output(BaseShader* sh, ChannelData* sd);
	static NodeData* Alloc(void)
	{
		return NewObjClear(DL_AOV_COLOR);
	}
};
Bool DL_AOV_COLOR::Init(GeListNode* node)
{
	BaseContainer* data = ((BaseShader*) node)->GetDataInstance();
	// data->SetVector(AOV_COLOR, HSVToRGB(Vector(0, 0, 0)));
	return true;
}

Bool DL_AOV_COLOR::GetDDescription(GeListNode* node,
								   Description* description,
								   DESCFLAGS_DESC& flags)
{
	description->LoadDescription(node->GetType());
	flags |= DESCFLAGS_DESC::LOADED;
	BaseContainer* dldata = ((BaseObject*) node)->GetDataInstance();
	return TRUE;
}

Bool DL_AOV_COLOR::Message(GeListNode* node, Int32 type, void* data)
{
	if (!node) {
		return false;
	}

	BaseDocument* doc = GetActiveDocument();
	BaseContainer* dldata = ((BaseMaterial*) node)->GetDataInstance();
	return true;
}

Vector
DL_AOV_COLOR::Output(BaseShader* sh, ChannelData* sd)
{
	return Vector(0, 0, 0);
}

Bool RegisterAovColor(void)
{
	return RegisterShaderPlugin(DL_COLOR_AOV,
								"Color AOV"_s,
								PLUGINFLAG_HIDE,
								DL_AOV_COLOR::Alloc,
								"dl_aov_color"_s,
								0);
}
