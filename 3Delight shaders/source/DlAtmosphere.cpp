#include "IDs.h"
#include "TextureUI_Functions.h"
#include "c4d.h"
#include "c4d_symbols.h"
#include "dl_atmosphere.h"
#include "RegisterPrototypes.h"

class DL_Atmoshphere : public ObjectData
{
	INSTANCEOF(DL_Atmoshphere, MaterialData)

private:
	Vector color;

public:
	virtual Bool Init(GeListNode* node);
	virtual INITRENDERRESULT InitRender(BaseMaterial* mat,
										const InitRenderStruct& irs);
	virtual void CalcSurface(BaseMaterial* mat, VolumeData* vd);
	static NodeData* Alloc(void)
	{
		return NewObjClear(DL_Atmoshphere);
	}
};

Bool DL_Atmoshphere::Init(GeListNode* node)
{
	BaseContainer* data = ((BaseMaterial*) node)->GetDataInstance();
	Vector atmosphere_color = HSVToRGB(Vector(1, 0.0, 0.1));
	data->SetVector(DL_ATMOSPHERE_COLOR, atmosphere_color);
	data->SetFloat(DL_ATMOSPHERE_DENSITY, 0.1);
	data->SetFloat(DL_ATMOSPHERE_SUPER_REFLECTIVE, 0);
	return true;
}

INITRENDERRESULT
DL_Atmoshphere::InitRender(BaseMaterial* mat, const InitRenderStruct& irs)
{
	return INITRENDERRESULT::OK;
}

void DL_Atmoshphere::CalcSurface(BaseMaterial* mat, VolumeData* vd)
{
	Vector diff, spec;
	vd->IlluminanceSimple(&diff, &spec, 0, 0, 0);
	vd->col = 0.8 * diff;
}

Bool RegisterDLAtmosphere(void)
{
	return RegisterObjectPlugin(DL_ATMOSPHERE_MAT,
								"Atmosphere"_s,
								OBJECT_GENERATOR | PLUGINFLAG_HIDEPLUGINMENU,
								DL_Atmoshphere::Alloc,
								"dl_atmosphere"_s,
								0,
								0);
}
