#include "main.h"
#include "OSL_shader_info.h"
#include <vector>
#include <unordered_map>

/*
	This function is used to dynamically change the structure of Create
	Material UI. We create a SubTitle with the name 3Delight when create material
	is pressed. Then under this Subtitle we put all our command plugins as below.
*/


//global table for keeping track of registered OSL shaders
std::map<Int32, OSL_shader_info> OSL_shader_table;
static void Create3DelightMenu(BaseContainer* bc)
{
	if (!bc) {
		return;
	}

	BrowseContainer browse(bc);
	Int32 id = 0;
	GeData* dat = nullptr;

	while (browse.GetNext(&id, &dat)) {
		if (id == MENURESOURCE_SUBMENU) {
			BaseContainer* container = dat->GetContainer();
			String subtitle = container->GetString(MENURESOURCE_SUBTITLE);

			if (container && subtitle == "IDS_EDITOR_PIPELINE") {
				BaseContainer sc;
				sc.InsData(MENURESOURCE_SUBTITLE, String("3Delight"));
				BaseContainer MaterialsSubMenu;
				MaterialsSubMenu.InsData(MENURESOURCE_SUBTITLE, String("Materials"));
				for (auto osl_shader : OSL_shader_table)
				{
					if (osl_shader.second.m_is_surface)
					{
						MaterialsSubMenu.InsData(MENURESOURCE_COMMAND,
							"PLUGIN_CMD_" + String::IntToString(osl_shader.first + 50));
					}
				}
				MaterialsSubMenu.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(ID_AOV_GROUP));
				BaseContainer VolumesSubMenu;
				VolumesSubMenu.InsData(MENURESOURCE_SUBTITLE, String("Volumes"));
				VolumesSubMenu.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(DL_OPENVDB));
				VolumesSubMenu.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(DL_ATMOSPHERE_MAT));
				BaseContainer LightsSubMenu;
				BaseContainer AreaLightSubMenu;
				AreaLightSubMenu.InsData(MENURESOURCE_SUBTITLE, String("Area Light"));
				AreaLightSubMenu.InsData(MENURESOURCE_SUBTITLE_ICONID, ID_AREALIGHT);
				AreaLightSubMenu.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(ID_AREALIGHT_SQUARE));
				AreaLightSubMenu.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(ID_AREALIGHT_DISC));
				AreaLightSubMenu.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(ID_AREALIGHT_SPHERE));
				AreaLightSubMenu.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(ID_AREALIGHT_CYLINDER));
				LightsSubMenu.InsData(MENURESOURCE_SUBTITLE, String("Lights"));
				LightsSubMenu.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(ID_DIRECTIONAL_LIGHT));
				LightsSubMenu.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(ID_POINTLIGHT));
				LightsSubMenu.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(ID_SPOTLIGHT));
				LightsSubMenu.InsData(MENURESOURCE_SUBMENU, AreaLightSubMenu);
				LightsSubMenu.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(ID_ENVIRONMENTLIGHT));
				LightsSubMenu.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(ID_INCANDESCENCELIGHT));
				LightsSubMenu.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(ID_LIGHTGROUP));
				BaseContainer RenderSubMenu;
				RenderSubMenu.InsData(MENURESOURCE_SUBTITLE, String("Render"));
				RenderSubMenu.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(ID_RENDER_COMMAND));
				RenderSubMenu.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(ID_BATCH_RENDER_COMMAND));
				RenderSubMenu.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(ID_NSI_EXPORT_COMMAND));
				RenderSubMenu.InsData(
					MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(ID_INTERACTIVE_RENDERING_START));
				RenderSubMenu.InsData(
					MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(ID_INTERACTIVE_RENDERING_STOP));

				BaseContainer StandInSubMenu;
				StandInSubMenu.InsData(MENURESOURCE_SUBTITLE, String("Stand-In"));
				StandInSubMenu.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(STAND_IN_NODE_CREATE));
				StandInSubMenu.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(STANDIN_EXPORT));
				sc.InsData(MENURESOURCE_SUBMENU, MaterialsSubMenu);
				sc.InsData(MENURESOURCE_SUBMENU, LightsSubMenu);
				sc.InsData(MENURESOURCE_SUBMENU, VolumesSubMenu);
				sc.InsData(MENURESOURCE_SEPERATOR, "");
				sc.InsData(MENURESOURCE_SUBMENU, RenderSubMenu);
				sc.InsData(MENURESOURCE_SEPERATOR, "");
				sc.InsData(MENURESOURCE_SUBMENU, StandInSubMenu);
				sc.InsData(MENURESOURCE_SEPERATOR, "");
				sc.InsData(MENURESOURCE_SEPERATOR, "");
				sc.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(ID_CREATE_SHELF));
				sc.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(PREFERENCES_ID));
				sc.InsData(MENURESOURCE_SEPERATOR, "");
				sc.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(HELP_ID));
				sc.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(ABOUT_ID));
				bc->InsDataAfter(MENURESOURCE_STRING, sc, dat);
			}
		}
	}
}

static void CreateMaterialUI(BaseContainer* bc, int& created)
{
	if (!bc) {
		return;
	}

	BrowseContainer browse(bc);
	Int32 id = 0;
	GeData* dat = nullptr;
	int index = 0;

	while (browse.GetNext(&id, &dat)) {
		// Should find a more stable way for manually creating and placing this
		// menu of commands(not materials) on materials manager menu.
		if (id == MENURESOURCE_SUBMENU) {
			CreateMaterialUI(dat->GetContainer(), created);

			if (index++ == TRUE && ++created == TRUE) {
				BaseContainer sc;
				sc.InsData(MENURESOURCE_SUBTITLE, String("3Delight"));
				for (auto osl_shader : OSL_shader_table)
				{
					if (osl_shader.second.m_is_surface)
					{
						sc.InsData(MENURESOURCE_COMMAND,
							"PLUGIN_CMD_" + String::IntToString(osl_shader.first + 100));
					}
				}
				bc->InsDataAfter(MENURESOURCE_STRING, sc, dat);
			}
		}
	}
}

Bool PluginStart(void)
{
	const int shader_id_start = 1057566;
	const int material_id_start = 1057706;
	const int material_objects_id_start = 1057756;

	// 3Delight Shaders
	if (!RegisterDLAtmosphere()) {
		return FALSE;
	}

	if (!RegisterEnvironmentLight()) {
		return FALSE;
	}

	if (!RegisterOpenVDB()) {
		return FALSE;
	}

	if (!RegisterAOVs()) {
		return FALSE;
	}

	if (!RegisterAovColor()) {
		return FALSE;
	}

	if (!RegisterAovGroup()) {
		return FALSE;
	}

	if (!RegisterOSL_shaders(shader_id_start)) { return FALSE; }
	if (!RegisterOSL_materials(material_id_start)) { return FALSE; }
	if (!RegisterOSL_materialObjects(material_objects_id_start)) { return FALSE; }

	// Load custom material preview scene for use by 3Delight materials
	String preview_scene_name("DL_Sphere");
	Filename pluginpath = GeGetPluginPath();
	AddUserPreviewScene(pluginpath.GetString() + "/res/Scenes/DL_Sphere.c4d",
		-1,
		&preview_scene_name);
	return true;
}

void PluginEnd(void) {}

Bool PluginMessage(Int32 id, void* data)
{
	switch (id) 
	{
		case C4DPL_INIT_SYS:
			if (!g_resource.Init()) {
				return FALSE; // don't start plugin without resource
			}

			return TRUE;
			break;

		case DL_LOAD_PLUGINS: {
			DL_PluginManager* pm = (DL_PluginManager*)data;
			pm->RegisterHook(AllocateHook<ShaderSettingsHook>);
			/*
				On the translator we have only allocated the Materials
				and Shaders that we support in our plugin. There will be others to
				add as we are working on it.
			*/

			// UVCoord
			pm->RegisterHook(AllocateHook<DelightUVCoord>);
			
			// 3Delight Materials
			pm->RegisterTranslator(DL_ATMOSPHERE_MAT,
				AllocateTranslator<Atmosphere_Translator>);
			// C4D Shaders
			// pm->RegisterTranslator(Osky,
			// AllocateTranslator<EnvironmentLightTranslator>);
			pm->RegisterTranslator(Xbitmap, AllocateTranslator<BitmapTranslator>);
			pm->RegisterTranslator(Ttexture,
				AllocateTranslator<TextureTagTranslator>);
			// 3Delight Shaders
			pm->RegisterTranslator(ID_ENVIRONMENTLIGHT,
				AllocateTranslator<EnvironmentLightTranslator>,
				true);
			pm->RegisterTranslator(DL_OPENVDB,
				AllocateTranslator<Delight_OpenVDBTranslator>);
			// C4D Tags
			pm->RegisterTranslator(Tpolygonselection,
				AllocateTranslator<C4DSelectionTagTranslator>);

			//Register automatically shaders whose resource file and a corresponding
			//OSL file is available. 
			for (auto osl_shader : OSL_shader_table)
			{
				if (osl_shader.second.m_is_surface)
				{
					pm->RegisterTranslator(osl_shader.first,
						AllocateTranslator<NSI_Export_Material>);
				}
				else
				{
					pm->RegisterTranslator(osl_shader.first,
						AllocateTranslator<NSI_Export_Shader>);
				}
			}
			break;
		}

		case C4DPL_BUILDMENU:
		{
			BaseContainer* bc = GetMenuResource("M_EDITOR"_s);
			if (!bc) {
				return FALSE;
			}
			Create3DelightMenu(bc);

			bc = GetMenuResource("M_MATERIAL_MANAGER"_s);
			if (!bc) 
			{
				return FALSE;
			}

			int created = 0;
			CreateMaterialUI(bc, created);
			break;
		}
	}
	return FALSE;
}
