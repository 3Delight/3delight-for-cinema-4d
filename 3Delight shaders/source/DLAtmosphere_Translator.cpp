#include "DLAtmosphere_Translator.h"
#include "delightenvironment.h"
#include "dl_atmosphere.h"
#include "nsi.hpp"

void Atmosphere_Translator::CreateNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	std::string atmosphere_handle = (std::string) Handle;
	ctx.Create(atmosphere_handle, "environment");
	std::string atmosphere_attributes = atmosphere_handle + "attributes";
	ctx.Create(atmosphere_attributes, "attributes");
	std::string atmosphere_shader = atmosphere_handle + "shader";
	ctx.Create(atmosphere_shader, "shader");
}

void Atmosphere_Translator::ConnectNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	std::string atmosphere_handle = (std::string) Handle;
	std::string atmosphere_attributes = atmosphere_handle + "attributes";
	std::string atmosphere_shader = atmosphere_handle + "shader";
	ctx.Connect(atmosphere_handle, "", NSI_SCENE_ROOT, "objects");
	ctx.Connect(atmosphere_shader, "", atmosphere_attributes, "volumeshader");
	ctx.Connect(
		atmosphere_attributes, "", atmosphere_handle, "geometryattributes");
}

void Atmosphere_Translator::SampleAttributes(DL_SampleInfo* info,
		const char* Handle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	BaseShader* shader = (BaseShader*) C4DNode;
	BaseContainer* data = shader->GetDataInstance();
	std::string atmosphere_shader = (std::string) Handle + "shader";
	const char* delightpath = DelightEnv::getDelightEnvironment();
	std::string c_shaderpath =
		delightpath + (std::string) "/osl" + (std::string) "/dlAtmosphere.oso";
	Vector atmosphere_color = data->GetVector(DL_ATMOSPHERE_COLOR);
	float color[3] { (float) atmosphere_color.x,
					 (float) atmosphere_color.y,
					 (float) atmosphere_color.z
				   };
	float atmosphere_density =
		(float) data->GetFloat(DL_ATMOSPHERE_DENSITY) / (float) 100.0;
	float atmosphere_reflective =
		(float) data->GetFloat(DL_ATMOSPHERE_SUPER_REFLECTIVE);
	NSI::ArgumentList args;
	args.Add(new NSI::StringArg("shaderfilename", c_shaderpath.c_str()));
	args.Add(new NSI::ColorArg("i_color", &color[0]));
	args.Add(new NSI::FloatArg("density", atmosphere_density));
	args.Add(new NSI::FloatArg("superReflective", atmosphere_reflective));
	ctx.SetAttribute(atmosphere_shader, args);
}
