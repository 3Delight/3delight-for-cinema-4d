#include "EnvironmentLightTranslator.h"
#include "delightenvironment.h"
#include "DL_TypeConversions.h"
#include "DL_Utilities.h"
#include "IDs.h"
#include "c4d.h"
#include "nsi.hpp"
#include "oenvironmentlight.h"

#ifdef __APPLE__
#	pragma clang diagnostic ignored "-Wimplicit-float-conversion"
#endif

void EnvironmentLightTranslator::CreateNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	// Create a mesh and connect it to the parent transform
	// m_handle = std::string(parser->GetUniqueName("environment_light"));
	m_handle = (std::string) Handle;
	m_transform_handle = std::string(ParentTransformHandle);
	// std::string transform_handle2 =
	// std::string(parser->GetUniqueName("transform"));
	std::string transform_handle2 =
		(std::string) Handle + (std::string) "transform";
	ctx.Create(transform_handle2, "transform");
	std::vector<double> change_direction{ 0, 0, -1, 0, 0, 1, 0, 0,
										  -1, 0, 0, 0, 0, 0, 0, 1 };
	NSI::Argument xform("transformationmatrix");
	xform.SetType(NSITypeDoubleMatrix);
	xform.SetValuePointer((void*) &change_direction[0]);
	ctx.SetAttribute(transform_handle2, xform);
	ctx.Connect(transform_handle2, "", m_transform_handle, "objects");
	ctx.Create(m_handle, "environment");
	ctx.Connect(m_handle, "", transform_handle2, "objects");
	std::string attributes_handle =
		(std::string) Handle + std::string("attributes");
	ctx.Create(attributes_handle, "attributes");
	// Create a shader for the mesh and connect it to the geometry attributes of
	// the mesh
	m_shader_handle = (std::string) Handle + std::string("shader");
	ctx.Create(m_shader_handle, "shader");
	ctx.Connect(m_shader_handle, "", attributes_handle, "surfaceshader");
}

void EnvironmentLightTranslator::ConnectNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	m_shader_handle = (std::string) Handle + std::string("shader");
	BaseList2D* obj = (BaseList2D*) C4DNode;
	BaseContainer* data = obj->GetDataInstance();
	m_handle = (std::string) Handle;
	std::string attributes_handle =
		(std::string) Handle + std::string("attributes");
	ctx.Connect(attributes_handle, "", m_handle, "geometryattributes");
	const char* delightpath = DelightEnv::getDelightEnvironment();
	std::string uv_shaderpath = delightpath + (std::string) "/osl" + (std::string) "/uvCoordEnvironment.oso";
	;
	std::string uv_handle = (std::string) Handle + std::string("uv_Coord");
	ctx.Create(uv_handle, "shader");
	ctx.SetAttribute(
		uv_handle,
		NSI::StringArg("shaderfilename", std::string(&uv_shaderpath[0])));
	// Get The connected Material to the Sky object (Environment).
	// Materials can have hdri textures (bitmap) connected.
	BaseList2D* shader = data->GetLink(ENVIRONMENT_TEXTURE_SHADER, doc);
	bool has_bitmap_connected = false;
	if (shader) {
		/*
			if shader type is a bitmap then set is_connected_to_sky osl
			attribute to 1 to make the proper texture mapping.
		*/
		std::string texture_handle = parser->GetHandleName((BaseList2D*) shader);
		if (shader->GetType() == Xbitmap)
			ctx.SetAttribute(texture_handle,
				NSI::IntegerArg("is_connected_to_sky", 1));

		bool process = true;
		while (shader)
		{
			if (shader->GetDown() && process) {
				shader = (BaseList2D*)shader->GetDown();
				process = true;
			}
			else if (shader->GetNext()) {
				shader = (BaseList2D*)shader->GetNext();
				process = true;
			}
			else if (shader->GetUp()) {
				shader = (BaseList2D*)shader->GetUp();
				process = false;
			}
			else
			{
				shader = NULL;
			}
			if (shader && shader->GetType() == Xbitmap)
			{
				/*
					Set connection to sky parameter even if it's connected to environment
					through another shader.
				*/
				std::string bitmap_tex = parser->GetHandleName((BaseList2D*)shader);
				{
					ctx.SetAttribute(bitmap_tex,
						NSI::IntegerArg("is_connected_to_sky", 1));
				}
			}
		}
		ctx.Connect(uv_handle, "o_outUV", texture_handle, "uvCoord");
		ctx.Connect(texture_handle, "outColor", m_shader_handle, "i_texture");
	}

	// Considering shaders for other attributes of Environment shader.
	BaseList2D* intensity_shader =
		data->GetLink(ENVIRONMENT_INTENSITY_SHADER, doc);

	if (intensity_shader) {
		std::string texture_handle =
			parser->GetHandleName((BaseList2D*) intensity_shader);
		ctx.Connect(texture_handle, "outColor[0]", m_shader_handle, "intensity");
	}

	BaseList2D* exposure_shader = data->GetLink(ENVIRONMENT_EXPOSURE_SHADER, doc);

	if (exposure_shader) {
		std::string texture_handle =
			parser->GetHandleName((BaseList2D*) exposure_shader);
		ctx.Connect(texture_handle, "outColor[0]", m_shader_handle, "exposure");
	}

	BaseList2D* tint_shader = data->GetLink(ENVIRONMENT_TINT_SHADER, doc);

	if (tint_shader) {
		std::string texture_handle =
			parser->GetHandleName((BaseList2D*) tint_shader);
		ctx.Connect(texture_handle, "outColor", m_shader_handle, "tint");
	}
}

void EnvironmentLightTranslator::SampleAttributes(DL_SampleInfo* info,
		const char* Handle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	m_shader_handle = (std::string) Handle + std::string("shader");
	BaseList2D* obj = (BaseList2D*) C4DNode;
	BaseContainer* data = obj->GetDataInstance();
	float intensity = data->GetFloat(ENVIRONMENT_INTENSITY);
	float exposure = data->GetFloat(ENVIRONMENT_EXPOSURE);
	Vector texture_color = data->GetVector(ENVIRONMENT_TEXTURE);
	float texturecolor[3] { (float) texture_color.x,
							(float) texture_color.y,
							(float) texture_color.z
						  };
	Vector tint_c4d = data->GetVector(ENVIRONMENT_TINT);
	float tint[3] { (float) tint_c4d.x, (float) tint_c4d.y, (float) tint_c4d.z };
	int mapping = data->GetInt32(ENVIRONMENT_MAPPING);
	int camera_visibility = data->GetBool(ENVIRONMENT_SEEN_BY_CAMERA);
	int prelit = data->GetBool(ENVIRONMENT_PRELIT);
	bool illumination = data->GetBool(ENVIRONMENT_ILLUMINATES_BY_DEFAULT);
	NSI::ArgumentList attributes_args;
	attributes_args.Add(
		new NSI::IntegerArg("visibility.camera", camera_visibility));
	attributes_args.Add(new NSI::IntegerArg("prelit", prelit));
	attributes_args.Add(new NSI::IntegerArg("visibility", illumination));
	std::string attributes_handle =
		(std::string) Handle + std::string("attributes");
	ctx.SetAttribute(attributes_handle, attributes_args);
	const char* delightpath = DelightEnv::getDelightEnvironment();
	std::string shaderpath =
		delightpath + (std::string) "/osl" + (std::string) "/dlEnvironment.oso";
	;
	NSI::ArgumentList shader_args;
	shader_args.Add(
		new NSI::StringArg("shaderfilename", std::string(&shaderpath[0])));
	shader_args.Add(new NSI::FloatArg("intensity", intensity));
	shader_args.Add(new NSI::FloatArg("exposure", exposure));
	BaseList2D* shader = data->GetLink(ENVIRONMENT_TEXTURE_SHADER, doc);

	if (!shader) {
		shader_args.Add(new NSI::ColorArg("i_texture", texturecolor));
	}

	shader_args.Add(new NSI::ColorArg("tint", tint));
	shader_args.Add(new NSI::IntegerArg("mapping", mapping));
	ctx.SetAttribute(m_shader_handle, shader_args);
}
