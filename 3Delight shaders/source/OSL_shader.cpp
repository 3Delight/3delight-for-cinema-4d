#include "c4d.h"
#include "c4d_symbols.h"
#include <map>
#include "OSL_shader_info.h"
#include "osl_utilities.h"
#include "NSIExportShader.h"
#include "TextureUI_Functions.h"
#include "customgui_texbox.h"
#include "RegisterPrototypes.h"

#include <cfloat>
#include <cstring>

//Global variable for shader table
extern std::map<Int32, OSL_shader_info> OSL_shader_table;
#define LEAKED(x) (x)

/*
	Use constant IDs for existing shaders (obtained from Maxon) on 3Delight installation
	so that when we add new shaders by ourselves, they will not mess with IDs and cause
	problems with previously saved scenes.
*/
std::map<std::string, int> shaders_id
{
	{"dlSolidRamp",1057521},
	{"dlDistanceFade",1057522},
	{"dlBoxNoise",1057523},
	{"bulge",1057524},
	{"dlFacingRatio",1057525},
	{"solidFractal",1057526},
	{"dlColorVariation",1057527},
	{"brownian",1057528},
	{"dlFloatToColor",1057529},
	{"checker",1057530},
	{"clamp",1057531},
	{"cloth",1057532},
	{"dlFlakes",1057533},
	{"cloud",1057534},
	{"dlRamp",1057535},
	{"dlFloat",1057536},
	{"condition",1057537},
	{"dlSky",1057538},
	{"dlRemap",1057539},
	{"dlDisplacementBlend",1057540},
	{"dlAttributeRead",1057541},
	{"dlColorBlend",1057542},
	{"marble",1057543},
	{"dlColor",1057544},
	{"dlTexture",1057545},
	{"dlColorBlendMulti",1057546},
	{"dlColorCorrection",1057547},
	{"noise",1057548},
	{"dlColorToFloat",1057549},
	{"dlFloatToUV",1057550},
	{"dlFloatBlend",1057551},
	{"dlFloatMath",1057552},
	{"dlNoise",1057553},
	{"grid",1057554},
	{"dlRandomInputColor",1057555},
	{"dlTriplanar",1057556},
	{"dlUV",1057902},
	{"fractal",1057557},
	{"dlWorleyNoise",1057558},
	{"gammaCorrect",1057559},
	{"granite",1057560},
	{"leather",1057561},
	{"rock",1057562},
	{"setRange",1057563},
	{"volumeNoise",1057564},
	{"wood",1057565},
	{"dlTransformMatrix",1057909},
};

const int POPUP_ATTRIBUTE = 1000;
const int GROUP_ATTRIBUTE = 2000;
const int SHADER_ATTRIBUTE = 3000;
const int SHADER_TEMP_ATTRIBUTE = 4000;
const int STATIC_TEXT_ATTRIBUTE = 5000;
const int uv_shader_id = 1057902;
const int transform_shader_id = 1057909;
const int uvcoord_parameter = 1000;
const int transform_parameter = 1001;

class OSL_shader : public ShaderData
{
	INSTANCEOF(OSL_shader, ShaderData)
public:
	OSL_shader() {};
	OSL_shader(String name) { m_shader_name = name; }
	virtual Bool Init(GeListNode* node);
	virtual Bool GetDDescription(GeListNode *node, Description *description, DESCFLAGS_DESC &flags);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
	static NodeData* Alloc(void) { 	return NewObjClear(OSL_shader); }

private:
	String m_shader_name;
	//String m_shader_path;
};


Bool OSL_shader::Init(GeListNode* node)
{
	Int32 ShaderID = node->GetNodeID();
	BaseContainer* dldata = ((BaseShader*)node)->GetDataInstance();

	std::string shader_path =
		OSL_shader_table[ShaderID].m_oso_file_path.GetCStringCopy();
	std::string output_name =
		OSL_shader_table[ShaderID].m_output_name;
	OSL_shader_info info;
	const char* first_page = "";
	const DlShaderInfo* shader_info = info.get_shader_info(shader_path.c_str());
	if (!shader_info)
	{
		return FALSE;
	}
	int param_idx = 10000;

	for (unsigned p = 0; p < shader_info->nparams(); p++)
	{
		const DlShaderInfo::Parameter* param = shader_info->getparam(p);
		if (param->isoutput)
		{
			continue;
		}
		/*
			Since we are showing Closures as Colors on the UI we exclude
			aovGroup from it since on OSL shader aovGroup is defined as a closure.
		*/
		if (param->isclosure && param->name == "aovGroup")
		{
			continue;
		}

		/*
			Don't display matrices in the UI for now. They can still be
			communicated through connections.
		*/
		if (param->type.elementtype == NSITypeMatrix ||
			param->type.elementtype == NSITypeDoubleMatrix)
		{
			continue;
		}

		const char* widget = "";
		osl_utilities::FindMetaData(widget, param->metadata, "widget");
		if (widget == osl_utilities::k_null)
		{
			continue;
		}

		const char* page_name = nullptr;
		osl_utilities::FindMetaData(page_name, param->metadata, "page");

		if (page_name == nullptr)
		{
			page_name = "Main";
		}
		
		osl_utilities::ParameterMetaData meta;
		meta.m_label = param->name.c_str();
		osl_utilities::GetParameterMetaData(meta, param->metadata);

		if (strcmp(first_page, page_name) != 0)
		{
			MAXON_SCOPE
			{
				param_idx++;
				param_idx++;
			}

			first_page = page_name;
		}

		if (param->type.elementtype == NSITypeInteger)
		{
			MAXON_SCOPE
			{
				if (meta.m_widget && meta.m_widget == osl_utilities::k_check_box)
				{
					dldata->SetBool(param_idx, param->idefault[0]);
				}
				else if (meta.m_options)
				{
					dldata->SetInt32(param_idx, param->idefault[0]);
				}
				else
				{
					dldata->SetInt32(param_idx, param->idefault[0]);
					OSL_shader_table[ShaderID].m_ids_to_names[param_idx + SHADER_ATTRIBUTE] = 
						std::make_pair(output_name.c_str(), param->name.c_str());
					
				}
			}
		}


		else if (param->type.elementtype == NSITypeFloat || param->type.elementtype == NSITypeDouble)
		{
			if (osl_utilities::ramp::IsRampWidget(meta.m_widget))
			{
				InitRamp(*dldata, param_idx);
			}
			else
			{
				dldata->SetFloat(param_idx, param->fdefault[0]);
				OSL_shader_table[ShaderID].m_ids_to_names[param_idx + SHADER_ATTRIBUTE] =
					std::make_pair(output_name.c_str(), param->name.c_str());
			}
		}
		else if (param->type.elementtype == NSITypeColor)
		{
			if (osl_utilities::ramp::IsRampWidget(meta.m_widget))
			{
				initGradient(*dldata, param_idx);
			}
			else
			{
				dldata->SetVector(param_idx, Vector(param->fdefault[0], param->fdefault[1], param->fdefault[2]));
				OSL_shader_table[ShaderID].m_ids_to_names[param_idx + SHADER_ATTRIBUTE] =
					std::make_pair(output_name.c_str(), param->name.c_str());
			}
		}

		else if (param->type.elementtype == NSITypePoint
			|| param->type.elementtype == NSITypeVector
			|| param->type.elementtype == NSITypeNormal)
		{
			dldata->SetVector(param_idx, Vector(param->fdefault[0], param->fdefault[1], param->fdefault[2]));
		}
		else if (param->type.elementtype == NSITypeString)
		{
		}
		else
		{
			continue;
		}
		OSL_shader_table[ShaderID].m_ids_to_names[param_idx++] = std::make_pair("", param->name.c_str());
	}

	return true;
}

Bool OSL_shader::Message(GeListNode* node, Int32 type, void* data)
{
	if (!node) {
		return false;
	}

	BaseContainer* dldata = ((BaseMaterial*)node)->GetDataInstance();

	if (type == MSG_DESCRIPTION_POPUP) 
	{
		DescriptionPopup* dp = (DescriptionPopup*)data;
		int clicked_button_id = dp->_descId[0].id;
		FillPopupMenu(dldata, dp, clicked_button_id + POPUP_ATTRIBUTE);
	}
	return true;
}

Bool OSL_shader::GetDDescription(GeListNode *node, Description *description, DESCFLAGS_DESC &flags)
{
	//Here we dynamically create the GUI of the shader. The name/path of the oso-file is stored in the member variable "m_shader_name"
	//Again, the liboslquery library can be used to query the parameters of the shader, for which GUI widgets should be created. 
	//The metadata of the shader can also be read to determine the style of the widgets, following the same conventions as 3delightforMaya. 

	//Alternatively, liboslquery could be used once in the Init() function instead, to query what GUI elements should be created 
	//and store these in some intermediate representation. This would mean the oso files themselves are only loaded at startup, which could 
	//possibly be more stable/efficient. You may check how this is performed in the other DCC plugins (maya, katana,...)

	description->LoadDescription(node->GetNodeID()); 
	flags |= DESCFLAGS_DESC::LOADED;

	BaseContainer* dldata = ((BaseShader*)node)->GetDataInstance();
	BaseShader* shader_node = (BaseShader*)node;
	OSL_shader_info info;
	std::string shader_path = 
		OSL_shader_table[node->GetNodeID()].m_oso_file_path.GetCStringCopy();
	const char *first_page = "";
	const DlShaderInfo *shader_info = info.get_shader_info(shader_path.c_str());
	if (!shader_info)
	{
		return FALSE;
	}

	int param_idx = 10000;
	const DescID parentGroup = DescLevel(10, DTYPE_GROUP, 0);
	const DescID mainGroup = DescLevel(101, DTYPE_GROUP, 0);

	MAXON_SCOPE
	{
	  BaseContainer settings = GetCustomDataTypeDefault(DTYPE_GROUP);
	  settings.SetString(DESC_NAME, shader_node->GetName() + " Shader");
	  settings.SetInt32(DESC_COLUMNS, 2); // group has two columns
	  settings.SetInt32(DESC_DEFAULT, 1); // group is unfolded by default
	  settings.SetInt32(DESC_SCALEH, 1);  // group is scaled horizontally
	  description->SetParameter(parentGroup, settings, 0);

	  settings = GetCustomDataTypeDefault(DTYPE_SEPARATOR);
	  settings.SetInt32(DESC_CUSTOMGUI, CUSTOMGUI_SEPARATOR);
	  settings.SetBool(DESC_SEPARATORLINE, true);
	  description->SetParameter(DescLevel(100, DTYPE_GROUP, 0), settings, parentGroup);

	  settings = GetCustomDataTypeDefault(DTYPE_GROUP);
	  settings.SetInt32(DESC_COLUMNS, 2); // group has two columns
	  description->SetParameter(mainGroup, settings, parentGroup);
	}
	for (unsigned p = 0; p < shader_info->nparams(); p++)
	{
		bool has_texture_connection = false;
		const DlShaderInfo::Parameter* param = shader_info->getparam(p);
		if (param->isoutput)
		{
			continue;
		}
				
		/*
			Since we are showing Closures as Colors on the UI we exclude
			aovGroup from it since on OSL shader aovGroup is defined as a closure.
		*/
		if (param->isclosure && param->name == "aovGroup")
		{
			continue;
		}

		/*
			Don't display matrices in the UI for now. They can still be
			communicated through connections.
		*/
		if (param->type.elementtype == NSITypeMatrix ||
			param->type.elementtype == NSITypeDoubleMatrix)
		{
			continue;
		}

		const char* widget = "";
		osl_utilities::FindMetaData(widget, param->metadata, "widget");
		if (widget == osl_utilities::k_null)
		{
			continue;
		}
		// FIXME : support variable length arrays
		if (param->type.arraylen < 0 &&
			!osl_utilities::ramp::IsRampWidget(widget))
		{
			continue;
		}

		const char* page_name = nullptr;
		osl_utilities::FindMetaData(page_name, param->metadata, "page");

		if (page_name == nullptr)
		{
			page_name = "Main";
		}

		osl_utilities::ParameterMetaData meta;
		meta.m_label = param->name.c_str();
		osl_utilities::GetParameterMetaData(meta, param->metadata);

		if (strcmp(first_page,page_name) !=0)
		{
			MAXON_SCOPE
			{
			  BaseContainer settings = GetCustomDataTypeDefault(DTYPE_SEPARATOR);
			  settings.SetString(DESC_NAME, (String)page_name);
			  settings.SetInt32(DESC_CUSTOMGUI, CUSTOMGUI_SEPARATOR);
			  settings.SetBool(DESC_SCALEH, true);
			  description->SetParameter(DescLevel(param_idx++, DTYPE_SEPARATOR, 0),
				  settings, mainGroup);

			  settings = GetCustomDataTypeDefault(DTYPE_STATICTEXT);
			  settings.SetString(DESC_NAME, ""_s);
			  description->SetParameter(DescID(DescLevel(param_idx++, DTYPE_STATICTEXT, 0)),
				  settings, mainGroup);
			}
			first_page = page_name;
		}

		if (param->type.elementtype == NSITypeInteger)
		{
			MAXON_SCOPE
			{
				if (meta.m_widget && meta.m_widget == osl_utilities::k_check_box)
				{
					BaseContainer settings = GetCustomDataTypeDefault(DTYPE_BOOL);
					settings.SetString(DESC_NAME, (String)meta.m_label);
					settings.SetBool(DESC_SCALEH, true);
					description->SetParameter(DescLevel(param_idx++, DTYPE_BOOL, 0), settings, mainGroup);
				}
				else if (meta.m_options)
				{
					BaseContainer settings = GetCustomDataTypeDefault(DTYPE_LONG);
					settings.SetString(DESC_NAME, (String)meta.m_label);
					settings.SetBool(DESC_SCALEH, true);
					settings.SetInt32(DESC_CUSTOMGUI, CUSTOMGUI_CYCLE);

					// add cycle elements
					BaseContainer items;
					std::string options = LEAKED(strdup(meta.m_options));

					formOptionsParameter(items, options);
					settings.SetContainer(DESC_CYCLE, items);
					settings.SetBool(DESC_SCALEH, true);
					description->SetParameter(DescLevel(param_idx++, DTYPE_LONG, 0), settings, mainGroup);
				}
				else
				{ 
					BaseContainer settings = GetCustomDataTypeDefault(DTYPE_LONG);
					settings.SetString(DESC_NAME, (String)meta.m_label);
					settings.SetBool(DESC_SCALEH, true);
					description->SetParameter(DescLevel(param_idx, DTYPE_LONG, 0), settings, mainGroup);
					has_texture_connection = true;
				}
			}
		}


		else if (param->type.elementtype == NSITypeFloat ||param->type.elementtype == NSITypeDouble)
		{
			MAXON_SCOPE
			{
				if (osl_utilities::ramp::IsRampWidget(meta.m_widget))
				{
					using namespace osl_utilities;
					using namespace osl_utilities::ramp;
					const DlShaderInfo::Parameter* knots = nullptr;
					const DlShaderInfo::Parameter* interpolation = nullptr;
					const DlShaderInfo::Parameter* shared_interpolation = nullptr;
					std::string base_name;
					if (!FindMatchingRampParameters(
						*shader_info,
						*param,
						knots,
						interpolation,
						shared_interpolation,
						base_name))
					{
						continue;
					}

					BaseContainer settings = GetCustomDataTypeDefault(CUSTOMDATATYPE_SPLINE);
					settings.SetString(DESC_NAME, (String)meta.m_label);
					settings.SetBool(SPLINECONTROL_GRID_H, true);
					settings.SetBool(SPLINECONTROL_GRID_V, true);
					settings.SetBool(SPLINECONTROL_VALUE_EDIT_H, true);
					settings.SetBool(SPLINECONTROL_VALUE_EDIT_V, true);
					settings.SetBool(SPLINECONTROL_NO_FLOATING_WINDOW, true);
					settings.SetInt32(DESC_CUSTOMGUI, CUSTOMGUI_SPLINE);
					settings.SetBool(DESC_SCALEH, true);
					description->SetParameter(DescLevel(param_idx++, CUSTOMDATATYPE_SPLINE, 0), settings, mainGroup);
				}

				else
				{
					BaseContainer settings = GetCustomDataTypeDefault(DTYPE_REAL);
					settings.SetString(DESC_NAME, (String)meta.m_label);
					settings.SetInt32(DESC_CUSTOMGUI, CUSTOMGUI_REALSLIDER);
					settings.SetFloat(DESC_MIN, meta.m_fmin ? *meta.m_fmin : -FLT_MAX);
					settings.SetFloat(DESC_MAX, meta.m_fmax ? *meta.m_fmax : FLT_MAX);
					settings.SetFloat(DESC_MINSLIDER, meta.m_slider_fmin ? *meta.m_slider_fmin : 0);
					settings.SetFloat(DESC_MAXSLIDER, meta.m_slider_fmax ? *meta.m_slider_fmax : 10);
					settings.SetFloat(DESC_STEP, 0.01);
					settings.SetBool(DESC_SCALEH, true);
					description->SetParameter(DescLevel(param_idx, DTYPE_REAL, 0), settings, mainGroup);

					has_texture_connection = true;
				}
			}
		}

		else if (param->type.elementtype == NSITypeColor)
		{
			MAXON_SCOPE
			{
				if (osl_utilities::ramp::IsRampWidget(meta.m_widget))
				{
					using namespace osl_utilities;
					using namespace osl_utilities::ramp;
					const DlShaderInfo::Parameter* knots = nullptr;
					const DlShaderInfo::Parameter* interpolation = nullptr;
					const DlShaderInfo::Parameter* shared_interpolation = nullptr;
					std::string base_name;
					if (!FindMatchingRampParameters(
						*shader_info,
						*param,
						knots,
						interpolation,
						shared_interpolation,
						base_name))
					{
						continue;
					}

					BaseContainer settings = GetCustomDataTypeDefault(CUSTOMDATATYPE_GRADIENT);
					settings.SetString(DESC_NAME, (String)meta.m_label);
					settings.SetInt32(DESC_CUSTOMGUI, CUSTOMGUI_GRADIENT);
					settings.SetBool(DESC_SCALEH, true);
					description->SetParameter(DescLevel(param_idx++, DTYPE_COLOR, 0), settings, mainGroup);
				}

				else
				{
					BaseContainer settings = GetCustomDataTypeDefault(DTYPE_COLOR);
					settings.SetString(DESC_NAME, (String)meta.m_label);
					settings.SetBool(DESC_SCALEH, TRUE);
					settings.SetBool(DESC_SCALEH, true);
					description->SetParameter(DescLevel(param_idx, DTYPE_COLOR, 0), settings, mainGroup);

					has_texture_connection = true;
				}
			}
		}
		
		else if (param->type.elementtype == NSITypePoint
				|| param->type.elementtype == NSITypeVector
				|| param->type.elementtype == NSITypeNormal)
		{
			MAXON_SCOPE
			{
				BaseContainer settings = GetCustomDataTypeDefault(DTYPE_VECTOR);
				settings.SetString(DESC_NAME, (String)meta.m_label);
				settings.SetBool(DESC_ANGULAR_XYZ, true);
				settings.SetBool(DESC_SCALEH, true);
				description->SetParameter(DescLevel(param_idx++, DTYPE_VECTOR, 0), settings, mainGroup);
			}
		}

		else if (param->type.elementtype == NSITypeString)
		{
			MAXON_SCOPE
			{
				if (meta.m_widget && meta.m_widget == osl_utilities::k_filename)
				{
					BaseContainer settings = GetCustomDataTypeDefault(DTYPE_FILENAME);
					settings.SetString(DESC_NAME, (String)meta.m_label);
					settings.SetBool(DESC_SCALEH, true);
					description->SetParameter(DescLevel(param_idx++, DTYPE_FILENAME, 0), settings, mainGroup);
				}
				else if (meta.m_widget && meta.m_widget == osl_utilities::k_popup)
				{
					BaseContainer settings = GetCustomDataTypeDefault(DTYPE_LONG);
					settings.SetString(DESC_NAME, (String)meta.m_label);
					settings.SetBool(DESC_SCALEH, true);
					settings.SetInt32(DESC_CUSTOMGUI, CUSTOMGUI_CYCLE);

					// add cycle elements
					BaseContainer items;
					std::string options = LEAKED(strdup(meta.m_options));

					formOptionsParameter(items, options);
					settings.SetContainer(DESC_CYCLE, items);
					settings.SetBool(DESC_SCALEH, true);
					description->SetParameter(DescLevel(param_idx++, DTYPE_LONG, 0), settings, mainGroup);
				}
				else
				{
					BaseContainer settings = GetCustomDataTypeDefault(DTYPE_STRING);
					settings.SetString(DESC_NAME, (String)meta.m_label);
					settings.SetBool(DESC_SCALEH, true);
					description->SetParameter(DescLevel(param_idx++, DTYPE_STRING, 0), settings, mainGroup);
				}
			}
		}

		else
		{
			continue;
		}

		if (has_texture_connection && 0 != std::strcmp(meta.m_label, ""))
		{
			MAXON_SCOPE
			{
				BaseContainer settings = GetCustomDataTypeDefault(DTYPE_BASELISTLINK);
				settings.SetString(DESC_NAME, (String)meta.m_label);
				settings.SetInt32(DESC_CUSTOMGUI, CUSTOMGUI_TEXBOX);  // use the tex box gui to handle shaders
				settings.SetBool(DESC_SHADERLINKFLAG, true);          // is a shader link
				settings.SetBool(DESC_HIDE, true);

				description->SetParameter(DescLevel(param_idx + SHADER_ATTRIBUTE, DTYPE_BASELISTLINK, 0), settings, mainGroup);

				settings = GetCustomDataTypeDefault(DTYPE_BASELISTLINK);
				settings.SetString(DESC_NAME, (String)meta.m_label);
				settings.SetInt32(DESC_CUSTOMGUI, CUSTOMGUI_TEXBOX);  // use the tex box gui to handle shaders
				settings.SetBool(DESC_SHADERLINKFLAG, true);          // is a shader link
				settings.SetBool(DESC_HIDE, true);

				description->SetParameter(DescLevel(param_idx + SHADER_TEMP_ATTRIBUTE, DTYPE_BASELISTLINK, 0), settings, mainGroup);

				settings = GetCustomDataTypeDefault(DTYPE_POPUP);
				settings.SetBool(DESC_ANIMATE, false);
				settings.SetBool(DESC_GROUPSCALEV, true);
				settings.SetInt32(DESC_CUSTOMGUI, CUSTOMGUI_POPUP);
				description->SetParameter(DescLevel(param_idx + POPUP_ATTRIBUTE, DTYPE_POPUP, 0), settings, mainGroup);

				HideAndShowTextures(param_idx + GROUP_ATTRIBUTE,
					param_idx,
					param_idx + SHADER_ATTRIBUTE,
					param_idx + SHADER_TEMP_ATTRIBUTE,
					node,
					description,
					dldata);

				param_idx++;
			}
		}
		else
		{
			MAXON_SCOPE
			{
				const DescID cid = DescID(DescLevel(param_idx + STATIC_TEXT_ATTRIBUTE, DTYPE_STATICTEXT, 0));
				BaseContainer settings = GetCustomDataTypeDefault(DTYPE_STATICTEXT);
				settings.SetString(DESC_NAME, ""_s);
				description->SetParameter(cid, settings, mainGroup);
			}
		}
	}

	if (OSL_shader_table[node->GetNodeID()].m_has_uv_connection)
	{
		BaseContainer settings = GetCustomDataTypeDefault(DTYPE_BASELISTLINK);
		settings = GetCustomDataTypeDefault(DTYPE_BASELISTLINK);
		settings.SetString(DESC_NAME, (String)"Mapping ");
		settings.SetInt32(DESC_CUSTOMGUI, CUSTOMGUI_TEXBOX);
		settings.SetBool(DESC_SHADERLINKFLAG, true);

		//Accept only dlUV shader on mapping link.
		BaseContainer ac;
		ac.SetInt32(uv_shader_id, 1);
		settings.SetContainer(DESC_ACCEPT, ac);

		description->SetParameter(DescLevel(uvcoord_parameter, DTYPE_BASELISTLINK, 0), settings, mainGroup);

		settings = GetCustomDataTypeDefault(DTYPE_STATICTEXT);
		settings.SetString(DESC_NAME, ""_s);
		description->SetParameter(DescLevel(uvcoord_parameter + STATIC_TEXT_ATTRIBUTE, DTYPE_STATICTEXT, 0), settings, mainGroup);
	}

	if (OSL_shader_table[node->GetNodeID()].m_has_transform_connection)
	{
		BaseContainer settings = GetCustomDataTypeDefault(DTYPE_BASELISTLINK);
		settings = GetCustomDataTypeDefault(DTYPE_BASELISTLINK);
		settings.SetString(DESC_NAME, (String)"Transform ");
		settings.SetInt32(DESC_CUSTOMGUI, CUSTOMGUI_TEXBOX);
		settings.SetBool(DESC_SHADERLINKFLAG, true);

		//Accept only transform shader on transformmation link.
		BaseContainer ac;
		ac.SetInt32(transform_shader_id, 1);
		settings.SetContainer(DESC_ACCEPT, ac);
		description->SetParameter(DescLevel(transform_parameter, DTYPE_BASELISTLINK, 0), settings, mainGroup);

		settings = GetCustomDataTypeDefault(DTYPE_STATICTEXT);
		settings.SetString(DESC_NAME, ""_s);
		description->SetParameter(DescLevel(transform_parameter + STATIC_TEXT_ATTRIBUTE, DTYPE_STATICTEXT, 0), settings, mainGroup);
	}

	return SUPER::GetDDescription(node, description, flags);
}

Bool RegisterOSL_shaders(Int32 shader_ID)
{
	Filename res_dir = GeGetPluginResourcePath();
	OSL_shader_info info;
	const char* shaders_path = info.m_shaders_path.c_str();
	info.find_all_shaders(shaders_path);
	int i = 0;
	for (auto &O : info.shaders_map)
	{
		bool shader_uv_connection = false;
		bool transform_matrix_connection = false;
		const char* name = O.first.c_str();
		const DlShaderInfo *shader_info = info.get_shader_info(O.second.c_str());
		const DlShaderInfo::constvector<DlShaderInfo::Parameter> &tags =
			shader_info->metadata();
		std::string output_name;
		int id;

		if (shaders_id[O.first.c_str()])
		{
			id = shaders_id[O.first.c_str()];
		}
		else
		{
			id = shader_ID + i++;
		}
		osl_utilities::FindMetaData(name, tags, "niceName");
		std::string better;
		if (::islower(name[0]))
		{
			/*
				Many utility nodes have no nice name but their ID is just fine with
				an upper case. For example: leather, granite, etc.
			*/
			better = name;
			better[0] = std::string::value_type(toupper(better[0]));
			name = better.c_str();
		}
		
		for (unsigned p = 0; p < shader_info->nparams(); p++)
		{
			const DlShaderInfo::Parameter* param = shader_info->getparam(p);
			if (param->name == "uvCoord" && O.first != "dlUV")
			{
				shader_uv_connection = true;
			}
			else if (param->name == "placementMatrix")
			{
				transform_matrix_connection = true;
			}
			if (param->isoutput && output_name == "")
			{
				output_name = param->name.c_str();
			}
		}

		info.m_has_uv_connection = shader_uv_connection;
		info.m_has_transform_connection = transform_matrix_connection;
		info.m_oso_file_path = O.second.c_str();
		info.m_is_surface = false;
		info.m_output_name = output_name;
		OSL_shader_table[id] = info;

		//Only register shaders whose resource file exists.
		Filename file;
		Filename search_resource = (Filename)O.first.c_str();
		search_resource.SetSuffix("res"_s);
		if (GeSearchFile(res_dir, search_resource, &file))
		{
			if (strcmp(name,"Noise") == 0)
			{
				if (transform_matrix_connection)
				{
					name = "Noise (Pattern 3D)";
				}
				else
				{
					name = "Noise (Pattern)";
				}
			}
			RegisterShaderPlugin(id, (String)name, 0, OSL_shader::Alloc, (String)O.first.c_str(), 0);
		}
	}
	return true;
}
