#include "DL_TypeConversions.h"
#include "IDs.h"
#include "c4d.h"
#include "dl_aov_group.h"
#include "RegisterPrototypes.h"

class DL_AOVGroup : public ObjectData
{
public:
	static NodeData* Alloc(void)
	{
		return NewObjClear(DL_AOVGroup);
	}
	virtual Bool Init(GeListNode* node);
	virtual Bool GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
	bool is_used = false;
};

Bool DL_AOVGroup::Message(GeListNode* node, Int32 type, void* data)
{
	return true;
}

Bool DL_AOVGroup::GetDDescription(GeListNode* node,
								  Description* description,
								  DESCFLAGS_DESC& flags)
{
	return false;
}

Bool DL_AOVGroup::Init(GeListNode* node)
{
	BaseObject* op = (BaseObject*) node;
	BaseContainer* data = op->GetDataInstance();
	data->SetBool(IS_USED, FALSE);
	return true;
}

bool RegisterAovGroup(void)
{
	return RegisterObjectPlugin(ID_AOV_GROUP,
								"AOVs"_s,
								OBJECT_GENERATOR | PLUGINFLAG_HIDEPLUGINMENU,
								DL_AOVGroup::Alloc,
								"dl_aov_group"_s,
								0,
								0);
}
