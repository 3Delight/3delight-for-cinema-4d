#include "NSIExportShader.h"
#include "delightenvironment.h"
#include "DL_TypeConversions.h"
#include "DL_Utilities.h"
#include "OSL_shader_info.h"
#include "nsi.hpp"
#include "osl_utilities.h"

/*
	NSI_Export_Shader Translater class is the class for exporting C4D
	shaders to NSI shaders. Same as with the material this class with it's
	functions will only be executed as many times as the number of shaders and
	subshader created.

	CreateNSINodes() function will create the NSI shader for the current
	shader with a unique name each time there is a new material and connect it
	with it's corresponding OSL shader containing attributes of the current
	shader.

	ConnectNSINodes() will check for the if any of the used shaders on the
	material have a subshader linked with them.
*/

extern std::map<Int32, OSL_shader_info> OSL_shader_table;
const int uv_shader_id = 1057902;
const int transform_shader_id = 1057909;
const int uvcoord_parameter = 1000;
const int transform_parameter = 1001;

void NSI_Export_Shader::CreateNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	BaseShader* shader = (BaseShader*)C4DNode;
	if (!shader) {
		return;
	}
	int shader_id = shader->GetNodeID();

	std::string shader_handle = (std::string)Handle;
	ctx.Create(shader_handle, "shader");

	// path for shaders used only on C4D
	std::string c_shaderpath = OSL_shader_table[shader_id].m_oso_file_path.GetCStringCopy();

	ctx.SetAttribute(
		shader_handle,
		NSI::StringArg("shaderfilename", std::string(&c_shaderpath[0])));
}

void NSI_Export_Shader::ConnectNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser)
{
	/*
		In this function we search the applied shaders if they have any
		Other shader connected to them. If so we find which is this subshader and
		connect it with the parent shader.
	*/
	std::string shader_handle = (std::string) Handle;
	NSI::Context& ctx(parser->GetContext());
	BaseShader* shader = (BaseShader*)C4DNode;
	BaseContainer* dldata = shader->GetDataInstance();

	if (!shader) {
		return;
	}

	int shader_id = shader->GetNodeID();
	BaseContainer* shader_container = shader->GetDataInstance();
	Int32 Id;
	GeData* data = nullptr;
	BrowseContainer browse(shader_container);

	if (OSL_shader_table[shader_id].m_has_uv_connection)
	{
		BaseList2D* UVlink = dldata->GetLink(uvcoord_parameter, doc);
		if(!UVlink || UVlink->GetType() != uv_shader_id)
		ctx.Connect("*uvCoord", "o_outUV", shader_handle, "uvCoord");
		else
		{
			std::string UVlink_shader = parser->GetHandleName(UVlink);
			ctx.Connect(UVlink_shader, "outUV", shader_handle, "uvCoord");
			ctx.Connect("*uvCoord", "o_outUV", UVlink_shader, "uvCoord");
		}
	}
	if (OSL_shader_table[shader_id].m_has_transform_connection)
	{
		BaseList2D* transformShader = dldata->GetLink(transform_parameter, doc);
		if (transformShader && transformShader->GetType() == transform_shader_id)
		{
			std::string transformShader_name = parser->GetHandleName(transformShader);
			ctx.Connect(transformShader_name, "xform", shader_handle, "placementMatrix");
		}
	}

	while (browse.GetNext(&Id, &data)) 
	{
		if (data->GetType() != DA_ALIASLINK) {
			continue;
		}

		std::string osl_parameter_name;
		if (OSL_shader_table[shader_id].m_ids_to_names.count(Id) == 1) 
		{
			osl_parameter_name = OSL_shader_table[shader_id].m_ids_to_names[Id].second;

			BaseList2D* shaderlink = data->GetLink(doc);

			if (!shaderlink) {
				continue;
			}

			std::string link_shader = parser->GetHandleName(shaderlink);
			int source_id = shaderlink->GetNodeID();
			std::string osl_source_attr = OSL_shader_table[source_id].m_output_name;

			//C4D shaders are not registered on our OSL_shader_table so we
			//manually write it's output color name.
			osl_source_attr = osl_source_attr == "" ? "outColor" : osl_source_attr;
			ctx.Connect(link_shader, osl_source_attr, shader_handle, osl_parameter_name);

#ifdef VERBOSE
			ApplicationOutput("ShaderParent @ Parameter ID @, Shader @",
				m_shader_handle.c_str(), Id, link_shader.c_str());
#endif
		}
	}
}

void NSI_Export_Shader::SampleAttributes(DL_SampleInfo* info,
	const char* Handle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	BaseShader* shader = (BaseShader*)C4DNode;

	if (!shader) {
		return;
	}

	int shader_id = shader->GetNodeID();
	std::string shader_path = OSL_shader_table[shader_id].m_oso_file_path.GetCStringCopy();
	const DlShaderInfo *shader_info = OSL_shader_table[shader_id].get_shader_info(shader_path.c_str());
	BaseContainer* shader_container = shader->GetDataInstance();
	NSI::ArgumentList args;
	GeData i_data;
	AutoAlloc<Description> desc;
	shader->GetDescription(desc, DESCFLAGS_DESC::NONE);

	for (auto val : OSL_shader_table[shader_id].m_ids_to_names)
	{
		shader->GetParameter(DescID(val.first), i_data, DESCFLAGS_GET::NONE);
		std::string parameter_name = val.second.second;
		switch (i_data.GetType())
		{
			case DA_LONG: 
			{
				if (parameter_name == "textureFile_meta_colorspace")
				{
					AutoAlloc<AtomArray> arr;
					BaseContainer* selectionParameter =
						desc->GetParameterI(DescLevel(val.first, DTYPE_LONG, 0), arr);

					if (selectionParameter != nullptr)
					{
						BaseContainer* items =
							selectionParameter->GetContainerInstance(DESC_CYCLE);

						if (items != nullptr)
						{
							std::string selected_name =
								items->GetData(i_data.GetInt32()).GetString().GetCStringCopy();
							args.Add(new NSI::StringArg(parameter_name, selected_name));
						}
					}
				}
				else
				{
					Int32 value = i_data.GetInt32();
					args.Add(new NSI::IntegerArg(parameter_name, value));
				}
				break;
			}

			case DA_REAL:
			{
				// Float data type
				float float_value = (float)i_data.GetFloat();
				args.Add(new NSI::FloatArg(parameter_name, float_value));
				break;
			}

			case DA_STRING:
			{
				String value = i_data.GetString();
				args.Add(new NSI::StringArg(parameter_name, value.GetCStringCopy()));
				break;
			}

			case DA_VECTOR: { // Vector data type
				Vector c4d_vector_value = i_data.GetVector();

				const float vector_val[3] = { (float)c4d_vector_value.x,
												(float)c4d_vector_value.y,
												(float)c4d_vector_value.z
				};
				args.Add(new NSI::ColorArg(parameter_name, &vector_val[0]));
				break;
			}

			case DA_FILENAME: { // Filename. For texture mapping
				Filename texturefile = i_data.GetFilename();
				Filename texturefile_path;
				GenerateTexturePath(
					doc->GetDocumentPath(), texturefile, Filename(), &texturefile_path);
				std::string texturename =
					StringToStdString(texturefile_path.GetString());
				args.Add(new NSI::StringArg(parameter_name, texturename));
				break;
			}

			case CUSTOMDATATYPE_GRADIENT: {
				if (shader->GetType() == Xbitmap) {
					break;
				}
				
				Gradient* gradient =
					(Gradient*)i_data.GetCustomDataType(CUSTOMDATATYPE_GRADIENT);
				int knot_count = gradient->GetKnotCount();
				std::vector<float> knots(knot_count);
				std::vector<float> colors(knot_count * 3);
				std::vector<int> interpolations(knot_count);

				for (int i = 0; i < knot_count; i++) {
					GradientKnot Knot = gradient->GetKnot(i);
					knots[i] = (float)Knot.pos;
					Vector KnotColor = toLinear((Knot.col), doc);
					colors[i * 3] = (float)KnotColor.x;
					colors[i * 3 + 1] = (float)KnotColor.y;
					colors[i * 3 + 2] = (float)KnotColor.z;
					interpolations[i] = C4DTo3DelightInterpolation(Knot.interpolation);
				}

				using namespace osl_utilities;
				using namespace osl_utilities::ramp;
				const DlShaderInfo::Parameter* knotss = nullptr;
				const DlShaderInfo::Parameter* interpolation = nullptr;
				const DlShaderInfo::Parameter* shared_interpolation = nullptr;
				std::string base_name;
				std::string pname;
				for (unsigned p = 0; p < shader_info->nparams(); p++)
				{
					osl_utilities::ParameterMetaData meta;
					const DlShaderInfo::Parameter* param = shader_info->getparam(p);

					meta.m_label = param->name.c_str();
					osl_utilities::GetParameterMetaData(meta, param->metadata);
					if (!osl_utilities::ramp::IsRampWidget(meta.m_widget))
					{
						continue;
					}

					if (param->name == parameter_name)
					{
						if (!FindMatchingRampParameters(
							*shader_info,
							*param,
							knotss,
							interpolation,
							shared_interpolation,
							base_name))
						{
							continue;
						}

						args.Add(NSI::Argument::New(knotss->name.c_str())
							->SetArrayType(NSITypeFloat, knot_count)
							->CopyValue((float*)&knots[0], knot_count * sizeof(float)));
						args.Add(
							NSI::Argument::New(param->name.c_str())
							->SetArrayType(NSITypeColor, knot_count)
							->CopyValue((float*)&colors[0], knot_count * sizeof(float) * 3));
						args.Add(
							NSI::Argument::New(interpolation->name.c_str())
							->SetArrayType(NSITypeInteger, knot_count)
							->CopyValue((int*)&interpolations[0], knot_count * sizeof(int)));
					}
				}
				break;
			}

			case CUSTOMDATATYPE_SPLINE: {

				SplineData* decay_ramp = (SplineData*)i_data.GetCustomDataType(CUSTOMDATATYPE_SPLINE);
				int decay_knot_count = decay_ramp->GetKnotCount();
				std::vector<float> decay_curve_knots(decay_knot_count);
				std::vector<float> decay_curve_floats(decay_knot_count);
				std::vector<int> decay_curve_interpolations(decay_knot_count);

				for (int i = 0; i < decay_knot_count; i++)
				{
					CustomSplineKnot* Knot = decay_ramp->GetKnot(i);
					decay_curve_knots[i] = (float)Knot->vPos.x;
					decay_curve_floats[i] = (float)Knot->vPos.y;
					decay_curve_interpolations[i] = Knot->interpol;
				}

				using namespace osl_utilities;
				using namespace osl_utilities::ramp;
				const DlShaderInfo::Parameter* knotss = nullptr;
				const DlShaderInfo::Parameter* interpolation = nullptr;
				const DlShaderInfo::Parameter* shared_interpolation = nullptr;
				std::string base_name;
				std::string pname;
				for (unsigned p = 0; p < shader_info->nparams(); p++)
				{
					osl_utilities::ParameterMetaData meta;
					const DlShaderInfo::Parameter* param = shader_info->getparam(p);

					meta.m_label = param->name.c_str();
					osl_utilities::GetParameterMetaData(meta, param->metadata);
					if (!osl_utilities::ramp::IsRampWidget(meta.m_widget))
					{
						continue;
					}

					if (param->name == parameter_name)
					{
						if (!FindMatchingRampParameters(
							*shader_info,
							*param,
							knotss,
							interpolation,
							shared_interpolation,
							base_name))
						{
							continue;
						}

						args.Add(NSI::Argument::New(knotss->name.c_str())
							->SetArrayType(NSITypeFloat, decay_knot_count)
							->CopyValue((float*)&decay_curve_knots[0], decay_knot_count * sizeof(float)));
						args.Add(
							NSI::Argument::New(param->name.c_str())
							->SetArrayType(NSITypeFloat, decay_knot_count)
							->CopyValue((float*)&decay_curve_floats[0], decay_knot_count * sizeof(float)));
						args.Add(
							NSI::Argument::New(interpolation->name.c_str())
							->SetArrayType(NSITypeInteger, decay_knot_count)
							->CopyValue((int*)&decay_curve_interpolations[0], decay_knot_count * sizeof(int)));
					}
				}
				break;
			}
		}
	}
	std::string shader_handle = (std::string) Handle;
	ctx.SetAttribute(shader_handle, args);
}
