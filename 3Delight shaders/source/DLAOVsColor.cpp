#include "DLAOVsColor.h"
#include "PluginManager.h"
#include "c4d_symbols.h"
#include "IDs.h"
#include <vector>
#include "maxon/stringencoding.h"
#include "DLCustomAOVS.h"
#include "NodeIdHash.h"
#include "RegisterPrototypes.h"

#ifdef __APPLE__
#	pragma clang diagnostic ignored "-Wmultichar"
#endif

using namespace std;

extern PluginManager PM;

enum {
	COLOR_ID = 10000,
	TEXT_ID = 5000
};

// Creating a unique ID for each aov.
static String
GetHandleName(BaseList2D* node)
{
	return NodeIdToHandle<String>(node);
}

// AOVs Structure.In the beginnig it is empty.
struct LayersData {
	Int32 id;
	Char name[50];
};

LayersData s_aovs[] = { { 0, "" } };

// Class to modify the TreeView dynimaclly in the way we want (define class).
class AOVsCustomTree
{
public:
	String Name = "";
	Int32 Type;
	Bool _selected = FALSE;
	Bool _usedSky = FALSE;
	Int32 ListPosition;
	Int32 Gobo;
	Int32 Decay;
	String objectGUID;
	AutoAlloc<BaseLink> link;
	AOVsCustomTree(String Name)
	{
		this->Name = Name;
	}

	AOVsCustomTree(BaseShader* object, Int32 position)
	{
		this->Name = object->GetName();
		this->Type = object->GetType();
		this->ListPosition = position;
		this->objectGUID = GetHandleName(object);
	}

	Bool isSelected()
	{
		return this->_selected;
	}

	void Select()
	{
		this->_selected = TRUE;
	}

	void Deselect()
	{
		this->_selected = FALSE;
	}

	String TO_STRING()
	{
		return this->Name;
	}
};

/*
        In this function we get all the aovs that are used in the scene,
        and store them within a vector of our custom treeview class. The aovs
        order depends on their order in the scene.
*/
static std::vector<AOVsCustomTree*>
Populate_TreeView()
{
	std::vector<AOVsCustomTree*> aov_list;
	AutoAlloc<BaseLink> link;
	BaseMaterial* material = GetActiveDocument()->GetActiveMaterial();

	if (!material) {
		return aov_list;
	}

	bool process = true;
	Int32 listPosition = 0;
	BaseShader* shader = material->GetFirstShader();
	std::vector<BaseShader*> vect;
	std::vector<int> positions;

	while (shader) {
		if (process && (shader->GetType() == DL_COLOR_AOV)) {
			BaseContainer* data = shader->GetDataInstance();
			ApplicationOutput(data->GetString(DL_AOV_COLOR_TEXT));
			vect.push_back(shader);
			positions.push_back(listPosition++);
		}

		if (shader->GetDown() && process) {
			shader = shader->GetDown();
			process = true;

		} else if (shader->GetNext()) {
			shader = shader->GetNext();
			process = true;

		} else if (shader->GetUp()) {
			shader = shader->GetUp();
			process = false;

		} else {
			shader = NULL;
		}
	}

	/*
	            Since the insertion of the objects is done like in a stack where
	   the new object goes to the top of the view, and we want to display them
	   in the opposite way, we are reversing the vectorto get the desired
	   result. Starting from the second items as the first one is the light
	   group.
	    */
	for (size_t i = 0; i < vect.size(); i++) {
		AOVsCustomTree* light_object = new AOVsCustomTree(vect[i], --listPosition);
		aov_list.push_back(light_object);
	}

	std::reverse(aov_list.begin(), aov_list.begin() + aov_list.size());
	return aov_list;
}

// class to supply data and view model for TreeViewCustomGui
class AOVColorTreeview : public TreeViewFunctions
{
public:
	std::vector<AOVsCustomTree*> mylist;
	void Populate()
	{
		mylist = Populate_TreeView();
	}
	// Called to retrieve the first object of the tree
	void* GetFirst(void* root, void* userdata)
	{
		if (mylist.size() > 0) {
			return mylist[0];
		}

		return nullptr;
	}

	// Called to retrieve the first child of @formatParam{obj}
	void* GetDown(void* root, void* userdata, void* obj)
	{
		return nullptr;
	}

	// Called to retrieve the next object after @formatParam{obj}
	void* GetNext(void* root, void* userdata, void* obj)
	{
		AOVsCustomTree* object = nullptr;
		AOVsCustomTree* currentObject = (AOVsCustomTree*) obj;
		std::vector<AOVsCustomTree*>::iterator it =
			std::find(mylist.begin(), mylist.end(), currentObject);
		Int32 index = (Int32) std::distance(mylist.begin(), it);
		Int32 nextIndex = index + 1;

		if (nextIndex < (int) mylist.size()) {
			object = mylist[nextIndex];
		}

		return object;
	}

	//  Called to retrieve the column width of object @formatParam{obj} in
	//  column @formatParam{col}
	Int32 GetColumnWidth(void* root,
						 void* userdata,
						 void* obj,
						 Int32 col,
						 GeUserArea* area)
	{
		return 80;
	}

	// Called to retrieve the folding status of the object.
	Bool IsOpened(void* root, void* userdata, void* obj)
	{
		return TRUE;
	}

	// Called to retrieve the name of object @formatParam{obj}.
	String GetName(void* root, void* userdata, void* obj)
	{
		if (!root || !obj) {
			return String();
		}

		String name = String();
		AOVsCustomTree* aov = (AOVsCustomTree*) obj;

		if (aov->Type == DL_COLOR_AOV) {
			name = aov->TO_STRING();
		}

		return name;
	}

	Int GetId(void* root, void* userdata, void* obj)
	{
		return 0;
	}

	// Called to retrieve the drag type of object @formatParam{obj},
	// i.e. the type that the user would get if he started a drag in that cell.
	Int32 GetDragType(void* root, void* userdata, void* obj)
	{
		return NOTOK;
	}

	// Called to select object @formatParam{obj}.
	void Select(void* root, void* userdata, void* obj, Int32 mode)
	{
		if (!root || !obj) {
			return;
		}

		AOVsCustomTree* aovs = (AOVsCustomTree*) obj;

		if (mode == SELECTION_NEW) {
			if (aovs->isSelected()) {
				aovs->Deselect();

			} else if (!aovs->isSelected()) {
				aovs->Select();
			}
		}

		return;
	}

	// Called to retrieve the selection status of object @formatParam{obj}
	Bool IsSelected(void* root, void* userdata, void* obj)
	{
		if (!root || !obj) {
			return FALSE;
		}

		AOVsCustomTree* aovs_param = (AOVsCustomTree*) obj;
		return aovs_param->isSelected();
		return true;
	}

	// Called to specify the text colors of object @formatParam{obj}
	void GetColors(void* root,
				   void* userdata,
				   void* obj,
				   GeData* pNormal,
				   GeData* pSelected)
	{
		if (!root || !obj) {
			return;
		}

		pSelected->SetVector(Vector(1, 1, 1));
	}

	// Called to specify the background color of @formatParam{line}.
	void GetBackgroundColor(void* root,
							void* userdata,
							void* obj,
							Int32 line,
							GeData* col)
	{
		if (!root || !obj) {
			return;
		}

		AOVsCustomTree* aovs_param = (AOVsCustomTree*) obj;
		col->SetVector(Vector(0.27, 0.27, 0.27));

		if (aovs_param->isSelected()) {
			col->SetVector(Vector(0.42, 0.42, 0.42));
		}

		return;
	}
};

AOVColorTreeview all_aovs;

AOVColorDialog::AOVColorDialog()
{
	m_data = NULL;
}

AOVColorDialog::~AOVColorDialog() {}

Bool AOVColorDialog::CreateLayout()
{
	return true;
}

Bool AOVColorDialog::InitValues()
{
	// First call the parent instance
	if (!GeDialog::InitValues()) {
		return false;
	}

	BaseContainer layout;
	BaseContainer data;
	Int32 i = 0;
	layout = BaseContainer();
	layout.SetInt32('name', LV_COLUMN_TEXT);
	// m_aov_view.SetLayout(1, layout);
	data = BaseContainer();

	for (i = 0; s_aovs[i].id; i++) {
		data.SetString('name', String(s_aovs[i].name));
		// m_aov_view.SetItem(s_aovs[i].id, data);
	}

	// m_aov_view.DataChanged();
	return true;
}

Bool AOVColorDialog::Command(Int32 i_id, const BaseContainer& i_msg)
{
	return true;
}

Int32 AOVColorDialog::Message(const BaseContainer& i_msg, BaseContainer& i_result)
{
	return GeDialog::Message(i_msg, i_result);
}

// Create a constructor.
iCustomDataTypeColorAOV::iCustomDataTypeColorAOV(
	const BaseContainer& i_settings,
	CUSTOMGUIPLUGIN* i_plugin)
	: iCustomGui(i_settings, i_plugin) {};

Bool iCustomDataTypeColorAOV::CreateLayout()
{
	Bool layout_create = GeDialog::CreateLayout();
	layout_create = LoadDialogResource(aovs_dialog, nullptr, 0);

	if (layout_create) {
		GroupBegin(0, BFH_SCALEFIT, 2, 0, String(), 0);
		{
			GroupBegin(
				0, BFV_SCALEFIT | BFH_SCALEFIT, 0, 1, "Color AOVs"_s, 0, 750, 150);
			{
				GroupBorder(UInt32(BORDER_GROUP_IN | BORDER_WITH_TITLE));
				GroupBorderSpace(4, 4, 4, 4);
				ScrollGroupBegin(123, BFH_SCALEFIT | BFV_SCALEFIT, SCROLLGROUP_VERT);
				{
					GroupBegin(1234, BFV_TOP | BFH_SCALEFIT, 3, 0, String(), 0);
					{
						Int32 i;

						for (i = 0; i < m_data.m_row_id.GetCount(); i++) {
							// Set checkbox value on layout create (this is
							// executed upon object create, menu change or obect
							// selection change.
							AddCheckbox(i + 1, BFH_LEFT, 0, 0, ""_s);
							SetBool(i + 1, m_data.aov_checked[i]);
							// Set text value
							AddEditText(TEXT_ID + i, BFH_SCALEFIT, 260, 0, BORDER_THIN_IN);
							SetString(TEXT_ID + i, m_data.aov_textName[i]);
							// Set color value
							AddColorField(COLOR_ID + i, BFH_SCALEFIT, 0, 0);
							SetColorField(COLOR_ID + i, m_data.aov_color[i], 1.0, 1.0, 0);
						}
					}
					GroupEnd();
				}
				GroupEnd();
			}
			GroupEnd();
		}
		GroupEnd();
		/*
			        GroupBegin(11, BFH_SCALEFIT, 2, 0, String(), 0);
			        {
			                GroupBegin(12, BFV_SCALEFIT | BFH_SCALEFIT, 0, 1, "Float
			   AOVs"_s, 0, 750, 80);
			                {
			                        GroupBorder(UInt32(BORDER_GROUP_IN |
			   BORDER_WITH_TITLE)); GroupBorderSpace(4, 4, 4, 4);

			                        ScrollGroupBegin(12345, BFH_SCALEFIT |
			   BFV_SCALEFIT, SCROLLGROUP_VERT);
			                        {
			                                GroupBegin(123456, BFV_TOP |
			   BFH_SCALEFIT, 3, 0, String(), 0);
			                                {
			                                        Int32 i;
			                                        for (i = 0; i <
			   m_data.m_float_row_id.GetCount(); i++)
			                                        {
			                                                // Set checkbox value on
			   layout create (this is executed upon object create,
			                                                // menu change or obect
			   selection change. AddCheckbox(1500+i, BFH_LEFT, 0, 0, "Aov  " +
			   String::IntToString(i + 1)); SetBool(1500+i,
			   m_data.aov_float_checked[i]);

			                                                // Set text value
			                                                AddEditText(2000 + i,
			   BFH_SCALEFIT, 260, 0, BORDER_THIN_IN); SetString(2000 + i,
			   m_data.aov_float_textName[i]);

			                                                // Set float value
			                                                AddEditNumber(2500 + i,
			   BFH_SCALEFIT, 0, 0); SetFloat(2500 + i, m_data.aov_float_value[i],
			   -1000.0, 1000.0, 1);

			                                        }
			                                }
			                                GroupEnd();
			                        }
			                        GroupEnd();
			                }
			                GroupEnd();
			        }
			        GroupEnd();
			        */
	}

	enable_parameters(1, COLOR_ID, true);
	// enable_parameters(1500, 2500,false);
	m_aov_view.DataChanged();
	return SUPER::CreateLayout();
};

Bool iCustomDataTypeColorAOV::InitValues()
{
	BaseContainer layout;
	BaseContainer data;
	BaseContainer multilight_data;
	m_aov_view.SetProperty(SLV_MULTIPLESELECTION, true);
	layout = BaseContainer();
	layout.SetInt32('Cbx', LV_COLUMN_CHECKBOX);
	layout.SetInt32('Txt', LV_COLUMN_EDITTEXT);
	layout.SetInt32('Clr', LV_COLUMN_COLORVIEW);
	m_aov_view.SetLayout(3, layout);
	m_aov_view.DataChanged();
	return SUPER::InitValues();
};

Int32 iCustomDataTypeColorAOV::Message(const BaseContainer& msg,
									   BaseContainer& result)
{
	switch (msg.GetId()) {
	case BFM_INTERACTEND: {
		/*
			                        Getting the state of the tree after an
			   interaction is finished in the view. This includes clicking somewhere
			   on the view where you can select or deselect aovs, or clicking
			   buttons in the view etc. After we store the latest state of the tree
			   in the vectors below: m_aovs_selected_layers => Store the names of
			   the selected aovs m_selected_aovs_itemID => Store the selected aov
			   row number. This is used as their ID in the RenderOptionsHook.
			                        m_data.m_selected_aovs_GUID => Store the
			   selected aovs Unique ID.
			                */
	}
	}

	return GeDialog::Message(msg, result);
}

void iCustomDataTypeColorAOV::enable_parameters(
	Int32 check_id,
	Int32 color_id,
	bool col_row)
{
	if (col_row) {
		for (int i = 0; i < m_data.m_row_id.GetCount(); i++) {
			Enable(check_id + i, !m_data.aov_textName[i].IsEqual(""_s));
			Enable(color_id + i, !m_data.aov_textName[i].IsEqual(""_s));
		}

	} else {
		for (int i = 0; i < m_data.m_float_row_id.GetCount(); i++) {
			Enable(check_id + i, !m_data.aov_float_textName[i].IsEqual(""_s));
			Enable(color_id + i, !m_data.aov_float_textName[i].IsEqual(""_s));
		}
	}
}

Bool iCustomDataTypeColorAOV::Command(Int32 i_id, const BaseContainer& i_msg)
{
	switch (i_id) {
	case DL_ADD_COLOR: {
		m_data.row = Int32(m_data.m_row_id.GetCount());
		m_data.aov_checked.Reset();
		m_data.aov_color.Reset();
		m_data.aov_textName.Reset();
		m_data.row++;
		(void) m_data.m_row_id.Append(m_data.row);

		for (int i = 0; i < m_data.row; i++) {
			const GadgetPtr& id = i + 1;
			Bool val = FALSE;

			if (!GetBool(id, val)) {
				(void) m_data.aov_checked.Append(FALSE);

			} else {
				(void) m_data.aov_checked.Append(val);
			}

			const GadgetPtr& text_id = TEXT_ID + i;
			String text;

			if (!GetString(text_id, text)) {
				(void) m_data.aov_textName.Append(""_s);

			} else {
				(void) m_data.aov_textName.Append(text);
			}

			const GadgetPtr& color_id = COLOR_ID + i;
			Vector color;
			Float brightness = 1.0;

			if (!GetColorField(color_id, color, brightness)) {
				(void) m_data.aov_color.Append(Vector(0, 0, 0));

			} else {
				(void) m_data.aov_color.Append(color);
			}
		}

		ApplicationOutput("BOOL :@", m_data.aov_checked.GetCount());
		LayoutFlushGroup(1234);
		Int32 i;

		for (i = 0; i < m_data.row; i++) {
			Float brightness = 1.0;
			Float maxbrightness = 1.0;
			Int32 flags = 0;
			AddCheckbox(i + 1, BFH_LEFT, 0, 0, ""_s);
			SetBool(i + 1, m_data.aov_checked[i]);
			AddEditText(TEXT_ID + i, BFH_SCALEFIT, 260, 0, BORDER_THIN_IN);
			SetString(TEXT_ID + i, m_data.aov_textName[i]);
			AddColorField(COLOR_ID + i, BFH_SCALEFIT, 0, 0);
			SetColorField(
				COLOR_ID + i, m_data.aov_color[i], brightness, maxbrightness, 0);
		}

		LayoutChanged(1234);
		enable_parameters(1, COLOR_ID, true);
		break;
	}

	case DL_AOV_COLOR_TEXT: {
		String value;

		//  get String value
		if (GetString(DL_AOV_COLOR_TEXT, value)) {
			//  get state of the "Enter" key.
			BaseContainer state;
			GetInputState(BFM_INPUT_KEYBOARD, KEY_ENTER, state);

			//  check if "Enter" key is pressed
			if (state.GetInt32(BFM_INPUT_VALUE)) {
				SetString(DL_AOV_COLOR_TEXT, ""_s);
				ApplicationOutput("okkkkkk");
			}
		}
	}

	/*
	        case DL_ADD_FLOAT:
	        {
	                m_data.float_row = m_data.m_float_row_id.GetCount();
	                m_data.aov_float_checked.Reset();
	                m_data.aov_float_value.Reset();
	                m_data.aov_float_textName.Reset();

	                m_data.float_row++;

	                (void)m_data.m_float_row_id.Append(m_data.float_row);

	                for (int i = 0; i < m_data.float_row; i++)
	                {

	                        const GadgetPtr &id = 1500 + i;
	                        Bool val = FALSE;
	                        if (!GetBool(id, val))
	                                (void)m_data.aov_float_checked.Append(FALSE);
	                        else
	                                (void)m_data.aov_float_checked.Append(val);

	                        const GadgetPtr &text_id = 2000 + i;
	                        String text;
	                        if (!GetString(text_id, text))
	                                (void)m_data.aov_float_textName.Append(""_s);
	                        else
	                                (void)m_data.aov_float_textName.Append(text);

	                        const GadgetPtr &float_id = 2500 + i;
	                        Float value;
	                        if (!GetFloat(float_id, value))
	                                (void)m_data.aov_float_value.Append(0.0);
	                        else
	                                (void)m_data.aov_float_value.Append(value);
	                }
	                ApplicationOutput("BOOL :@",
	   m_data.aov_float_checked.GetCount());

	                LayoutFlushGroup(123456);
	                Int32 i;
	                for (i = 0; i < m_data.float_row; i++)
	                {
	                        Float brightness = 1.0;
	                        Float maxbrightness = 1.0;
	                        Int32 flags = 0;

	                        AddCheckbox(1500 + i, BFH_LEFT, 0, 0, "Aov " +
	   String::IntToString(i + 1)); SetBool(1500 + i,
	   m_data.aov_float_checked[i]);

	                        AddEditText(2000 + i, BFH_SCALEFIT, 260, 0,
	   BORDER_THIN_IN); SetString(2000 + i, m_data.aov_float_textName[i]);

	                        // Set float value
	                        AddEditNumber(2500 + i, BFH_SCALEFIT, 0, 0);
	                        SetFloat(2500 + i, m_data.aov_float_value[i],
	   -1000.0, 1000.0, 1);
	                }
	                LayoutChanged(123456);
	                enable_parameters(1500, 2500,false);
	                break;
	        }
	        */
	default: {
		m_data.row = Int32(m_data.m_row_id.GetCount());
		m_data.aov_checked.Reset();
		m_data.aov_color.Reset();
		m_data.aov_textName.Reset();
		m_data.float_row = Int32(m_data.m_float_row_id.GetCount());
		m_data.aov_float_checked.Reset();
		m_data.aov_float_value.Reset();
		m_data.aov_float_textName.Reset();

		for (int i = 0; i < m_data.row; i++) {
			const GadgetPtr& id = i + 1;
			Bool val = FALSE;

			if (!GetBool(id, val)) {
				(void) m_data.aov_checked.Append(FALSE);

			} else {
				(void) m_data.aov_checked.Append(val);
			}

			const GadgetPtr& text_id = TEXT_ID + i;
			String text;

			if (!GetString(text_id, text)) {
				(void) m_data.aov_textName.Append(""_s);

			} else {
				(void) m_data.aov_textName.Append(text);
			}

			const GadgetPtr& color_id = COLOR_ID + i;
			Vector color;
			Float brightness = 1.0;

			if (!GetColorField(color_id, color, brightness)) {
				(void) m_data.aov_color.Append(Vector(0, 0, 0));

			} else {
				(void) m_data.aov_color.Append(color);
			}
		}

		for (int i = 0; i < m_data.float_row; i++) {
			const GadgetPtr& id = 1500 + 1;
			Bool val = FALSE;

			if (!GetBool(id, val)) {
				(void) m_data.aov_float_checked.Append(FALSE);

			} else {
				(void) m_data.aov_float_checked.Append(val);
			}

			const GadgetPtr& text_id = 2000 + i;
			String text;

			if (!GetString(text_id, text)) {
				(void) m_data.aov_float_textName.Append(""_s);

			} else {
				(void) m_data.aov_float_textName.Append(text);
			}

			const GadgetPtr& float_id = 2500 + i;
			Float value;

			if (!GetFloat(float_id, value)) {
				(void) m_data.aov_float_value.Append(0.0);

			} else {
				(void) m_data.aov_float_value.Append(value);
			}
		}

		enable_parameters(1, COLOR_ID, true);
		// enable_parameters(1500, 2500,false);
	}
	}

	BaseContainer m(i_msg);
	m.SetInt32(BFM_ACTION_ID, GetId());
	m.SetData(BFM_ACTION_VALUE, this->GetData().GetValue());
	SendParentMessage(m);
	m_aov_view.DataChanged();
	return true;
}

Bool iCustomDataTypeColorAOV::CoreMessage(Int32 id, const BaseContainer& msg)
{
	return true;
}

/**
        Called to update the custom GUI to display the value in
   @formatParam{tristate}.
*/
Bool iCustomDataTypeColorAOV::SetData(const TriState<GeData>& i_tristate)
{
	iCustomDataTypeCOLORAOV* data =
		(iCustomDataTypeCOLORAOV*) (i_tristate.GetValue().GetCustomDataType(
										ID_CUSTOMDATATYPE_AOV));

	if (data) {
		iferr(m_data.m_row_id.CopyFrom(data->m_row_id)) return false;
		iferr(m_data.aov_checked.CopyFrom(data->aov_checked)) return false;
		iferr(m_data.aov_color.CopyFrom(data->aov_color)) return false;
		iferr(m_data.aov_textName.CopyFrom(data->aov_textName)) return false;
		iferr(m_data.m_float_row_id.CopyFrom(data->m_float_row_id)) return false;
		iferr(
			m_data.aov_float_checked.CopyFrom(data->aov_float_checked)) return false;
		iferr(m_data.aov_float_value.CopyFrom(data->aov_float_value)) return false;
		iferr(m_data.aov_float_textName.CopyFrom(
				  data->aov_float_textName)) return false;
	}

	return true;
};

//  Called to retrieve the value(s) currently displayed.
TriState<GeData>
iCustomDataTypeColorAOV::GetData()
{
	TriState<GeData> tri;
	tri.Add(GeData(ID_CUSTOMDATATYPE_AOV, m_data));
	return tri;
};

static Int32 g_stringtable[] = {
	ID_CUSTOMDATATYPE_AOV
}; //  This array defines the applicable datatypes.

//  This CustomGuiData class registers a new custom GUI for the Layers datatype.
class CustomGuiAOVColor : public CustomGuiData
{
public:
	virtual Int32 GetId();
	virtual CDialog* Alloc(const BaseContainer& i_settings);
	virtual void Free(CDialog* i_dlg, void* i_userdata);
	virtual const Char* GetResourceSym();
	virtual CustomProperty* GetProperties();
	virtual Int32 GetResourceDataType(Int32*& i_table);
};

// Called to get the plugin ID of the custom GUI.
Int32 CustomGuiAOVColor::GetId()
{
	return ID_CUSTOMGUI_AOVGROUP;
};

/**
We override Alloc function to create a new instance of the plugin.It is used
instead of a constructor at this one provide means to return an error if
something goes wrong.
*/
CDialog*
CustomGuiAOVColor::Alloc(const BaseContainer& i_settings)
{
	iferr(iCustomDataTypeColorAOV* dlg = NewObj(
			iCustomDataTypeColorAOV, i_settings, GetPlugin())) return nullptr;
	CDialog* cdlg = dlg->Get();

	if (!cdlg) {
		return nullptr;
	}

	return cdlg;
};

// This function is called when an instance gets freed.
void CustomGuiAOVColor::Free(CDialog* i_dlg, void* i_userdata)
{
	if (!i_dlg || !i_userdata) {
		return;
	}

	iCustomDataTypeColorAOV* sub =
		static_cast<iCustomDataTypeColorAOV*>(i_userdata);
	DeleteObj(sub);
};

//  Returns the resource symbol. This symbol can be used in resource files in
//  combination with "CUSTOMGUI".
const Char*
CustomGuiAOVColor::GetResourceSym()
{
	return "AOVsCUSTOMGUI";
};

CustomProperty*
CustomGuiAOVColor::GetProperties()
{
	return nullptr;
};

//  Returns the applicable datatypes defined in the stringtable array.
Int32 CustomGuiAOVColor::GetResourceDataType(Int32*& i_table)
{
	i_table = g_stringtable;
	return sizeof(g_stringtable) / sizeof(Int32);
};

/**
A data class for creating custom data types.
These can be used in descriptions and container just like built-in data types.
As we do not have a buil-in data type for the Listview we create our custom data
type using the CUSTOMDATATYPECLASS.
*/
class AovsCustomDataTypeClass : public CustomDataTypeClass
{
	INSTANCEOF(AovsCustomDataTypeClass, CustomDataTypeClass)

public:
	virtual Int32 GetId()
	{
		return ID_CUSTOMDATATYPE_AOV;
	}

	virtual CustomDataType* AllocData()
	{
		iCustomDataTypeCOLORAOV* data = NewObjClear(iCustomDataTypeCOLORAOV);
		return data;
	};

	virtual void FreeData(CustomDataType* i_data)
	{
		iCustomDataTypeCOLORAOV* d = static_cast<iCustomDataTypeCOLORAOV*>(i_data);
		DeleteObj(d);
	}

	/**
	    Called to copy an instance of the custom data type.
	    Copy the data from @formatParam{src} to @formatParam{dest}.
	    */
	virtual Bool CopyData(const CustomDataType* i_src,
						  CustomDataType* i_dst,
						  AliasTrans* i_aliastrans)
	{
		//  copy the data to the given target data
		iCustomDataTypeCOLORAOV* s = (iCustomDataTypeCOLORAOV*) (i_src);
		iCustomDataTypeCOLORAOV* d = (iCustomDataTypeCOLORAOV*) (i_dst);

		if (!s || !d) {
			return false;
		}

		d->m_row_id.Flush();
		iferr(d->m_row_id.CopyFrom(s->m_row_id)) return false;
		d->aov_checked.Flush();
		iferr(d->aov_checked.CopyFrom(s->aov_checked)) return false;
		d->aov_color.Flush();
		iferr(d->aov_color.CopyFrom(s->aov_color)) return false;
		d->aov_textName.Flush();
		iferr(d->aov_textName.CopyFrom(s->aov_textName)) return false;
		// Floats
		d->m_float_row_id.Flush();
		iferr(d->m_float_row_id.CopyFrom(s->m_float_row_id)) return false;
		d->aov_float_checked.Flush();
		iferr(d->aov_float_checked.CopyFrom(s->aov_float_checked)) return false;
		d->aov_float_value.Flush();
		iferr(d->aov_float_value.CopyFrom(s->aov_float_value)) return false;
		d->aov_float_textName.Flush();
		iferr(d->aov_float_textName.CopyFrom(s->aov_float_textName)) return false;
		return true;
	}

	virtual Int32 Compare(const CustomDataType* i_dst1,
						  const CustomDataType* i_dst2)
	{
		return 1;
	}

	/**Called to write the custom data type to a file.*/
	virtual Bool WriteData(const CustomDataType* i_t_d, HyperFile* i_hf)
	{
		const iCustomDataTypeCOLORAOV* const d =
			static_cast<const iCustomDataTypeCOLORAOV*>(i_t_d);
		const maxon::Int row_numbers = d->m_row_id.GetCount();
		i_hf->WriteInt64((Int64) row_numbers);
		const maxon::Int float_row_num = d->m_float_row_id.GetCount();
		i_hf->WriteInt64((Int64) float_row_num);

		for (Int64 i = 0; i < row_numbers; ++i) {
			i_hf->WriteInt32(d->m_row_id[i]);
			i_hf->WriteBool(d->aov_checked[i]);
			i_hf->WriteVector(d->aov_color[i]);
			i_hf->WriteString(d->aov_textName[i]);
		}

		for (Int64 i = 0; i < float_row_num; ++i) {
			i_hf->WriteInt32(d->m_float_row_id[i]);
			i_hf->WriteBool(d->aov_float_checked[i]);
			i_hf->WriteFloat(d->aov_float_value[i]);
			i_hf->WriteString(d->aov_float_textName[i]);
		}

		const maxon::Int _all_aovs = d->m_all_aovs.GetCount();
		i_hf->WriteInt64((Int64) _all_aovs);

		for (Int64 i = 0; i < _all_aovs; ++i) {
			i_hf->WriteString(d->m_all_aovs[i]);
		}

		return true;
	}

	// Called to read the custom data type from a file.
	virtual Bool ReadData(CustomDataType* i_t_d, HyperFile* i_hf, Int32 i_level)
	{
		iCustomDataTypeCOLORAOV* const d =
			static_cast<iCustomDataTypeCOLORAOV*>(i_t_d);

		if (i_level > 0) {
			Int64 aov_selected = 0;

			if (i_hf->ReadInt64(&aov_selected)) {
				ApplicationOutput("Read @", aov_selected);

				for (Int64 i = 0; i < aov_selected; ++i) {
					Int32 row_id;
					Bool checked;
					Vector color;
					String text;

					if (i_hf->ReadInt32(&row_id)) {
						iferr(d->m_row_id.Append(row_id)) return false;
					}

					if (i_hf->ReadBool(&checked)) {
						iferr(d->aov_checked.Append(checked)) return false;
					}

					if (i_hf->ReadVector(&color)) {
						iferr(d->aov_color.Append(color)) return false;
					}

					if (i_hf->ReadString(&text)) {
						iferr(d->aov_textName.Append(text)) return false;
					}

					Int32 float_row_id;
					Bool float_checked;
					Float float_color;
					String float_text;

					if (i_hf->ReadInt32(&float_row_id)) {
						iferr(d->m_float_row_id.Append(float_row_id)) return false;
					}

					if (i_hf->ReadBool(&float_checked)) {
						iferr(d->aov_float_checked.Append(float_checked)) return false;
					}

					if (i_hf->ReadFloat(&float_color)) {
						iferr(d->aov_float_value.Append(float_color)) return false;
					}

					if (i_hf->ReadString(&float_text)) {
						iferr(d->aov_float_textName.Append(float_text)) return false;
					}
				}
			}
		}

		return true;
	}

	// Called to get the symbol to use in resource files
	virtual const Char* GetResourceSym()
	{
		return "AOVSCUSTOMTYPE";
	}

	virtual void GetDefaultProperties(BaseContainer& i_data)
	{
		//  the default values of this datatype
		//  use the custom GUI as default
		i_data.SetInt32(DESC_CUSTOMGUI, ID_CUSTOMGUI_AOVGROUP);
		i_data.SetInt32(DESC_ANIMATE, DESC_ANIMATE_ON);
	}
};

/**
        With this function we register the custom GUI plugin that we have alread
   build.
*/
Bool RegisterAOVs()
{
	if (!RegisterCustomDataTypePlugin("AOVsData"_s,
									  CUSTOMDATATYPE_INFO_LOADSAVE | CUSTOMDATATYPE_INFO_TOGGLEDISPLAY | CUSTOMDATATYPE_INFO_HASSUBDESCRIPTION,
									  NewObjClear(AovsCustomDataTypeClass),
									  1)) {
		return false;
	}

	static BaseCustomGuiLib myStringGUIlib;
	ClearMem(&myStringGUIlib, sizeof(myStringGUIlib));
	FillBaseCustomGui(myStringGUIlib);
	InstallLibrary(
		ID_CUSTOMGUI_AOVGROUP, &myStringGUIlib, 1001, sizeof(myStringGUIlib));
	RegisterCustomGuiPlugin("AOVs"_s, 0, NewObjClear(CustomGuiAOVColor));
	return true;
}
