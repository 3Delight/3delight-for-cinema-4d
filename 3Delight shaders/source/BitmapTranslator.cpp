#include "BitmapTranslator.h"
#include "delightenvironment.h"
#include "DL_TypeConversions.h"
#include "nsi.hpp"

void BitmapTranslator::CreateNSINodes(const char* Handle,
									  const char* ParentTransformHandle,
									  BaseList2D* C4DNode,
									  BaseDocument* doc,
									  DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	// Create the shader node
	std::string shader_handle = (std::string) Handle;
	ctx.Create(shader_handle, "shader");
	BaseShader* shader = (BaseShader*) C4DNode;
	BaseContainer* _data = shader->GetDataInstance();

	// Shader file path
	std::string shaderpath = (GeGetPluginPath().GetDirectory() + Filename("osl")
							+ Filename("Bitmap.oso")).GetString().GetCStringCopy();
	Filename texturefile = _data->GetFilename(BITMAPSHADER_FILENAME);
	Filename texturefile_absolute;
	GenerateTexturePath(
		doc->GetDocumentPath(), texturefile, Filename(), &texturefile_absolute);
	std::string texturename = StringToStdString(texturefile_absolute.GetString());
	long colorprofile = _data->GetInt32(BITMAPSHADER_COLORPROFILE);
	std::string color_space = "srgb";

	if (colorprofile == BITMAPSHADER_COLORPROFILE_EMBEDDED) {
		color_space = "auto";

	} else if (colorprofile == BITMAPSHADER_COLORPROFILE_LINEAR) {
		color_space = "linear";

	} else if (colorprofile == BITMAPSHADER_COLORPROFILE_SRGB) {
		color_space = "srgb";

	} else if (colorprofile == BITMAPSHADER_COLORPROFILE_CUSTOM) {
		color_space = "none";
	}

	ctx.SetAttribute(
		shader_handle,
		(NSI::StringArg("shaderfilename", shaderpath),
		 NSI::StringArg("texturename", texturename),
		 NSI::StringArg("texturename.meta.colorspace", color_space)));
}
