#pragma once

#include "c4d.h"
#include "DLCustomAOVS.h"

class AovsCustomDataTypeClass; // forward declaration

/**
        In this class we have stored the information that we need for the Aovs.
        We have inherited from iCustomDataType so we can override its funtcions
        to store this data and also write them on a hyperfile so we can access
   them even after we have closed c4d.
*/
class iCustomDataTypeCOLORAOV : public iCustomDataType<iCustomDataTypeCOLORAOV>
{
	friend class AovsCustomDataTypeClass;

public:
	maxon::BaseArray<maxon::String> m_aovs_selected_layers;
	maxon::BaseArray<maxon::String> m_selected_aovs_GUID;
	maxon::BaseArray<maxon::String> m_all_aovs;

	maxon::BaseArray<Int32> m_row_id;
	maxon::BaseArray<maxon::Bool> aov_checked;
	maxon::BaseArray<Vector> aov_color;
	maxon::BaseArray<maxon::String> aov_textName;

	maxon::BaseArray<Int32> m_float_row_id;
	maxon::BaseArray<maxon::Bool> aov_float_checked;
	maxon::BaseArray<Float> aov_float_value;
	maxon::BaseArray<maxon::String> aov_float_textName;

	maxon::Int32 row = 0;
	maxon::Int32 float_row = 0;
	iCustomDataTypeCOLORAOV() {}
};

class AOVColorDialog : public GeDialog
{
private:
	// SimpleListView				m_aov_view;
	AutoAlloc<BaseSelect> m_selection;

public:
	AOVColorDialog();
	virtual ~AOVColorDialog();
	maxon::BaseArray<String> m_aov_selector_selected;
	maxon::BaseArray<Int32> m_aov_selector_selectedID;
	iCustomDataTypeCOLORAOV* m_data;
	iCustomDataTypeCOLORAOV* m_data_float;
	virtual Bool CreateLayout();
	virtual Bool InitValues();
	virtual Bool Command(Int32 i_id, const BaseContainer& i_msg);
	virtual Int32 Message(const BaseContainer& i_msg, BaseContainer& i_result);
};

/**
iCustomDataTypeColorAOV is the class used to buid the Aovs View and
it inherits from iCustomGui because the tree view for the Aovs param
can not be build directly on a descriprion UI so we use Custom Gui
DataType to build on a parameter description. Basically we are putting
a GeDialog in a parameter description UI using the iCustomGui class
*/

class iCustomDataTypeColorAOV : public iCustomGui

{
	INSTANCEOF(iCustomDataTypeColorAOV, iCustomGui)

private:
	// Int32					rows;
	iCustomDataTypeCOLORAOV m_data;
	iCustomDataTypeCOLORAOV m_data_float;
	AOVColorDialog m_dialog;
	SimpleListView m_lightfiters;
	AutoAlloc<BaseSelect> m_aov_selection;
	SimpleListView m_aov_view;
	SimpleListView m_aov_float_view;
	DLCustomAOVS selected;
	void enable_parameters(Int32 check_id, Int32 color_id, bool col_row);

public:
	iCustomDataTypeColorAOV(const BaseContainer& i_settings,
							CUSTOMGUIPLUGIN* i_plugin);
	virtual Bool CreateLayout();
	virtual Bool InitValues();
	virtual Int32 Message(const BaseContainer& msg, BaseContainer& result);
	virtual Bool CoreMessage(Int32 id, const BaseContainer& msg);
	virtual Bool Command(Int32 i_id, const BaseContainer& i_msg);
	virtual Bool SetData(const TriState<GeData>& i_tristate);
	virtual TriState<GeData> GetData();
};
