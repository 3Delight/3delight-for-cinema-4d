#pragma once

#include "DL_HookPlugin.h"

class ShaderSettingsHook : public DL_HookPlugin
{
public:
	virtual void CreateNSINodes(BaseDocument* doc, DL_SceneParser* parser);
	// virtual void ConnectNSINodes(BaseDocument* doc, DL_SceneParser* parser);
};