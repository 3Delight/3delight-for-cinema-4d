#include "c4d.h"
#include "c4d_symbols.h"
#include <DL_Utilities.h>
#include "OSL_shader_info.h"
#include "osl_utilities.h"
#include "RegisterPrototypes.h"

/*
	Used to register command plugins which will be used to create
	the materials that we have registered. We use commands because
	we can arrange them the way we want on the UI. We register
	two types of commands for each material, where one corresponds
	to shelf commands in which the material created is assigned 
	directly to the selected objects. 
*/

//Global variable for shader table
extern std::map<Int32, OSL_shader_info> OSL_shader_table;
extern std::map<std::string, int> materials_id;

#define LEAKED(x) (x)

const int POPUP_ATTRIBUTE = 1000;
const int GROUP_ATTRIBUTE = 2000;
const int SHADER_ATTRIBUTE = 3000;
const int SHADER_TEMP_ATTRIBUTE = 4000;
const int STATIC_TEXT_ATTRIBUTE = 5000;

class OSL_Material_Objects : public CommandData
{
public:
	int shelf_used = 0;
	int node_id;
	virtual Bool Execute(BaseDocument* doc, GeDialog* parentManager);
};


Bool OSL_Material_Objects::Execute(BaseDocument* doc, GeDialog* parentManager)
{
	CreateMaterial(shelf_used, node_id, GetActiveDocument());
	return true;
}

/*
	IDs are obtained from Maxon Plugin ID generator. They have been
	generated for each kind of material as below:

	ID -> for Material plugin where it's incremented by 1 for each material next.
	ID + 50 -> For Command Plugin where it's incremented by 1 for each command next.
	This means that whatever the ID of the material is, ID + 50 will be the command
	that corresponds to it.
	ID + 100 -> For Command Plugin used on the shelf menu. This would be the same as
	the previous ID, where no matter what the ID of the material is, ID + 100 will be
	the command that corresponds to it.
*/

Bool RegisterOSL_materialObjects(Int32 shader_ID)
{
	Filename res_dir = GeGetPluginResourcePath();
	OSL_shader_info info;
	const char* shaders_path = info.m_shaders_path.c_str();
	info.find_all_shaders(shaders_path);
	int i = 0;

	for (auto &mat : info.materials_map)
	{
		const char* name = mat.first.c_str();
		const DlShaderInfo *shader_info = info.get_shader_info(mat.second.c_str());
		const DlShaderInfo::constvector<DlShaderInfo::Parameter> &tags =
			shader_info->metadata();

		int id;

		if (materials_id[mat.first.c_str()])
		{
			id = materials_id[mat.first.c_str()]+50;
		}
		else
		{
			id = shader_ID + i++;
		}

		osl_utilities::FindMetaData(name, tags, "niceName");
		std::string better;
		if (::islower(name[0]))
		{
			/*
				Many utility nodes have no nice name but their ID is just fine with
				an upper case. For example: leather, granite, etc.
			*/
			better = name;
			better[0] = std::string::value_type(toupper(better[0]));
			name = better.c_str();
		}

		info.m_oso_file_path = mat.second.c_str();
		OSL_shader_table[id] = info;

		//Only register shaders whose resource file exists.
		Filename file;
		Filename search_resource = (Filename)mat.first.c_str();
		search_resource.SetSuffix("res"_s);
		if (GeSearchFile(res_dir, search_resource, &file))
		{
			OSL_Material_Objects* new_material_object = NewObjClear(OSL_Material_Objects);
			OSL_Material_Objects* new_shelf_material_object = NewObjClear(OSL_Material_Objects);
			String icon_str = (String)name;
			icon_str = icon_str.ToLower();
			maxon::Result res = icon_str.Replace(" "_s, "_"_s);
			res = icon_str.Replace("&"_s, "and"_s);
			if (RegisterCommandPlugin(id,
				(String)name,
				PLUGINFLAG_HIDEPLUGINMENU,
				AutoBitmap("shelf_" + icon_str + ".png"),
				String("Assign new Material"),
				new_material_object))
			{
				new_material_object->node_id = id - 50;
			}

			if (RegisterCommandPlugin(id+50,
				(String)name,
				PLUGINFLAG_HIDEPLUGINMENU,
				AutoBitmap("shelf_" + icon_str + ".png"),
				(String)"Assign new " + name + (String)(" Material"),
				new_shelf_material_object))
			{
				new_shelf_material_object->shelf_used = 1;
				new_shelf_material_object->node_id = id - 50;
			}
		}
	}
	return true;
}
