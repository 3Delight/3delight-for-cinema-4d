#pragma once
#include "c4d.h"
#include "3Delight/ShaderQuery.h"
#include "nsi_dynamic.hpp"
#include <unordered_map>
#include <map>

class OSL_shader_info {
public:
	OSL_shader_info();

	DlShaderInfo *get_shader_info(const char *i_path) const;
	DlShaderInfo *get_shader_info(BaseShader *i_shader) const;
	void find_all_shaders(const char *i_installation_root);
	std::map<int, std::pair<std::string, std::string>> m_ids_to_names;
public:

	String m_name;
	String m_oso_file_path;
	std::string m_shaders_path;
	std::string m_plugin_path_osl;
	Bool m_has_uv_connection;
	Bool m_has_transform_connection;
	Bool m_is_surface;
	std::string m_output_name;

	//Map having shader's name as key and shader's path as value.
	std::unordered_map<std::string, std::string> shaders_map;
	std::unordered_map<std::string, std::string> materials_map;
};
