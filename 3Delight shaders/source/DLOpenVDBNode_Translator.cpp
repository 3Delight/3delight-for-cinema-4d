#include "DLOpenVDBNode_Translator.h"
#include <DL_Utilities.h>
#include <map>
#include "delightenvironment.h"
#include "DL_TypeConversions.h"
#include "DL_Utilities.h"
#include "VDBQuery.h"
#include "dl_open_vdb_node.h"
#include "nsi.hpp"

#ifdef __APPLE__
#	pragma clang diagnostic ignored "-Wimplicit-float-conversion"
#endif

// Use this function to get the Text value from the Dropdown (CYCLE)
static std::string
getDropDownNames(Description* const description,
				 Int32 group_id,
				 Int32 SelectedItem)
{
	const DescID* singleid = description->GetSingleDescID();
	const DescID cid = DescLevel(group_id, DTYPE_LONG, 0);
	std::string selected_name = "";

	if (!singleid || cid.IsPartOf(*singleid, nullptr)) {
		AutoAlloc<AtomArray> arr;
		BaseContainer* selectionParameter =
			description->GetParameterI(DescLevel(group_id, DTYPE_LONG, 0), arr);

		if (selectionParameter != nullptr) {
			BaseContainer* items =
				selectionParameter->GetContainerInstance(DESC_CYCLE);

			if (items != nullptr) {
				selected_name =
					items->GetData(SelectedItem).GetString().GetCStringCopy();
			}
		}
	}

	if (selected_name == "None") {
		selected_name = "";
	}

	return selected_name;
}

void Delight_OpenVDBTranslator::CreateNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	m_handle = (std::string) Handle;
	m_transform_handle = std::string(ParentTransformHandle);
	ctx.Create(m_handle, "volume");
	std::string transform_handle2 = m_handle + "transform";
	ctx.Create(transform_handle2, "transform");
	BaseList2D* baselist = (BaseList2D*) C4DNode;
	BaseContainer* data = baselist->GetDataInstance();
	Float xmax = data->GetFloat(xMax);
	Float xmin = data->GetFloat(xMin);
	Float ymax = data->GetFloat(yMax);
	Float ymin = data->GetFloat(yMin);
	Float zmax = data->GetFloat(zMax);
	Float zmin = data->GetFloat(zMin);
	/*
	            Storing the default size values of the vdb file.
	            These values will be constant for each vdb and won't
	            change through any rendering process. these will be used
	            to scale the vdb file whenever the size of it (x,y,z dir)
	            is changed. So the ratio between new size values and the
	            default ones will be used to build the tranform matrix.
	            ratio formula:(new ?max - new ?min)/(Const_?max-Const_?min)
	    */
	const Float Const_xmax = data->GetFloat(Const_xMax);
	const Float Const_xmin = data->GetFloat(Const_xMin);
	const Float Const_ymax = data->GetFloat(Const_yMax);
	const Float Const_ymin = data->GetFloat(Const_yMin);
	const Float Const_zmax = data->GetFloat(Const_zMax);
	const Float Const_zmin = data->GetFloat(Const_zMin);
	std::vector<double> vbb_size_matrix{
		(xmax - xmin) / (Const_xmax - Const_xmin),
		0,
		0,
		0,
		0,
		(ymax - ymin) / (Const_ymax - Const_ymin),
		0,
		0,
		0,
		0,
		-((zmax - zmin) / (Const_zmax - Const_zmin)),
		0,
		0,
		0,
		0,
		1
	};
	NSI::Argument xform("transformationmatrix");
	xform.SetType(NSITypeDoubleMatrix);
	xform.SetValuePointer((void*) &vbb_size_matrix[0]);
	ctx.SetAttribute(transform_handle2, xform);
	ctx.Connect(transform_handle2, "", m_transform_handle, "objects");
	ctx.Connect(m_handle, "", transform_handle2, "objects");
	m_attributes_handle = m_handle + "attributes";
	ctx.Create(m_attributes_handle, "attributes");
	AutoAlloc<Description> description;

	if (description == nullptr) {
		return;
	}

	if (!baselist->GetDescription(description, DESCFLAGS_DESC::NONE)) {
		return;
	}

	std::string texturename = "";
	Filename texturefile = data->GetFilename(FILE_FILENAME);
	BaseTime time = doc->GetTime();
	Int32 outside_range = data->GetInt32(FILE_OUTSIDE_RANGE);
	Int32 offset = data->GetInt32(FILE_START_OFFSET);
	Int32 start_frame = time.GetFrame(doc->GetFps()) + offset;
	Int32 increment = data->GetInt32(FILE_INCREMENT);
	bool confine_to_frame = data->GetBool(FILE_CONFINE_TO_FRAME_RANGE);
	Int32 frame_range_start = data->GetInt32(FILE_FRAME_RANGE_START);
	Int32 frame_range_end = data->GetInt32(FILE_FRAME_RANGE_END);

	if (confine_to_frame) {
		start_frame = frame_range_start;
	}

	if (texturefile.IsPopulated()) {
		texturename = StringToStdString(texturefile.GetString());
		std::string file_string = "";

		if (data->GetBool(FILE_USE_FILE_SEQUENCE) == true) {
			file_string = getFormattedFilename(texturename);

			if (file_string != texturename) {
				std::string frame_string = std::to_string(start_frame);
				Replace(file_string, "#", frame_string);
				int i = 0;

				// Taking in consideration that frame padding can have up to
				// three 0 before the current frame to be rendered
				while (i++ < 4) {
					if (file_exists(file_string)) {
						exists = TRUE;
						break;

					} else {
						Replace(file_string, frame_string, "0" + frame_string);
						frame_string = "0" + frame_string;
					}
				}

				if (!exists) {
					ApplicationOutput("File @ does not exist", texturename.c_str());
					return;
				}

				texturename = file_string;
				// Print that the file does not exist and render the currently
				// uploaded vdb file instead.
			}

			/*
				                    If file sequence is on but there is no sequence
				   on the filename we check if that file exists. If not exist we do
				   nothing and end the function.
				            */
			else if (!file_exists(texturename)) {
				ApplicationOutput("File @ does not exist", texturename.c_str());
				return;
			}

			// set exists to true if file exists.
			exists = true;
		}

		// checking if file exists when Use file sequence checking is off.
		else {
			if (!file_exists(texturename)) {
				ApplicationOutput("File @ does not exist", texturename.c_str());
				return;
			}

			// set exists to true if file exists.
			exists = true;
		}
	}

	double velocity_scale = data->GetFloat(GRIDS_GROUP_VELOCITY_SCALE);
	/*
	            Here we get the selected ID for the grids but we need the text
	            that it contains. Get String won't work as dropdown can be only
	            of LONG type
	    */
	int smoke = data->GetInt32(GRIDS_GROUP_SMOKE);
	int smoke_color = data->GetInt32(GRIDS_GROUP_SMOKE_COLOR);
	int temperature = data->GetInt32(GRIDS_GROUP_TEMPERATURE);
	int emission = data->GetInt32(GRIDS_GROUP_EMISSION_INTENSITY);
	int velocity = data->GetInt32(GRIDS_GROUP_VELOCITY);
	std::string smoke_text =
		getDropDownNames(description, GRIDS_GROUP_SMOKE, smoke);
	std::string smoke_color_text =
		getDropDownNames(description, GRIDS_GROUP_SMOKE_COLOR, smoke_color);
	std::string temperature_text =
		getDropDownNames(description, GRIDS_GROUP_TEMPERATURE, temperature);
	std::string emission_text =
		getDropDownNames(description, GRIDS_GROUP_EMISSION_INTENSITY, emission);
	std::string velocity_text =
		getDropDownNames(description, GRIDS_GROUP_VELOCITY, velocity);
	NSI::ArgumentList args;
	args.Add(new NSI::StringArg("vdbfilename", texturename));
	args.Add(new NSI::StringArg("densitygrid", smoke_text.c_str()));
	args.Add(new NSI::StringArg("colorgrid", smoke_color_text.c_str()));
	args.Add(new NSI::StringArg("temperaturegrid", temperature_text.c_str()));
	args.Add(new NSI::StringArg("emissionintensitygrid", emission_text.c_str()));
	args.Add(new NSI::StringArg("velocitygrid", velocity_text.c_str()));
	args.Add(new NSI::DoubleArg("velocityscale", velocity_scale));
	ctx.SetAttribute(m_handle, args);
	m_shader_handle =
		m_handle + "shader"; // std::string(parser->GetUniqueName("OpenVDBShader"));
	ctx.Create(m_shader_handle, "shader");
	args.clear();
	const char* delightpath = DelightEnv::getDelightEnvironment();
	std::string c_shaderpath =
		(std::string) delightpath + "/osl" + "/vdbVolume.oso";
	// Scattering Group
	float density = (float) data->GetFloat(SCATTERING_DENSITY);
	Vector c4dScatteringColor = toLinear(data->GetVector(SCATTERING_COLOR), doc);
	float scattering_color[3] { (float) c4dScatteringColor.x,
								(float) c4dScatteringColor.y,
								(float) c4dScatteringColor.z
							  };
	float scattering_anisotropy = (float) data->GetFloat(SCATTERING_ANISOTROPY);
	float multiple_scattering = data->GetFloat(SCATTERING_MULTIPLE_SCATTERING);
	args.Add(new NSI::StringArg("shaderfilename", &c_shaderpath[0]));
	args.Add(new NSI::FloatArg("density", density));
	args.Add(new NSI::ColorArg("scattering_color", &scattering_color[0]));
	args.Add(new NSI::FloatArg("scattering_anisotropy", scattering_anisotropy));
	args.Add(new NSI::IntegerArg("multiple_scattering", 1));
	args.Add(
		new NSI::FloatArg("multiple_scattering_intensity", multiple_scattering));
	// Density Ramp Group
	bool density_ramp_enable = data->GetBool(DENSITY_RAMP_ENABLE);
	float density_range_start = (float) data->GetFloat(DENSITY_RAMP_RANGE_START);
	float density_range_end = (float) data->GetFloat(DENSITY_RAMP_RANGE_END);
	GeData density_ramp_data = data->GetData(DENSITY_RAMP_RAMP);
	SplineData* density_ramp =
		(SplineData*) density_ramp_data.GetCustomDataType(CUSTOMDATATYPE_SPLINE);
	int density_knot_count = density_ramp->GetKnotCount();
	std::vector<float> density_curve_knots(density_knot_count);
	std::vector<float> density_curve_floats(density_knot_count);
	std::vector<int> density_curve_interpolations(density_knot_count);

	for (int i = 0; i < density_knot_count; i++) {
		CustomSplineKnot* Knot = density_ramp->GetKnot(i);
		density_curve_knots[i] = (float) Knot->vPos.x;
		density_curve_floats[i] = (float) Knot->vPos.y;
		density_curve_interpolations[i] = Knot->interpol;
	}

	args.Add(new NSI::IntegerArg("density_remap_enable", density_ramp_enable));
	args.Add(new NSI::FloatArg("density_remap_range_start", density_range_start));
	args.Add(new NSI::FloatArg("density_remap_range_end", density_range_end));
	args.Add(NSI::Argument::New("density_remap_curve_Knots")
			 ->SetArrayType(NSITypeFloat, density_knot_count)
			 ->SetValuePointer(&density_curve_knots[0]));
	args.Add(NSI::Argument::New("density_remap_curve_Floats")
			 ->SetArrayType(NSITypeFloat, density_knot_count)
			 ->SetValuePointer(&density_curve_floats[0]));
	args.Add(NSI::Argument::New("density_remap_curve_Interp")
			 ->SetArrayType(NSITypeInteger, density_knot_count)
			 ->SetValuePointer(&density_curve_interpolations[0]));
	//Transparency Group
	Vector c4dTransparencyColor = toLinear(data->GetVector(TRANSPARENCY_COLOR), doc);
	float transparency_color[3] { (float) c4dTransparencyColor.x, (float) c4dTransparencyColor.y, (float) c4dTransparencyColor.z };
	float transparency_scale = (float) data->GetFloat(TRANSPARENCY_SCALE);
	float incandescence[3] { 1,1,1 };
	args.Add(new NSI::ColorArg("transparency_color", &transparency_color[0]));
	args.Add(new NSI::FloatArg("transparency_scale", transparency_scale));
	args.Add(new NSI::ColorArg("incandescence", &incandescence[0]));
	//Emission Group
	float emmission_scale = (float) data->GetFloat(EMMISSION_SCALE);
	bool intensity_use_grid = data->GetBool(INTENSITY_USE_GRID);
	float intensity_range_start = (float) data->GetFloat(INTENSITY_RANGE_START);
	float intensity_range_end = (float) data->GetFloat(INTENSITY_RANGE_END);
	GeData intensity_ramp_data = data->GetData(INTENSITY_RAMP);
	SplineData* intensity_ramp =
		(SplineData*) intensity_ramp_data.GetCustomDataType(CUSTOMDATATYPE_SPLINE);
	int intensity_knot_count = intensity_ramp->GetKnotCount();
	std::vector<float> intensity_curve_knots(intensity_knot_count);
	std::vector<float> intensity_curve_floats(intensity_knot_count);
	std::vector<int> intensity_curve_interpolations(intensity_knot_count);

	for (int i = 0; i < intensity_knot_count; i++) {
		CustomSplineKnot* Knot = intensity_ramp->GetKnot(i);
		intensity_curve_knots[i] = (float) Knot->vPos.x;
		intensity_curve_floats[i] = (float) Knot->vPos.y;
		intensity_curve_interpolations[i] = Knot->interpol;
	}

	args.Add(new NSI::FloatArg("emission_intensity_scale", emmission_scale));
	args.Add(
		new NSI::IntegerArg("emission_intensity_grid_enable", intensity_use_grid));
	args.Add(
		new NSI::FloatArg("emission_intensity_range_start", intensity_range_start));
	args.Add(
		new NSI::FloatArg("emission_intensity_range_end", intensity_range_end));
	args.Add(NSI::Argument::New("emission_intensity_curve_Knots")
			 ->SetArrayType(NSITypeFloat, intensity_knot_count)
			 ->SetValuePointer(&intensity_curve_knots[0]));
	args.Add(NSI::Argument::New("emission_intensity_curve_Floats")
			 ->SetArrayType(NSITypeFloat, intensity_knot_count)
			 ->SetValuePointer(&intensity_curve_floats[0]));
	args.Add(NSI::Argument::New("emission_intensity_curve_Interp")
			 ->SetArrayType(NSITypeInteger, intensity_knot_count)
			 ->SetValuePointer(&intensity_curve_interpolations[0]));
	// Blackbody Group
	float blackbody_intensity = (float) data->GetFloat(BLACKBODY_INTENSITY);
	int blackbody_mode = data->GetInt32(BLACKBODY_MODE);
	float blackbody_kelvin = (float) data->GetFloat(BLACKBODY_KELVIN);
	Vector c4dBlackBodyTint = data->GetVector(BLACKBODY_TINT);
	float blackbody_tint[3] { (float) c4dBlackBodyTint.x,
							  (float) c4dBlackBodyTint.y,
							  (float) c4dBlackBodyTint.z
							};
	float blackbody_range_start = (float) data->GetFloat(BLACKBODY_RANGE_START);
	float blackbody_range_end = (float) data->GetFloat(BLACKBODY_RANGE_END);
	GeData blackbody_ramp_data = data->GetData(BLACKBODY_RAMP);
	SplineData* blackbody_ramp =
		(SplineData*) blackbody_ramp_data.GetCustomDataType(CUSTOMDATATYPE_SPLINE);
	int blackbody_knot_count = blackbody_ramp->GetKnotCount();
	std::vector<float> blackbody_curve_knots(blackbody_knot_count);
	std::vector<float> blackbody_curve_floats(blackbody_knot_count);
	std::vector<int> blackbody_curve_interpolations(blackbody_knot_count);

	for (int i = 0; i < blackbody_knot_count; i++) {
		CustomSplineKnot* Knot = blackbody_ramp->GetKnot(i);
		blackbody_curve_knots[i] = (float) Knot->vPos.x;
		blackbody_curve_floats[i] = (float) Knot->vPos.y;
		blackbody_curve_interpolations[i] = Knot->interpol;
	}

	args.Add(new NSI::FloatArg("blackbody_intensity", blackbody_intensity));
	args.Add(new NSI::IntegerArg("blackbody_mode", blackbody_mode));
	args.Add(new NSI::FloatArg("blackbody_kelvin", blackbody_kelvin));
	args.Add(new NSI::ColorArg("blackbody_tint", &blackbody_tint[0]));
	args.Add(new NSI::FloatArg("blackbody_temperature_range_start",
							   blackbody_range_start));
	args.Add(
		new NSI::FloatArg("blackbody_temperature_range_end", blackbody_range_end));
	args.Add(NSI::Argument::New("blackbody_temperature_curve_Knots")
			 ->SetArrayType(NSITypeFloat, blackbody_knot_count)
			 ->SetValuePointer(&blackbody_curve_knots[0]));
	args.Add(NSI::Argument::New("blackbody_temperature_curve_Floats")
			 ->SetArrayType(NSITypeFloat, blackbody_knot_count)
			 ->SetValuePointer(&blackbody_curve_floats[0]));
	args.Add(NSI::Argument::New("blackbody_temperature_curve_Interp")
			 ->SetArrayType(NSITypeInteger, blackbody_knot_count)
			 ->SetValuePointer(&blackbody_curve_interpolations[0]));
	// Ramp Group
	float ramp_intensity = (float) data->GetFloat(RAMP_INTENSITY);
	Vector c4dRampTint = toLinear(data->GetVector(RAMP_TINT), doc);
	float ramp_tint[3] { (float) c4dRampTint.x,
						 (float) c4dRampTint.y,
						 (float) c4dRampTint.z
					   };
	float ramp_range_start = (float) data->GetFloat(RAMP_RANGE_START);
	float ramp_range_end = (float) data->GetFloat(RAMP_RANGE_END);
	GeData ramp_data = data->GetData(EMISSION_RAMP);
	Gradient* emmission_ramp =
		(Gradient*) ramp_data.GetCustomDataType(CUSTOMDATATYPE_GRADIENT);
	int ramp_knot_count = emmission_ramp->GetKnotCount();
	std::vector<float> ramp_curve_knots(ramp_knot_count);
	std::vector<float> ramp_curve_colors(ramp_knot_count * 3);
	std::vector<int> ramp_curve_interpolations(ramp_knot_count);

	for (int i = 0; i < ramp_knot_count; i++) {
		int j = i * 3;
		GradientKnot Knot = emmission_ramp->GetKnot(i);
		ramp_curve_knots[i] = (float) Knot.pos;
		Vector KnotColor = toLinear((Knot.col), doc);
		ramp_curve_colors[j] = (float) KnotColor.x;
		ramp_curve_colors[j + 1] = (float) KnotColor.y;
		ramp_curve_colors[j + 2] = (float) KnotColor.z;
		ramp_curve_interpolations[i] = Knot.interpolation;
	}

	args.Add(new NSI::FloatArg("emissionramp_intensity", ramp_intensity));
	args.Add(new NSI::ColorArg("emissionramp_tint", &ramp_tint[0]));
	args.Add(new NSI::FloatArg("emissionramp_temperature_range_start",
							   ramp_range_start));
	args.Add(
		new NSI::FloatArg("emissionramp_temperature_range_end", ramp_range_end));
	args.Add(NSI::Argument::New("emissionramp_color_curve_Knots")
			 ->SetArrayType(NSITypeFloat, ramp_knot_count)
			 ->SetValuePointer(&ramp_curve_knots[0]));
	args.Add(NSI::Argument::New("emissionramp_color_curve_Colors")
			 ->SetArrayType(NSITypeColor, ramp_knot_count)
			 ->SetValuePointer(&ramp_curve_colors[0]));
	args.Add(NSI::Argument::New("emissionramp_color_curve_Interp")
			 ->SetArrayType(NSITypeInteger, ramp_knot_count)
			 ->SetValuePointer(&ramp_curve_interpolations[0]));
	ctx.SetAttribute(m_shader_handle, args);
	// parser->SetAssociatedHandle((BaseList2D*)C4DNode,
	// m_shader_handle.c_str());
}

void Delight_OpenVDBTranslator::ConnectNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	/*
	            We do not connect the vdb shader if vdb file defined
	            on filename does not exists.
	    */
	if (!exists) {
		return;
	}

	NSI::Context& ctx(parser->GetContext());
	ctx.Connect(m_shader_handle, "", m_attributes_handle, "volumeshader");
	ctx.Connect(m_attributes_handle, "", m_handle, "geometryattributes");
}
