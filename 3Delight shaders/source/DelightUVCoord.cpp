#include "DelightUVCoord.h"
#include "delightenvironment.h"
#include "nsi.hpp"

void DelightUVCoord::CreateNSINodes(BaseDocument* doc, DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	std::string uv_handle = "*uvCoord";
	ctx.Create(uv_handle, "shader");
	const char* delightpath = DelightEnv::getDelightEnvironment();
	std::string shaderpath =
		delightpath + (std::string) "/osl" + (std::string) "/uvCoord.oso";
	ctx.SetAttribute(uv_handle,
					 NSI::StringArg("shaderfilename", shaderpath.c_str()));
}
