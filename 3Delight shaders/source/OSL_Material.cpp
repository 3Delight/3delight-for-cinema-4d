#include "c4d.h"
#include "c4d_symbols.h"
#include <map>
#include "OSL_shader_info.h"
#include "osl_utilities.h"
#include "NSIExportShader.h"
#include "TextureUI_Functions.h"
#include "customgui_texbox.h"
#include "mbase.h"
#include "customgui_matpreview.h"
#include "RegisterPrototypes.h"

#include <cfloat>
#include <cstring>

//Global variable for shader table
extern std::map<Int32, OSL_shader_info> OSL_shader_table;
#define LEAKED(x) (x)

/*
	Use constant IDs for existing materials (obtained from Maxon) on 3Delight installation
	so that when we add new materials by ourselves, they will not mess with IDs and cause
	problems with previously saved scenes.
*/
std::map<std::string, int> materials_id
{
	{"anisotropic",1057671},
	{"blinn",1057672},
	{"dlToon",1057673},
	{"delightEnvironment",1057674},
	{"dlGlass",1057675},
	{"dlHairAndFur",1057676},
	{"_3DelightMaterial",1057677},
	{"material3DelightMetal",1057678},
	{"matteObject",1057679},
	{"dlPrincipled",1057680},
	{"dlRandomMaterial",1057681},
	{"materialHairAndFur",1057682},
	{"dlCarPaint",1057683},
	{"dlSkin",1057684},
	{"surfaceShader",1057685},
	{"dlConstant",1057686},
	{"dlEnvironment",1057687},
	{"material3DelightLayered",1057688},
	{"dlLayeredMaterial",1057689},
	{"_3DelightLayered",1057690},
	{"dlMetal",1057691},
	{"dlShadowMatte",1057692},
	{"dlStandard",1057693},
	{"projection",1057694},
	{"dlSubstance",1057695},
	{"dlTerminal",1057696},
	{"dlThin",1057697},
	{"dlToonGlass",1057698},
	{"lambert",1057699},
	{"material3Delight",1057700},
	{"material3DelightGlass",1057701},
	{"material3DelightSkin",1057702},
	{"_3DelightGlass",1057703},
	{"_3DelightMetal",1057704},
	{"_3DelightSkin",1057705},
};

const int POPUP_ATTRIBUTE = 1000;
const int GROUP_ATTRIBUTE = 2000;
const int SHADER_ATTRIBUTE = 3000;
const int SHADER_TEMP_ATTRIBUTE = 4000;
const int STATIC_TEXT_ATTRIBUTE = 5000;

class OSL_Materials : public MaterialData
{
	INSTANCEOF(OSL_Materials, MaterialData)
public:
	OSL_Materials() {};
	OSL_Materials(String name) { m_shader_name = name; }
	virtual Bool Init(GeListNode* node);
	virtual Bool GetDDescription(GeListNode *node, Description *description, DESCFLAGS_DESC &flags);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
	static NodeData* Alloc(void) { return NewObjClear(OSL_Materials); }

private:
	String m_shader_name;
	//String m_shader_path;
};


static void DL_SetDefaultMatpreview(BaseMaterial* material)
{
	GeData d;

	if (material->GetParameter(MATERIAL_PREVIEW, d, DESCFLAGS_GET::NONE)) {
		MaterialPreviewData* mpd =
			(MaterialPreviewData*)d.GetCustomDataType(CUSTOMDATATYPE_MATPREVIEW);

		if (mpd) {
			mpd->SetPreviewType(MatPreviewUser);
			mpd->SetUserPreviewSceneName(String("DL_Sphere"));
			material->SetParameter(MATERIAL_PREVIEW, d, DESCFLAGS_SET::NONE);
		}
	}
}

Bool OSL_Materials::Init(GeListNode* node)
{
	DL_SetDefaultMatpreview((BaseMaterial*)node);
	Int32 ShaderID = node->GetNodeID();
	BaseContainer* dldata = ((BaseShader*)node)->GetDataInstance();
	OSL_shader_info info;
	std::string shader_path =
		OSL_shader_table[ShaderID].m_oso_file_path.GetCStringCopy();
	std::string output_name =
		OSL_shader_table[ShaderID].m_output_name;
	const char *first_page = "";
	const DlShaderInfo *shader_info = info.get_shader_info(shader_path.c_str());
	if (!shader_info)
	{
		return FALSE;
	}
	int param_idx = 10000;

	for (unsigned p = 0; p < shader_info->nparams(); p++)
	{
		const DlShaderInfo::Parameter* param = shader_info->getparam(p);
		if (param->isoutput)
			continue;

		/*
			Since we are showing Closures as Colors on the UI we exclude
			aovGroup from it since on OSL shader aovGroup is defined as a closure.
		*/
		if (param->isclosure && param->name == "aovGroup")
		{
			continue;
		}

		/*
			Don't display matrices in the UI for now. They can still be
			communicated through connections.
		*/
		if (param->type.elementtype == NSITypeMatrix ||
			param->type.elementtype == NSITypeDoubleMatrix)
		{
			continue;
		}

		const char* widget = "";
		osl_utilities::FindMetaData(widget, param->metadata, "widget");
		if (widget == osl_utilities::k_null)
		{
			continue;
		}

		const char* page_name = nullptr;
		osl_utilities::FindMetaData(page_name, param->metadata, "page");

		if (page_name == nullptr)
		{
			page_name = "Main";
		}

		osl_utilities::ParameterMetaData meta;
		meta.m_label = param->name.c_str();
		osl_utilities::GetParameterMetaData(meta, param->metadata);

		if (strcmp(first_page, page_name) != 0)
		{
			MAXON_SCOPE
			{
				param_idx++;
				param_idx++;
			}

			first_page = page_name;
		}
		bool check_shader = false;
		if (param->type.elementtype == NSITypeInteger)
		{
			MAXON_SCOPE
			{
				if (meta.m_widget && meta.m_widget == osl_utilities::k_check_box)
				{
					dldata->SetBool(param_idx, param->idefault[0]);
				}
				else if (meta.m_options)
				{
					dldata->SetInt32(param_idx, param->idefault[0]);
				}
				else
				{
					OSL_shader_table[ShaderID].m_ids_to_names[param_idx + SHADER_ATTRIBUTE] =
						std::make_pair(output_name.c_str(), param->name.c_str());
				}
			}
		}


		else if (param->type.elementtype == NSITypeFloat || param->type.elementtype == NSITypeDouble)
		{
			if (osl_utilities::ramp::IsRampWidget(meta.m_widget))
			{
				InitRamp(*dldata, param_idx);
			}
			else
			{
				dldata->SetFloat(param_idx, param->fdefault[0]);
				OSL_shader_table[ShaderID].m_ids_to_names[param_idx + SHADER_ATTRIBUTE] =
					std::make_pair(output_name.c_str(), param->name.c_str());
			}
		}
		else if (param->type.elementtype == NSITypeColor)
		{
			if (osl_utilities::ramp::IsRampWidget(meta.m_widget))
			{
				initGradient(*dldata, param_idx);
			}
			else
			{
				dldata->SetVector(param_idx, Vector(param->fdefault[0], param->fdefault[1], param->fdefault[2]));
				OSL_shader_table[ShaderID].m_ids_to_names[param_idx + SHADER_ATTRIBUTE] =
					std::make_pair(output_name.c_str(), param->name.c_str());
			}
		}

		else if (param->type.elementtype == NSITypePointer)
		{
			check_shader = true;
			OSL_shader_table[ShaderID].m_ids_to_names[param_idx++ + SHADER_ATTRIBUTE] =
				std::make_pair(output_name.c_str(), param->name.c_str());
		}

		else if (param->type.elementtype == NSITypePoint
			|| param->type.elementtype == NSITypeVector
			|| param->type.elementtype == NSITypeNormal)
		{
			dldata->SetVector(param_idx, HSVToRGB(Vector(param->fdefault[0], param->fdefault[2], param->fdefault[2])));
		}
		else if (param->type.elementtype == NSITypeString)
		{
		}
		else
		{
			continue;
		}
		if(!check_shader)
			OSL_shader_table[ShaderID].m_ids_to_names[param_idx++] = std::make_pair("", param->name.c_str());
	}
	return true;
}

Bool OSL_Materials::Message(GeListNode* node, Int32 type, void* data)
{
	if (!node) {
		return false;
	}

	BaseContainer* dldata = ((BaseMaterial*)node)->GetDataInstance();

	if (type == MSG_DESCRIPTION_POPUP)
	{
		DescriptionPopup* dp = (DescriptionPopup*)data;
		int clicked_button_id = dp->_descId[0].id;
		FillPopupMenu(dldata, dp, clicked_button_id + POPUP_ATTRIBUTE);
	}
	return true;
}

Bool OSL_Materials::GetDDescription(GeListNode *node, Description *description, DESCFLAGS_DESC &flags)
{
	//Here we dynamically create the GUI of the shader. The name/path of the oso-file is stored in the member variable "m_shader_name"
	//Again, the liboslquery library can be used to query the parameters of the shader, for which GUI widgets should be created. 
	//The metadata of the shader can also be read to determine the style of the widgets, following the same conventions as 3delightforMaya. 

	//Alternatively, liboslquery could be used once in the Init() function instead, to query what GUI elements should be created 
	//and store these in some intermediate representation. This would mean the oso files themselves are only loaded at startup, which could 
	//possibly be more stable/efficient. You may check how this is performed in the other DCC plugins (maya, katana,...)

	description->LoadDescription(node->GetNodeID());
	flags |= DESCFLAGS_DESC::LOADED;

	BaseContainer* dldata = ((BaseShader*)node)->GetDataInstance();
	BaseShader* shader_node = (BaseShader*)node;
	OSL_shader_info info;
	std::string shader_path = OSL_shader_table[node->GetNodeID()].m_oso_file_path.GetCStringCopy();
	const char *first_page = "";
	const DlShaderInfo *shader_info = info.get_shader_info(shader_path.c_str());
	if (!shader_info)
	{
		return FALSE;
	}

	int param_idx = 10000;
	const DescID parentGroup = DescLevel(ID_MATERIALPROPERTIES, DTYPE_GROUP, 0);
	const DescID mainGroup = DescLevel(10, DTYPE_GROUP, 0);

	MAXON_SCOPE
	{
	  BaseContainer settings = GetCustomDataTypeDefault(DTYPE_GROUP);
	  settings.SetString(DESC_NAME, shader_node->GetName() + " Material");
	  settings.SetInt32(DESC_DEFAULT, 1); // group is unfolded by default
	  description->SetParameter(parentGroup, settings, 0);
	  
	  settings = GetCustomDataTypeDefault(DTYPE_SEPARATOR);
	  settings.SetInt32(DESC_CUSTOMGUI, CUSTOMGUI_SEPARATOR);
	  settings.SetBool(DESC_SEPARATORLINE, true);
	  description->SetParameter(DescLevel(100, DTYPE_GROUP, 0), settings, parentGroup);
	}

	MAXON_SCOPE
	{
	  BaseContainer settings = GetCustomDataTypeDefault(DTYPE_GROUP);
	  settings.SetInt32(DESC_COLUMNS, 2); // group has two columns
	  description->SetParameter(mainGroup, settings, parentGroup);
	}


	for (unsigned p = 0; p < shader_info->nparams(); p++)
	{
		bool has_texture_connection = false;
		const DlShaderInfo::Parameter* param = shader_info->getparam(p);
		if (param->isoutput)
			continue;

		/*
			Since we are showing Closures as Colors on the UI we exclude
			aovGroup from it since on OSL shader aovGroup is defined as a closure.
		*/
		if (param->isclosure && param->name == "aovGroup")
		{
			continue;
		}

		/*
			Don't display matrices in the U
			
			I for now. They can still be
			communicated through connections.
		*/
		if (param->type.elementtype == NSITypeMatrix ||
			param->type.elementtype == NSITypeDoubleMatrix)
		{
			continue;
		}

		const char* widget = "";
		osl_utilities::FindMetaData(widget, param->metadata, "widget");
		if (widget == osl_utilities::k_null)
		{
			continue;
		}
		// FIXME : support variable length arrays
		if (param->type.arraylen < 0 &&
			!osl_utilities::ramp::IsRampWidget(widget))
		{
			continue;
		}

		const char* page_name = nullptr;
		osl_utilities::FindMetaData(page_name, param->metadata, "page");

		if (page_name == nullptr)
		{
			page_name = "Main";
		}

		osl_utilities::ParameterMetaData meta;
		meta.m_label = param->name.c_str();
		osl_utilities::GetParameterMetaData(meta, param->metadata);

		if (strcmp(first_page, page_name) != 0)
		{
			MAXON_SCOPE
			{
			  BaseContainer settings = GetCustomDataTypeDefault(DTYPE_SEPARATOR);
			  settings.SetString(DESC_NAME, (String)page_name);
			  settings.SetInt32(DESC_SCALEH, 1);
			  settings.SetInt32(DESC_CUSTOMGUI, CUSTOMGUI_SEPARATOR);
			  description->SetParameter(DescLevel(param_idx++, DTYPE_SEPARATOR, 0),
				  settings, mainGroup);

			  settings = GetCustomDataTypeDefault(DTYPE_STATICTEXT);
			  settings.SetString(DESC_NAME, ""_s);
			  description->SetParameter(DescID(DescLevel(param_idx++, DTYPE_STATICTEXT, 0)),
				  settings, mainGroup);
			}
			first_page = page_name;
		}

		if (param->type.elementtype == NSITypeInteger)
		{
			MAXON_SCOPE
			{
				if (meta.m_widget && meta.m_widget == osl_utilities::k_check_box)
				{
					BaseContainer settings = GetCustomDataTypeDefault(DTYPE_BOOL);
					settings.SetString(DESC_NAME, (String)meta.m_label);
					settings.SetInt32(DESC_SCALEH, 1);
					description->SetParameter(DescLevel(param_idx++, DTYPE_BOOL, 0), settings, mainGroup);
				}
				else if (meta.m_options)
				{
					BaseContainer settings = GetCustomDataTypeDefault(DTYPE_LONG);
					settings.SetString(DESC_NAME, (String)meta.m_label);
					settings.SetInt32(DESC_SCALEH, 1);
					settings.SetInt32(DESC_CUSTOMGUI, CUSTOMGUI_CYCLE);

					// add cycle elements
					BaseContainer items;
					std::string options = LEAKED(strdup(meta.m_options));

					formOptionsParameter(items, options);
					settings.SetContainer(DESC_CYCLE, items);
					settings.SetInt32(DESC_SCALEH, 1);
					description->SetParameter(DescLevel(param_idx++, DTYPE_LONG, 0), settings, mainGroup);
				}
				else
				{
					BaseContainer settings = GetCustomDataTypeDefault(DTYPE_LONG);
					settings.SetString(DESC_NAME, (String)meta.m_label);
					settings.SetInt32(DESC_SCALEH, 1);
					description->SetParameter(DescLevel(param_idx, DTYPE_LONG, 0), settings, mainGroup);
					has_texture_connection = true;
				}
			}
		}


		else if (param->type.elementtype == NSITypeFloat || param->type.elementtype == NSITypeDouble)
		{
			MAXON_SCOPE
			{
				if (osl_utilities::ramp::IsRampWidget(meta.m_widget))
				{
					using namespace osl_utilities;
					using namespace osl_utilities::ramp;
					const DlShaderInfo::Parameter* knots = nullptr;
					const DlShaderInfo::Parameter* interpolation = nullptr;
					const DlShaderInfo::Parameter* shared_interpolation = nullptr;
					std::string base_name;
					if (!FindMatchingRampParameters(
						*shader_info,
						*param,
						knots,
						interpolation,
						shared_interpolation,
						base_name))
					{
						continue;
					}

					BaseContainer bc = GetCustomDataTypeDefault(CUSTOMDATATYPE_SPLINE);
					bc.SetString(DESC_NAME, (String)meta.m_label);
					bc.SetInt32(DESC_SCALEH, 1);
					bc.SetBool(SPLINECONTROL_GRID_H, true);
					bc.SetBool(SPLINECONTROL_GRID_V, true);
					bc.SetBool(SPLINECONTROL_VALUE_EDIT_H, true);
					bc.SetBool(SPLINECONTROL_VALUE_EDIT_V, true);
					bc.SetBool(SPLINECONTROL_NO_FLOATING_WINDOW, true);
					bc.SetInt32(DESC_CUSTOMGUI, CUSTOMGUI_SPLINE);
					description->SetParameter(DescLevel(param_idx++, CUSTOMDATATYPE_SPLINE, 0), bc, mainGroup);
				}

				else
				{
					BaseContainer settings = GetCustomDataTypeDefault(DTYPE_REAL);
					settings.SetString(DESC_NAME, (String)meta.m_label);
					settings.SetInt32(DESC_SCALEH, 1);
					settings.SetInt32(DESC_CUSTOMGUI, CUSTOMGUI_REALSLIDER);
					settings.SetFloat(DESC_MIN, meta.m_fmin ? *meta.m_fmin : -FLT_MAX);
					settings.SetFloat(DESC_MAX, meta.m_fmax ? *meta.m_fmax : FLT_MAX);
					settings.SetFloat(DESC_MINSLIDER, meta.m_slider_fmin ? *meta.m_slider_fmin : 0);
					settings.SetFloat(DESC_MAXSLIDER, meta.m_slider_fmax ? *meta.m_slider_fmax : 10);
					settings.SetFloat(DESC_STEP, meta.m_slider_fmax ? *meta.m_slider_fmax/100.0 : 0.1);
					description->SetParameter(DescLevel(param_idx, DTYPE_REAL, 0), settings, mainGroup);

					has_texture_connection = true;
				}
			}
		}

		else if (param->type.elementtype == NSITypeColor)
		{
			MAXON_SCOPE
			{
				if (osl_utilities::ramp::IsRampWidget(meta.m_widget))
				{
					using namespace osl_utilities;
					using namespace osl_utilities::ramp;
					const DlShaderInfo::Parameter* knots = nullptr;
					const DlShaderInfo::Parameter* interpolation = nullptr;
					const DlShaderInfo::Parameter* shared_interpolation = nullptr;
					std::string base_name;
					if (!FindMatchingRampParameters(
						*shader_info,
						*param,
						knots,
						interpolation,
						shared_interpolation,
						base_name))
					{
						continue;
					}

					BaseContainer bc = GetCustomDataTypeDefault(CUSTOMDATATYPE_GRADIENT);
					bc.SetString(DESC_NAME, (String)meta.m_label);
					bc.SetInt32(DESC_SCALEH, 1);
					bc.SetInt32(DESC_CUSTOMGUI, CUSTOMGUI_GRADIENT);
					description->SetParameter(DescLevel(param_idx++, DTYPE_COLOR, 0), bc, mainGroup);
				}
				else
				{
					BaseContainer settings = GetCustomDataTypeDefault(DTYPE_COLOR);
					settings.SetString(DESC_NAME, (String)meta.m_label);
					settings.SetBool(DESC_SCALEH, TRUE);
					description->SetParameter(DescLevel(param_idx, DTYPE_COLOR, 0), settings, mainGroup);

					has_texture_connection = true;
				}
			}
		}

		else if (param->type.elementtype == NSITypePoint
			|| param->type.elementtype == NSITypeVector
			|| param->type.elementtype == NSITypeNormal)
		{
			MAXON_SCOPE
			{
				BaseContainer settings = GetCustomDataTypeDefault(DTYPE_VECTOR);
				settings.SetString(DESC_NAME, (String)meta.m_label);
				settings.SetInt32(DESC_SCALEH, 1);
				settings.SetBool(DESC_ANGULAR_XYZ, true);
				description->SetParameter(DescLevel(param_idx++, DTYPE_VECTOR, 0), settings, mainGroup);
			}
		}

		else if (param->type.elementtype == NSITypeString)
		{
			MAXON_SCOPE
			{
				if (meta.m_widget && meta.m_widget == osl_utilities::k_filename)
				{
					BaseContainer settings = GetCustomDataTypeDefault(DTYPE_FILENAME);
					settings.SetString(DESC_NAME, (String)meta.m_label);
					settings.SetInt32(DESC_SCALEH, 1);
					description->SetParameter(DescLevel(param_idx++, DTYPE_FILENAME, 0), settings, mainGroup);
				}
				else
				{

					BaseContainer settings = GetCustomDataTypeDefault(DTYPE_STRING);
					settings.SetString(DESC_NAME, (String)meta.m_label);
					settings.SetInt32(DESC_SCALEH, 1);
					description->SetParameter(DescLevel(param_idx++, DTYPE_STRING, 0), settings, mainGroup);
				}
			}
		}

		else if (param->type.elementtype == NSITypePointer)
		{
			MAXON_SCOPE
			{
				BaseContainer settings = GetCustomDataTypeDefault(DTYPE_BASELISTLINK);
				settings.SetString(DESC_NAME, (String)meta.m_label);
				settings.SetInt32(DESC_CUSTOMGUI, CUSTOMGUI_LINKBOX);

				BaseContainer accept;
				accept.SetInt32(Mbase, 1);
				settings.SetContainer(DESC_ACCEPT, accept);

				description->SetParameter(DescLevel(param_idx++ + SHADER_ATTRIBUTE, DTYPE_BASELISTLINK, 0), settings, mainGroup);
			}
		}

		else
		{
			continue;
		}

		if (has_texture_connection && 0 != std::strcmp(meta.m_label, ""))
		{
			MAXON_SCOPE
			{
				BaseContainer settings = GetCustomDataTypeDefault(DTYPE_BASELISTLINK);
				settings.SetString(DESC_NAME, (String)meta.m_label);
				settings.SetInt32(DESC_CUSTOMGUI, CUSTOMGUI_TEXBOX);  // use the tex box gui to handle shaders
				settings.SetBool(DESC_SHADERLINKFLAG, true);          // is a shader link
				settings.SetBool(DESC_HIDE, true);

				description->SetParameter(DescLevel(param_idx + SHADER_ATTRIBUTE, DTYPE_BASELISTLINK, 0), settings, mainGroup);

				settings = GetCustomDataTypeDefault(DTYPE_BASELISTLINK);
				settings.SetString(DESC_NAME, (String)meta.m_label);
				settings.SetInt32(DESC_CUSTOMGUI, CUSTOMGUI_TEXBOX);  // use the tex box gui to handle shaders
				settings.SetBool(DESC_SHADERLINKFLAG, true);          // is a shader link
				settings.SetBool(DESC_HIDE, true);

				description->SetParameter(DescLevel(param_idx + SHADER_TEMP_ATTRIBUTE, DTYPE_BASELISTLINK, 0), settings, mainGroup);

				settings = GetCustomDataTypeDefault(DTYPE_POPUP);
				settings.SetBool(DESC_ANIMATE, false);
				settings.SetBool(DESC_GROUPSCALEV, true);
				settings.SetInt32(DESC_CUSTOMGUI, CUSTOMGUI_POPUP);
				description->SetParameter(DescLevel(param_idx + POPUP_ATTRIBUTE, DTYPE_POPUP, 0), settings, mainGroup);

				HideAndShowTextures(param_idx + GROUP_ATTRIBUTE,
					param_idx,
					param_idx + SHADER_ATTRIBUTE,
					param_idx + SHADER_TEMP_ATTRIBUTE,
					node,
					description,
					dldata);

				param_idx++;
			}
		}
		else
		{
			MAXON_SCOPE
			{
				const DescID cid = DescID(DescLevel(param_idx + STATIC_TEXT_ATTRIBUTE, DTYPE_STATICTEXT, 0));
				BaseContainer settings = GetCustomDataTypeDefault(DTYPE_STATICTEXT);
				settings.SetString(DESC_NAME, ""_s);
				description->SetParameter(cid, settings, mainGroup);
			}
		}
	}
	return SUPER::GetDDescription(node, description, flags);
}

Bool RegisterOSL_materials(Int32 shader_ID)
{
	Filename res_dir = GeGetPluginResourcePath();
	OSL_shader_info info;
	const char* shaders_path = info.m_shaders_path.c_str();
	info.find_all_shaders(shaders_path);
	int i = 0;
	
	for (auto &mat : info.materials_map)
	{
		bool shader_uv_connection = false;
		const char* name = mat.first.c_str();
		const DlShaderInfo *shader_info = info.get_shader_info(mat.second.c_str());
		const DlShaderInfo::constvector<DlShaderInfo::Parameter> &tags =
			shader_info->metadata();
		std::string output_name;
		int id;

		if (materials_id[mat.first.c_str()])
		{
			id = materials_id[mat.first.c_str()];
		}
		else
		{
			id = shader_ID + i++;
		}

		osl_utilities::FindMetaData(name, tags, "niceName");
		std::string better;
		if (::islower(name[0]))
		{
			/*
				Many utility nodes have no nice name but their ID is just fine with
				an upper case. For example: leather, granite, etc.
			*/
			better = name;
			better[0] = std::string::value_type(toupper(better[0]));
			name = better.c_str();
		}

		for (unsigned p = 0; p < shader_info->nparams(); p++)
		{
			const DlShaderInfo::Parameter* param = shader_info->getparam(p);
			if (param->name == "uvCoord")
			{
				shader_uv_connection = true;
			}
			if (param->isoutput && output_name == "")
			{
				info.m_output_name = param->name.c_str();
			}
		}

		info.m_has_uv_connection = shader_uv_connection;
		info.m_oso_file_path = mat.second.c_str();
		info.m_is_surface = true;
		info.m_output_name = output_name;
		OSL_shader_table[id] = info;

		//Only register materials whose resource file exists.
		Filename file;
		Filename search_resource = (Filename)mat.first.c_str();
		search_resource.SetSuffix("res"_s);
		if (GeSearchFile(res_dir, search_resource, &file))
		{
			RegisterMaterialPlugin(id, (String)name, PLUGINFLAG_HIDE, OSL_Materials::Alloc, (String)mat.first.c_str(), 0);
		}
	}
	return true;
}
