#ifndef RegisterPrototypes_h
#define RegisterPrototypes_h

#include "ge_sys_math.h"

// 3Delight Materials
Bool RegisterDLAtmosphere(void);
Bool RegisterOSL_materials(Int32 shader_ID);
Bool RegisterOSL_materialObjects(Int32 shader_ID);

// 3Delight Shaders
Bool RegisterSkyTexture(void);
Bool RegisterEnvironmentLight(void);
Bool RegisterOpenVDB(void);
Bool RegisterAOVs(void);
Bool RegisterAovColor(void);
Bool RegisterAovGroup(void);
Bool RegisterOSL_shaders(Int32 shader_ID);

#endif
