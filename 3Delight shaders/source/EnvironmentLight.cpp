#include "IDs.h"
#include "TextureUI_Functions.h"
#include "c4d.h"
#include "oenvironmentlight.h"
#include "RegisterPrototypes.h"

class EnvironmentLight : public ObjectData
{
public:
	static NodeData* Alloc(void)
	{
		return NewObjClear(EnvironmentLight);
	}
	virtual Bool Init(GeListNode* node);
	virtual Bool GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
};

Bool EnvironmentLight::Init(GeListNode* node)
{
	BaseObject* op = (BaseObject*) node;
	BaseContainer* data = op->GetDataInstance();
	data->SetFloat(ENVIRONMENT_INTENSITY, 1.0);
	data->SetFloat(ENVIRONMENT_EXPOSURE, 0);
	data->SetVector(ENVIRONMENT_TEXTURE, HSVToRGB(Vector(0, 0, 0.5)));
	data->SetVector(ENVIRONMENT_TINT, Vector(1, 1, 1));
	data->SetInt32(ENVIRONMENT_MAPPING, ENVIRONMENT_MAPPING_SPHERICAL);
	data->SetBool(ENVIRONMENT_SEEN_BY_CAMERA, false);
	data->SetBool(ENVIRONMENT_PRELIT, false);
	data->SetBool(ENVIRONMENT_ILLUMINATES_BY_DEFAULT, true);
	return true;
}

Bool EnvironmentLight::GetDDescription(GeListNode* node,
									   Description* description,
									   DESCFLAGS_DESC& flags)
{
	description->LoadDescription(ID_ENVIRONMENTLIGHT);
	flags |= DESCFLAGS_DESC::LOADED;
	BaseObject* op = (BaseObject*) node;
	BaseContainer* data = op->GetDataInstance();
	HideAndShowTextures(ENVIRONMENT_INTENSITY_GROUP_PARAM,
						ENVIRONMENT_INTENSITY,
						ENVIRONMENT_INTENSITY_SHADER,
						ENVIRONMENT_INTENSITY_SHADER_TEMP,
						node,
						description,
						data);
	HideAndShowTextures(ENVIRONMENT_EXPOSURE_GROUP_PARAM,
						ENVIRONMENT_EXPOSURE,
						ENVIRONMENT_EXPOSURE_SHADER,
						ENVIRONMENT_EXPOSURE_SHADER_TEMP,
						node,
						description,
						data);
	HideAndShowTextures(ENVIRONMENT_TEXTURE_GROUP_PARAM,
						ENVIRONMENT_TEXTURE,
						ENVIRONMENT_TEXTURE_SHADER,
						ENVIRONMENT_TEXTURE_SHADER_TEMP,
						node,
						description,
						data);
	HideAndShowTextures(ENVIRONMENT_TINT_GROUP_PARAM,
						ENVIRONMENT_TINT,
						ENVIRONMENT_TINT_SHADER,
						ENVIRONMENT_TINT_SHADER_TEMP,
						node,
						description,
						data);
	return true;
}

Bool EnvironmentLight::Message(GeListNode* node, Int32 type, void* data)
{
	if (!node) {
		return false;
	}

	BaseContainer* dldata = ((BaseObject*) node)->GetDataInstance();

	if (type == MSG_DESCRIPTION_POPUP) {
		DescriptionPopup* dp = (DescriptionPopup*) data;
		int clicked_button_id = dp->_descId[0].id;

		switch (clicked_button_id) {
		case POPUP_ENVIRONMENT_INTENSITY:
			FillPopupMenu(dldata, dp, ENVIRONMENT_INTENSITY_GROUP_PARAM);
			break;

		case POPUP_ENVIRONMENT_EXPOSURE:
			FillPopupMenu(dldata, dp, ENVIRONMENT_EXPOSURE_GROUP_PARAM);
			break;

		case POPUP_ENVIRONMENT_TEXTURE:
			FillPopupMenu(dldata, dp, ENVIRONMENT_TEXTURE_GROUP_PARAM);
			break;

		case POPUP_ENVIRONMENT_TINT:
			FillPopupMenu(dldata, dp, ENVIRONMENT_TINT_GROUP_PARAM);
			break;

		default:
			break;
		}
	}

	return true;
}

Bool RegisterEnvironmentLight(void)
{
	return RegisterObjectPlugin(ID_ENVIRONMENTLIGHT,
								"Environment"_s,
								OBJECT_GENERATOR | PLUGINFLAG_HIDEPLUGINMENU,
								EnvironmentLight::Alloc,
								"Oenvironmentlight"_s,
								AutoBitmap("dlEnvironmentShape.png"_s),
								0);
}
