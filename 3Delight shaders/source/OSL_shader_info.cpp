#include "OSL_shader_info.h"
#include "DL_Utilities.h"
#include "DL_TypeConversions.h"
#include "delightenvironment.h"
#include <assert.h>

static bool scan_dir(const std::string& i_path,
	std::unordered_map<std::string, std::string>& io_list)
{
	AutoAlloc<BrowseFiles> bf;

	// Initialize the BrowseFiles instance.
	const Int32 bfFlags = BROWSEFILES_CALCSIZE;
	bf->Init((Filename)i_path.c_str(), bfFlags);

	// Note: GetNext() needs to be called at least once, to get the first element.
	while (bf->GetNext()) 
	{
		// Get the extension of files.
		const maxon::String suffix = maxon::String(bf->GetFilename().GetSuffix());
		if (suffix == "oso")
		{
			std::string fileName = bf->GetFilename().GetString().GetCStringCopy();
			std::size_t pos = fileName.find(".oso");
			io_list[fileName.substr(0, pos)] = (Filename(i_path.c_str()) + Filename(fileName.c_str())).GetString().GetCStringCopy();
		}
	}
	return true;
}

DlShaderInfo *OSL_shader_info::get_shader_info(const char *path) const
{
	NSI::DynamicAPI apis;
	decltype(&DlGetShaderInfo) m_shader_info_ptr = nullptr;
	apis.LoadFunction(m_shader_info_ptr, "DlGetShaderInfo");

	if (!m_shader_info_ptr)
	{
		assert(false);
		return nullptr;
	}

	return m_shader_info_ptr(path);
}

DlShaderInfo *OSL_shader_info::get_shader_info(BaseShader* i_shader) const
{
	std::string shader_path = 
		m_shaders_path + i_shader->GetName().GetCStringCopy() + ".oso";
	return get_shader_info(shader_path.c_str());
}

OSL_shader_info::OSL_shader_info()
{
	NSI::DynamicAPI apis;
	decltype(&DlGetShaderInfo) m_shader_info_ptr = nullptr;
	apis.LoadFunction(m_shader_info_ptr, "DlGetShaderInfo");
	assert(m_shader_info_ptr);

	const char* delightpath = DelightEnv::getDelightEnvironment();
	m_plugin_path_osl = (GeGetPluginPath().GetDirectory() + Filename("osl")).GetString().GetCStringCopy();
	m_shaders_path = (Filename(delightpath) + Filename("osl")).GetString().GetCStringCopy();
}

void OSL_shader_info::find_all_shaders(const char *i_installation_root)
{
	std::string plugin_osl = m_shaders_path;
	std::unordered_map<std::string, std::string> all_shaders;
	scan_dir(plugin_osl, all_shaders);
	scan_dir(m_plugin_path_osl, all_shaders);
	for (const auto &shader : all_shaders)
	{
		if (shader.second.find(".oso") == std::string::npos)
		{
			continue;
		}

		const DlShaderInfo *shader_info = get_shader_info(shader.second.c_str());
		if (!shader_info)
		{
			/* Not an .oso file */
			continue;
		}
		const DlShaderInfo::constvector<DlShaderInfo::Parameter> &tags =
			shader_info->metadata();
		if (shader.first == "dlSky")
		{
			shaders_map[shader.first] = shader.second;
			continue;
		}

		for (const auto &meta : tags)
		{
			if (meta.name != "tags" || meta.type.elementtype != NSITypeString)
				continue;
			for (const DlShaderInfo::conststring& tag : meta.sdefault)
			{
				if (tag == "texture/2d" || tag == "texture/3d" || tag == "utility")
				{
					shaders_map[shader.first] = shader.second;
				}
				else if (tag == "surface")
				{
					materials_map[shader.first] = shader.second;
				}
			}
			
		}
	}
}

