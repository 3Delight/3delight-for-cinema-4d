#include "DLAOVsColor.h"
#include "DL_Utilities.h"
#include "DL_TypeConversions.h"
#include "nsi.hpp"
#include "NSIExportMaterial.h"
#include "IDs.h"
#include "delightenvironment.h"
#include "dl_aov_group.h"
#include "OSL_shader_info.h"
#include "osl_utilities.h"

/*
	NSI_Export_Material Translater class is the class for exporting C4D
	standard materials to NSI materials. This class with it's functions will only
	be executed as many times as the number of materials created. So one time for
	each material. CreateNSINodes() function will create the NSI shader for the
	current material with a unique name each time there is a new material and
	connect it with the OSL shader containing all the materials attributes
	ConnectNSINodes() will check for the materials' textures if there is
	used any shader on that specific material or not. if there is we find which
	of the materials' parameter has used that shader and then use nsiConnect to
	connect that shader with it's material and parameter
*/

extern std::map<Int32, OSL_shader_info> OSL_shader_table;

// Convert Cinema4D Interpolation to the corresponding 3Delight one.
static int C4DToDelightInterpolation(int c4d_interp)
{
	if (c4d_interp == GRADIENT_INTERPOLATION_NONE) {
		return 0;

	} else if (c4d_interp == GRADIENT_INTERPOLATION_LINEARKNOT) {
		return 1;
	}

	return 2;
}

void NSI_Export_Material::CreateNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());

	BaseShader* shader = (BaseShader*)C4DNode;

	if (!shader) {
		return;
	}

	int shader_id = shader->GetNodeID();
	std::string m_material_handle = (std::string) Handle + std::string("shader");
	std::string m_material_attributes = (std::string) Handle;
	ctx.Create(m_material_handle, "shader");
	ctx.Create(m_material_attributes, "attributes");
	const char* delightpath = DelightEnv::getDelightEnvironment();
	c_shaderpath = delightpath + (std::string) "/cinema4d" + (std::string) "/osl" + (std::string) "/Main_Shader.oso";

	if (OSL_shader_table[shader_id].m_ids_to_names.size() > 0) {
		c_shaderpath = (OSL_shader_table[shader_id].m_oso_file_path.GetCStringCopy());
	}

	ctx.SetAttribute(
		m_material_handle,
		NSI::StringArg("shaderfilename", std::string(&c_shaderpath[0])));
	
}

void NSI_Export_Material::ConnectNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	std::string m_material_handle = (std::string) Handle + std::string("shader");
	std::string m_material_attributes = (std::string) Handle;
	BaseMaterial* material = (BaseMaterial*) C4DNode;

	if (!material) {
		return;
	}

	int shader_id = material->GetNodeID();

	ctx.Connect(m_material_handle, "", m_material_attributes, "surfaceshader");
	if (OSL_shader_table[shader_id].m_has_uv_connection)
	{
		ctx.Connect("*uvCoord", "o_outUV", m_material_handle, "uvCoord");
	}

	std::string shader_path = OSL_shader_table[shader_id].m_oso_file_path.GetCStringCopy();
	const DlShaderInfo *shader_info = OSL_shader_table[shader_id].get_shader_info(shader_path.c_str());
	GeData i_data;
	BaseContainer* material_container = material->GetDataInstance();
	for (auto val : OSL_shader_table[shader_id].m_ids_to_names)
	{
		material_container->GetParameter(DescID(val.first), i_data);
		std::string parameter_name = val.second.second;

		switch (i_data.GetType())
		{
		case DA_ALIASLINK:
		{
			BaseList2D* shader = i_data.GetLink(doc);

			if (!shader) {
				continue;
			}

			std::string osl_parameter_name = val.second.second;
			int source_id = shader->GetNodeID();
			std::string osl_source_attr = OSL_shader_table[source_id].m_output_name;

			//C4D shaders are not registered on our OSL_shader_table so we
			//manually write it's output color name.
			osl_source_attr = osl_source_attr == "" ? "outColor" : osl_source_attr;
			for (unsigned p = 0; p < shader_info->nparams(); p++)
			{
				osl_utilities::ParameterMetaData meta;
				const DlShaderInfo::Parameter* param = shader_info->getparam(p);

				meta.m_label = param->name.c_str();
				osl_utilities::GetParameterMetaData(meta, param->metadata);
				
				if (param->name == parameter_name)
				{
					if (param->type.elementtype == NSITypePointer)
					{
						std::string link_shader = parser->GetHandleName(shader) + "shader";
						ctx.Connect(link_shader,
							osl_source_attr,
							m_material_handle,
							osl_parameter_name);
					}
					else
					{
						std::string link_shader = parser->GetHandleName(shader);
						ctx.Connect(
							link_shader, osl_source_attr, m_material_handle, osl_parameter_name);
					}
				}
			}
			break;
		}

		default:
			break;
		}
	}
#ifdef VERBOSE
	ApplicationOutput("Material @ Parameter ID @, Shader @",
	m_material_handle.c_str(), id, link_shader.c_str());
#endif
}

void NSI_Export_Material::SampleAttributes(DL_SampleInfo* info,
		const char* Handle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	std::string m_material_handle = (std::string) Handle + std::string("shader");
	NSI::Context& ctx(parser->GetContext());
	BaseMaterial* material = (BaseMaterial*) C4DNode;
	BaseContainer* material_container = material->GetDataInstance();
	int material_id = material->GetNodeID();
	std::string material_path = OSL_shader_table[material_id].m_oso_file_path.GetCStringCopy();
	const DlShaderInfo *shader_info = OSL_shader_table[material_id].get_shader_info(material_path.c_str());
	NSI::ArgumentList args;
	GeData data;

	for (auto val : OSL_shader_table[material_id].m_ids_to_names)
	{
		std::string osl_parameter_name = val.second.second;
		material_container->GetParameter(DescID(val.first), data);
		std::string parameter_name = val.second.second;
		switch (data.GetType())
		{
		case DA_LONG: { // Integer data type
			Int32 value = data.GetInt32();
			args.Add(new NSI::IntegerArg(osl_parameter_name, value));
			break;
		}

		case DA_REAL: { // Float data type
			float float_value = (float)data.GetFloat();
			args.Add(new NSI::FloatArg(osl_parameter_name, float_value));
			break;
		}

		case DA_VECTOR: { // Vector data type
			Vector c4d_vector_value = toLinear(data.GetVector(), doc);
			const float vector_val[3] = { (float)c4d_vector_value.x,
										  (float)c4d_vector_value.y,
										  (float)c4d_vector_value.z
			};
			args.Add(new NSI::ColorArg(osl_parameter_name, &vector_val[0]));
			break;
		}

		case CUSTOMDATATYPE_GRADIENT: 
		{
			Gradient* gradient =
				(Gradient*)data.GetCustomDataType(CUSTOMDATATYPE_GRADIENT);
			int knot_count = gradient->GetKnotCount();
			std::vector<float> knots(knot_count);
			std::vector<float> colors(knot_count * 3);
			std::vector<int> interpolations(knot_count);

			for (int i = 0; i < knot_count; i++) {
				GradientKnot Knot = gradient->GetKnot(i);
				knots[i] = (float)Knot.pos;
				Vector KnotColor = toLinear((Knot.col), doc);
				colors[i * 3] = (float)KnotColor.x;
				colors[i * 3 + 1] = (float)KnotColor.y;
				colors[i * 3 + 2] = (float)KnotColor.z;
				interpolations[i] = C4DTo3DelightInterpolation(Knot.interpolation);
			}

			using namespace osl_utilities;
			using namespace osl_utilities::ramp;
			const DlShaderInfo::Parameter* knotss = nullptr;
			const DlShaderInfo::Parameter* interpolation = nullptr;
			const DlShaderInfo::Parameter* shared_interpolation = nullptr;
			std::string base_name;
			std::string pname;
			for (unsigned p = 0; p < shader_info->nparams(); p++)
			{
				osl_utilities::ParameterMetaData meta;
				const DlShaderInfo::Parameter* param = shader_info->getparam(p);

				meta.m_label = param->name.c_str();
				osl_utilities::GetParameterMetaData(meta, param->metadata);
				if (!osl_utilities::ramp::IsRampWidget(meta.m_widget))
				{
					continue;
				}

				if (param->name == parameter_name)
				{
					if (!FindMatchingRampParameters(
						*shader_info,
						*param,
						knotss,
						interpolation,
						shared_interpolation,
						base_name))
					{
						continue;
					}

					args.Add(NSI::Argument::New(knotss->name.c_str())
						->SetArrayType(NSITypeFloat, knot_count)
						->CopyValue((float*)&knots[0], knot_count * sizeof(float)));
					args.Add(
						NSI::Argument::New(param->name.c_str())
						->SetArrayType(NSITypeColor, knot_count)
						->CopyValue((float*)&colors[0], knot_count * sizeof(float) * 3));
					args.Add(
						NSI::Argument::New(interpolation->name.c_str())
						->SetArrayType(NSITypeInteger, knot_count)
						->CopyValue((int*)&interpolations[0], knot_count * sizeof(int)));
				}
			}
			break;
		}

		case CUSTOMDATATYPE_SPLINE:
		{
			SplineData* decay_ramp = (SplineData*)data.GetCustomDataType(CUSTOMDATATYPE_SPLINE);
			int decay_knot_count = decay_ramp->GetKnotCount();
			std::vector<float> decay_curve_knots(decay_knot_count);
			std::vector<float> decay_curve_floats(decay_knot_count);
			std::vector<int> decay_curve_interpolations(decay_knot_count);

			for (int i = 0; i < decay_knot_count; i++)
			{
				CustomSplineKnot* Knot = decay_ramp->GetKnot(i);
				decay_curve_knots[i] = (float)Knot->vPos.x;
				decay_curve_floats[i] = (float)Knot->vPos.y;
				decay_curve_interpolations[i] = Knot->interpol;
			}

			using namespace osl_utilities;
			using namespace osl_utilities::ramp;
			const DlShaderInfo::Parameter* knotss = nullptr;
			const DlShaderInfo::Parameter* interpolation = nullptr;
			const DlShaderInfo::Parameter* shared_interpolation = nullptr;
			std::string base_name;
			std::string pname;
			for (unsigned p = 0; p < shader_info->nparams(); p++)
			{
				osl_utilities::ParameterMetaData meta;
				const DlShaderInfo::Parameter* param = shader_info->getparam(p);

				meta.m_label = param->name.c_str();
				osl_utilities::GetParameterMetaData(meta, param->metadata);
				if (!osl_utilities::ramp::IsRampWidget(meta.m_widget))
				{
					continue;
				}

				if (param->name == parameter_name)
				{
					if (!FindMatchingRampParameters(
						*shader_info,
						*param,
						knotss,
						interpolation,
						shared_interpolation,
						base_name))
					{
						continue;
					}

					args.Add(NSI::Argument::New(knotss->name.c_str())
						->SetArrayType(NSITypeFloat, decay_knot_count)
						->CopyValue((float*)&decay_curve_knots[0], decay_knot_count * sizeof(float)));
					args.Add(
						NSI::Argument::New(param->name.c_str())
						->SetArrayType(NSITypeFloat, decay_knot_count)
						->CopyValue((float*)&decay_curve_floats[0], decay_knot_count * sizeof(float)));
					args.Add(
						NSI::Argument::New(interpolation->name.c_str())
						->SetArrayType(NSITypeInteger, decay_knot_count)
						->CopyValue((int*)&decay_curve_interpolations[0], decay_knot_count * sizeof(int)));
				}
			}
			break;
		}

		default:
			break;
		}
	}

	if (args.size() > 0) {
		ctx.SetAttribute(m_material_handle, args);
	}
}
