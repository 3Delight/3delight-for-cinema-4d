# 3Delight For Cinema 4D
A [3Delight](https://www.3delight.com/) rendering plugin for *MAXON
Cinema 4D*.

*3Delight for Cinema 4D* consists of four separate plugins:

* `3delight`
* `3delight_geometry`
* `3delight_shaders`
* `3delight_lights`

Each of these plugins comes with a `projectdefinition.txt` file that must be
used together with the *Cinema 4D Project Tool* to generate build solutions
for your desired platform.
## Building
Make sure you have a current and working [3Delight](https://www.3delight.com/download)
installation.
### macOS
1.  Make sure you have a current [XCode](https://apps.apple.com/us/app/xcode/id497799835)
    installation.
    *   If the *App Store* tells you that you need to upgrade *macOS* and
        you do not want to do that, grab an older Xcode installer that is
        compatible with your macOS version [from this website](https://xcodereleases.com/)

    The following build instructions are for *Cinema 4D R23*. They have
    also been tested to work with *R21* and *R22*.
    If you want to build against an older version of the *SDK* you need
    to download a different version of the *Cinema 4D Project Tool* from
    [here](https://developers.maxon.net/?page_id=1118) (step 3) and
    change the path in *step 7* and possibly *step 14*.

2.  Create a folder to work in and change into it:
    ```
    mkdir 3dlfc4d
    cd 3dlfc4d
    ```
3.  Download the *Cinema 4D Project Tool*:
    ```
    wget https://developers.maxon.net/?dl_id=136 -O project_tool_r23.zip
    ```
4.  Remove the *quarantine attribute* from the *Project Tool ZIP*:
    ```
    xattr -d "com.apple.quarantine" project_tool_r23.zip
    ```
5.  Unzip the *Project Tool* into the `project_tool` folder:
    ```
    unzip -d project_tool project_tool_r23.zip
    ```
6.  Make the *Project Tool* application executable:
    ```
    chmod 555 project_tool/kernel_app.app/Contents/MacOS/kernel_app
    ```
7.  Unzip the *SDK* from your *Cinema 4D* installation:
    ```
    unzip -d c4d_r23_sdk /Applications/Maxon\ Cinema\ 4D\ R23/sdk.zip
    ```
8.  Copy the `frameworks` folder from the *SDK* into the project folder.
    ```
    cp -R c4d_r23_sdk/frameworks .
    ```
    *If you are building for R22 or later you can also create a symbolic
    link to `frameworks` instead.*
9.  Clone the *3Delight For Cinema 4D* repository into the `plugins`
    folder:
    ```
    git clone https://gitlab.com/3Delight/3delight-for-cinema-4d plugins
    ```
10. Run the *Project Tool*:
    ```
    project_tool/kernel_app.app/Contents/MacOS/kernel_app g_updateproject=`pwd`
    ```
11. Build the plugins:
    ```
    xcodebuild -project plugins/project/plugins.xcodeproj -alltargets -configuration Release -UseModernBuildSystem=NO
    ```
    *  You may get an error along those lines:
        ```
        xcode-select: error: tool 'xcodebuild' requires Xcode, but active developer directory '/Library/Developer/CommandLineTools' is a command line tools instance
        ```
        In this case, you need to re-link your *Xcode* installation via `xcode-select`.
        This requires `root` privileges (will ask for your password):
        ```
        sudo xcode-select -s /Applications/Xcode.app/Contents/Developer
        ```
        After this re-run step *11*.
12. After the build finished plugins are found in the `build` folder.
    All plugins in `plugin` folder are symlinked therefore you can use `/plugins` folder for C4D.
    You can print the full path of this folder:
    ```
    echo `pwd`/plugins
    ```
13. Add this folder inside *Cinema 4D* under **Preferences ⟶ Plugins**.
14. Restart Cinema 4D to load the plugins.
    *   If you have trouble rendering/opening a *3Delight Display*, close
        *Cinema 4D* and open it again from a terminal:
        ```
        /Applications/Maxon\ Cinema\ 4D\ R23/Cinema\ 4D.app/Contents/MacOS/Cinema\ 4D&
        ```
        This is a bit cumbersome but it is the only workaround until we
        find out what the cause for [this issue](https://gitlab.com/3Delight/3delight-for-cinema-4d/-/issues/30)
        is.

### Windows

To build for windows you need to have the following items installed in your system.
*(Note, most of the below steps will need to be done once. Once you have a successful first build, all that needs in the future to get the latest build is explained in step number 4 "Updating the plugin")*
1. Cinema 4D
2. Cinema 4D SDK (The SDK is found in the C4D installation folder in an archive named sdk.zip)
3. Cinema 4D Project Tool [Download here](https://developers.maxon.net/?page_id=1118). The project tool builds a Visual Studio project file from a definition file.
4. [Visual Studio](https://visualstudio.microsoft.com/downloads/). **Note that Visual Studio 2019 is required to build against R23 and S24**
5. [Git for Windows](https://git-scm.com/download/win)

#### 1. Setup 

1. Unarchive the SDK into a folder and copy the **frameworks** and **plugins** folder to the root of the C4D installation folder. *(You can unpack the SDK archive directly into C4D to avoid copy and pate)*
2. Unzip the Cinema 4D Project Tool in any folder you want.
3. Look for "Git Bash" in your start menu and start it with Admin rights. navigate to your C4D installation plugin folder (e.g. C:\Program Files\Maxon Cinema 4D R24\plugins\) and clone the 3Delight For Cinema 4D repository into the C4D installations plugins folder:

```shell
git clone https://gitlab.com/3Delight/3delight-for-cinema-4d plugins
```

*The resulting clone of the repo will result in having a folder in your plugin folder named "3delight-for-cinema-4d plugins". Inside this folder, there is a folder named "project" and a file named "projectdefinition.txt. Before building the project you will need to modify the definition file to point to the correct path of the project*

4. Edit the "projectdefinition.txt" located in the project folder (e.g. C:\Program Files\Maxon Cinema 4D R24\plugins\3delight-for-cinema-4d\project\projectdefinition.txt)
5. There are 4 Entries in the definition file, all point to the folders in the root of the "3delight-for-cinema-4d plugins", so modify them accordingly:
```make
    plugins/3Delight;\          --> plugins/3delight-for-cinema-4d/3Delight;\
    plugins/3Delight Geometry;\ --> plugins/3delight-for-cinema-4d/3Delight Geometry;\
    plugins/3Delight lights;\   --> plugins/3delight-for-cinema-4d/3Delight lights;\
    plugins/3Delight shaders;   --> plugins/3delight-for-cinema-4d/3Delight shaders;
```
6. Save the new definition file

#### 2. Test
If this is your first time building a plugin for C4D it is advised to perform a test build from one of the projects that ships with C4D SDK. Follow these steps to make sure you have a working build environment.
1. Locate and open "Developer Command Prompt for VS 2019" from your start menu with **Admin Rights**. Admin rights is required since you will be creating files in a C:\Program Files\.. which requires admin privileges.
2. In the command prompt, navigate to where you downloaded and unpacked the C4D Project Tool.
3. Run the following command from the root of the C4D Project Tool.
```bash
start .\kernel_app_64bit.exe g_updateproject="C:\Program Files\Maxon Cinema 4D R24\plugins"
```
4. Once the build is successful, run the following command from within the project tool:
```bash
msbuild "c:\Program Files\Maxon Cinema 4D R24\plugins\cinema4dsdk\project\cinema4dsdk.vcxproj" /p:configuration=release /p:platform=x64
```
*The "cinema4dsdk.vcxproj" was created by the C4D Project Tool*
You should get a build with no errors if everything is configured properly. However, in case of getting some errors in red, check if any of the errors mention anything with a "denied" in the error description. If there is any access denied then it means you didn't launch the command prompt as Admin.
5. To test if the build is successful, open C4D R23 or S24 and check under the extensions for an Entry named "Cinema4dsdk". If you have that entry in the menu then you can start building the project.

#### 3. Building the plugin.
*Make sure that you've completed step 2.1 to 2.4 before attempting to make this build. If you already have completed 2.1 to 2.4 then you are good to go*
1. From the opened command prompt, run the following command:
```bash
msbuild "C:\Program Files\Maxon Cinema 4D R24\plugins\3delight-for-cinema-4d\project\3delight-for-cinema-4d.sln" /p:configuration=release /p:platform=x64
```
*(The C4D Project tool created a solution instead of a Visual Studio project because the actual projects are in the four folders sitting in the root of "3delight-for-cinema-4d" which you configured in the definition file. The solution is simply a housing for these project into a single one)*
2. The build tool will take some time and you should see a lot of output, mainly grey and yellow highlighted. If there aren't red lines or reported errors at the end of the build process then you should be good to go.
3. If the build finishes without any error, then open Cinema 4D and 3Delight plugin would be loaded for you now.

#### 4. Updating the plugin
Once the above is completed the first time, updating the plugin is a much simpler process later on.

1. Open Git Bash with admin rights
2. navigate to the location of 3delight repo (e.g. C:\Program Files\Maxon Cinema 4D R24\plugins\3delight-for-cinema-4d\) and run the following command:
```bash
git pull
```
3. Check if the definition files still point to the right folder as in step 1.5 from above.
4. Open "Developer Command Prompt for VS 2019" with admin rights and run the following command:

Run the Project Tool:
<location of project tool>\kernel_app_64bit.exe g_updateproject="plugins_directory"
```bash
msbuild "C:\Program Files\Maxon Cinema 4D R24\plugins\3delight-for-cinema-4d\project\3delight-for-cinema-4d.sln" /p:configuration=release /p:platform=x64
```

### *Note for Windows Users.*
The difference between the plugin installed from the official download of 3Delight and the build is the location of the C4D plugins folder. The official installation packs all the compiled files (without the source files) and deploys them to the folder where 3Delight is installed, usually C:\Program Files\3Delight. C4D then references those plugins through a system environment variable.

However, building from the source we are placing all the source files and compiled files into the C4D plugins folder. This is not required and you can place the repo in a separate folder/drive and copy the required plugins and folders into the C4D plugins folder after building. This does require that you are comfortable with building in Visual Studio and are able to troubleshoot any errors that might arise. For Artists requiring to get the latest build with as little configuration as possible, placing everything in the root of the C4D plugins folder will ensure a successful build from the first time.

Also note that if you want to build for both R23 and S24 and any version of the future, you will need to clone the repo for each C4D location you want the plugin to operate in and perform the above steps. Once you become seasoned and decide to move the repo into a single location, you can just copy the built plugins into respective C4D installations or simple point C4D preference to search for a single path where the plugins are built.
