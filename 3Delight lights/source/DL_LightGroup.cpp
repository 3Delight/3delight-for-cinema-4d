#include "IDs.h"
#include "c4d.h"
#include "olightgroup.h"
#include "RegisterPrototypes.h"

class DL_LightGroup : public ObjectData
{
public:
	static NodeData* Alloc(void)
	{
		return NewObjClear(DL_LightGroup);
	}
	virtual Bool Init(GeListNode* node);
};

Bool DL_LightGroup::Init(GeListNode* node)
{
	BaseObject* op = (BaseObject*) node;
	BaseContainer* data = op->GetDataInstance();
	return true;
}

bool RegisterLightGroup(void)
{
	return RegisterObjectPlugin(ID_LIGHTGROUP,
								"Light Group"_s,
								OBJECT_GENERATOR | PLUGINFLAG_HIDEPLUGINMENU,
								DL_LightGroup::Alloc,
								"Olightgroup"_s,
								AutoBitmap("shelf_dlSet_200.png"_s),
								0);
}
