#ifndef RegisterPrototypes_h
#define RegisterPrototypes_h

bool RegisterAreaLight(void);
bool RegisterDirectionalLight(void);
bool RegisterPointLight(void);
bool RegisterIncandescenceLight(void);
bool RegisterSpotLight(void);
bool RegisterCustomLightFilters(void);
bool RegisterGoboFilter(void);
bool RegisterDecayFilter(void);
bool RegisterLightGroup(void);
bool Register_LightShape(void);

#endif
