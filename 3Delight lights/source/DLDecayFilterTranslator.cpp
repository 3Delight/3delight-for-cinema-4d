#include "DLDecayFilterTranslator.h"
#include "delightenvironment.h"
#include "IDs.h"
#include "dldecayfilter.h"
#include "nsi.hpp"

void DlDecayFilterTranslator::CreateNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	BaseShader* shader = (BaseShader*) C4DNode;
	BaseContainer* data = shader->GetDataInstance();
	Bool is_filter_selected = data->GetBool(DECAY_IS_SELECTED);

	if (!is_filter_selected) {
		return;
	}

	std::string filter_transform = (std::string) ParentTransformHandle;
	std::string filter_shader = (std::string) Handle + "shader";
	ctx.Create(filter_transform, "transform");
	ctx.Create(filter_shader, "shader");
}

void DlDecayFilterTranslator::ConnectNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	return;
}

void DlDecayFilterTranslator::SampleAttributes(DL_SampleInfo* info,
		const char* Handle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	BaseList2D* obj = (BaseList2D*) C4DNode;
	BaseContainer* data = obj->GetDataInstance();
	Bool is_filter_selected = data->GetInt32(DECAY_IS_SELECTED);

	if (!is_filter_selected) {
		return;
	}

	std::string filter_shader = (std::string) Handle + "shader";
	const char* delightpath = DelightEnv::getDelightEnvironment();
	std::string c_shaderpath_decay =
		delightpath + (std::string) "/osl" + (std::string) "/dlDecayFilter.oso";
	int decay_type = data->GetInt32(DECAY_TYPE);
	float dacay_range_start = float(data->GetFloat(DECAY_RANGE_START));
	float dacay_range_end = float(data->GetFloat(DECAY_RANGE_END));
	GeData decay_ramp_data = data->GetData(DECAY_RAMP);
	SplineData* decay_ramp =
		(SplineData*) decay_ramp_data.GetCustomDataType(CUSTOMDATATYPE_SPLINE);
	int decay_knot_count = decay_ramp->GetKnotCount();
	std::vector<float> decay_curve_knots(decay_knot_count);
	std::vector<float> decay_curve_floats(decay_knot_count);
	std::vector<int> decay_curve_interpolations(decay_knot_count);

	for (int i = 0; i < decay_knot_count; i++) {
		CustomSplineKnot* Knot = decay_ramp->GetKnot(i);
		decay_curve_knots[i] = (float) Knot->vPos.x;
		decay_curve_floats[i] = (float) Knot->vPos.y;
		decay_curve_interpolations[i] = Knot->interpol;
	}

	NSI::ArgumentList args;
	args.Add(NSI::Argument::New("decayCurve_Knots")
			 ->SetArrayType(NSITypeFloat, decay_knot_count)
			 ->CopyValue((float*) &decay_curve_knots[0],
						 decay_knot_count * sizeof(float)));
	args.Add(NSI::Argument::New("decayCurve_Floats")
			 ->SetArrayType(NSITypeFloat, decay_knot_count)
			 ->CopyValue((float*) &decay_curve_knots[0],
						 decay_knot_count * sizeof(float)));
	args.Add(NSI::Argument::New("decayCurve_Interp")
			 ->SetArrayType(NSITypeInteger, decay_knot_count)
			 ->CopyValue((int*) &decay_curve_interpolations[0],
						 decay_knot_count * sizeof(int)));
	ctx.SetAttributeAtTime(filter_shader,
						   info->sample_time,
						   (NSI::StringArg("shaderfilename", c_shaderpath_decay),
							NSI::IntegerArg("decayType", decay_type),
							NSI::FloatArg("decayRangeStart", dacay_range_start),
							NSI::FloatArg("decayRangeEnd", dacay_range_end)));
	ctx.SetAttributeAtTime(filter_shader, info->sample_time, (args));
}
