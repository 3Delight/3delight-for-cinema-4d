#define _USE_MATH_DEFINES
#include <math.h>
#include "delightenvironment.h"
#include "AreaLightTranslator.h"
#include "DL_Light_Filters.h"
#include "DL_Utilities.h"
#include "IDs.h"
#include "LightFilterFunctions.h"
#include "customgui_inexclude.h"
#include "nsi.hpp"
#include "oarealight.h"

void AreaLightTranslator::CreateNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	// Handle names
	std::string handle = std::string(Handle);
	std::string m_cap1 = handle + "cap_front";
	std::string m_cap2 = handle + "cap_back";
	std::string shader_handle = handle + "shader";
	std::string transform_handle = std::string(ParentTransformHandle);
	BaseList2D* obj = (BaseList2D*) C4DNode;
	BaseContainer* data = obj->GetDataInstance();
	float halfwidth = 0.5f * (float) data->GetFloat(LIGHTCARD_WIDTH);
	float halfheight = 0.5f * (float) data->GetFloat(LIGHTCARD_HEIGHT);
	float zHeight = 1;

	if (data->GetInt32(LIGHTCARD_SHAPE) == LIGHTCARD_SHAPE_RECTANGLE) {
		ctx.Create(handle, "mesh");

	} else if (data->GetInt32(LIGHTCARD_SHAPE) == LIGHTCARD_SHAPE_DISC) {
		halfwidth = (float) data->GetFloat(LIGHTCARD_WIDTH);
		halfheight = (float) data->GetFloat(LIGHTCARD_HEIGHT);
		zHeight = (float) data->GetFloat(LIGHTCARD_RADIUS);
		ctx.Create(handle, "particles");

	} else if (data->GetInt32(LIGHTCARD_SHAPE) == LIGHTCARD_SHAPE_SPHERE) {
		halfwidth = (float) data->GetFloat(LIGHTCARD_RADIUS) * 2.0f;
		halfheight = (float) data->GetFloat(LIGHTCARD_RADIUS) * 2.0f;
		zHeight = (float) data->GetFloat(LIGHTCARD_RADIUS) * 2.0f;
		ctx.Create(handle, "particles");

	} else if (data->GetInt32(LIGHTCARD_SHAPE) == LIGHTCARD_SHAPE_CYLINDER) {
		halfwidth = (float) data->GetFloat(LIGHTCARD_RADIUS);
		halfheight = (float) data->GetFloat(LIGHTCARD_RADIUS);
		zHeight = 0.5f * (float) data->GetFloat(LIGHTCARD_HEIGHT);
		ctx.Create(handle, "mesh");
		ctx.Create(m_cap1, "particles");
		ctx.Create(m_cap2, "particles");
		std::string transform_cap = handle + "cap_front_transform";
		ctx.Create(transform_cap, "transform");
		std::string transform_cap2 = handle + "cap_back_transform";
		ctx.Create(transform_cap2, "transform");
		double cap11[16] = { halfwidth * 2.0, 0, 0, 0, halfheight * 2.0, 0, 0, 0,
							 zHeight * 2.0, 0, 0, 0, zHeight, 1
						   };
		double cap22[16] = { halfwidth * 2.0,
							 0,
							 0,
							 0,
							 0,
							 halfheight * 2.0,
							 0,
							 0,
							 0,
							 0,
							 zHeight * 2.0,
							 0,
							 0,
							 0,
							 -zHeight,
							 1
						   };
		NSI::Argument xform2("transformationmatrix");
		xform2.SetType(NSITypeDoubleMatrix);
		xform2.SetValuePointer((void*) &cap11[0]);
		ctx.SetAttribute(transform_cap, xform2);
		NSI::Argument xform3("transformationmatrix");
		xform3.SetType(NSITypeDoubleMatrix);
		xform3.SetValuePointer((void*) &cap22[0]);
		ctx.SetAttribute(transform_cap2, xform3);
		ctx.Connect(transform_cap, "", transform_handle, "objects");
		ctx.Connect(m_cap1, "", transform_cap, "objects");
		ctx.Connect(transform_cap2, "", transform_handle, "objects");
		ctx.Connect(m_cap2, "", transform_cap2, "objects");
	}

	std::string scale_transform = handle + "scale_transform";
	ctx.Create(scale_transform, "transform");
	double scale[16] { halfwidth, 0, 0, 0, 0, -halfheight, 0, 0,
					   0, 0, -zHeight, 0, 0, 0, 0, 1
					 };
	NSI::Argument xform("transformationmatrix");
	xform.SetType(NSITypeDoubleMatrix);
	xform.SetValuePointer((void*) &scale[0]);
	ctx.SetAttribute(scale_transform, (xform));
	Bool seen_by_camera = data->GetBool(LIGHTCARD_SEEN_BY_CAMERA);
	int camera_visibility = 0;

	if (seen_by_camera) {
		camera_visibility = 1;
	}

	// Create an attributes node, and connect it to the mesh
	std::string attributes_handle = handle + "attributes";
	ctx.Create(attributes_handle, "attributes");
	ctx.SetAttribute(attributes_handle,
					 (NSI::IntegerArg("visibility.camera", camera_visibility)));

	if (data->GetInt32(LIGHTCARD_SHAPE) == LIGHTCARD_SHAPE_CYLINDER) {
		ctx.Connect(attributes_handle, "", m_cap1, "geometryattributes");
		ctx.Connect(attributes_handle, "", m_cap2, "geometryattributes");
	}

	ctx.Create(shader_handle, "shader");
	int nvertices = 4;
	std::vector<int> indices(4);
	indices[0] = 0;
	indices[1] = 1;
	indices[2] = 2;
	indices[3] = 3;
	NSI::Argument arg_nvertices("nvertices");
	arg_nvertices.SetType(NSITypeInteger);
	arg_nvertices.SetCount(1);
	arg_nvertices.SetValuePointer((void*) &nvertices);
	NSI::Argument arg_indices("P.indices");
	arg_indices.SetType(NSITypeInteger);
	arg_indices.SetCount(indices.size());
	arg_indices.SetValuePointer((void*) &indices[0]);
	ctx.SetAttribute(handle, (arg_nvertices, arg_indices));
}

void AreaLightTranslator::ConnectNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	std::string handle = std::string(Handle);
	std::string scale_transform = handle + "scale_transform";
	std::string transform_handle = std::string(ParentTransformHandle);
	std::string attributes_handle = handle + "attributes";
	std::string shader_handle = handle + "shader";
	ctx.Connect(shader_handle, "", attributes_handle, "surfaceshader");
}

void AreaLightTranslator::SampleAttributes(DL_SampleInfo* info,
		const char* Handle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	BaseList2D* obj = (BaseList2D*) C4DNode;
	BaseContainer* data = obj->GetDataInstance();
	NSI::Context& ctx(parser->GetContext());
	// Handle names
	std::string handle = std::string(Handle);
	std::string scale_transform = handle + "scale_transform";
	std::string transform_handle = "X_" + handle;
	std::string attributes_handle = handle + "attributes";
	std::string m_cap1 = handle + "cap_front";
	std::string m_cap2 = handle + "cap_back";
	std::string shader_handle = handle + "shader";
	InExcludeData* geometry_list = NULL;
	geometry_list = (InExcludeData*) data->GetCustomDataType(
						AREA_LIGHT_GEOMETRY, CUSTOMDATATYPE_INEXCLUDE_LIST);
	BaseList2D* geometry_object = NULL;

	if (geometry_list) {
		Int32 nlights = geometry_list->GetObjectCount();

		for (Int32 i = 0; i < nlights; i++) {
			geometry_object = NULL;
			geometry_object = geometry_list->ObjectFromIndex(doc, i);

			if (geometry_object) {
				BaseObject* object = (BaseObject*) geometry_object;

				if (!object) {
					continue;
				}

				ctx.Connect(
					attributes_handle, "", transform_handle, "geometryattributes");
				std::string object_handle = parser->GetHandleName((BaseList2D*) object);
				std::string object_transform =
					std::string("X_") + std::string(object_handle);
				ctx.Disconnect(object_transform, "", ".all", "objects");
				ctx.Connect(object_transform, "", transform_handle, "objects");
			}
		}

	} else if (!geometry_list || geometry_list->GetObjectCount() == 0) {
		ctx.Connect(scale_transform, "", transform_handle, "objects");
		ctx.Connect(handle, "", scale_transform, "objects");
		ctx.Connect(attributes_handle, "", scale_transform, "geometryattributes");
	}

	if (data->GetInt32(LIGHTCARD_SHAPE) == LIGHTCARD_SHAPE_RECTANGLE) {
		std::vector<float> P(4 * 3); // One quad
		P[0] = -1;
		P[1] = -1;
		P[2] = 0;
		P[3] = 1;
		P[4] = -1;
		P[5] = 0;
		P[6] = 1;
		P[7] = 1;
		P[8] = 0;
		P[9] = -1;
		P[10] = 1;
		P[11] = 0;
		NSI::Argument arg_P("P");
		arg_P.SetType(NSITypePoint);
		arg_P.SetCount(4);
		arg_P.SetValuePointer((void*) &P[0]);
		// arg_P.SetFlags(NSIParamIndirect);
		ctx.SetAttribute(handle, (arg_P));

	} else if (data->GetInt32(LIGHTCARD_SHAPE) == LIGHTCARD_SHAPE_DISC || data->GetInt32(LIGHTCARD_SHAPE) == LIGHTCARD_SHAPE_SPHERE) {
		float P[3];
		P[0] = 0;
		P[1] = 0;
		P[2] = 0;
		NSI::Argument arg_P("P");
		arg_P.SetType(NSITypePoint);
		arg_P.SetCount(1);
		arg_P.SetValuePointer((void*) &P[0]);
		ctx.SetAttribute(handle, (arg_P, NSI::FloatArg("width", 1)));

		if (data->GetInt32(LIGHTCARD_SHAPE) == LIGHTCARD_SHAPE_DISC) {
			float N[3];
			N[0] = 0;
			N[1] = 0;
			N[2] = -1;
			NSI::Argument arg_N("N");
			arg_N.SetType(NSITypeNormal);
			arg_N.SetCount(1);
			arg_N.SetValuePointer((void*) &N[0]);
			ctx.SetAttribute(handle, arg_N);
		}

	} else if (data->GetInt32(LIGHTCARD_SHAPE) == LIGHTCARD_SHAPE_CYLINDER) {
		std::vector<float> P;
		std::vector<int> indices, nvertices;
		const int kNumSteps = 30;

		for (int i = 0; i < kNumSteps; ++i) {
			float angle = (float(i) / float(kNumSteps)) * float(2.0 * M_PI);
			float z = 1.0f * cos(angle);
			float y = 1.0f * sin(angle);
			P.push_back(y);
			P.push_back(z);
			P.push_back(1.0f);
			P.push_back(y);
			P.push_back(z);
			P.push_back(-1.0f);
			nvertices.push_back(4);
			indices.push_back(i * 2);
			indices.push_back(i * 2 + 1);
			indices.push_back((i * 2 + 3) % (2 * kNumSteps));
			indices.push_back((i * 2 + 2) % (2 * kNumSteps));
		}

		NSI::ArgumentList args;
		args.push(NSI::Argument::New("nvertices")
				  ->SetType(NSITypeInteger)
				  ->SetCount(nvertices.size())
				  ->SetValuePointer(&nvertices[0]));
		args.push(NSI::Argument::New("P")
				  ->SetType(NSITypePoint)
				  ->SetCount(2 * kNumSteps)
				  ->SetValuePointer(&P[0]));
		args.push(NSI::Argument::New("P.indices")
				  ->SetType(NSITypeInteger)
				  ->SetCount(4 * kNumSteps)
				  ->SetValuePointer(&indices[0]));
		ctx.SetAttribute(handle, (args));
		float Point[3];
		Point[0] = 0;
		Point[1] = 0;
		Point[2] = 0;
		float N[3];
		N[0] = 0;
		N[1] = 0;
		N[2] = -1;
		NSI::Argument arg_P("P");
		arg_P.SetType(NSITypePoint);
		arg_P.SetCount(1);
		arg_P.SetValuePointer((void*) &Point[0]);
		NSI::Argument arg_N("N");
		arg_N.SetType(NSITypeNormal);
		arg_N.SetCount(1);
		arg_N.SetValuePointer((void*) &N[0]);
		ctx.SetAttribute(m_cap1, (arg_P, NSI::FloatArg("width", 1), arg_N));
		ctx.SetAttribute(m_cap2, (arg_P, NSI::FloatArg("width", 1), arg_N));
	}

	float intensity = (float) data->GetFloat(LIGHTCARD_INTENSITY);
	float exposure = (float) data->GetFloat(LIGHTCARD_EXPOSURE);
	float spread = (float) data->GetFloat(LIGHTCARD_SPREAD);
	int decayRate = data->GetInt32(LIGHTCARD_DECAY);
	int towSided = data->GetInt32(LIGHTCARD_TWO_SIDED);

	if (data->GetInt32(LIGHTCARD_SHAPE) == LIGHTCARD_SHAPE_SPHERE || data->GetInt32(LIGHTCARD_SHAPE) == LIGHTCARD_SHAPE_CYLINDER) {
		towSided = 1;
	}

	int normalize = data->GetInt32(LIGHTCARD_NORMALIZE);

	if (normalize) {
		intensity = intensity * 100 * 100; // Normalize to meters, not centimeters
	}

	float diffuse_contribution = (float) data->GetFloat(LIGHTCARD_DIFFUSE);
	float specular_contribution = (float) data->GetFloat(LIGHTCARD_SPECULAR);
	float hair_contribution = (float) data->GetFloat(LIGHTCARD_HAIR);
	float volume_contribution = (float) data->GetFloat(LIGHTCARD_VOLUME);
	Vector color = toLinear(data->GetVector(LIGHTCARD_COLOR), doc);
	float col[3];
	col[0] = (float) color.x;
	col[1] = (float) color.y;
	col[2] = (float) color.z;
	BaseList2D* color_shader = data->GetLink(LIGHTCARD_COLOR_SHADER, doc);
	BaseList2D* intensity_shader = data->GetLink(LIGHTCARD_INTENSITY_SHADER, doc);

	std::string shaderpath = (GeGetPluginPath().GetDirectory() + Filename("osl")
							+ Filename("areaLight.oso")).GetString().GetCStringCopy();

	NSI::ArgumentList args;
	args.Add(new NSI::StringArg("shaderfilename", &shaderpath[0]));

	if (color_shader) {
		std::string link_shader = parser->GetHandleName(color_shader);
		ctx.Connect(link_shader, "outColor", shader_handle, "i_color");

	} else {
		args.Add(new NSI::ColorArg("i_color", &col[0]));
	}

	if (intensity_shader) {
		std::string link_shader = parser->GetHandleName(intensity_shader);
		ctx.Connect(link_shader, "outColor[0]", shader_handle, "intensity");

	} else {
		args.Add(new NSI::FloatArg("intensity", intensity));
	}

	args.Add(new NSI::FloatArg("exposure", exposure));
	args.Add(new NSI::FloatArg("spread", spread));
	args.Add(new NSI::IntegerArg("decayRate", decayRate));
	args.Add(new NSI::IntegerArg("twosided", towSided));
	args.Add(new NSI::FloatArg("diffuse_contribution", diffuse_contribution));
	args.Add(new NSI::FloatArg("specular_contribution", specular_contribution));
	args.Add(new NSI::FloatArg("hair_contribution", hair_contribution));
	args.Add(new NSI::FloatArg("volume_contribution", volume_contribution));
	args.Add(new NSI::IntegerArg("normalize_area", normalize));
	ctx.SetAttributeAtTime(shader_handle, info->sample_time, args);
	const CustomDataType* light_filters = data->GetCustomDataType(
			DL_CUSTOM_LIGHT_FILTERS, ID_CUSTOMDATATYPE_LIGHTFILTERS);
	iCustomDataTypeFilters* light_filter_data =
		(iCustomDataTypeFilters*) (light_filters);

	if (!light_filter_data) {
		light_filter_data = NewObjClear(iCustomDataTypeFilters);
	}

	std::vector<Int32> selected_light_filters_guid;

	for (int i = 0;
			i < light_filter_data->m_selected_light_filters_itemID.GetCount();
			i++)
		selected_light_filters_guid.push_back(
			light_filter_data->m_selected_light_filters_itemID[i]);

	// Getting selected filters
	std::vector<BaseShader*> selected_filters =
		getSelectedFilters((BaseObject*) obj,
						   selected_light_filters_guid,
						   light_filter_data->m_all_light_filters.GetCount());

	for( BaseShader *filter : selected_filters )
	{
		std::string filter_transform =
			"X_" + parser->GetHandleName(filter);
		std::string filter_shader =
			parser->GetHandleName(filter) + "shader";
		ctx.Connect(filter_shader, "filter_output", shader_handle, "filter");
	}
}
