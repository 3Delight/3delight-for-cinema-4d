#include "DirectionalLightTranslator.h"
#include "delightenvironment.h"
#include "DL_TypeConversions.h"
#include "DL_Utilities.h"
#include "nsi.hpp"
#include "odirectionallight.h"

void DirectionalLightTranslator::CreateNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	std::string m_transform_handle = std::string(ParentTransformHandle);
	BaseList2D* obj = (BaseList2D*) C4DNode;
	GeData data;
	std::string light_handle = parser->GetHandleName(obj);
	std::string m_handle = std::string(Handle);
	ctx.Create(m_handle, "environment");
	std::string m_shader_handle = m_handle + "shader";
	ctx.Create(m_shader_handle, "shader");
	std::string attributes_handle = m_handle + "attributes";
	ctx.Create(attributes_handle, "attributes");
	std::string transform_handle2 = m_handle + "env_transform";
	ctx.Create(transform_handle2, "transform");
}

void DirectionalLightTranslator::ConnectNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	std::string handle = std::string(Handle);
	std::string shader_handle = handle + "shader";
	std::string attributes_handle = handle + "attributes";
	std::string transform_handle2 = handle + "env_transform";
	ctx.Connect(attributes_handle, "", handle, "geometryattributes");
	ctx.Connect(shader_handle, "", attributes_handle, "surfaceshader");
	/*
	    In 3Delight the light comes from Z+ towards the Z- direction
	    whereas in Cinema4D the light comes from Z- towards the Z+
	    direction.  Thus we create this tranformation matrix to change the
	    direction of the light and then connect (multiply) it with the
	    transformation matrix of the object.
	    */
	double change_direction[16] {
		1, 0, 0, 0, 0, -1, 0, 0, 0, 0, -1, 0, 0, 0, 0, 1
	};
	std::string m_transform_handle = std::string(ParentTransformHandle);
	NSI::Argument xform("transformationmatrix");
	xform.SetType(NSITypeDoubleMatrix);
	xform.SetValuePointer((void*) &change_direction[0]);
	ctx.SetAttribute(transform_handle2, xform);
	ctx.Connect(transform_handle2, "", m_transform_handle, "objects");
	ctx.Connect(handle, "", transform_handle2, "objects");
}

void DirectionalLightTranslator::SampleAttributes(DL_SampleInfo* info,
		const char* Handle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	std::string shader_handle = std::string(Handle) + std::string("shader");
	std::string handle = std::string(Handle);
	NSI::Context& ctx(parser->GetContext());
	BaseList2D* obj = (BaseList2D*) C4DNode;
	BaseContainer* data = obj->GetDataInstance();
	const char* delightpath = DelightEnv::getDelightEnvironment();
	std::string shaderpath =
		delightpath + (std::string) "/osl" + (std::string) "/directionalLight.oso";
	float intensity = (float) data->GetFloat(DIRECTIONAL_LIGHT_INTENSITY);
	float exposure = (float) data->GetFloat(DIRECTIONAL_LIGHT_EXPOSURE);
	float angular_diameter =
		(float) data->GetFloat(DIRECTIONAL_LIGHT_ANGULAR_DIAMETER);
	float diffuse_contribution = (float) data->GetFloat(DIRECTIONAL_LIGHT_DIFFUSE);
	float specular_contribution =
		(float) data->GetFloat(DIRECTIONAL_LIGHT_SPECULAR);
	float hair_contribution = (float) data->GetFloat(DIRECTIONAL_LIGHT_HAIR);
	float volume_contribution = (float) data->GetFloat(DIRECTIONAL_LIGHT_VOLUME);
	Vector color = toLinear(data->GetVector(DIRECTIONAL_LIGHT_COLOR), doc);
	float col[3];
	col[0] = (float) color.x;
	col[1] = (float) color.y;
	col[2] = (float) color.z;
	NSI::ArgumentList args;
	args.Add(new NSI::StringArg("shaderfilename", &shaderpath[0]));
	BaseList2D* color_shader = data->GetLink(DIRECTIONAL_LIGHT_COLOR_SHADER, doc);
	BaseList2D* intensity_shader =
		data->GetLink(DIRECTIONAL_LIGHT_INTENSITY_SHADER, doc);

	if (color_shader) {
		std::string link_shader = parser->GetHandleName(color_shader);
		ctx.Connect(link_shader, "outColor", shader_handle, "i_color");

	} else {
		args.Add(new NSI::ColorArg("i_color", &col[0]));
	}

	if (intensity_shader) {
		std::string link_shader = parser->GetHandleName(intensity_shader);
		ctx.Connect(link_shader, "outColor[0]", shader_handle, "intensity");

	} else {
		args.Add(new NSI::FloatArg("intensity", intensity));
	}

	args.Add(new NSI::FloatArg("exposure", exposure));
	args.Add(new NSI::FloatArg("diffuse_contribution", diffuse_contribution));
	args.Add(new NSI::FloatArg("specular_contribution", specular_contribution));
	args.Add(new NSI::FloatArg("hair_contribution", hair_contribution));
	args.Add(new NSI::FloatArg("volume_contribution", volume_contribution));
	ctx.SetAttributeAtTime(shader_handle, info->sample_time, args);
	ctx.SetAttribute(handle, (NSI::DoubleArg("angle", angular_diameter)));
}
