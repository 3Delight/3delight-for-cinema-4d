#include <DL_Utilities.h>
#include "IDs.h"
#include "c4d.h"
#include "oarealight.h"
#include "RegisterPrototypes.h"

class DL_AreaLight_command : public CommandData
{
public:
	virtual Bool Execute(BaseDocument* doc, GeDialog* parentManager);
	int shape_created = LIGHTCARD_SHAPE_RECTANGLE;
};

Bool DL_AreaLight_command::Execute(BaseDocument* doc, GeDialog* parentManager)
{
	BaseObject* const areaLight = BaseObject::Alloc(ID_AREALIGHT);

	if (areaLight == nullptr) {
		return false;
	}

	// Changing Area Light shape based on the selection.
	BaseContainer* data = areaLight->GetDataInstance();
	data->SetInt32(LIGHTCARD_SHAPE, shape_created);
	doc->InsertObject(areaLight, nullptr, nullptr, true);
	doc->SetActiveObject(areaLight, SELECTION_NEW);
	EventAdd();
	return true;
}

Bool Register_LightShape(void)
{
	/*
	            Using CommandData instead of Object Data to register four
	   different shapes of AreaLight. This because for the ObjectData we can not
	   have more then one object with the same description name used whereas for
	   a Command plugin there is no description resource needed. This is the
	   reason why we are using a CommandData to create the same object from
	   different commands.
	    */
	DL_AreaLight_command* square = NewObjClear(DL_AreaLight_command);
	DL_AreaLight_command* disc = NewObjClear(DL_AreaLight_command);
	DL_AreaLight_command* sphere = NewObjClear(DL_AreaLight_command);
	DL_AreaLight_command* cylinder = NewObjClear(DL_AreaLight_command);

	if (RegisterCommandPlugin(ID_AREALIGHT_SQUARE,
							  "Square"_s,
							  PLUGINFLAG_HIDEPLUGINMENU,
							  0,
							  String(""),
							  square)) {
		square->shape_created = LIGHTCARD_SHAPE_RECTANGLE;
	}

	if (RegisterCommandPlugin(ID_AREALIGHT_DISC,
							  "Disc"_s,
							  PLUGINFLAG_HIDEPLUGINMENU,
							  0,
							  String(""),
							  disc)) {
		disc->shape_created = LIGHTCARD_SHAPE_DISC;
	}

	if (RegisterCommandPlugin(ID_AREALIGHT_SPHERE,
							  "Sphere"_s,
							  PLUGINFLAG_HIDEPLUGINMENU,
							  0,
							  String(""),
							  sphere)) {
		sphere->shape_created = LIGHTCARD_SHAPE_SPHERE;
	}

	if (RegisterCommandPlugin(ID_AREALIGHT_CYLINDER,
							  "Cylinder"_s,
							  PLUGINFLAG_HIDEPLUGINMENU,
							  0,
							  String(""),
							  cylinder)) {
		cylinder->shape_created = LIGHTCARD_SHAPE_CYLINDER;
	}

	return true;
}
