#include "IDs.h"
#include "c4d.h"
#include "c4d_symbols.h"
#include "dldecayfilter.h"
#include "RegisterPrototypes.h"

static void
InitRamp(BaseContainer& bc, Int32 id)
{
	GeData data(CUSTOMDATATYPE_SPLINE, DEFAULTVALUE);
	SplineData* spline =
		(SplineData*) data.GetCustomDataType(CUSTOMDATATYPE_SPLINE);

	if (spline) {
		spline->MakeLinearSplineLinear(2); // Make Linear Spline
		Int32 count = spline->GetKnotCount();

		if (count == 2) {
			CustomSplineKnot* knot0 = spline->GetKnot(0);

			if (knot0) {
				knot0->vPos = Vector(0, 0, 0);
			}

			CustomSplineKnot* knot1 = spline->GetKnot(1);

			if (knot1) {
				knot1->vPos = Vector(1, 1, 0);
			}
		}
	}

	bc.SetData(id, data);
}

class DLDecayFilter : public ShaderData
{
public:
	virtual Bool Init(GeListNode* node);
	virtual Bool GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
	virtual Vector Output(BaseShader* sh, ChannelData* sd);
	static NodeData* Alloc(void)
	{
		return NewObjClear(DLDecayFilter);
	}
};

Bool DLDecayFilter::Init(GeListNode* node)
{
	BaseContainer* data = ((BaseShader*) node)->GetDataInstance();
	data->SetInt32(DECAY_TYPE, DECAY_TYPE_DISTANCE_LIGHT);
	data->SetFloat(DECAY_RANGE_START, 0.0);
	data->SetFloat(DECAY_RANGE_END, 10.0);
	InitRamp(*data, DECAY_RAMP);
	return true;
}

Bool DLDecayFilter::GetDDescription(GeListNode* node,
									Description* description,
									DESCFLAGS_DESC& flags)
{
	description->LoadDescription(node->GetType());
	flags |= DESCFLAGS_DESC::LOADED;
	return TRUE;
}

Bool DLDecayFilter::Message(GeListNode* node, Int32 type, void* data)
{
	if (!node) {
		return false;
	}

	return true;
}

Vector
DLDecayFilter::Output(BaseShader* sh, ChannelData* sd)
{
	return Vector(0, 0, 0);
}

Bool RegisterDecayFilter(void)
{
	return RegisterShaderPlugin(DL_DECAYFILTER,
								"dlDecayFilter"_s,
								PLUGINFLAG_HIDE,
								DLDecayFilter::Alloc,
								"dldecayfilter"_s,
								0);
}
