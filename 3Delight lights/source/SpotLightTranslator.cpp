#include "SpotLightTranslator.h"
#include "delightenvironment.h"
#include "DL_Light_Filters.h"
#include "DL_Utilities.h"
#include "IDs.h"
#include "LightFilterFunctions.h"
#include "c4d.h"
#include "nsi.hpp"
#include "ospotlight.h"

void SpotLightTranslator::CreateNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	BaseList2D* obj = (BaseList2D*) C4DNode;
	BaseContainer* data = obj->GetDataInstance();
	float radius = (float) data->GetFloat(SPOTLIGHT_RADIUS);
	// Create a mesh and connect it to the parent transform
	// handle = std::string(parser->GetUniqueName("poitlight"));
	std::string handle = std::string(Handle);
	std::string transform_handle = std::string(ParentTransformHandle);
	ctx.Create(handle, "mesh");
	float P[12] = { -radius, -radius, 0, -radius, radius, 0,
					radius, radius, 0, radius, -radius, 0
				  }; // One particle
	int nvertices = 4;
	NSI::Argument arg_P("P");
	arg_P.SetType(NSITypePoint);
	arg_P.SetCount(4);
	arg_P.SetValuePointer((void*) &P[0]);
	ctx.SetAttribute(handle, (arg_P));
	ctx.SetAttribute(handle, NSI::IntegerArg("nvertices", nvertices));
	std::string transform =
		handle + "transform"; // std::string(parser->GetUniqueName("spot_tranform"));
	ctx.Create(transform, "transform");
	double scale_direction[16] {
		1, 0, 0, 0, 0, -1, 0, 0, 0, 0, -1, 0, 0, 0, 0, 1
	};
	NSI::Argument xform("transformationmatrix");
	xform.SetType(NSITypeDoubleMatrix);
	xform.SetValuePointer((void*) &scale_direction[0]);
	ctx.SetAttribute(transform, (xform));
	ctx.Connect(transform, "", transform_handle, "objects");
	ctx.Connect(handle, "", transform, "objects");
	// Create an attributes node, and connect it to the mesh
	std::string attributes_handle = handle + ("attributes");
	// ctx.Connect(transform_attributes, "", transform_handle,
	// "geometryattributes");
	ctx.Create(attributes_handle, "attributes");
	ctx.Connect(attributes_handle, "", transform, "geometryattributes");
	// Create a shader for the mesh and connect it to the geometry attributes of
	// the mesh
	std::string shader_handle = handle + "shader";
	ctx.Create(shader_handle, "shader");
	ctx.Connect(shader_handle, "", attributes_handle, "surfaceshader");
}

void SpotLightTranslator::SampleAttributes(DL_SampleInfo* info,
		const char* Handle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	BaseList2D* obj = (BaseList2D*) C4DNode;
	BaseContainer* data = obj->GetDataInstance();
	std::string handle = std::string(Handle);
	std::string shader_handle = handle + "shader";
	const char* delightpath = DelightEnv::getDelightEnvironment();
	std::string shaderpath =
		delightpath + (std::string) "/osl" + (std::string) "/spotLight.oso";
	Vector color = toLinear(data->GetVector(SPOTLIGHT_COLOR), doc);
	float col[3];
	col[0] = (float) color.x;
	col[1] = (float) color.y;
	col[2] = (float) color.z;
	int quadratic_intensity = 10000;
	/*
	            Because the intensity on the osl shader has been calculated for
	   distance of 1 unit in Cinema4D the default distance or objects size is
	   100. So to have the same behaviours as in other plugins regarding the
	   spot light, based on the decay type we multiply the value of intenity;
	   With the change of decay rate we should also change the intensity to get
	   the result we want.
	    */
	float intensity = (float) data->GetFloat(SPOTLIGHT_INTENSITY);
	float exposure = (float) data->GetFloat(SPOTLIGHT_EXPOSURE);
	int decay = data->GetInt32(SPOTLIGHT_DECAY);
	float cone_angle = (float) data->GetFloat(SPOTLIGHT_CONE_ANGLE);
	float penumbra_angle = (float) data->GetFloat(SPOTLIGHT_PENUMBRA_ANGLE);
	float drop_off = (float) data->GetFloat(SPOTLIGHT_DROPOFF);
	bool barndoors = data->GetBool(SPOTLIGHT_BARNDOORS_USE);
	float left_barn = (float) data->GetFloat(SPOTLIGHT_BARNDOORS_LEFT);
	float right_barn = (float) data->GetFloat(SPOTLIGHT_BARNDOORS_RIGHT);
	float top_barn = (float) data->GetFloat(SPOTLIGHT_BARNDOORS_TOP);
	float bottom_barn = (float) data->GetFloat(SPOTLIGHT_BARNDOORS_BOTTOM);
	float diffuse = (float) data->GetFloat(SPOTLIGHT_DIFFUSE);
	float specular = (float) data->GetFloat(SPOTLIGHT_SPECULAR);
	float hair = (float) data->GetFloat(SPOTLIGHT_HAIR);
	float volume = (float) data->GetFloat(SPOTLIGHT_VOLUME);
	float spread = (float) data->GetFloat(SPOTLIGHT_SPREAD);
	BaseList2D* color_shader = data->GetLink(SPOTLIGHT_COLOR_SHADER, doc);
	BaseList2D* intensity_shader = data->GetLink(SPOTLIGHT_INTENSITY_SHADER, doc);

	if (decay == SPOTLIGHT_DECAY_LINEAR) {
		intensity *= quadratic_intensity / 100.0f;

	} else if (decay == SPOTLIGHT_DECAY_QUADRATIC) {
		intensity *= quadratic_intensity;

	} else if (decay == SPOTLIGHT_DECAY_CUBIC) {
		intensity *= quadratic_intensity * 100.0f;
	}

	NSI::ArgumentList args;
	args.Add(new NSI::StringArg("shaderfilename", &shaderpath[0]));

	if (color_shader) {
		std::string link_shader = parser->GetHandleName(color_shader);
		ctx.Connect(link_shader, "outColor", shader_handle, "i_color");

	} else {
		args.Add(new NSI::ColorArg("i_color", &col[0]));
	}

	if (intensity_shader) {
		std::string link_shader = parser->GetHandleName(intensity_shader);
		ctx.Connect(link_shader, "outColor[0]", shader_handle, "intensity");

	} else {
		args.Add(new NSI::FloatArg("intensity", intensity));
	}

	args.Add(new NSI::IntegerArg("decayRate", decay));
	args.Add(new NSI::FloatArg("coneAngle", cone_angle));
	args.Add(new NSI::FloatArg("penumbraAngle", penumbra_angle));
	args.Add(new NSI::FloatArg("dropoff", drop_off));
	args.Add(new NSI::IntegerArg("barnDoors", barndoors));
	args.Add(new NSI::FloatArg("leftBarnDoor", left_barn));
	args.Add(new NSI::FloatArg("rightBarnDoor", right_barn));
	args.Add(new NSI::FloatArg("topBarnDoor", top_barn));
	args.Add(new NSI::FloatArg("bottomBarnDoor", bottom_barn));
	args.Add(new NSI::FloatArg("diffuse_contribution", diffuse));
	args.Add(new NSI::FloatArg("specular_contribution", specular));
	args.Add(new NSI::FloatArg("hair_contribution", hair));
	args.Add(new NSI::FloatArg("volume_contribution", volume));
	args.Add(new NSI::FloatArg("exposure", exposure));
	args.Add(new NSI::FloatArg("spread", spread));
	ctx.SetAttributeAtTime(shader_handle, info->sample_time, args);
	const CustomDataType* light_filters = data->GetCustomDataType(
			DL_CUSTOM_SPOTLIGHT_FILTERS, ID_CUSTOMDATATYPE_LIGHTFILTERS);
	iCustomDataTypeFilters* light_filter_data =
		(iCustomDataTypeFilters*) (light_filters);

	if (!light_filter_data) {
		light_filter_data = NewObjClear(iCustomDataTypeFilters);
	}

	std::vector<Int32> selected_lights_guid;

	for (int i = 0;
			i < light_filter_data->m_selected_light_filters_itemID.GetCount();
			i++)
		selected_lights_guid.push_back(
			light_filter_data->m_selected_light_filters_itemID[i]);

	// Getting selected filters
	std::vector<BaseShader*> selected_filters =
		getSelectedFilters((BaseObject*) obj,
						   selected_lights_guid,
						   light_filter_data->m_all_light_filters.GetCount());

	for( BaseShader *filter : selected_filters )
	{
		std::string filter_transform =
			"X_" + parser->GetHandleName(filter);
		std::string filter_shader =
			parser->GetHandleName(filter) + "shader";
		ctx.Connect(filter_shader, "filter_output", shader_handle, "filter");
	}
}
