#include "DL_Light_Filters.h"
#include <vector>
#include "IDs.h"
#include "NodeIdHash.h"
#include "PluginManager.h"
#include "c4d_symbols.h"
#include "maxon/stringencoding.h"
#include "RegisterPrototypes.h"

using namespace std;
extern PluginManager PM;

// Create a unique ID for each filter shader.
static String
GetHandleName(BaseList2D* node)
{
	return NodeIdToHandle<String>(node);
}

// Light Filters Structure
struct LayersData {
	Int32 id;
	Char name[50];
};

LayersData s_light_filters[] = { { 50, "Decay Filter" },
	{ 51, "Gobo Filter" },
	{ 0, "" }
};

// Class to modify the TreeView dynimaclly in the way we want(define class)
class lightFiltersCustomTree
{
public:
	String Name = "";
	Int32 Type;
	Bool _selected = FALSE;
	Int32 ListPosition;
	Int32 Gobo;
	Int32 Decay;
	String objectGUID;
	AutoAlloc<BaseLink> link;
	lightFiltersCustomTree(String Name)
	{
		this->Name = Name;
	}

	lightFiltersCustomTree(BaseShader* object,
						   Int32 position,
						   Int32 gobo,
						   Int32 decay)
	{
		this->Name = object->GetName();
		this->Type = object->GetType();
		this->ListPosition = position;
		this->objectGUID = GetHandleName(object);
		Gobo = gobo;
		Decay = decay;
	}

	Bool isSelected()
	{
		return this->_selected;
	}

	void Select()
	{
		this->_selected = TRUE;
	}

	void Deselect()
	{
		this->_selected = FALSE;
	}

	String TO_STRING()
	{
		return this->Name;
	}
};

/*
        In this function we get all the light objects that are used in the
   scene, and store them within a vector of our custom treeview class. The light
   objects order depends on their order in the scene.
*/
static std::vector<lightFiltersCustomTree*>
Populate_TreeView()
{
	std::vector<lightFiltersCustomTree*> light_list;
	BaseObject* object = GetActiveDocument()->GetActiveObject();

	if (!object) {
		return light_list;
	}

	bool process = true;
	Int32 listPosition = 0;
	Int32 gobo = 1;
	Int32 decay = 1;
	BaseShader* shader = object->GetFirstShader();
	std::vector<BaseShader*> vect;
	std::vector<int> positions;

	while (shader) {
		if (process && (shader->GetType() == DL_GOBOFILTER || shader->GetType() == DL_DECAYFILTER)) {
			if (shader->GetType() == DL_GOBOFILTER) {
				vect.push_back(shader);
				positions.push_back(listPosition++);
				gobo++;

			} else if (shader->GetType() == DL_DECAYFILTER) {
				vect.push_back(shader);
				positions.push_back(listPosition++);
				decay++;
			}
		}

		if (shader->GetDown() && process) {
			shader = shader->GetDown();
			process = true;

		} else if (shader->GetNext()) {
			shader = shader->GetNext();
			process = true;

		} else if (shader->GetUp()) {
			shader = shader->GetUp();
			process = false;

		} else {
			shader = NULL;
		}
	}

	// Since the insertion of the objects is done like in a stack where the new
	// object goes to the top of the view, and we want to display them in the
	// opposite way, we are reversing the vectorto get the desired result.
	// Starting from the second items as the first one is the light group.
	for (size_t i = 0; i < vect.size(); i++) {
		if (vect[i]->GetType() == DL_GOBOFILTER) {
			lightFiltersCustomTree* light_object =
				new lightFiltersCustomTree(vect[i], --listPosition, --gobo, decay);
			light_list.push_back(light_object);

		} else if (vect[i]->GetType() == DL_DECAYFILTER) {
			lightFiltersCustomTree* light_object =
				new lightFiltersCustomTree(vect[i], --listPosition, gobo, --decay);
			light_list.push_back(light_object);
		}
	}

	std::reverse(light_list.begin(), light_list.begin() + light_list.size());
	return light_list;
}

// Class to supply data and view model for TreeViewCustomGui.
class LightFilterTreeview : public TreeViewFunctions
{
public:
	std::vector<lightFiltersCustomTree*> mylist;
	void Populate()
	{
		mylist = Populate_TreeView();
	}

	// Called to retrieve the first object of the tree.
	void* GetFirst(void* root, void* userdata)
	{
		if (mylist.size() > 0) {
			return mylist[0];
		}

		return nullptr;
	}

	// Called to retrieve the first child of @formatParam{obj}.
	void* GetDown(void* root, void* userdata, void* obj)
	{
		return nullptr;
	}

	// Called to retrieve the next object after @formatParam{obj}.
	void* GetNext(void* root, void* userdata, void* obj)
	{
		lightFiltersCustomTree* object = nullptr;
		lightFiltersCustomTree* currentObject = (lightFiltersCustomTree*) obj;
		std::vector<lightFiltersCustomTree*>::iterator it =
			std::find(mylist.begin(), mylist.end(), currentObject);
		Int32 index = (Int32) std::distance(mylist.begin(), it);
		Int32 nextIndex = index + 1;

		if (nextIndex < (int) mylist.size()) {
			object = mylist[nextIndex];
		}

		return object;
		return nullptr;
	}

	// Called to retrieve the column width of object @formatParam{obj} in column
	// @formatParam{col}.
	Int32 GetColumnWidth(void* root,
						 void* userdata,
						 void* obj,
						 Int32 col,
						 GeUserArea* area)
	{
		return 80;
	}

	// Called to retrieve the folding status of the object.
	Bool IsOpened(void* root, void* userdata, void* obj)
	{
		return TRUE;
	}

	// Called to retrieve the name of object @formatParam{obj}.
	String GetName(void* root, void* userdata, void* obj)
	{
		if (!root || !obj) {
			return String();
		}

		String name = String();
		lightFiltersCustomTree* light_filters = (lightFiltersCustomTree*) obj;

		if (light_filters->Type == DL_GOBOFILTER)
			name =
				light_filters->TO_STRING() + String::IntToString(light_filters->Gobo);

		else if (light_filters->Type == DL_DECAYFILTER)
			name =
				light_filters->TO_STRING() + String::IntToString(light_filters->Decay);

		return name;
	}

	Int GetId(void* root, void* userdata, void* obj)
	{
		return 0;
	}

	// Called to retrieve the drag type of object @formatParam{obj},
	// i.e. the type that the user would get if he started a drag in that cell.
	Int32 GetDragType(void* root, void* userdata, void* obj)
	{
		return NOTOK;
	}

	// Called to select object @formatParam{obj}.
	void Select(void* root, void* userdata, void* obj, Int32 mode)
	{
		if (!root || !obj) {
			return;
		}

		lightFiltersCustomTree* light_filters = (lightFiltersCustomTree*) obj;

		if (mode == SELECTION_NEW) {
			if (light_filters->isSelected()) {
				light_filters->Deselect();

			} else if (!light_filters->isSelected()) {
				light_filters->Select();
			}
		}

		return;
	}

	// Called to retrieve the selection status of object @formatParam{obj}
	Bool IsSelected(void* root, void* userdata, void* obj)
	{
		if (!root || !obj) {
			return FALSE;
		}

		lightFiltersCustomTree* light_filters = (lightFiltersCustomTree*) obj;
		return light_filters->isSelected();
	}

	// Called to specify the text colors of object @formatParam{obj}
	void GetColors(void* root,
				   void* userdata,
				   void* obj,
				   GeData* pNormal,
				   GeData* pSelected)
	{
		if (!root || !obj) {
			return;
		}

		pSelected->SetVector(Vector(1, 1, 1));
	}

	// Called to specify the background color of @formatParam{line}.
	void GetBackgroundColor(void* root,
							void* userdata,
							void* obj,
							Int32 line,
							GeData* col)
	{
		if (!root || !obj) {
			return;
		}

		lightFiltersCustomTree* light_filters = (lightFiltersCustomTree*) obj;
		col->SetVector(Vector(0.27, 0.27, 0.27));

		if (light_filters->isSelected()) {
			col->SetVector(Vector(0.42, 0.42, 0.42));
		}

		return;
	}
};

LightFilterTreeview all_filters;

LightFitersDialog::LightFitersDialog()
{
	m_data = NULL;
}

LightFitersDialog::~LightFitersDialog() {}

Bool LightFitersDialog::CreateLayout()
{
	// first call the parent instance
	Bool filter_selector = GeDialog::CreateLayout();
	filter_selector = LoadDialogResource(light_filters_dialog, nullptr, 0);

	if (filter_selector) {
		m_filtersview.AttachListView(this, DL_LIGHT_FILTERS_LISTVIEW);
	}

	AutoAlloc<BaseSelect> selected;
	m_filtersview.SetSelection(selected);
	return filter_selector;
}

Bool LightFitersDialog::InitValues()
{
	// first call the parent instance
	if (!GeDialog::InitValues()) {
		return false;
	}

	BaseContainer layout;
	BaseContainer data;
	Int32 i = 0;
	layout = BaseContainer();
	layout.SetInt32('name', LV_COLUMN_TEXT);
	m_filtersview.SetLayout(1, layout);
	data = BaseContainer();

	for (i = 0; s_light_filters[i].id; i++) {
		data.SetString('name', String(s_light_filters[i].name));
		m_filtersview.SetItem(s_light_filters[i].id, data);
	}

	m_filtersview.DataChanged();
	return true;
}

Bool LightFitersDialog::Command(Int32 i_id, const BaseContainer& i_msg)
{
	switch (i_id) {
	case BUTTON_OK: {
		BaseObject* object = GetActiveDocument()->GetActiveObject();
		AutoAlloc<BaseSelect> s2;

		if (m_selection && s2) {
			Int32 i, id2, count = m_filtersview.GetItemCount();
			BaseContainer test;

			for (i = 0; i < count; i++) {
				m_filtersview.GetItemLine(i, &id2, &test);
			}

			if (m_filtersview.GetSelection(m_selection)) {
				BaseContainer data;

				for (i = 0; s_light_filters[i].id; i++) {
					if (m_selection->IsSelected(s_light_filters[i].id)) {
						maxon::String name = maxon::String(s_light_filters[i].name);

						if (name == "Decay Filter") {
							BaseShader* shader = BaseShader::Alloc(DL_DECAYFILTER);
							object->InsertShader(shader);

						} else if (name == "Gobo Filter") {
							BaseShader* shader = BaseShader::Alloc(DL_GOBOFILTER);
							object->InsertShader(shader);
						}
					}
				}
			}
		}

		this->Close();
		EventAdd();
	}
	}

	return true;
}

Int32 LightFitersDialog::Message(const BaseContainer& i_msg, BaseContainer& i_result)
{
	return GeDialog::Message(i_msg, i_result);
}

// Create a constructor
iCustomDataTypeLightFilters::iCustomDataTypeLightFilters(
	const BaseContainer& i_settings,
	CUSTOMGUIPLUGIN* i_plugin)
	: iCustomGui(i_settings, i_plugin) {};

Bool iCustomDataTypeLightFilters::CreateLayout()
{
	Bool layout_create = GeDialog::CreateLayout();
	layout_create = LoadDialogResource(light_filters, nullptr, 0);

	if (layout_create) {
		m_filter_view = (TreeViewCustomGui*) FindCustomGui(DL_LIGHT_FILTERS_TREE,
						CUSTOMGUI_TREEVIEW);
		all_filters.Populate();
		BaseContainer layout;
		layout.SetInt32(1, LV_TREE);
		m_filter_view->SetLayout(1, layout);
		m_filter_view->SetRoot(m_filter_view, &all_filters, this);
		m_filter_view->Refresh();
		Int32 treeSize = (Int32) all_filters.mylist.size();

		if (treeSize > 0) {
			for (int i = 0; i < treeSize; i++) {
				for (int j = 0; j < m_data.m_selected_light_filters_itemID.GetCount();
						j++) {
					// Checks for the selected light filters that were on the
					// m_selected_light_filters_GUID stored before the layout
					// was changed.
					if (all_filters.mylist[treeSize - i - 1]->objectGUID == m_data.m_selected_light_filters_GUID[j]) {
						all_filters.mylist[treeSize - i - 1]->Select();
					}
				}
			}
		}
	}

	return SUPER::CreateLayout();
};

Bool iCustomDataTypeLightFilters::InitValues()
{
	m_filter_view->Refresh();
	return SUPER::InitValues();
};

Int32 iCustomDataTypeLightFilters::Message(const BaseContainer& msg,
		BaseContainer& result)
{
	switch (msg.GetId()) {
	case BFM_INTERACTEND: {
		/*
			                Getting the state of the tree after an interaction is
			   finished in the view. This includes clicking somewhere on the view
			   where you can select or deselect ligt filters, or clicking buttons in
			   the view etc. After we store the latest state of the tree in the
			   vectors below: m_light_filters_selected_layers => Store the names of
			   the selected light filters m_selected_light_filters_itemID => Store
			   the selected light filters row number. This is used as their ID in
			   the RenderOptionsHook. m_data.m_selected_light_filters_GUID => Store
			   the selected light filters Unique ID.
			        */
		m_filter_view->Refresh();
		m_data.m_light_filters_selected_layers.Reset();
		m_data.m_selected_light_filters_itemID.Reset();
		m_data.m_selected_light_filters_GUID.Reset();
		m_data.m_all_light_filters.Reset();
		Int32 treeSize = (Int32) all_filters.mylist.size();

		if (treeSize > 0) {
			for (int i = 0; i < treeSize; i++) {
				(void) m_data.m_all_light_filters.Append(
					all_filters.mylist[i]->TO_STRING());

				// Adding only the selected light filters objects in the
				// BaseArray.
				if (all_filters.mylist[i]->isSelected()) {
					(void) m_data.m_light_filters_selected_layers.Append(
						all_filters.mylist[i]->TO_STRING());
					(void) m_data.m_selected_light_filters_itemID.Append(i);
					(void) m_data.m_selected_light_filters_GUID.Append(
						all_filters.mylist[i]->objectGUID);
				}
			}
		}

		m_filter_view->Refresh();
		BaseContainer m(BFM_ACTION);
		m.SetInt32(BFM_ACTION_ID, this->GetId());
		m.SetData(BFM_ACTION_VALUE, this->GetData().GetValue());
		SendParentMessage(m);
	}
	}

	return GeDialog::Message(msg, result);
}

Bool iCustomDataTypeLightFilters::Command(Int32 i_id, const BaseContainer& i_msg)
{
	switch (i_id) {
	case DL_ADD_LIGHT_FILTER: {
		m_dialog.Open(DLG_TYPE::MODAL, 10, -1, -1, 300, 100);
		m_filter_view->Refresh();
		break;
	}
	}

	return true;
}

Bool iCustomDataTypeLightFilters::CoreMessage(Int32 id, const BaseContainer& msg)
{
	if (id == EVMSG_CHANGE) {
		all_filters.Populate();
		// Keeping the selected items selected after the layout is changed or
		// refreshed.
		Int32 treeSize = (Int32) all_filters.mylist.size();

		if (treeSize > 0) {
			for (int i = 0; i < treeSize; i++) {
				for (int j = 0; j < m_data.m_selected_light_filters_itemID.GetCount();
						j++) {
					// Checks for the selected light filters that were on the
					// m_selected_light_filters_GUID stored before the layout
					// was changed.
					if (all_filters.mylist[treeSize - i - 1]->objectGUID == m_data.m_selected_light_filters_GUID[j]) {
						all_filters.mylist[treeSize - i - 1]->Select();
					}
				}
			}
		}

		m_data.m_light_filters_selected_layers.Reset();
		m_data.m_selected_light_filters_itemID.Reset();
		m_data.m_selected_light_filters_GUID.Reset();
		m_data.m_all_light_filters.Reset();
		treeSize = (Int32) all_filters.mylist.size();

		if (treeSize > 0) {
			for (int i = 0; i < treeSize; i++) {
				(void) m_data.m_all_light_filters.Append(
					all_filters.mylist[i]->TO_STRING());

				// Adding only the selected light filetrs shaders in the
				// BaseArray
				if (all_filters.mylist[i]->isSelected()) {
					(void) m_data.m_light_filters_selected_layers.Append(
						all_filters.mylist[i]->TO_STRING());
					(void) m_data.m_selected_light_filters_itemID.Append(i);
					(void) m_data.m_selected_light_filters_GUID.Append(
						all_filters.mylist[i]->objectGUID);
				}
			}
		}

		m_filter_view->Refresh();
	}

	return true;
}

/**
        Called to update the custom GUI to display the value in
   @formatParam{tristate}.
*/
Bool iCustomDataTypeLightFilters::SetData(const TriState<GeData>& i_tristate)
{
	iCustomDataTypeFilters* data =
		(iCustomDataTypeFilters*) (i_tristate.GetValue().GetCustomDataType(
									   ID_CUSTOMDATATYPE_LIGHTFILTERS));

	if (data) {
		iferr(m_data.m_light_filters_selected_layers.CopyFrom(
				  data->m_light_filters_selected_layers)) return false;
		iferr(m_data.m_selected_light_filters_itemID.CopyFrom(
				  data->m_selected_light_filters_itemID)) return false;
		iferr(m_data.m_selected_light_filters_GUID.CopyFrom(
				  data->m_selected_light_filters_GUID)) return false;
		iferr(m_data.m_all_light_filters.CopyFrom(
				  data->m_all_light_filters)) return false;
	}

	return true;
};

/**
        Called to retrieve the value(s) currently displayed.
*/
TriState<GeData>
iCustomDataTypeLightFilters::GetData()
{
	TriState<GeData> tri;
	tri.Add(GeData(ID_CUSTOMDATATYPE_LIGHTFILTERS, m_data));
	return tri;
};

// This array defines the applicable datatypes.
static Int32 g_stringtable[] = { ID_CUSTOMDATATYPE_LIGHTFILTERS };

/**
        This CustomGuiData class registers a new custom GUI for the Layers
   datatype.
*/
class CustomGuiLightFilters : public CustomGuiData
{
public:
	virtual Int32 GetId();
	virtual CDialog* Alloc(const BaseContainer& i_settings);
	virtual void Free(CDialog* i_dlg, void* i_userdata);
	virtual const Char* GetResourceSym();
	virtual CustomProperty* GetProperties();
	virtual Int32 GetResourceDataType(Int32*& i_table);
};

/**
        Called to get the plugin ID of the custom GUI.
*/
Int32 CustomGuiLightFilters::GetId()
{
	return ID_CUSTOMGUI_LIGHTFILTERS;
};

/**
        We override Alloc function to create a new instance of the plugin.It is
   used instead of a constructor at this one provide means to return an error if
   something goes wrong.
*/
CDialog*
CustomGuiLightFilters::Alloc(const BaseContainer& i_settings)
{
	iferr(iCustomDataTypeLightFilters* dlg = NewObj(
				iCustomDataTypeLightFilters, i_settings, GetPlugin())) return nullptr;
	CDialog* cdlg = dlg->Get();

	if (!cdlg) {
		return nullptr;
	}

	return cdlg;
};

/**
        This function is called when an instance gets freed.
*/
void CustomGuiLightFilters::Free(CDialog* i_dlg, void* i_userdata)
{
	if (!i_dlg || !i_userdata) {
		return;
	}

	iCustomDataTypeLightFilters* sub =
		static_cast<iCustomDataTypeLightFilters*>(i_userdata);
	DeleteObj(sub);
};

/**
        Returns the resource symbol. This symbol can be used in resource
        files in combination with "CUSTOMGUI".
*/
const Char*
CustomGuiLightFilters::GetResourceSym()
{
	return "LIGHTFILTERSCUSTOMGUI";
};

CustomProperty*
CustomGuiLightFilters::GetProperties()
{
	return nullptr;
};

/**
        Returns the applicable datatypes defined in the stringtable array.
*/
Int32 CustomGuiLightFilters::GetResourceDataType(Int32*& i_table)
{
	i_table = g_stringtable;
	return sizeof(g_stringtable) / sizeof(Int32);
};

/**
        A data class for creating custom data types.
        These can be used in descriptions and container just like built-in data
   types. As we do not have a buil-in data type for the Listview we create our
   custom data type using the CUSTOMDATATYPECLASS.
*/
class FiltersCustomDataTypeClass : public CustomDataTypeClass
{
	INSTANCEOF(FiltersCustomDataTypeClass, CustomDataTypeClass)

public:
	virtual Int32 GetId()
	{
		return ID_CUSTOMDATATYPE_LIGHTFILTERS;
	}

	virtual CustomDataType* AllocData()
	{
		iCustomDataTypeFilters* data = NewObjClear(iCustomDataTypeFilters);
		return data;
	};

	virtual void FreeData(CustomDataType* i_data)
	{
		iCustomDataTypeFilters* d = static_cast<iCustomDataTypeFilters*>(i_data);
		DeleteObj(d);
	}

	/**
	            Called to copy an instance of the custom data type.
	            Copy the data from @formatParam{src} to @formatParam{dest}.
	    */
	virtual Bool CopyData(const CustomDataType* i_src,
						  CustomDataType* i_dst,
						  AliasTrans* i_aliastrans)
	{
		// Copy the data to the given target data.
		iCustomDataTypeFilters* s = (iCustomDataTypeFilters*) (i_src);
		iCustomDataTypeFilters* d = (iCustomDataTypeFilters*) (i_dst);

		if (!s || !d) {
			return false;
		}

		d->m_light_filters_selected_layers.Flush();
		iferr(d->m_light_filters_selected_layers.CopyFrom(
				  s->m_light_filters_selected_layers)) return false;
		d->m_selected_light_filters_itemID.Flush();
		iferr(d->m_selected_light_filters_itemID.CopyFrom(
				  s->m_selected_light_filters_itemID)) return false;
		d->m_selected_light_filters_GUID.Flush();
		iferr(d->m_selected_light_filters_GUID.CopyFrom(
				  s->m_selected_light_filters_GUID)) return false;
		d->m_all_light_filters.Flush();
		iferr(d->m_all_light_filters.CopyFrom(s->m_all_light_filters)) return false;
		return true;
	}

	virtual Int32 Compare(const CustomDataType* i_dst1,
						  const CustomDataType* i_dst2)
	{
		return 1;
	}

	/**
	            Called to write the custom data type to a file.
	    */
	virtual Bool WriteData(const CustomDataType* i_t_d, HyperFile* i_hf)
	{
		const iCustomDataTypeFilters* const d =
			static_cast<const iCustomDataTypeFilters*>(i_t_d);
		const maxon::Int light_filter_selected =
			d->m_light_filters_selected_layers.GetCount();
		i_hf->WriteInt64((Int64) light_filter_selected);

		for (Int64 i = 0; i < light_filter_selected; ++i) {
			i_hf->WriteString(d->m_light_filters_selected_layers[i]);
			i_hf->WriteInt32(d->m_selected_light_filters_itemID[i]);
			i_hf->WriteString(d->m_selected_light_filters_GUID[i]);
		}

		const maxon::Int all_light_filters = d->m_all_light_filters.GetCount();
		i_hf->WriteInt64((Int64) all_light_filters);

		for (Int64 i = 0; i < all_light_filters; ++i) {
			i_hf->WriteString(d->m_all_light_filters[i]);
		}

		return true;
	}

	/**
	            Called to read the custom data type from a file.
	    */
	virtual Bool ReadData(CustomDataType* i_t_d, HyperFile* i_hf, Int32 i_level)
	{
		iCustomDataTypeFilters* const d =
			static_cast<iCustomDataTypeFilters*>(i_t_d);

		if (i_level > 0) {
			Int64 light_filter_selected = 0;

			if (i_hf->ReadInt64(&light_filter_selected)) {
				for (Int64 i = 0; i < light_filter_selected; ++i) {
					String selected_layer;
					Int32 selected_item_id;
					String selected_guid;

					if (i_hf->ReadString(&selected_layer)) {
						iferr(d->m_light_filters_selected_layers.Append(
								  selected_layer)) return false;
					}

					if (i_hf->ReadInt32(&selected_item_id)) {
						iferr(d->m_selected_light_filters_itemID.Append(
								  selected_item_id)) return false;
					}

					if (i_hf->ReadString(&selected_guid)) {
						iferr(d->m_selected_light_filters_GUID.Append(
								  selected_guid)) return false;
					}
				}
			}
		}

		return true;
	}

	// Called to get the symbol to use in resource files.
	virtual const Char* GetResourceSym()
	{
		return "LIGHTFILTERSTCUSTOMTYPE";
	}

	virtual void GetDefaultProperties(BaseContainer& i_data)
	{
		// The default values of this datatype
		// use the custom GUI as default.
		i_data.SetInt32(DESC_CUSTOMGUI, ID_CUSTOMGUI_LIGHTFILTERS);
		i_data.SetInt32(DESC_ANIMATE, DESC_ANIMATE_ON);
	}
};

/**
        With this function we register the custom GUI plugin that we have alread
   build.
*/
Bool RegisterCustomLightFilters()
{
	if (!RegisterCustomDataTypePlugin("Light Filters"_s,
									  CUSTOMDATATYPE_INFO_LOADSAVE | CUSTOMDATATYPE_INFO_TOGGLEDISPLAY | CUSTOMDATATYPE_INFO_HASSUBDESCRIPTION,
									  NewObjClear(FiltersCustomDataTypeClass),
									  1)) {
		return false;
	}

	static BaseCustomGuiLib myStringGUIlib;
	ClearMem(&myStringGUIlib, sizeof(myStringGUIlib));
	FillBaseCustomGui(myStringGUIlib);
	InstallLibrary(
		ID_CUSTOMGUI_LIGHTFILTERS, &myStringGUIlib, 1001, sizeof(myStringGUIlib));
	RegisterCustomGuiPlugin(
		"LightFilters"_s, 0, NewObjClear(CustomGuiLightFilters));
	return true;
}
