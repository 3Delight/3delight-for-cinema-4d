#ifndef DL_UTILITIES_H
#define DL_UTILITIES_H

#include <string>
#include "c4d.h"

inline Vector
toLinear(Vector color, BaseDocument* doc)
{
	BaseContainer* data = doc->GetDataInstance();
	long colorprofile = data->GetInt32(DOCUMENT_COLORPROFILE,
									   2); // 0=SRGB, 1=LINEAR, 2=DISABLED;
	bool lwf = data->GetBool(DOCUMENT_LINEARWORKFLOW, TRUE);

	if ((!lwf) || (colorprofile != 0)) {
		return color;
	}

	return TransformColor(color, COLORSPACETRANSFORMATION::SRGB_TO_LINEAR);
}

inline bool
file_exists(const std::string& name)
{
	const maxon::Url file = maxon::Url((String) name.c_str());
	return file.IoDetect() == maxon::IODETECT::FILE;
}

inline bool
Replace(std::string& data, std::string toSearch, std::string replaceStr)
{
	bool found = false;
	// Get the first occurrence
	size_t pos = data.find(toSearch);

	// Repeat till end is reached
	while (pos != std::string::npos) {
		found = true;
		// Replace this occurrence of Sub String
		data.replace(pos, toSearch.size(), replaceStr);
		// Get the next occurrence from the current position
		pos = data.find(toSearch, pos + replaceStr.size());
	}

	return found;
}

inline std::string
getFormattedFilename(std::string texturename)
{
	std::string index = strrchr(&texturename[0], '.');
	std::string formatted_filename = texturename;
	// We only get the filename string without the extension
	std::string filename =
		texturename.substr(0, texturename.size() - index.size());
	// get the last index of a possible frame number
	int i = int(filename.size() - 1);
	int start_index = i;
	int end_index = i;

	// std::string curr_frame="";
	for (; i >= 0; i--) {
		if (filename[i] < '0' || filename[i] > '9') {
			start_index = i + 1;
			break;

		} else {
			start_index = i;
		}
	}

	if (start_index > end_index) {
		formatted_filename = texturename;

	} else if (start_index > 0) {
		formatted_filename = texturename.substr(0, start_index) + "#" + texturename.substr(end_index + 1, texturename.size());
	}

	return formatted_filename;
}

/*
        Makes possible that the materials created from the shelf menu
        get directly assigned to the selected geometry or geometries if there
        is any. Otherwise the object will just be created without any assignment
        to any object.
*/
inline bool
CreateMaterial(int shelf_used, int material_id, BaseDocument* doc)
{
	if (shelf_used == 1) {
		Material* material = (Material*) BaseMaterial::Alloc(material_id);

		if (!material) {
			return false;
		}

		doc->InsertMaterial(material);
		AutoAlloc<AtomArray> selected_objects;
		doc->GetActiveObjects(*selected_objects, GETACTIVEOBJECTFLAGS::NONE);

		if (selected_objects) {
			Int32 object_count = selected_objects->GetCount();

			for (int i = 0; i < object_count; i++) {
				BaseObject* object = (BaseObject*) selected_objects->GetIndex(i);

				if (object) {
					TextureTag* const textureTag =
						static_cast<TextureTag*>(object->MakeTag(Ttexture));
					textureTag->SetMaterial(material);
				}
			}
		}

	} else if (shelf_used == 0) {
		Material* material = (Material*) BaseMaterial::Alloc(material_id);

		if (!material) {
			return false;
		}

		doc->InsertMaterial(material);
	}

	EventAdd();
	return true;
}


// Convert Cinema4D Interpolation to the corresponding 3Delight one.
inline int C4DTo3DelightInterpolation(int c4d_interp)
{
	if (c4d_interp == GRADIENT_INTERPOLATION_NONE) {
		return 0;

	}
	else if (c4d_interp == GRADIENT_INTERPOLATION_LINEARKNOT) {
		return 1;
	}

	return 2;
}

static BaseObject*
GetNextObject(BaseObject* op)
{
	if (!op) {
		return NULL;
	}

	if (op->GetDown()) {
		return op->GetDown();
	}

	while (!op->GetNext() && op->GetUp()) {
		op = op->GetUp();
	}

	return op->GetNext();
}
#endif
