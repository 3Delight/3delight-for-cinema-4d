#pragma once

#include "c4d.h"
#include <string.h>
#include "c4d_gui.h"
#include "PluginManager.h"
#include "BitmapDisplayDriver.h"
#include "CameraHook.h"
#include "RenderOptionsHook.h"
#include "VisibilityTagTranslator.h"
#include "DL_StandIn_Translator.h"
#include "IDs.h"
#include "NSIAPI.h"
#include "RegisterPrototypes.h"

PluginManager PM;
