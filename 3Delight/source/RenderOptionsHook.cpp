#include <algorithm>
#include <iostream>
#include "RenderOptionsHook.h"
#include <assert.h>
#include <string.h>
#include <algorithm>
#include <iomanip>
#include <set>
#include <sstream>
#include <tuple>
#include "DLAOVsColor.h"
#include "DL_TypeConversions.h"
#include "ExpandFilename.h"
#include "IDs.h"
#include "PluginManager.h"
#include "customgui_inexclude.h"
#include "dlrendersettings.h"
#include "nsi.hpp"
#include "DL_Utilities.h"

#ifdef __APPLE__
#	pragma clang diagnostic ignored "-Wimplicit-float-conversion"
#endif

#define DL_CUSTOM_COLORS 1008

using namespace std;

extern PluginManager PM;

static vector<BaseObject*>
GetCustomAOV(BaseDocument* doc)
{
	vector<BaseObject*> custom_aovs;
	// Find light groups in scene
	BaseObject* aov_obj = doc->GetFirstObject();

	while (aov_obj) {
		if (aov_obj->GetType() == ID_AOV_GROUP && aov_obj->GetDeformMode()) { // Only look for enabled a groups
			BaseContainer* data = aov_obj->GetDataInstance();

			if (data->GetBool(1012) == TRUE) {
				custom_aovs.push_back(aov_obj);
			}
		}

		aov_obj = GetNextObject(aov_obj);
	}

	return custom_aovs;
}

/*
	Function to get the selected lights from the multilights UI.
*/
static vector<BaseObject*> getSelectedLights(
	BaseDocument* doc,
	vector<Int32> objectID,
	maxon::Int all_multi_lights,
	int multilight_option)
{
	vector<BaseObject*> light_list;
	set<BaseList2D*> lights_in_groups;

	BaseObject* object= doc->GetFirstObject();
	if (!object)
			return light_list;
	Int32 Position = 2;
	int lights_size = (int)objectID.size();

	//checking if environment is used and selected.
	bool skyUsed = false;
	for (size_t i = 0; i < objectID.size(); i++)
	{
		if (objectID[i] == 0)
		{
				skyUsed = true;
		}
	}

	BaseObject *envShader = NULL;

	while (object)
	{
		//Check for selected light groups in multi-light table.
		if (object->GetType() == ID_LIGHTGROUP && object->GetDeformMode())
		{
			BaseContainer* data = object->GetDataInstance();
			InExcludeData* lightlist = (InExcludeData*)data->GetCustomDataType(
				1000, CUSTOMDATATYPE_INEXCLUDE_LIST); // LIGHTGROUP_LIGHTS=1000

			if (lightlist)
			{
				Int32 nlights = lightlist->GetObjectCount();

				if (nlights > 0)
				{
					if (multilight_option == DL_RENDER_MULTILIGHT_ALL)
					{
						light_list.push_back(object);
					}
					else
					{
						for (int i = 0; i < lights_size; i++)
						{
							//Finding the objects that are selected in the Multi_light UI
							//objectID contains the ID of the selected objects in the UI and
							//now we are getting the corresponing object for that ID
							if (objectID[i] == Position)
								light_list.push_back(object);

						}
						Position++;
					}

					//Don't render lights that are part of a group light as multilights.
					for (int i = 0; i < nlights; i++) 
					{
						BaseList2D* light = lightlist->ObjectFromIndex(doc, i);
						lights_in_groups.insert(light);
					}
				}
			}
		}
		object = GetNextObject(object);
	}

	object = doc->GetFirstObject();

	while (object)
	{
		if(object->GetType() == ID_ENVIRONMENTLIGHT)
		{
			envShader = object;
		}
		if (PM.IsLight(object) && object->GetType() != ID_ENVIRONMENTLIGHT)
		{
			if (lights_in_groups.find((BaseList2D*)object) == lights_in_groups.end())
			{
				if (multilight_option == DL_RENDER_MULTILIGHT_ALL)
				{
					light_list.push_back(object);
				}
				else
				{
					for (int i = 0; i < lights_size; i++)
					{
						//Finding the objects that are selected in the Multi_light UI
						//objectID contains the ID of the selected objects in the UI and
						//now we are getting the corresponing object for that ID
						if (objectID[i] == Position)
							light_list.push_back(object);

					}
					Position++;
				}
			}
		}
		object = GetNextObject(object);
	}

	//add environment shader to the beginning of the list if exists.
	if (envShader!=NULL)
			light_list.insert(light_list.begin(), envShader);

	return light_list;
}

struct DL_LayersData {
	std::string name;
	std::string shortName;
	std::string varName;
	std::string varSource;
	std::string varType;
	bool isShadingComponent;
};

std::vector<DL_LayersData> aov_layers_list = {
	{ "RGBA (color + alpha)", "RGBA", "Ci", "shader", "color", true },
	{ "RGBA (direct)", "directrgba", "Ci.direct", "shader", "color", true },
	{ "RGBA (indirect)", "indirectrgba", "Ci.indirect", "shader", "color", true },
	{ "Diffuse", "Diffuse", "diffuse", "shader", "color", true },
	{
		"Diffuse (direct)",
		"directdiffuse",
		"diffuse.direct",
		"shader",
		"color",
		true
	},
	{
		"Diffuse (indirect)",
		"directdiffuse",
		"diffuse.indirect",
		"shader",
		"color",
		true
	},
	{ "Hair and Fur", "hair", "hair", "shader", "color", true },
	{ "Subsurface scattering", "SSS", "subsurface", "shader", "color", true },
	{ "Reflection", "Reflection", "reflection", "shader", "color", true },
	{
		"Reflection (direct)",
		"directreflection",
		"reflection.direct",
		"shader",
		"color",
		true
	},
	{
		"Reflection (indirect)",
		"indirectreflection",
		"reflection.indirect",
		"shader",
		"color",
		true
	},
	{ "Refraction", "Refraction", "refraction", "shader", "color", true },
	{
		"Volume Scattering",
		"VolumeScattering",
		"volume",
		"shader",
		"color",
		true
	},
	{
		"Incandescence",
		"Incandescence",
		"incandescence",
		"shader",
		"color",
		true
	},
	{ "Toon Base", "ToonBase", "toon_base", "shader", "color", true },
	{ "Toon Diffuse", "ToonDiffuse", "toon_diffuse", "shader", "color", true },
	{ "Toon Specular", "ToonSpecular", "toon_specular", "shader", "color", true },
	{ "Toon Matte", "ToonMatte", "toon_matte", "shader", "color", true },
	{ "Outlines", "outlines", "outlines", "shader", "quad", true },
	{ "Albedo", "Albedo", "albedo", "shader", "color", false },
	{ "Z (depth)", "Z", "z", "builtin", "scalar", false },
	{ "Camera space position", "camP", "P.camera", "builtin", "vector", false },
	{ "Camera space normal", "camN", "N.camera", "builtin", "vector", false },
	{ "World space position", "worldP", "P.world", "builtin", "vector", false },
	{ "World space normal", "worldN", "N.world", "builtin", "vector", false },
	{ "Shadow Mask", "shadow_mask", "shadow_mask", "shader", "color", false },
	{ "UV", "UV", "st", "attribute", "vector", false },
	{
		"Geometry Cryptomatte",
		"GeometryCryptomatte",
		"id.geometry",
		"builtin",
		"scalar",
		false
	},
	{
		"Scene Path Cryptomatte",
		"ScenePathCryptomatte",
		"id.scenepath",
		"builtin",
		"scalar",
		false
	},
	{
		"Surface Shader Cryptomatte",
		"SurfaceShaderCryptomatte",
		"id.surfaceshader",
		"builtin",
		"scalar",
		false
	},
	{
		"Relighting Multiplier",
		"RelightingMultiplier",
		"relighting_multiplier",
		"shader",
		"color",
		false
	},
	{
		"Relighting Reference",
		"RelightingReference",
		"relighting_reference",
		"shader",
		"color",
		false
	},
	{
		"Motion Vector",
		"MotionVector",
		"motionvector",
		"builtin",
		"vector",
		false
	}
};

static DL_LayersData
GetLayerData(String layer_name)
{
	DL_LayersData data;

	for( const DL_LayersData &aov_layer : aov_layers_list )
	{
		if (layer_name == String(aov_layer.name.c_str())) {
			data = aov_layer;
		}
	}

	return data;
}

// Get the Filter to be used on nsi for the selectedFilter on the render
// settings
static std::string
getFilter(Int32 i_id)
{
	std::string filter;

	switch (i_id) {
	case DL_BLACKMAN_HARRIS:
		filter = "blackman-harris";
		break;

	case DL_MITCHELL:
		filter = "mitchell";
		break;

	case DL_CATMULL_ROM:
		filter = "catmull-rom";
		break;

	case DL_SINC:
		filter = "sinc";
		break;

	case DL_BOX:
		filter = "box";
		break;

	case DL_TRIANGLE:
		filter = "triangle";
		break;

	case DL_GAUSSIAN:
		filter = "gaussian";
		break;

	default:
		filter = "blackman-harris";
		break;
		break;
	}

	assert(filter != "");
	return filter;
}

static std::tuple<string, string, string>
OutputLayer(DL_SceneParser* i_parser)
{
	BaseContainer* settings = i_parser->GetSettings();
	string extension;
	string output_format;
	string scalar_format;

	if (settings->GetInt32(DL_DEFAULT_IMAGE_FORMAT) == DL_FORMAT_OPEN_EXR ||
		settings->GetInt32(DL_DEFAULT_IMAGE_FORMAT) == DL_FORMAT_OPEN_EXR_DEEP ||
		settings->GetInt32(DL_DEFAULT_IMAGE_FORMAT) == DL_FORMAT_OPEN_EXR_DWAA)
	{
		extension = ".exr";

		if (settings->GetInt32(DL_DEFAULT_IMAGE_FORMAT) == DL_FORMAT_OPEN_EXR) {
			output_format = "exr";

		}
		else if (settings->GetInt32(DL_DEFAULT_IMAGE_FORMAT) == DL_FORMAT_OPEN_EXR_DEEP)
		{
			output_format = "deepexr";
		}
		else
		{
			output_format = "dwaaexr";
		}

		if (settings->GetInt32(DL_DEFAULT_IMAGE_BITDEPTH) == DL_SIXTEEN_BIT_FLOAT) {
			scalar_format = "half";

		} else if (settings->GetInt32(DL_DEFAULT_IMAGE_BITDEPTH) == DL_THIRTYTWO_BIT) {
			scalar_format = "float";
		}

	} else if (settings->GetInt32(DL_DEFAULT_IMAGE_FORMAT) == DL_FORMAT_TIFF) {
		extension = ".tif";
		output_format = "tiff";

		if (settings->GetInt32(DL_DEFAULT_IMAGE_BITDEPTH) == DL_EIGHT_BIT) {
			scalar_format = "uint8";

		} else if (settings->GetInt32(DL_DEFAULT_IMAGE_BITDEPTH) == DL_SIXTEEN_BIT) {
			scalar_format = "uint16";

		} else if (settings->GetInt32(DL_DEFAULT_IMAGE_BITDEPTH) == DL_THIRTYTWO_BIT) {
			scalar_format = "float";
		}

	} else if (settings->GetInt32(DL_DEFAULT_IMAGE_FORMAT) == DL_FORMAT_PNG) {
		extension = ".png";
		output_format = "png";

		if (settings->GetInt32(DL_DEFAULT_IMAGE_BITDEPTH) == DL_EIGHT_BIT) {
			scalar_format = "uint8";

		} else if (settings->GetInt32(DL_DEFAULT_IMAGE_BITDEPTH) == DL_SIXTEEN_BIT) {
			scalar_format = "uint16";
		}

	}

	// assert( extension != "" && output_format != "" && scalar_format != "" );
	return std::make_tuple(extension, output_format, scalar_format);
}

void RenderOptionsHook::ExportOneOutputLayer(
	DL_SceneParser* parser,
	const std::string& i_layer_handle,
	const std::string& i_variable_source,
	const std::string& i_scalar_format,
	const std::string& i_layer_type,
	const std::string& i_filter,
	const std::string& i_aov,
	double i_filter_width,
	int i_alpha,
	const std::string& i_screen_handle,
	const std::string& i_driver_handle,
	const std::string& i_driver_name,
	unsigned& io_sort_key)
{
	NSI::Context& ctx(parser->GetContext());
	BaseContainer* settings = parser->GetSettings();

	bool has_speed_boost = settings->GetBool(DL_ENABLE_INTERACTIVE_PREVIEW);
	bool disable_image_layer = settings->GetBool(DL_DISABLE_IMAGE_LAYER);

	// Output only RGBA layer if Disable extra Image Layer has been selected.
	if ((has_speed_boost && disable_image_layer&& i_aov != "Ci")|| i_aov == "")
	{
		return;
	}

	ctx.Create(i_layer_handle, "outputlayer");
	ctx.SetAttribute(i_layer_handle,
		(
			NSI::StringArg("variablename", i_aov.c_str()),
			NSI::StringArg("variablesource", i_variable_source.c_str()),
			NSI::StringArg("scalarformat", i_scalar_format.c_str()),
			NSI::StringArg("layertype", i_layer_type.c_str()),
			NSI::IntegerArg("withalpha", i_alpha),
			NSI::IntegerArg("sortkey", io_sort_key++)
		));

	if (i_aov == "relighting_multiplier")
	{
		ctx.SetAttribute(i_layer_handle,
			(
				NSI::CStringPArg("filter", "box"),
				NSI::DoubleArg("filterwidth", 1.0)
			));
		// Setting "maximumvalue" is probably not a good idea in this case
	}

	else
	{
		ctx.SetAttribute(
			i_layer_handle,
			(
				NSI::CStringPArg("filter", i_filter.c_str()),
				NSI::DoubleArg("filterwidth", i_filter_width)
				));

		if (i_layer_type == "color")
		{
			/*
				Use 3Delight's fancy tonemapping technique. This allows
				rendering of very high dynamic range images with no ugly
				aliasing on the edges (think about area lights).
				The value here can really be very high and things will still
				work fine. Note that this also eliminates some fireflies.
			*/
			ctx.SetAttribute(
				i_layer_handle,
				NSI::DoubleArg("maximumvalue", 50));
		}
	}

	if (i_aov == "Ci" || i_aov == "outlines")
	{
		/* We only draw outlines on "Ci" and "outlines" AOV */
		ctx.SetAttribute(i_layer_handle,
			NSI::IntegerArg("drawoutlines", 1));
	}

	if (i_scalar_format == "uint8")
	{
		ctx.SetAttribute(i_layer_handle,
			NSI::StringArg("colorprofile", "srgb"));
	}

	// Decide whether to output ID AOVs in Cryptomatte format.
	unsigned cryptomatte_layers = 0;
	if (i_aov.substr(0, 3) == "id." &&
		i_variable_source == "builtin" &&
		(i_driver_name == "exr" || i_driver_handle == "dwaaexr"))
	{
		cryptomatte_layers = 2;
	}

	ctx.Connect(
		i_layer_handle, "", i_screen_handle, "outputlayers");

	ctx.Connect(
		i_driver_handle, "",
		i_layer_handle, "outputdrivers");

	if (cryptomatte_layers > 0)
	{
		// Change the filter and output type to fit the Cryptomatte format
		ctx.SetAttribute(
			i_layer_handle,
			(
				NSI::StringArg("layertype", "color"),
				NSI::IntegerArg("withalpha", 0),
				NSI::StringArg("filter", "cryptomatteheader")
				));

		/*
			Export one additional layer per Cryptomatte level. Each will
			output 2 values from those present in each pixel's samples.
		*/
		for (unsigned cl = 0; cl < cryptomatte_layers; cl++)
		{
			std::string cl_handle = i_layer_handle + std::to_string(cl);
			ctx.Create(cl_handle, "outputlayer");

			std::string cl_filter = "cryptomattelayer" + std::to_string(cl * 2);
			ctx.SetAttribute(
				cl_handle,
				(
					NSI::StringArg("variablename", i_aov),
					NSI::StringArg("layertype", "quad"),
					NSI::StringArg("scalarformat", "float"),
					NSI::IntegerArg("withalpha", 0),
					NSI::StringArg("filter", cl_filter),
					NSI::DoubleArg("filterwidth", i_filter_width),
					NSI::IntegerArg("sortkey", io_sort_key++),
					NSI::StringArg("variablesource", "builtin")
					));

			ctx.Connect(cl_handle, "", i_screen_handle, "outputlayers");

			ctx.Connect(i_driver_handle, "", cl_handle, "outputdrivers");
		}
	}
}


void RenderOptionsHook::ExportMultiLights(
	DL_SceneParser* parser,
	vector<BaseObject*> i_lights,
	const std::string& i_variable_source,
	const std::string& i_scalar_format,
	const std::string& i_layer_type,
	const std::string& i_aov,
	const std::string& i_screen_handle,
	const std::string& i_driver_handle,
	const std::string& i_driver_name,
	int i_index,
	unsigned& io_sort_key)
{
	BaseContainer* settings = parser->GetSettings();
	NSI::Context& ctx(parser->GetContext());

	int lights_size = (int)i_lights.size();
	string light_handle;
	string multilight_layer_handle;
	float filter_width = settings->GetFloat(DL_FILTER_WIDTH);
	string filter_output = getFilter(settings->GetInt32(DL_PIXEL_FILTER));

	for (int j = 0; j < lights_size; j++) {
		BaseList2D* passed_light = (BaseList2D*)i_lights[j];

		if (passed_light) {
			multilight_layer_handle = string("3dlfc4d::MultiLightDisplayLayer" + 
										std::to_string(i_index) + string("_") + std::to_string(j));

			ExportOneOutputLayer(parser, multilight_layer_handle, i_variable_source, i_scalar_format,
				m_variable_type, filter_output, i_aov, filter_width, (int)(i_aov == "Ci"),
				i_screen_handle, i_driver_handle, i_driver_name, io_sort_key);
		}

		light_handle = parser->GetHandleName(passed_light);

		//Use the handle of the parant transform of the light for light groups.
		if (passed_light && passed_light->GetType() != ID_LIGHTGROUP) {
			light_handle =
				string("X_") + light_handle;
		}
		ctx.Connect(light_handle, "", multilight_layer_handle, "lightset");
	}
}

/**
        This function is executed each time we render a scene on 3Delight
        Here we get the information from the render settings and according to
        the retrieved information we pass these values with on nsi based on its
   criteria.
*/
void RenderOptionsHook::CreateNSINodes(BaseDocument* doc, DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	BaseContainer* settings = parser->GetSettings();
	RENDER_MODE mode = parser->GetRenderMode();

	if (mode == DL_STANDIN_EXPORT) {
		return;
	}

	const CustomDataType* dt =
		settings->GetCustomDataType(DL_CUSTOM_AOV_LAYER, ID_CUSTOMDATATYPE_LAYERS);
	const CustomDataType* multi_light_dt = settings->GetCustomDataType(
			DL_CUSTOM_MULTI_LIGHT, ID_CUSTOMDATATYPE_MULTILIGHTS);
	iCustomDataTypeLayers* data = (iCustomDataTypeLayers*) (dt);
	iCustomDataTypeLights* multi_light_data =
		(iCustomDataTypeLights*) (multi_light_dt);

	if (!data) {
		data = NewObjClear(iCustomDataTypeLayers);
		(void) data->m_selected_layers.Append(String("RGBA (color + alpha)"));
		(void) data->m_selected_id.Append(50);
		(void) data->m_output_layer.Append(TRUE);
	}

	if (!multi_light_data) {
		multi_light_data = NewObjClear(iCustomDataTypeLights);
	}

	double samplingReduceFactor = true;
	bool use_displacement = true;
	bool use_subsurface = true;
	bool disable_image_layer = false;
	bool use_atmosphere = true;
	bool use_multiple_scattering = true;

	if (settings->GetBool(DL_ENABLE_INTERACTIVE_PREVIEW) == true && mode != C4D_PREVIEW)
	{
		int sampling = settings->GetInt32(DL_SAMPLING);

		if (sampling == DL_ONE_PERCENT) {
			samplingReduceFactor = 0.01;

		} else if (sampling == DL_FOUR_PERCENT) {
			samplingReduceFactor = 0.04;

		} else if (sampling == DL_TEN_PERCENT) {
			samplingReduceFactor = 0.10;

		} else if (sampling == DL_TWENTYFIVE_PERCENT) {
			samplingReduceFactor = 0.25;

		} else if (sampling == DL_HUNDRED_PERCENT) {
			samplingReduceFactor = 1;
		}

		use_displacement = !settings->GetBool(DL_DISABLE_DISPLACEMENT);
		use_subsurface = !settings->GetBool(DL_DISABLE_SUBSURFACE);
		disable_image_layer = settings->GetBool(DL_DISABLE_IMAGE_LAYER);
		use_atmosphere = !settings->GetBool(DL_DISABLE_ATMOSPHERE);
		use_multiple_scattering = !settings->GetBool(DL_DISABLE_MULTIPLE_SCATTER);
	}

	int shading_samples = max(
							  1,
							  (int) (settings->GetInt32(DL_SHADING_SAMPLES) * samplingReduceFactor + 0.5));
	int volume_samples = max(
							 1,
							 (int) (settings->GetInt32(DL_VOLUME_SAMPLES) * samplingReduceFactor + 0.5));
	int diffuse_depth = settings->GetInt32(DL_MAX_DIFFUSE_DEPTH);
	int reflection_depth = settings->GetInt32(DL_MAX_REFLECTION_DEPTH);
	int refraction_depth = settings->GetInt32(DL_MAX_REFRACTION_DEPTH);
	int hair_depth = settings->GetInt32(DL_MAX_HAIR_DEPTH);
	double max_distance = settings->GetFloat(DL_MAX_DISTANCE);
	ctx.SetAttribute(
		NSI_SCENE_GLOBAL,
		(NSI::IntegerArg("quality.shadingsamples", shading_samples),
		 NSI::IntegerArg("quality.volumesamples", volume_samples),
		 NSI::IntegerArg("maximumraydepth.diffuse", diffuse_depth),
		 NSI::IntegerArg("maximumraydepth.reflection", reflection_depth),
		 NSI::IntegerArg("maximumraydepth.refraction", refraction_depth),
		 NSI::IntegerArg("maximumraydepth.hair", hair_depth)));
	ctx.SetAttribute(NSI_SCENE_GLOBAL,
					 (NSI::DoubleArg("maximumraylength.specular", max_distance),
					  NSI::DoubleArg("maximumraylength.diffuse", max_distance),
					  NSI::DoubleArg("maximumraylength.reflection", max_distance),
					  NSI::DoubleArg("maximumraylength.refraction", max_distance),
					  NSI::DoubleArg("maximumraylength.hair", max_distance)));
	ctx.SetAttribute(NSI_SCENE_GLOBAL,
					 (NSI::IntegerArg("show.displacement", use_displacement),
					  NSI::IntegerArg("show.osl.subsurface", use_subsurface),
					  NSI::IntegerArg("show.atmosphere", use_atmosphere),
					  NSI::DoubleArg("show.multiplescattering",
									 (double) use_multiple_scattering)));

	int multilight_option = settings->GetInt32(DL_RENDER_MULTILIGHT);
	vector<BaseObject*> selected_lights; 
	if (multilight_option != DL_RENDER_MULTILIGHT_OFF)
	{
		vector<Int32> selected_lights_guid;
		for (int i = 0; i < multi_light_data->m_selected_lights_itemID.GetCount(); i++)
			selected_lights_guid.push_back(multi_light_data->m_selected_lights_itemID[i]);

		//Getting selected objects
		Int lights_number = multi_light_data->m_all_multi_lights.GetCount();
		selected_lights = getSelectedLights(doc, selected_lights_guid, lights_number, multilight_option);
	}

	string filter_output = getFilter(settings->GetInt32(DL_PIXEL_FILTER));
	float filter_width = settings->GetFloat(DL_FILTER_WIDTH);
	int m_layer_number = (int) data->m_selected_layers.GetCount();
	unsigned sortkey = 0;
	auto layeroutput = OutputLayer(parser);
	m_extension = get<0>(layeroutput);
	m_output_format = get<1>(layeroutput);
	m_scalar_format = get<2>(layeroutput);
	bool render_to_display = (settings->GetBool(DISPLAY_RENDERED_IMAGES) || settings->GetBool(SAVE_AND_DISPLAY));
	bool render_to_file = settings->GetBool(SAVE_RENDERED_IMAGES) || settings->GetBool(SAVE_AND_DISPLAY);
	bool render_to_jpeg = settings->GetBool(SAVE_ADDITIONAL_JPEG);
	bool export_to_nsi = false;
	BaseTime t = doc->GetTime();
	long frame = t.GetFrame(doc->GetFps());
	// Generate zero padded frame number
	ostringstream ss;
	ss << std::setw(4) << std::setfill('0') << frame;
	string framenumber_ext = "_" + ss.str();

	// Check if we should render to display
	if (mode == C4D_PREVIEW || mode == C4D_FINAL || mode == DL_BATCH) {
		render_to_display = false;
	}

	// Check if we should render to file
	if (mode == C4D_PREVIEW || mode == C4D_FINAL || mode == DL_IPR) {
		render_to_file = false;
	}

	if (mode == DL_EXPORT_NSI) {
		export_to_nsi = true;
		render_to_display = false;
	}

	if (settings->GetBool(OUTPUT_NSI_FILES)) {
		export_to_nsi = true;
		render_to_display = false;
		render_to_file = false;
	}

	Filename fn = settings->GetFilename(DL_DEFAULT_IMAGE_FILENAME);

	if (!fn.IsPopulated()) {
		render_to_file = false;
	}

	fn.ClearSuffix();

	if (!render_to_file) {
		render_to_jpeg = false;
	}

	bool multilayer = true;
	Int32 fileformat = settings->GetInt32(DL_DEFAULT_IMAGE_FORMAT);

	if (fileformat == DL_FORMAT_PNG) { // Only output multilayer file if the
		// fileformat supports this
		multilayer = false;
	}

	m_file_driver_handle = "3dlfc4d::multilayerfileDriver";

	if (multilayer && (render_to_file || export_to_nsi)) { // create output driver for multilayer file
		std::string filename = StringToStdString(fn.GetString());
		filename = filename + framenumber_ext + m_extension;
		ctx.Create(m_file_driver_handle, "outputdriver");
		ctx.SetAttribute(m_file_driver_handle,
						 (NSI::StringArg("drivername", m_output_format.c_str()),
						  NSI::StringArg("imagefilename", filename.c_str())));
	}

	// create iDisplay output driver
	string displaydriver_filename;

	if (fn.IsPopulated()) {
		displaydriver_filename = StringToStdString(fn.GetFileString());

	} else {
		Filename docname = doc->GetDocumentName();
		displaydriver_filename = StringToStdString(docname.GetFileString());
	}

	std::string idisplay_driver = "idisplay";

	if (render_to_display) {
		m_display_driver_handle = "displayDriver";
		ctx.Create(m_display_driver_handle, "outputdriver");
		ctx.SetAttribute(m_display_driver_handle,
						 (NSI::StringArg("drivername", idisplay_driver),
						  NSI::StringArg("imagefilename", displaydriver_filename + framenumber_ext)));
	}

	for (int i = 0; i < m_layer_number; i++) {
		Bool is_layer_active = data->m_output_layer[i];
		DL_LayersData LayerData = GetLayerData(data->m_selected_layers[i]);
		m_aov = LayerData.varName;
		m_variable_source = LayerData.varSource;
		m_aovname = LayerData.shortName;
		m_variable_type = LayerData.varType;
		bool IsShadingComponent = LayerData.isShadingComponent;
		int with_alpha = 0;

		if (m_aov == "Ci") { // Only export alpha for the RGBA layer
			with_alpha = 1;
		}

		string aov_ext = "_" + m_aovname;

		if (m_layer_number == 1 && m_aov == "Ci") { // Don't add aov name to file name if we are only
			// rendering a single RGBA (Ci) layer
			aov_ext = "";
		}

		// Render only RGBA if Disable Extra Image Layers toggle is selected
		if (disable_image_layer && m_aov != "Ci") {
			continue;
		}
		std::string screen_handle = "3dlfc4d::scene_camera_screen";
		//-------------------------------------------
		//     Render layer to JPEG
		//-------------------------------------------
		if (is_layer_active && render_to_jpeg && m_aov == "Ci") {
			m_layer_jpg = string("3dlfc4d::jpg_outputlayer") + std::to_string(i);

			string output_driver_handle =
					string("3dlfc4d::jpg_driver") + std::to_string(i);

			ctx.Create(output_driver_handle, "outputdriver");
			std::string filename = StringToStdString(fn.GetString());
			filename = filename + aov_ext + framenumber_ext + ".jpg";
			ctx.SetAttribute(output_driver_handle,
				(
					NSI::StringArg("drivername", "jpeg"),
					NSI::StringArg("imagefilename", filename.c_str()))
				);

			ExportOneOutputLayer(parser, m_layer_jpg, m_variable_source, "uint8",
				m_variable_type, filter_output, m_aov, filter_width, with_alpha,
				screen_handle, output_driver_handle, "jpeg", sortkey);
			
			// Multi light.
			// Only export multilight layers for shading components
			if (IsShadingComponent) {
				ExportMultiLights(parser, selected_lights, m_variable_source, "uint8", m_variable_type,
					m_aov, screen_handle, output_driver_handle, "jpeg", i, sortkey);
			}
		}

		//-------------------------------------------
		//     Render layer to file
		//-------------------------------------------
		if (is_layer_active && (render_to_file || export_to_nsi)) {
			string filedriver;

			if (multilayer) { // Render to existing multilayer driver
				filedriver = m_file_driver_handle;

			} else { // Create new driver for this layer
				filedriver = "3dlfc4d::file_outputdriver" + std::to_string(i);
				std::string filename = StringToStdString(fn.GetString());
				filename = filename + aov_ext + framenumber_ext + m_extension;
				ctx.Create(filedriver, "outputdriver");
				ctx.SetAttribute(filedriver,
								 (NSI::StringArg("drivername", m_output_format.c_str()),
								  NSI::StringArg("imagefilename", filename.c_str())));
			}

			m_layer_file = string("3dlfc4d::file_outputlayer") + std::to_string(i);
			ExportOneOutputLayer(parser, m_layer_file, m_variable_source, m_scalar_format,
				m_variable_type, filter_output, m_aov, filter_width, with_alpha,
				screen_handle, filedriver, m_output_format, sortkey);

			// Export multilight layers for shading components
			if (IsShadingComponent) {
				ExportMultiLights(parser, selected_lights, m_variable_source, m_scalar_format, m_variable_type,
					m_aov, screen_handle, filedriver, m_output_format, i, sortkey);

			}
		}

		//-------------------------------------------
		//     Render layer to iDislay
		//-------------------------------------------
		if ((is_layer_active && render_to_display)) {
			m_layer_handle = string("3dlfc4d::DisplayLayer") + std::to_string(i);

			ExportOneOutputLayer(parser, m_layer_handle, m_variable_source, m_scalar_format,
				m_variable_type, filter_output, m_aov, filter_width, with_alpha,
				screen_handle, m_display_driver_handle, m_output_format, sortkey);

			// Export multilight layers for shading components
			if (IsShadingComponent) {
				ExportMultiLights(parser, selected_lights, m_variable_source, m_scalar_format, m_variable_type,
					m_aov, screen_handle, m_display_driver_handle, m_output_format, i, sortkey);
			}

			// Custom AOVs
			std::vector<BaseObject*> getCustomAvs = GetCustomAOV(doc);

			for (size_t k = 0; k < getCustomAvs.size(); k++) {
				BaseContainer* aovs_data = getCustomAvs[k]->GetDataInstance();
				const CustomDataType* aov_dt =
					aovs_data->GetCustomDataType(DL_CUSTOM_COLORS, ID_CUSTOMDATATYPE_AOV);
				iCustomDataTypeCOLORAOV* aov_data = (iCustomDataTypeCOLORAOV*) aov_dt;

				if (!aov_data) {
					aov_data = NewObjClear(iCustomDataTypeCOLORAOV);
				}

				for (int j = 0; j < aov_data->m_row_id.GetCount(); j++) {
					if (aov_data->aov_textName[j].IsEqual(""_s) || aov_data->aov_checked[j] == FALSE) {
						continue;
					}
					String layer = (String) aov_data->aov_textName[j];
					std::string custom_layer = string(
												   "3dlfc4d::CustomLayer " + (std::string) layer.GetCStringCopy() + std::to_string(i) + string("_") + std::to_string(j));

					ExportOneOutputLayer(parser, custom_layer, "shader", m_scalar_format,
						"color", filter_output, layer.GetCStringCopy(), filter_width, with_alpha,
						screen_handle, m_display_driver_handle, m_output_format, sortkey);
				}
			}
		}
	}
}

void RenderOptionsHook::ConnectNSINodes(BaseDocument* doc, DL_SceneParser* parser)
{
	return;
}
