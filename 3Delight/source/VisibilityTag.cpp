#include "IDs.h"
#include "c4d.h"
#include "RegisterPrototypes.h"
#include "tvisibility.h"

class DL_VisibilityTag : public TagData
{
public:
	virtual Bool Init(GeListNode* node);

	static NodeData* Alloc(void)
	{
		return NewObjClear(DL_VisibilityTag);
	}
};

Bool DL_VisibilityTag::Init(GeListNode* node)
{
	BaseTag* tag = (BaseTag*) node;
	BaseContainer* data = tag->GetDataInstance();
	data->SetBool(VISIBLE_TO_CAMERA, true);
	data->SetBool(VISIBLE_IN_DIFFUSE, true);
	data->SetBool(VISIBLE_IN_REFLECTION, true);
	data->SetBool(VISIBLE_IN_REFRACTION, true);
	data->SetBool(CASTS_SHADOWS, true);
	data->SetInt32(COMPOSITING_OPTIONS, COMPOSITING_REGULAR_OPTION);
	return TRUE;
}

Bool RegisterDL_VisibilityTag(void)
{
	return RegisterTagPlugin(ID_DL_VISIBILITYTAG,
							 "Geometry Properties"_s,
							 TAG_VISIBLE,
							 DL_VisibilityTag::Alloc,
							 "tvisibility"_s,
							 AutoBitmap("compositing.png"_s),
							 0);
}
