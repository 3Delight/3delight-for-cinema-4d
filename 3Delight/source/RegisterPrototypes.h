#ifndef RegisterPrototypes_h
#define RegisterPrototypes_h

#include "ge_sys_math.h"

// Bool RegisterInteractiveRenderManager(void);
// Bool RegisterImageLayer(void);
Bool RegisterDL_CameraTag(void);
Bool RegisterDL_MotionBlurTag(void);
// Bool RegisterDL_SubdivisionSurfaceTag(void);

Bool RegisterDL_VisibilityTag(void);
Bool RegisterDL_SubdivisionSurfaceTag(void);
Bool Register3DelightPlugin(void);
Bool Register3DelightCloudPlugin(void);
Bool RegisterCustomListView(void);
Bool RegisterCustomMultiLight(void);
Bool RegisterInteractiveRenderingStart(void);
Bool RegisterInteractiveRenderingStop(void);
Bool RegisterInteractiveRenderManager(void);
Bool RegisterRenderManager(void);
Bool Register3DelightCommand(void);
Bool Register3DelightBatchRenderCommand(void);
Bool RegisterNSIExportCommand(void);
Bool RegisterCreateShelf(void);
Bool RegisterAbout(void);
Bool RegisterHelp(void);
Bool RegisterPreferences();
Bool RegisterStandinNode();
Bool RegisterStandInExport();

#endif
