#include "IDs.h"
#include "c4d.h"
#include "RegisterPrototypes.h"

class CreateShelf : public CommandData
{
public:
	virtual Bool Execute(BaseDocument* doc, GeDialog* parentManager);
};

Bool CreateShelf::Execute(BaseDocument* doc, GeDialog* parentManager)
{
	const Filename file =
		GeGetPluginPath() + Filename("Layout") + Filename("3Delight_shelf.l4d");
	Bool result = LoadFile(file);

	// check the result.
	if (!result) {
		DiagnosticOutput("Failed to load the 3Delight shelf");
	}

	// EventAdd();
	return true;
}

Bool RegisterCreateShelf(void)
{
	return RegisterCommandPlugin(ID_CREATE_SHELF,
								 "Create 3Delight Shelf"_s,
								 PLUGINFLAG_HIDEPLUGINMENU,
								 0,
								 String(),
								 NewObjClear(CreateShelf));
}
