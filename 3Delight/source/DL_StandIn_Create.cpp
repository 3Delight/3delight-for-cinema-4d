#include "IDs.h"
#include "c4d.h"
#include "c4d_symbols.h"
#include "RegisterPrototypes.h"

class DL_StandInNode : public ObjectData
{
	INSTANCEOF(DL_StandInNode, MaterialData)

private:
public:
	virtual Bool Init(GeListNode* node);
	virtual INITRENDERRESULT InitRender(BaseMaterial* mat,
										const InitRenderStruct& irs);
	static NodeData* Alloc(void)
	{
		return NewObjClear(DL_StandInNode);
	}
};

Bool DL_StandInNode::Init(GeListNode* node)
{
	return true;
}

INITRENDERRESULT
DL_StandInNode::InitRender(BaseMaterial* mat, const InitRenderStruct& irs)
{
	return INITRENDERRESULT::OK;
}

Bool RegisterStandinNode(void)
{
	return RegisterObjectPlugin(STAND_IN_NODE_CREATE,
								"Create"_s,
								OBJECT_GENERATOR | PLUGINFLAG_HIDEPLUGINMENU,
								DL_StandInNode::Alloc,
								"dl_standin"_s,
								0,
								0);
}
