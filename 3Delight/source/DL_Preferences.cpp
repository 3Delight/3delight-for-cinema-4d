#include "dl_preferences.h"
#include "IDs.h"
#include "c4d_basedocument.h"
#include "c4d_customdatatypeplugin.h"
#include "c4d_general.h"
#include "lib_prefs.h"
#include "RegisterPrototypes.h"

// Unique plugin ID for world preference container obtained from
// www.plugincafe.com

class DL_Preferences : public PrefsDialogObject
{
	INSTANCEOF(DL_Preferences, PrefsDialogObject)

public:
	static NodeData* Alloc()
	{
		return NewObjClear(DL_Preferences);
	}

	virtual Bool Init(GeListNode* node)
	{
		InitValues(DL_RENCER_VIEW);
		InitValues(DL_LIVE_RENDER_COARSENESS);
		InitValues(DL_SCANNING);
		InitValues(DL_PROGRESSIVE_REFINEMENT);
		return true;
	}

	virtual Bool InitValues(const DescID& id, Description* description = nullptr)
	{
		// Retrieves our container inside the world Container
		BaseContainer* bc = GetMyPreference();

		if (bc == nullptr) {
			return false;
		}

		switch (id[0].id) {
		case DL_RENCER_VIEW: {
			InitPrefsValue(
				DL_RENCER_VIEW, GeData(DELIGHT_DISPLAY_VIEW), description, id, bc);
			break;
		}

		case DL_LIVE_RENDER_COARSENESS: {
			InitPrefsValue(
				DL_LIVE_RENDER_COARSENESS, GeData(TWO_PIXELS), description, id, bc);
			break;
		}

		case DL_SCANNING: {
			InitPrefsValue(DL_SCANNING, GeData(CIRCLE), description, id, bc);
			break;
		}

		case DL_PROGRESSIVE_REFINEMENT: {
			InitPrefsValue(
				DL_PROGRESSIVE_REFINEMENT, GeData(FALSE), description, id, bc);
			break;
		}
		}

		return true;
	}

	virtual Bool GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags)
	{
		if (!description->LoadDescription(PREFERENCES_ID)) {
			return false;
		}

		// Checks if the default value have been asked by a right click ->
		// "default value"
		if (flags & DESCFLAGS_DESC::NEEDDEFAULTVALUE) {
			InitValues(DL_RENCER_VIEW, description);
			InitValues(DL_LIVE_RENDER_COARSENESS, description);
			InitValues(DL_SCANNING, description);
			InitValues(DL_PROGRESSIVE_REFINEMENT, description);
		}

		flags |= DESCFLAGS_DESC::LOADED;
		return SUPER::GetDDescription(node, description, flags);
	}

	virtual Bool GetDParameter(GeListNode* node,
							   const DescID& id,
							   GeData& t_data,
							   DESCFLAGS_GET& flags)
	{
		// Retrieves our container inside the world Container
		BaseContainer* bc = GetMyPreference();

		if (bc == nullptr) {
			return SUPER::GetDParameter(node, id, t_data, flags);
		}

		switch (id[0].id) {
		case DL_RENCER_VIEW:
		case DL_LIVE_RENDER_COARSENESS:
		case DL_SCANNING:
		case DL_PROGRESSIVE_REFINEMENT: {
			t_data = bc->GetInt32(id[0].id);
			flags |= DESCFLAGS_GET::PARAM_GET;
			return true;
			break;
		}
		}

		return SUPER::GetDParameter(node, id, t_data, flags);
	}

	virtual Bool SetDParameter(GeListNode* node,
							   const DescID& id,
							   const GeData& t_data,
							   DESCFLAGS_SET& flags)
	{
		// Retrieves our container inside the world Container
		BaseContainer* bc = GetMyPreference();

		if (bc == nullptr) {
			return SUPER::SetDParameter(node, id, t_data, flags);
		}

		switch (id[0].id) {
		case DL_RENCER_VIEW:
		case DL_LIVE_RENDER_COARSENESS:
		case DL_SCANNING:
		case DL_PROGRESSIVE_REFINEMENT: {
			bc->SetInt32(id[0].id, t_data.GetInt32());
			flags |= DESCFLAGS_SET::PARAM_SET;
			GeUpdateUI();
			return true;
			break;
		}
		}

		return SUPER::SetDParameter(node, id, t_data, flags);
		;
	}

	virtual Bool GetDEnabling(GeListNode* node,
							  const DescID& id,
							  const GeData& t_data,
							  DESCFLAGS_ENABLE flags,
							  const BaseContainer* itemdesc)
	{
		return SUPER::GetDEnabling(node, id, t_data, flags, itemdesc);
	}

private:
	BaseContainer* GetMyPreference()
	{
		// Retrieves our container inside the world Container
		BaseContainer* bc =
			GetWorldContainerInstance()->GetContainerInstance(PREFERENCES_ID);

		// Doesn't exist ? Create it
		if (!bc) {
			GetWorldContainerInstance()->SetContainer(PREFERENCES_ID,
					BaseContainer());
			bc = GetWorldContainerInstance()->GetContainerInstance(PREFERENCES_ID);

			if (!bc) {
				return nullptr;
			}
		}

		return bc;
	}
};

Bool RegisterPreferences(void)
{
	iferr_scope;

	if (!PrefsDialogObject::Register(PREFERENCES_ID,
									 DL_Preferences::Alloc,
									 "3Delight Preferences"_s,
									 "dl_preferences",
									 0,
									 0)) {
		return false;
	}

	return true;
}
