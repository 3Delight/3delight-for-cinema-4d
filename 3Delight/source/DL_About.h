#pragma once

#include "c4d.h"

class AboutDialog : public GeDialog
{
public:
	virtual Bool CreateLayout();
};

class DL_About : public CommandData
{
public:
	virtual Bool Execute(BaseDocument* doc, GeDialog* parentManager);

private:
	AboutDialog dialog;
};
