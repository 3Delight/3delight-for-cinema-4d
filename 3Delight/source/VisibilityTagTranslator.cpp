#include "VisibilityTagTranslator.h"
#include "nsi.hpp"
#include "tvisibility.h"
using namespace std;

void VisibilityTagTranslator::CreateNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	BaseList2D* obj = (BaseList2D*) C4DNode;
	BaseContainer* data = obj->GetDataInstance();

	bool camera_visibility = data->GetBool(VISIBLE_TO_CAMERA);
	bool diffuse_visibility = data->GetBool(VISIBLE_IN_DIFFUSE);
	bool reflection_visibility = data->GetBool(VISIBLE_IN_REFLECTION);
	bool refraction_visibility = data->GetBool(VISIBLE_IN_REFRACTION);
	bool casts_shadows = data->GetBool(CASTS_SHADOWS);
	int compositing_option = data->GetInt32(COMPOSITING_OPTIONS);
	bool matte_object = compositing_option == COMPOSITING_MATTE_OPTION;
	bool prelit_object = compositing_option == COMPOSITING_PRELIT_OPTION;

	string attributes_handle = string(Handle);
	ctx.Create(attributes_handle, "attributes");
	ctx.SetAttribute(
		attributes_handle,
		(NSI::IntegerArg("visibility.camera", camera_visibility), //,
		 NSI::IntegerArg("visibility.diffuse", diffuse_visibility),
		 NSI::IntegerArg("visibility.reflection", reflection_visibility),
		 NSI::IntegerArg("visibility.refraction", refraction_visibility),
		 NSI::IntegerArg("visibility.shadow", casts_shadows),
		 NSI::IntegerArg("matte", matte_object),
		 NSI::IntegerArg("prelit", prelit_object)
		));
	ctx.Connect(
		attributes_handle, "", ParentTransformHandle, "geometryattributes");
}