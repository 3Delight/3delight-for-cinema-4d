#include <iomanip>
#include <sstream>
#include "3DelightRenderer.h"
#include "DL_TypeConversions.h"
#include "ExpandFilename.h"
#include "IDs.h"
#include "SceneParser.h"
#include "c4d.h"
#include "nsi.hpp"
#include "RegisterPrototypes.h"

using namespace std;

class DL_ExportStandInNode : public CommandData
{
public:
	virtual Bool Execute(BaseDocument* doc, GeDialog* parentManager);
};

Bool DL_ExportStandInNode::Execute(BaseDocument* doc, GeDialog* parentManager)
{
	RenderData* rd = doc->GetActiveRenderData();
	BaseContainer render_data = rd->GetData();
	BaseTime t = doc->GetTime();
	Filename outfile;
	outfile.FileSelect(
		FILESELECTTYPE::ANYTHING, FILESELECT::SAVE, String(""), String("nsi"));
	outfile.ClearSuffix();
	String filepart = outfile.GetFileString();
	outfile.SetFile(Filename(filepart));
	outfile.SetSuffix(String("nsi"));
	BaseDocument* renderdoc =
		(BaseDocument*) doc->GetClone(COPYFLAGS::DOCUMENT, nullptr);
	SceneParser* parser = new SceneParser(renderdoc);
	string filename_string = StringToStdString(outfile.GetString());
	NSI::ArgumentList arglist;
	arglist.Add(new NSI::StringArg("type", "apistream"));
	arglist.Add(new NSI::StringArg("streamfilename", filename_string));
	arglist.Add(new NSI::StringArg("streamformat", "nsi"));
	parser->GetContext().Begin(arglist);
	parser->SetRenderMode(DL_STANDIN_EXPORT);
	// Render scene.
	long frame = t.GetFrame(doc->GetFps());
	bool RenderOK = parser->InitScene(true, frame);
	parser->SampleFrameMotion(frame);
	BaseDocument::Free(renderdoc);
	delete parser;
	return true;
}

Bool RegisterStandInExport(void)
{
	return RegisterCommandPlugin(STANDIN_EXPORT,
								 "Export..."_s,
								 PLUGINFLAG_HIDEPLUGINMENU,
								 0,
								 String("Export NSI File"_s),
								 NewObjClear(DL_ExportStandInNode));
}
