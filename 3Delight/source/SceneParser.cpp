#include "SceneParser.h"
#include "DL_API.h"
#include "DL_TypeConversions.h"
#include "PluginManager.h"
#include "tmotionsamples.h"
#include "IDs.h"
#include "dlrendersettings.h"
#include <vector>
#include <stdint.h>
#include <string>
#include <sstream>
#include <algorithm>
#include "maxon/stringencoding.h"
#include <math.h>
#include "NSIAPI.h"
#include "tcamera.h"
#include <unordered_set>
#include "NodeIdHash.h"

// Human readable node names via mnemonic hashes.
#include "mnemonic.h"

using namespace std;

extern PluginManager PM;

SceneParser::~SceneParser() {}

SceneParser::SceneParser(BaseDocument* document)
	: context(NSIAPI::Instance().API()), doc(document), rendermode(DL_INTERACTIVE)
{
	hasCustomResolution = false;
	CustomResolutionHeight = 0;
	CustomResolutionWidth = 0;
	n_nodes = 0;
}

void SceneParser::GetNodesAndTransforms(std::vector<Node>& nodes,
	std::vector<Transform>& transforms,
	bool animate)
{
	nodes.clear();
	transforms.clear();
	cacheData.clear();
	// Traverse objects (and their tags)
	HierarchyData hdata;
	hdata.parent_transform = ".root";
	hdata.cachePos = 0;
	hdata.isVisible = true;
	hdata.deformationSamples = 2;
	hdata.transformSamples = 2;
	// StopAllThreads();
	doc->ExecutePasses(
		GeGetCurrentThread(), animate, true, true, BUILDFLAGS::EXTERNALRENDERER);
	int count = 0;
	TraverseObjects(doc->GetFirstObject(), hdata, nodes, transforms, count);
	// Traverse materials
	BaseMaterial* mat = doc->GetFirstMaterial();
	HierarchyData mat_hdata;
	mat_hdata.cachePos = 0;
	mat_hdata.deformationSamples = 0;
	mat_hdata.transformSamples = 0;
	mat_hdata.parent_transform = "";
	mat_hdata.isVisible = true;

	while (mat) {
		// Create node for material.
		Node n(mat);
		n.hdata = mat_hdata;
		n.handle = GetHandleName((BaseList2D*)mat);
		DL_Translator* translator = n.GetTranslator();

		if (translator) {
			nodes.push_back(n);
		}

		// Traverse sub-shaders.
		BaseShader* shader = mat->GetFirstShader();
		TraverseShaders(shader, nodes);
		mat = mat->GetNext();
	}

	n_nodes = nodes.size();

	// Init all translators.
	for (long i = 0; i < (long)nodes.size(); i++) {
		DL_Translator* translator = nodes[i].GetTranslator();

		if (translator) {
			translator->Init(nodes[i].GetC4DNode(), doc, this);
		}
	}
}

RENDER_MODE
SceneParser::GetRenderMode()
{
	return rendermode;
}

void SceneParser::SetCustomRenderResolution(int width, int height)
{
	hasCustomResolution = true;
	CustomResolutionWidth = width;
	CustomResolutionHeight = height;
}

void SceneParser::GetRenderResolution(int* width, int* height)
{
	// Use custom resolution if available, otherwise, get resolution
	// from render settings.
	if (hasCustomResolution) {
		*width = CustomResolutionWidth;
		*height = CustomResolutionHeight;

	}
	else {
		RenderData* rd = doc->GetActiveRenderData();
		BaseContainer* render_data = rd->GetDataInstance();
		*width = render_data->GetInt32(RDATA_XRES);
		*height = render_data->GetInt32(RDATA_YRES);
	}
}

NSI::Context&
SceneParser::GetContext()
{
	return context;
}

void SceneParser::FillRenderSettings()
{
	if (!doc) {
		return;
	}

	RenderData* rd = doc->GetActiveRenderData();
	BaseVideoPost* vp = rd->GetFirstVideoPost();
	bool has_vp = false;

	while (vp != NULL && !has_vp) {
		// Look for rendersettings.
		has_vp = (vp->GetType() == ID_RENDERSETTINGS);

		if (!has_vp) {
			vp = vp->GetNext();
		}
	}

	// If there are not rendersettings in the document, create them.
	if (!has_vp) {
		BaseVideoPost* pvp = BaseVideoPost::Alloc(ID_RENDERSETTINGS);
		rd->InsertVideoPostLast(pvp);
		vp = rd->GetFirstVideoPost();

		while (vp != NULL && !has_vp) {
			// Look for rendersettings.
			has_vp = (vp->GetType() == ID_RENDERSETTINGS);

			if (!has_vp) {
				vp = vp->GetNext();
			}
		}
	}

	// EventAdd();
	if (has_vp) {
		settings = vp->GetData();
	}
}

bool SceneParser::InitScene(bool animate, long _frame)
{
	if (!doc) {
		return false;
	}

	FillRenderSettings();
	long fps = doc->GetFps();

	if (animate) {
		AnimateDoc(BaseTime(_frame, fps));
	}

	// Create nodes and transforms.
	vector<Node> nodes;
	vector<Transform> transforms;
	GetNodesAndTransforms(nodes, transforms, animate);
	// Create hooks
	vector<DL_HookPtr> hooks = PM.GetHooks();

	// --------------------------------------//
	// Create NSI nodes
	// --------------------------------------//
	// Transforms.
	for (long i = 0; i < (long)transforms.size(); i++) {
		transforms[i].CreateNodes(doc, this);
	}

	// Nodes (after transforms – important!).
	for (long i = 0; i < (long)nodes.size(); i++) {
		DL_Translator* translator = nodes[i].GetTranslator();

		if (translator) {
			HierarchyData hdata = nodes[i].hdata;
			string transform_handle = "X0_" + nodes[i].handle;
			translator->CreateNSINodes(nodes[i].handle.c_str(),
				hdata.parent_transform.c_str(),
				nodes[i].GetC4DNode(),
				doc,
				this);
		}
	}

	// Hooks.
	for (long i = 0; i < (long)hooks.size(); i++) {
		hooks[i]->Init(doc, this);
		hooks[i]->CreateNSINodes(doc, this);
	}

	// --------------------------------------//
	// Make connections
	// --------------------------------------//
	// Nodes.
	for (long i = 0; i < (long)nodes.size(); i++) {
		DL_Translator* translator = nodes[i].GetTranslator();

		if (translator) {
			string transform_handle = "X0_" + nodes[i].handle;
			// translator->ConnectNSINodes(nodes[i].handle.c_str(),
			// transform_handle.c_str(), nodes[i].GetC4DNode(), doc, this);
			translator->ConnectNSINodes(nodes[i].handle.c_str(),
				nodes[i].hdata.parent_transform.c_str(),
				nodes[i].GetC4DNode(),
				doc,
				this);
		}
	}

	// Hooks.
	for (long i = 0; i < (long)hooks.size(); i++) {
		hooks[i]->ConnectNSINodes(doc, this);
	}

	return true;
}

// Get "binned" sample number for variable number of motion samples.
// Return the sample number, or 0 if the object should not be sampled
// It is assumed that max_samples>=max_object_samples
static int GetObjectSampleNumber(
	int sample, int max_samples, int max_object_samples)
{
	// Handle this simple case first.
	if (max_object_samples == max_samples) {
		return sample;
	}

	// Motion blur disabled for this object
	if (max_object_samples == 1) {
		if (sample == 1) {
			return 1;

		}
		else {
			return 0;
		}
	}

	// First sample.
	if (sample == 1) {
		return 1;
	}

	// Last sample.
	else if (max_object_samples > 1 && sample == max_samples) {
		return max_object_samples;

	}
	else {
		for (int i = 1; i <= max_object_samples; i++) {
			double t = double(i) / double(max_object_samples);
			t = t * double(max_samples);
			int rounded = int(t);

			if (t == sample) {
				return i;
			}
		}
	}

	// Don't sample this object at "sample".
	return 0;
}

void SceneParser::SampleFrameMotion(long _frame)
{
	FillRenderSettings();
	// Motion sampling settings
	bool useMotionBlur = settings.GetBool(DL_MOTION_BLUR);

	if (useMotionBlur && settings.GetBool(DL_ENABLE_INTERACTIVE_PREVIEW)) {
		useMotionBlur = !settings.GetBool(DL_DISABLE_MOTION_BLUR);
	}

	// Default, unless set from the CameraTag.
	double ShutterAngle = 0.5;
	// Get scene camera.
	BaseDraw* bd = doc->GetActiveBaseDraw();
	CameraObject* camera = (CameraObject*)bd->GetSceneCamera(doc);

	if (camera) {
		BaseTag* cameratag = camera->GetTag(ID_DL_CAMERATAG);

		if (cameratag) {
			BaseContainer* tag_data = cameratag->GetDataInstance();
			ShutterAngle = maxon::RadToDeg(tag_data->GetFloat(SHUTTER_ANGLE))
				/ 360.0;
		}
	}

	long fps = doc->GetFps();
	double shutterOpen_t = double(_frame) / double(fps);
	double shutterClose_t = (double(_frame) + ShutterAngle) / double(fps);
	// Create nodes and transforms
	vector<Node> nodes;
	vector<Transform> transforms;
	BaseTime onFrame = BaseTime(_frame, fps);
	AnimateDoc(onFrame);
	GetNodesAndTransforms(nodes, transforms, true);
	// Create hooks
	vector<DL_HookPtr> hooks = PM.GetHooks();

	for (long i = 0; i < (long)hooks.size(); i++) {
		hooks[i]->Init(doc, this);
	}

	// Get maximum number of motion samples for any object in the scene.
	int max_samples = 2;

	for( const Transform &trs : transforms )
	{
		if (trs.motionSamples > max_samples) {
			max_samples = trs.motionSamples;
		}
	}

	for( const Node &n : nodes )
	{
		if (n.hdata.deformationSamples > max_samples) {
			max_samples = n.hdata.deformationSamples;
		}
	}

	if (!useMotionBlur) {
		max_samples = 1;
	}

	DL_SampleInfo info;
	info.samples_max = max_samples;
	info.shutter_open_time = shutterOpen_t;
	info.shutter_close_time = shutterClose_t;

	for (int s = 1; s <= max_samples; s++) {
		double t = 0;

		if (max_samples > 1) {
			t = double(s - 1) / double(max_samples - 1);
		}

		info.sample = s;
		info.sample_time = (1.0 - t) * shutterOpen_t + (t)*shutterClose_t;

		if (s == 1) {
			// Set first motion sample to be exactly on the frame, to
			// avoid minor numerical issues associated with BaseTime.
			AnimateDoc(onFrame);

		}
		else {
			AnimateDoc(BaseTime(info.sample_time));
		}

		GetNodesAndTransforms(nodes, transforms, true);
		int sample_number;
		int object_samples;

		// TODO: Add options for controlling the number of samples for hooks.
		for (size_t i = 0; i < hooks.size(); i++) {
			int hook_samples = 2;

			if (max_samples == 1) {
				hook_samples = 1;
			}

			sample_number = GetObjectSampleNumber(s, max_samples, hook_samples);

			if (sample_number > 0) {
				info.sample = sample_number;
				info.samples_max = hook_samples;
				hooks[i]->SampleAttributes(&info, doc, this);
			}
		}

		for (size_t i = 0; i < transforms.size(); i++) {
			object_samples = transforms[i].motionSamples;

			if (object_samples > max_samples) {
				object_samples = max_samples;
			}

			sample_number = GetObjectSampleNumber(s, max_samples, object_samples);

			if (sample_number > 0) { // Should the object transform be sampled at this time?
				info.sample = sample_number;
				info.samples_max = object_samples;
				transforms[i].SampleAttributes(&info, doc, this);
			}
		}

		DL_Translator* translator;

		for (long i = 0; i < (long)nodes.size(); i++) {
			translator = nodes[i].GetTranslator();
			BaseList2D* c4d_node = nodes[i].GetC4DNode();

			if (translator && c4d_node) {
				object_samples = nodes[i].hdata.deformationSamples;

				if (object_samples > max_samples) {
					object_samples = max_samples;
				}

				sample_number = GetObjectSampleNumber(s, max_samples, object_samples);

				if (sample_number > 0) { // Should the object deformation be
					// sampled at this time?
					info.sample = sample_number;
					info.samples_max = object_samples;
					translator->SampleAttributes(
						&info, nodes[i].handle.c_str(), c4d_node, doc, this);
				}
			}
		}
	}
}

/*bool SceneParser::IsDirty(BaseList2D* node) {
		if (!node) { return false; }

		bool dirty = false;

		UInt32 dirty_checksum = node->GetDirty(DIRTYFLAGS::ALL);
		auto it = dirtystates.find(n->handle);

		if (it == dirtystates.end()) {
				dirty = true;
		}
		else if (it->second != dirty_checksum) {
				dirty = true;
		}

		dirtystates[n->handle] = dirty_checksum; // Touch this node

		return dirty;
}*/

void SceneParser::TouchDirty(Node* n)
{
	if (!n) {
		return;
	}

	BaseList2D* c4d_node = n->GetC4DNode();

	if (!c4d_node) {
		return;
	}

	UInt32 dirty_checksum;

	if (c4d_node->IsInstanceOf(Obase)) {
		BaseObject* obj = (BaseObject*)c4d_node;
		BaseObject* CacheParent = obj->GetCacheParent();

		if (CacheParent) {
			dirty_checksum = CacheParent->GetHDirty(HDIRTYFLAGS::ALL);
			std::string parent_handle = GetHandleName(CacheParent);
			// Touch this node.
			dirtystates[parent_handle] = dirty_checksum;
		}
	}

	dirty_checksum = c4d_node->GetDirty(DIRTYFLAGS::ALL);
	dirtystates[n->handle] = dirty_checksum;
}

bool SceneParser::IsDirty(Node* n)
{
	if (!n) {
		return false;
	}

	BaseList2D* c4d_node = n->GetC4DNode();

	if (!c4d_node) {
		return false;
	}

	UInt32 dirty_checksum;
	bool dirty = false;

	if (c4d_node->IsInstanceOf(Obase)) {
		BaseObject* obj = (BaseObject*)c4d_node;
		BaseObject* CacheParent = obj->GetCacheParent();

		if (CacheParent) {
			dirty_checksum = CacheParent->GetHDirty(HDIRTYFLAGS::ALL);
			std::string parent_handle = GetHandleName(CacheParent);
			auto it = dirtystates.find(parent_handle);

			if (it == dirtystates.end()) {
				dirty = true;

			}
			else if (it->second != dirty_checksum) {
				dirty = true;
			}
		}
	}

	dirty_checksum = c4d_node->GetDirty(DIRTYFLAGS::ALL);
	auto it = dirtystates.find(n->handle);

	if (it == dirtystates.end()) {
		dirty = true;

	}
	else if (it->second != dirty_checksum) {
		dirty = true;
	}

	return dirty;
}

void SceneParser::InteractiveUpdate()
{
	// Create nodes and transforms.
	vector<Node> nodes;
	vector<Transform> transforms;
	// StopAllThreads();
	doc->ExecutePasses(0, true, true, true, BUILDFLAGS::EXTERNALRENDERER);
	long n_old_nodes = n_nodes;
	GetNodesAndTransforms(nodes, transforms, false);

	// The number of NSI nodes in the scene changed. Rebuild scene.
	if (n_old_nodes != n_nodes) {
		NSI::Context& ctx(this->GetContext());

		// FIXME: Find a better way to delete all nodes.
		for (auto it = dirtystates.begin(); it != dirtystates.end(); it++) {
			ctx.Delete(it->first);
		}

		InitScene(false, 0);
		dirtystates.clear();
	}

	// Create hooks
	vector<DL_HookPtr> hooks = PM.GetHooks();

	for (long i = 0; i < (long)hooks.size(); i++) {
		hooks[i]->Init(doc, this);
	}

	DL_SampleInfo info;
	info.sample = 0;
	info.samples_max = 1;
	info.shutter_open_time = 0;
	info.shutter_close_time = 0;
	info.sample_time = 0;

	for (long i = 0; i < (long)hooks.size(); i++) {
		hooks[i]->SampleAttributes(&info, doc, this);
	}

	for (long i = 0; i < (long)transforms.size(); i++) {
		transforms[i].SampleAttributes(&info, doc, this);
	}

	unordered_set<long> dirty_nodes;
	DL_Translator* translator;

	for (long i = 0; i < (long)nodes.size(); i++) {
		BaseList2D* c4d_node = nodes[i].GetC4DNode();

		if (c4d_node && IsDirty(&nodes[i])) {
			dirty_nodes.insert(i);
			translator = nodes[i].GetTranslator();

			if (translator) {
				HierarchyData hdata = nodes[i].hdata;
				translator->CreateNSINodes(nodes[i].handle.c_str(),
					hdata.parent_transform.c_str(),
					nodes[i].GetC4DNode(),
					doc,
					this);
				translator->SampleAttributes(
					&info, nodes[i].handle.c_str(), c4d_node, doc, this);
				translator->ConnectNSINodes(nodes[i].handle.c_str(),
					nodes[i].hdata.parent_transform.c_str(),
					nodes[i].GetC4DNode(),
					doc,
					this);
			}
		}
	}

	for (auto it = dirty_nodes.begin(); it != dirty_nodes.end(); it++) {
		TouchDirty(&nodes[*it]);
	}
}

void SceneParser::SetRenderMode(RENDER_MODE mode)
{
	rendermode = mode;
}

BaseContainer*
SceneParser::GetSettings()
{
	return &settings;
}

std::string
SceneParser::GetHandleName(BaseList2D* node)
{
	// If the object has a cache parent, return the handle of that object
	// instead.
	auto it = cacheData.find((BaseObject*)node);

	if (it != cacheData.end()) {
		CacheInfo ci = it->second;
		return std::string(ci.cache_root) + std::to_string(ci.cachePos) + "::";
	}

	return NodeIdToHandle<std::string>(node);
}

void SceneParser::AnimateDoc(BaseTime t)
{
	doc->SetTime(t);
}

void SceneParser::TraverseShaders(BaseShader* shader, vector<Node>& nodes)
{
	if (!shader) {
		return;
	}

	// Create translator node for shader.
	Node n(shader);
	DL_Translator* translator = n.GetTranslator();

	if (translator) {
		// Fill hierarchy data.
		n.hdata.cachePos = 0;
		n.hdata.isVisible = true;
		n.hdata.deformationSamples = 0;
		n.hdata.transformSamples = 0;
		n.hdata.parent_transform = "";
		n.handle = GetHandleName(shader);
		nodes.push_back(n);
	}

	TraverseShaders(shader->GetDown(), nodes);
	TraverseShaders(shader->GetNext(), nodes);
}

// Function to determine if an object is used as an input to another generator.
// If it is, then it should not be rendered Just checking if the
// BIT_CONTROLOBJECT bit is set for the object is not sufficient, as this bit is
// also set for generators. Instead, we check this bit for ALL recursively
// generated objects in the cache of "obj". If at least one object in the cache
// does not have this bit set, then "obj" is not an input object.
//
// Calling IsInputObject(obj) returns true if obj is an input object, and false
// otherwise.
static bool IsInputObject(BaseObject* obj, bool inCache = false)
{
	while (obj) {
		if (!obj->GetBit(BIT_CONTROLOBJECT)) {
			return false;
		}

		if (!IsInputObject(obj->GetCache(), true)) {
			return false;
		}

		if (inCache) {
			if (!IsInputObject(obj->GetDown(), true)) {
				return false;
			}

			obj = obj->GetNext();

		}
		else {
			obj = NULL;
		}
	}

	return true;
}

void SceneParser::TraverseObjects(BaseObject* obj,
	HierarchyData hdata,
	std::vector<Node>& nodes,
	std::vector<Transform>& transforms,
	int& count,
	std::string cache_root,
	int cacheStart,
	bool inCache)
{
	while (obj) {
		count++;
		bool obj_visible = hdata.isVisible;
		long _rendermode = obj->GetRenderMode();

		if (_rendermode == MODE_ON) {
			obj_visible = true;

		}
		else if (_rendermode == MODE_OFF) {
			obj_visible = false;
		}

		int transform_samples = hdata.transformSamples;
		int deformation_samples = hdata.deformationSamples;
		BaseTag* motionblur_tag = obj->GetTag(ID_DL_MOTIONBLURTAG);

		if (motionblur_tag) {
			BaseContainer* mbdata = motionblur_tag->GetDataInstance();
			transform_samples = mbdata->GetInt32(TRANSFORMATION_EXTRA_SAMPLES) + 2;

			if (mbdata->GetBool(USE_TRANSFORMATION_BLUR) == false) {
				transform_samples = 1;
			}

			deformation_samples = mbdata->GetInt32(DEFORMATION_EXTRA_SAMPLES) + 2;

			if (mbdata->GetBool(USE_DEFORMATION_BLUR) == false) {
				deformation_samples = 1;
			}
		}

		string object_handle;

		if (inCache) {
			object_handle = cache_root;

		}
		else {
			object_handle = string(GetHandleName((BaseList2D*)obj));
		}

		int cs = cacheStart;
		string cr = cache_root;

		if (cs == -1) {
			cs = count;
			cr = object_handle;
		}

		HierarchyData n_hdata = hdata;
		n_hdata.deformationSamples = deformation_samples;
		n_hdata.transformSamples = transform_samples;
		n_hdata.isVisible = obj_visible;

		if (inCache) {
			n_hdata.cachePos = count - cacheStart;
			object_handle = object_handle + std::to_string(n_hdata.cachePos) + "::";
			CacheInfo ci;
			ci.cachePos = n_hdata.cachePos;
			ci.cache_root = cache_root;
			cacheData[obj] = ci;
		}

		// Transform.
		Transform t(object_handle, obj, hdata.parent_transform);
		t.motionSamples = transform_samples;
		std::string transform_handle = t.GetHandle();
		transform_handle = t.GetNullHandle();
		transforms.push_back(t);
		n_hdata.parent_transform = transform_handle;
		BaseObject* tp = obj->GetDeformCache();

		if (tp) {
			TraverseObjects(tp, n_hdata, nodes, transforms, count, cr, cs, true);

		}
		else {
			Node n(obj);
			n.hdata = n_hdata;
			n.handle = object_handle;
			DL_Translator* translator = n.GetTranslator();
			bool InputObject = false;

			// A translator exists for this object type, and accepts this
			// object.
			if (translator && translator->Accept(obj)) {
				InputObject = IsInputObject(obj);

				if ((!InputObject) && obj_visible && obj->GetDeformMode()) {
					nodes.push_back(n);
				}

			}
			else { // No translator exists for this object type. We continue
			 // traversing cache.
				tp = obj->GetCache(nullptr);

				if ((!InputObject) && tp) { // There is a cache, and obj is not an input object
					TraverseObjects(tp, n_hdata, nodes, transforms, count, cr, cs, true);
				}
			}
		}

		BaseShader* shader = obj->GetFirstShader();
		TraverseShaders(shader, nodes);
		// Handle tags
		BaseTag* tag = obj->GetFirstTag();

		while (tag) {
			Node tagnode(tag);
			tagnode.hdata = n_hdata;
			string tag_handle = string(GetHandleName((BaseList2D*)tag));

			if (inCache && n_hdata.cachePos > 0) {
				tag_handle = tag_handle + std::to_string(n_hdata.cachePos) + "::";
			}

			tagnode.handle = tag_handle;
			DL_Translator* tagtranslator = tagnode.GetTranslator();

			if (tagtranslator) {
				nodes.push_back(tagnode);
			}

			tag = tag->GetNext();
		}

		TraverseObjects(obj->GetDown(),
			n_hdata,
			nodes,
			transforms,
			count,
			cache_root,
			cacheStart,
			inCache);
		obj = obj->GetNext();
	}
}
