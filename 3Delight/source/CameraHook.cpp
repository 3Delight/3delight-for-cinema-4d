#include "CameraHook.h"
#include "IDs.h"
#include "dlrendersettings.h"
#include "tcamera.h"
#ifdef _WIN32
#	pragma warning(disable : 4265)
#endif
#include "DL_TypeConversions.h"
#include "nsi.hpp"

#include <cfloat>

using namespace std;

void CameraHook::Init(BaseDocument* doc, DL_SceneParser* parser)
{
	transform_name = string(
						 "3dlfc4d::scene_camera_transform"); // string(parser->GetUniqueName("transform"));
	camera_name = string("3dlfc4d::scene_camera");
}

void CameraHook::CreateNSINodes(BaseDocument* doc, DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	RENDER_MODE mode = parser->GetRenderMode();

	// Do not create Camera node if exporting standIn node.
	if (mode == DL_STANDIN_EXPORT) {
		return;
	}

	double shutter_opening[2];
	shutter_opening[0] = 0.5 * (1 - 0.75);
	shutter_opening[1] = 1 - 0.5 * (1 - 0.75);
	double film_offset_x = 0, film_offset_y = 0;
	// Get scene camera
	BaseDraw* bd = doc->GetActiveBaseDraw();
	CameraObject* camera = (CameraObject*) bd->GetSceneCamera(doc);
	const char* typeName = "perspectivecamera";
	NSI::DynamicArgumentList camera_args;
	bool camera_args_bool = false;
	BaseContainer* tag_data = nullptr;

	if (camera && camera->GetProjection() != Pperspective) {
		/*
			Check for the selected camera in perspective view if any other view is selected.
		*/
		for (int i = 0; i < doc->GetBaseDrawCount(); i++) {
			BaseDraw* views_bd = doc->GetBaseDraw(i);
			CameraObject* view_camera = (CameraObject*)views_bd->GetSceneCamera(doc);

			if (view_camera->GetProjection() == Pperspective) {
				camera = view_camera;
				break;
			}
		}
	}

	if (camera) {
		BaseTag* cameratag = camera->GetTag(ID_DL_CAMERATAG);
		BaseContainer* camdata = camera->GetDataInstance();
		film_offset_x = camdata->GetFloat(CAMERAOBJECT_FILM_OFFSET_X);
		film_offset_y = camdata->GetFloat(CAMERAOBJECT_FILM_OFFSET_Y);
		if (cameratag) {
			tag_data = cameratag->GetDataInstance();
			shutter_opening[0] =
				0.5 * (1 - tag_data->GetFloat(SHUTTER_OPENING_EFFICIENCY));
			shutter_opening[1] =
				1 - 0.5 * (1 - tag_data->GetFloat(SHUTTER_CLOSING_EFFICIENCY));
			int projection = tag_data->GetInt32(PROJECTION_TYPE);
			double horizontal_fov = tag_data->GetFloat(AUXILIARY_FOV);

			if (projection == DL_ORTHOGRAPHIC) {
				typeName = "orthographiccamera";

			} else if (projection == DL_CYLINDRICAL) {
				typeName = "cylindricalcamera";
				camera_args.Add(
					new NSI::FloatArg("horizontalfov", float(horizontal_fov)));
				camera_args_bool = true;

			} else if (projection == DL_FISHEYE_STEREOGRAPHIC) {
				typeName = "fisheyecamera";
				camera_args.Add(new NSI::StringArg("mapping", "stereographic"));
				camera_args_bool = true;

			} else if (projection == DL_FISHEYE_EQUIDISTANT) {
				typeName = "fisheyecamera";
				camera_args.Add(new NSI::StringArg("mapping", "equidistant"));
				camera_args_bool = true;

			} else if (projection == DL_FISHEYE_EQUISOLID) {
				typeName = "fisheyecamera";
				camera_args.Add(new NSI::StringArg("mapping", "equisolid"));
				camera_args_bool = true;

			} else if (projection == DL_FISHEYE_ORTHOGRAPHIC) {
				typeName = "fisheyecamera";
				camera_args.Add(new NSI::StringArg("mapping", "orthographic"));
				camera_args_bool = true;

			} else if (projection == DL_SPHERICAL) {
				typeName = "sphericalcamera";
			}
		}
	}

	ctx.Create(camera_name, typeName);
	transform_name = string("3dlfc4d::scene_camera_transform");
	// string(parser->GetUniqueName("transform"));
	// transform_name = string("3dlfc4d::scene_camera_transform");
	// //string(parser->GetUniqueName("transform"));
	ctx.Create(transform_name, "transform");
	ctx.Create("3dlfc4d::scene_camera_screen", "screen");
	ctx.Connect(camera_name, "", transform_name, "objects");
	ctx.Connect(transform_name, "", ".root", "objects");
	ctx.Connect(
		"3dlfc4d::scene_camera_screen", "", "3dlfc4d::scene_camera", "screens");
	RenderData* rd = doc->GetActiveRenderData();
	BaseContainer* render_data = rd->GetDataInstance();
	BaseContainer* DL_Settings = parser->GetSettings();
	vector<int> resolution(2);
	parser->GetRenderResolution(&resolution[0], &resolution[1]);
	vector<int> renderdata_resolution(2);
	renderdata_resolution[0] = render_data->GetInt32(RDATA_XRES);
	renderdata_resolution[1] = render_data->GetInt32(RDATA_YRES);

	if (DL_Settings->GetBool(DL_ENABLE_INTERACTIVE_PREVIEW) == true && mode != C4D_PREVIEW) {
		float resolution_factor = 1;

		if (DL_Settings->GetInt32(DL_RESOLUTION) == DL_HALF) {
			resolution_factor = 0.5;

		} else if (DL_Settings->GetInt32(DL_RESOLUTION) == DL_QUARTER) {
			resolution_factor = 0.25;

		} else if (DL_Settings->GetInt32(DL_RESOLUTION) == DL_EIGHTH) {
			resolution_factor = 0.125;
		}

		resolution[0] = (int) (resolution[0] * resolution_factor);
		resolution[1] = (int) (resolution[1] * resolution_factor);
	}

	NSI::Argument resolution_arg("resolution");
	resolution_arg.SetArrayType(NSITypeInteger, 2);
	resolution_arg.SetValuePointer((void*) &resolution[0]);
	double f = double(resolution[1]) / double(resolution[0]);
	double f2 =
		double(renderdata_resolution[1]) / double(renderdata_resolution[0]);
	double c = f2 / f;
	double screen_window[4];
	double pixel_aspect = render_data->GetFloat(RDATA_PIXELASPECT);

	if (c >= 1) {
		screen_window[0] = -1 * c*pixel_aspect + 2 * film_offset_x;
		screen_window[1] = -f * c - 2 * film_offset_y*f;
		screen_window[2] = 1 * c*pixel_aspect + 2 * film_offset_x;
		screen_window[3] = f * c - 2 * film_offset_y*f;

	} else {
		screen_window[0] = -1 + 2 * film_offset_x;
		screen_window[1] = -f - 2 * film_offset_y * f2;
		screen_window[2] = 1 + 2 * film_offset_x;
		screen_window[3] = f - 2 * film_offset_y * f2;
	}

	if (tag_data && tag_data->GetInt32(PROJECTION_TYPE) == DL_ORTHOGRAPHIC)
	{
		//Consider transform to be in cm by default instead of m.
		double half_width = 100*tag_data->GetFloat(DL_ORTHOGRAPHIC_WIDTH)/2.0;
		double half_height = half_width * f2;
		screen_window[0] = -half_width;
		screen_window[1] = -half_height;
		screen_window[2] = half_width;
		screen_window[3] = half_height;
	}

	NSI::Argument screenwindow_arg("screenwindow");
	screenwindow_arg.SetArrayType(NSITypeDouble, 2);
	screenwindow_arg.SetCount(2);
	screenwindow_arg.SetValuePointer((void*) &screen_window[0]);
	// shutter_opening[0]=0.5*(1.0-DL_Settings->GetFloat(DL_SHUTTER_OPENING_EFFICIENCY,1));
	// shutter_opening[1]=1-0.5*(1.0-DL_Settings->GetFloat(DL_SHUTTER_CLOSING_EFFICIENCY,1));
	NSI::Argument shutteropening_arg("shutteropening");
	shutteropening_arg.SetType(NSITypeDouble);
	shutteropening_arg.SetCount(2);
	shutteropening_arg.SetValuePointer((void*) &shutter_opening[0]);
	ctx.SetAttribute(camera_name,
					 (shutteropening_arg
					  // NSI::IntegerArg("depthoffield.enable", enable_dof),
					  // NSI::DoubleArg("depthoffield.fstop", fstop)
					 ));
	int pixel_samples = DL_Settings->GetInt32(DL_PIXEL_SAMPLES, 16);

	if (camera_args_bool) {
		ctx.SetAttribute("3dlfc4d::scene_camera", camera_args);
	}

	ctx.SetAttribute("3dlfc4d::scene_camera_screen",
					 (resolution_arg,
					  screenwindow_arg,
					  NSI::IntegerArg("oversampling", pixel_samples),
					  NSI::FloatArg("pixelaspectratio", float(pixel_aspect))));
}

void CameraHook::SampleAttributes(DL_SampleInfo* info,
								  BaseDocument* doc,
								  DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	BaseContainer* DL_Settings = parser->GetSettings();
	// Do not set Camera node attributes if exporting on standIn node.
	RENDER_MODE mode = parser->GetRenderMode();
	//Get scene camera
	BaseDraw* bd = doc->GetActiveBaseDraw();
	CameraObject* camera = (CameraObject*)bd->GetSceneCamera(doc);

	RenderData* rd = doc->GetActiveRenderData();
	BaseContainer* render_data = rd->GetDataInstance();
	double pixel_aspect = render_data->GetFloat(RDATA_PIXELASPECT);

	if (camera == nullptr) {
		camera = (CameraObject*)bd->GetEditorCamera();
	}

	if (camera == nullptr) {
		return;
	}

	/*
		Avoid setting invalid parameters on ipr for not supported cameras.
	*/
	if(camera->GetProjection() != Pperspective) {
		/*
			Check if any of the views has a perspective camera to use for our scene.
			This will avoid rendering a black scene if we chose a non supported camera.
		*/
		for(int i=0; i<doc->GetBaseDrawCount(); i++) {
			BaseDraw* views_bd = doc->GetBaseDraw(i);
			CameraObject* view_camera = (CameraObject*)views_bd->GetSceneCamera(doc);

			if (view_camera->GetProjection() == Pperspective) {
				camera = view_camera;
				break;
			}
		}
	}

	if (mode == DL_STANDIN_EXPORT) {
		return;
	}

	if (info->sample == 1) { // Set camera shutter range at first motion sample
		double shutter_range[2];
		shutter_range[0] = info->shutter_open_time;
		shutter_range[1] = info->shutter_close_time;
		NSI::Argument shutter_arg("shutterrange");
		shutter_arg.SetType(NSITypeDouble);
		shutter_arg.SetCount(2);
		shutter_arg.SetValuePointer((void*) &shutter_range[0]);
		ctx.SetAttribute("3dlfc4d::scene_camera", (shutter_arg));
	}

	Float aperture = camera->GetAperture();
	Float focus = camera->GetFocus();

	float fov = (float) RadToDeg(2 * ATan(0.5 * aperture / (pixel_aspect * focus)));
	float focal_length_cm = (float) focus / 10.0f;
	BaseContainer* camdata = camera->GetDataInstance();
	double focus_distance = camdata->GetFloat(CAMERAOBJECT_TARGETDISTANCE);
// Sample camera transform
	Matrix m = camera->GetMg();
	Matrix flip;
	flip.sqmat.v1 = Vector(1, 0, 0);
	flip.sqmat.v2 = Vector(0, 1, 0);
	flip.sqmat.v3 = Vector(0, 0, -1);
	m = (m* flip);
	vector<double> v = MatrixToNSIMatrix(m);
	NSI::Argument xform("transformationmatrix");
	xform.SetType(NSITypeDoubleMatrix);
	xform.SetValuePointer((void*) &v[0]);
	ctx.SetAttributeAtTime(transform_name, info->sample_time, (xform));

	BaseTag* cameratag = camera->GetTag(ID_DL_CAMERATAG);

	if (cameratag) {
		BaseContainer* tag_data = cameratag->GetDataInstance();
		int enable_dof = tag_data->GetInt32(USE_DEPTH_OF_FIELD);

		if (enable_dof && DL_Settings->GetBool(DL_ENABLE_INTERACTIVE_PREVIEW)) {
			enable_dof = !DL_Settings->GetBool(DL_DISABLE_DEPTH_OF_FIELD);
		}

		if (tag_data->GetInt32(PROJECTION_TYPE) == DL_ORTHOGRAPHIC)
			return;

		int use_finite = tag_data->GetInt32(FINITE_BLADES);
		int blades_number = tag_data->GetInt32(NUMBER_OF_BLADES);
		double rotation = (double) tag_data->GetFloat(ROTATION);
		double focal_length = (double) tag_data->GetFloat(FOCAL_LENGTH);
		double focal_distance = (double) tag_data->GetFloat(FOCAL_DISTANCE);
		double fstop = (double) tag_data->GetFloat(F_STOP);
		NSI::DynamicArgumentList camera_args;
		camera_args.Add(new NSI::IntegerArg("depthoffield.enable", enable_dof));
		camera_args.Add(
			new NSI::IntegerArg("depthoffield.aperture.enable", use_finite));
		camera_args.Add(
			new NSI::IntegerArg("depthoffield.aperture.sides", blades_number));
		camera_args.Add(
			new NSI::DoubleArg("depthoffield.aperture.angle", rotation));

		if (enable_dof) {
			/* Don't output these when no DoF. Otherwise, 3Delight emits a
						warning about out of range values. */
			camera_args.Add(
				new NSI::DoubleArg("depthoffield.focallength", focal_length));
			camera_args.Add(
				new NSI::DoubleArg("depthoffield.focaldistance", focus_distance));
			camera_args.Add(new NSI::DoubleArg("depthoffield.fstop", fstop));
		}

		ctx.SetAttributeAtTime(
			"3dlfc4d::scene_camera", info->sample_time, (camera_args));
	}

	double clipping_range[2] ={	0, FLT_MAX };
	if (camdata->GetBool(CAMERAOBJECT_NEAR_CLIPPING_ENABLE))
	{
		clipping_range[0] = camdata->GetFloat(CAMERAOBJECT_NEAR_CLIPPING);
	}

	if (camdata->GetBool(CAMERAOBJECT_FAR_CLIPPING_ENABLE))
	{
		clipping_range[1] = camdata->GetFloat(CAMERAOBJECT_FAR_CLIPPING);
	}

	ctx.SetAttribute(
		"3dlfc4d::scene_camera",
		*NSI::Argument("clippingrange")
		.SetType(NSITypeDouble)
		->SetCount(2)
		->CopyValue(clipping_range, sizeof(clipping_range)));

// Sample fov
	ctx.SetAttributeAtTime(
		"3dlfc4d::scene_camera",
		info->sample_time,
		(NSI::FloatArg("fov", fov), NSI::FloatArg("focallength", focal_length_cm)));
}
