#ifndef SCENE_PARSER_H
#define SCENE_PARSER_H

#include "c4d.h"
#include "DL_SceneParser.h"
#include "PluginManager.h"
#include "DL_Hook.h"
#include "Node.h"
#include "Transform.h"
#include "nsi.hpp"
#include <vector>
#include <map>
#include <string>
#include <unordered_map>

struct CacheInfo {
	std::string cache_root;
	long cachePos;
};

class SceneParser : public DL_SceneParser
{
public:
	SceneParser(BaseDocument* document);
	virtual ~SceneParser();

	NSI::Context& GetContext();

	// New methods:
	bool InitScene(bool animate, long frame);
	void SampleFrameMotion(long frame);
	void InteractiveUpdate();

	void FillRenderSettings();

	RENDER_MODE GetRenderMode();
	void SetRenderMode(RENDER_MODE mode);

	void GetRenderResolution(int* width, int* height);
	void SetCustomRenderResolution(int width, int height);

	BaseContainer* GetSettings();

	// Return the base handle name of any c4d item.
	// Primarily intended to be used for building connections in the NSI scene,
	// e.g. in ConnectNSINodes()
	virtual std::string GetHandleName(BaseList2D* node);

private:
	SceneParser();

	void TraverseShaders(BaseShader* shader, std::vector<Node>& nodes);
	void TraverseObjects(BaseObject* obj,
						 HierarchyData hdata,
						 std::vector<Node>& nodes,
						 std::vector<Transform>& transforms,
						 int& count,
						 std::string cache_root = "",
						 int cacheStart = -1,
						 bool inCache = false);
	void GetNodesAndTransforms(std::vector<Node>& nodes,
							   std::vector<Transform>& transforms,
							   bool animate);
	void AnimateDoc(BaseTime t);

private:
	NSI::Context context;

	BaseContainer settings;
	BaseDocument* doc;

	RENDER_MODE rendermode;

	bool IsDirty(Node* n);
	void TouchDirty(Node* n);

	long n_nodes; // number of nodes generated in the last call to
	// "GetNodesAndTransforms"
	std::unordered_map<std::string, UInt32> dirtystates;
	std::unordered_map<BaseObject*, CacheInfo> cacheData;

	int nMotionSamples;

	bool hasCustomResolution;
	int CustomResolutionWidth;
	int CustomResolutionHeight;

	long frame;
};

#endif
