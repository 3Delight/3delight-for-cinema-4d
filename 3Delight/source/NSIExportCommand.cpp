#include <iomanip>
#include <sstream>
#include "3DelightRenderer.h"
#include "DL_TypeConversions.h"
#include "ExpandFilename.h"
#include "IDs.h"
#include "SceneParser.h"
#include "c4d.h"
#include "nsi.hpp"
#include "RegisterPrototypes.h"

using namespace std;

class NSIExportCommand : public CommandData
{
public:
	virtual Bool Execute(BaseDocument* doc, GeDialog* parentManager);
};

Bool NSIExportCommand::Execute(BaseDocument* doc, GeDialog* parentManager)
{
	RenderData* rd = doc->GetActiveRenderData();
	BaseContainer render_data = rd->GetData();
	BaseTime t = doc->GetTime();
	Filename outfile;
	outfile.FileSelect(
		FILESELECTTYPE::ANYTHING, FILESELECT::SAVE, String(""), String("nsi"));
	outfile.ClearSuffix();
	String filepart = outfile.GetFileString();
	long frame_start = 0;
	long frame_end = 0;
	long FrameSequence = render_data.GetInt32(RDATA_FRAMESEQUENCE);

	if (FrameSequence == RDATA_FRAMESEQUENCE_MANUAL) {
		BaseTime t1 = render_data.GetTime(RDATA_FRAMEFROM);
		BaseTime t2 = render_data.GetTime(RDATA_FRAMETO);
		frame_start = t1.GetFrame(doc->GetFps());
		frame_end = t2.GetFrame(doc->GetFps());

	} else if (FrameSequence == RDATA_FRAMESEQUENCE_CURRENTFRAME) {
		frame_start = frame_end = t.GetFrame(doc->GetFps());

	} else if (FrameSequence == RDATA_FRAMESEQUENCE_ALLFRAMES) {
		BaseTime t1 = doc->GetMinTime();
		BaseTime t2 = doc->GetMaxTime();
		frame_start = t1.GetFrame(doc->GetFps());
		frame_end = t2.GetFrame(doc->GetFps());

	} else if (FrameSequence == RDATA_FRAMESEQUENCE_PREVIEWRANGE) {
		BaseTime t1 = doc->GetLoopMinTime();
		BaseTime t2 = doc->GetLoopMaxTime();
		frame_start = t1.GetFrame(doc->GetFps());
		frame_end = t2.GetFrame(doc->GetFps());
	}

	long framestep = render_data.GetInt32(RDATA_FRAMESTEP, 1);
	bool renderOK = true;

	for (long i = frame_start; i <= frame_end && renderOK; i = i + framestep) {
		StatusSetText("NSI export, frame " + String::IntToString(int(i)));
		ostringstream ss;
		ss << std::setw(4) << std::setfill('0') << i;
		String framenumber_padded(ss.str().c_str());
		outfile.SetFile(Filename(filepart + framenumber_padded));
		outfile.SetSuffix(String("nsi"));
		BaseDocument* renderdoc =
			(BaseDocument*) doc->GetClone(COPYFLAGS::DOCUMENT, nullptr);
		SceneParser* parser = new SceneParser(renderdoc);
		string filename_string = StringToStdString(outfile.GetString());
		NSI::ArgumentList arglist;
		arglist.Add(new NSI::StringArg("type", "apistream"));
		arglist.Add(new NSI::StringArg("streamfilename", filename_string));
		arglist.Add(new NSI::StringArg("streamformat", "nsi"));
		parser->GetContext().Begin(arglist);
		parser->SetRenderMode(DL_EXPORT_NSI);
		// Render scene
		bool RenderOK = parser->InitScene(true, i);
		parser->SampleFrameMotion(i);
		parser->GetContext().RenderControl(NSI::StringArg("action", "start"));
		BaseDocument::Free(renderdoc);
		delete parser;
	}

	StatusSetText(String(""));
	return true;
}

Bool RegisterNSIExportCommand(void)
{
	return RegisterCommandPlugin(ID_NSI_EXPORT_COMMAND,
								 "Export to NSI File..."_s,
								 PLUGINFLAG_HIDEPLUGINMENU,
								 0,
								 String("Export to NSI File"_s),
								 NewObjClear(NSIExportCommand));
}
