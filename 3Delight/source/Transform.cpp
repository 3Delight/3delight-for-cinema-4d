#include "Transform.h"
#include "DL_TypeConversions.h"
#include "nsi.hpp"

Transform::Transform(std::string object_handle,
					 BaseObject* object,
					 std::string parent_handle)
{
	obj = object;
	handle = std::string("X_") + object_handle;
	null_handle = std::string("X0_") + object_handle;
	parent = parent_handle;
	motionSamples = 2;
}

void Transform::CreateNodes(BaseDocument* document, DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	ctx.Create(handle, "transform");
	ctx.Create(null_handle, "transform");
	ctx.Connect(handle, "", parent, "objects");
	ctx.Connect(null_handle, "", handle, "objects");
}

void Transform::SampleAttributes(DL_SampleInfo* info,
								 BaseDocument* document,
								 DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	// Sample transform matrix
	Matrix m = obj->GetRelMl();
	Matrix frozen_m = obj->GetFrozenMln();
	Matrix final_m = m * frozen_m;
	std::vector<double> v = MatrixToNSIMatrix(final_m);
	NSI::Argument xform("transformationmatrix");
	xform.SetType(NSITypeDoubleMatrix);
	xform.SetValuePointer((void*) &v[0]);
	ctx.SetAttributeAtTime(handle, info->sample_time, (xform));
}

std::string
Transform::GetHandle()
{
	return handle;
}

std::string
Transform::GetNullHandle()
{
	return null_handle;
}