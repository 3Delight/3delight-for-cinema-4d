#include "IDs.h"
#include "c4d.h"
#include "RegisterPrototypes.h"
#include "tcamera.h"

class DL_CameraTag : public TagData
{
public:
	virtual Bool Init(GeListNode* node);
	virtual Bool GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags);
	virtual Bool GetDEnabling(GeListNode* node,
							  const DescID& id,
							  const GeData& t_data,
							  DESCFLAGS_ENABLE flags,
							  const BaseContainer* itemdesc);
	static NodeData* Alloc(void)
	{
		return NewObjClear(DL_CameraTag);
	}
};

Bool DL_CameraTag::Init(GeListNode* node)
{
	BaseTag* tag = (BaseTag*) node;
	BaseContainer* data = tag->GetDataInstance();
	data->SetFloat(SHUTTER_ANGLE, maxon::DegToRad(180.0));
	data->SetFloat(SHUTTER_OPENING_EFFICIENCY, 0.75);
	data->SetFloat(SHUTTER_CLOSING_EFFICIENCY, 0.75);
	data->SetBool(ENABLE_DOF, false);
	data->SetFloat(F_STOP, 5.6);
	data->SetFloat(FOCAL_LENGTH, 1);
	data->SetInt32(NUMBER_OF_BLADES, 6);
	data->SetInt32(PROJECTION_TYPE, DL_PERSPECTIVE);
	data->SetFloat(DL_ORTHOGRAPHIC_WIDTH, 10.0);
	data->SetInt32(AUXILIARY_FOV, 180);
	return TRUE;
}

Bool DL_CameraTag::GetDDescription(GeListNode* node,
								   Description* description,
								   DESCFLAGS_DESC& flags)
{
	return true;
}

Bool DL_CameraTag::GetDEnabling(GeListNode* node,
								const DescID& id,
								const GeData& t_data,
								DESCFLAGS_ENABLE flags,
								const BaseContainer* itemdesc)
{
	if (!node) {
		return false;
	}

	BaseObject* camera_object = (BaseObject*)node;
	BaseContainer* dldata = camera_object->GetDataInstance();

	switch (id[0].id) {
	case NUMBER_OF_BLADES:
	case ROTATION:
		return (dldata->GetBool(FINITE_BLADES) == TRUE);

	case AUXILIARY_FOV:
		return (dldata->GetInt32(PROJECTION_TYPE) != DL_PERSPECTIVE
			&& dldata->GetInt32(PROJECTION_TYPE) != DL_ORTHOGRAPHIC
			&& dldata->GetInt32(PROJECTION_TYPE) != DL_SPHERICAL);
	case DL_ORTHOGRAPHIC_WIDTH:
		return dldata->GetInt32(PROJECTION_TYPE) == DL_ORTHOGRAPHIC;
	}

	return true;
}

Bool RegisterDL_CameraTag(void)
{
	return RegisterTagPlugin(ID_DL_CAMERATAG,
							 "Camera Properties"_s,
							 TAG_VISIBLE,
							 DL_CameraTag::Alloc,
							 "tcamera"_s,
							 AutoBitmap("filmcamera.tif"_s),
							 0);
}
