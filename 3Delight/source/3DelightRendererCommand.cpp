#include "c4d.h"
#include "IDs.h"
#include "nsi.hpp"
#include "SceneParser.h"
#include "3DelightRenderer.h"
#include "ExpandFilename.h"
#include "RegisterPrototypes.h"

class DL_3DelightRender_command : public CommandData
{
public:
	virtual Bool Execute(BaseDocument* doc, GeDialog* parentManager);
};

Bool DL_3DelightRender_command::Execute(BaseDocument* doc, GeDialog* parentManager)
{
	// Render scene
	// BaseTime t = doc->GetTime();
	// long frame = t.GetFrame(doc->GetFps());
	SpecialEventAdd(DL_START_RENDER);
	return true;
	// return DL_RenderFrame(doc, frame); //TO DO: Add ErorHandler when
	// rendering from the shelf.
}

Bool Register3DelightCommand(void)
{
	return RegisterCommandPlugin(ID_RENDER_COMMAND,
								 "Render"_s,
								 PLUGINFLAG_HIDEPLUGINMENU,
								 AutoBitmap("shelf_launchRender_200.png"_s),
								 String("Render With 3Delight"_s),
								 NewObjClear(DL_3DelightRender_command));
}
