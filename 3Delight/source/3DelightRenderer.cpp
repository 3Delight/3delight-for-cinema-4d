#include "c4d.h"
#include "c4d_symbols.h"
#include "dlrendersettings.h"
#include "3DelightRenderer.h"
#include "ErrorHandler.h"
#include <iostream>
#include "nsi.hpp"
#include "IDs.h"
#include <assert.h>
#include <algorithm>
#include "RegisterPrototypes.h"
#include "SceneParser.h"
#include "dl_preferences.h"

static void FrameStopped(void* callbackdata)
{
	bool* stopped = (bool*) callbackdata;
	*stopped = true;
}

static void FrameStoppedCallbck(
	void* stoppedcallbackdata, NSIContext_t ctx, int status)
{
	// End context when rendering is completed (or stopped)
	SceneParser* parser = (SceneParser*) stoppedcallbackdata;
	delete parser;
	parser = nullptr;
}

Bool RenderSettings::AddCycleButton(Description* dc,
									Int32 id,
									const DescID& groupid,
									const String& name,
									const BaseContainer& itemnames)
{
	const DescID* singleid = dc->GetSingleDescID();

	if (!singleid || ((DescID) id).IsPartOf(*singleid, NULL)) {
		BaseContainer bc = GetCustomDataTypeDefault(DTYPE_LONG);
		//  Set CycleButton properties
		bc.SetBool(DESC_ANIMATE, false);
		bc.SetInt32(DESC_CUSTOMGUI, CUSTOMGUI_CYCLEBUTTON);
		bc.SetInt32(DESC_DEFAULT, 0);
		bc.SetInt32(DESC_SCALEH, TRUE);
		bc.SetString(DESC_NAME, name);
		bc.SetContainer(DESC_CYCLE, itemnames);
		//  Create CycleButton
		return dc->SetParameter(DescLevel(id, DTYPE_LONG, 0), bc, groupid);
	}

	return TRUE;
}

#if 0
Bool AddButton(Description* dc, Int32 id, const DescID& groupid)
{
	const DescID* singleid = dc->GetSingleDescID();

	if (!singleid || ((DescID) id).IsPartOf(*singleid, NULL)) {
		BaseContainer bc = GetCustomDataTypeDefault(DTYPE_BUTTON);
		//  Set CycleButton properties
		bc.SetBool(DESC_ANIMATE, false);
		bc.SetInt32(DESC_CUSTOMGUI, CUSTOMGUI_BUTTON);
		bc.SetInt32(DESC_SCALEH, TRUE);
		bc.SetInt32(DESC_HIDE, TRUE);
		bc.SetString(DESC_NAME, " Abort Live Render "_s);
		//  Create CycleButton
		return dc->SetParameter(DescLevel(id, DTYPE_BUTTON, 0), bc, groupid);
	}

	return TRUE;
}
#endif

Bool RenderSettings::AddEmpty(Description* dc, Int32 id, const DescID& groupid)
{
	const DescID* singleid = dc->GetSingleDescID();

	if (!singleid || ((DescID) id).IsPartOf(*singleid, NULL)) {
		BaseContainer bc = GetCustomDataTypeDefault(DTYPE_STATICTEXT);
		bc.SetBool(DESC_ANIMATE, false);
		bc.SetInt32(DESC_SCALEH, TRUE);
		return dc->SetParameter(DescLevel(id, DTYPE_STATICTEXT, 0), bc, groupid);
	}

	return TRUE;
}

bool RenderSettings::Render3Dl(BaseDocument* doc,
							   long frame,
							   RENDER_MODE mode,
							   bool progressive,
							   BaseContainer* settings)
{
	String action = settings->GetString(DL_ISCLICKED);
	NSI::ArgumentList arglist;
	NSIErrorHandler_t eh = NSIErrorHandlerC4D;
	BaseContainer* bc =
		GetWorldContainerInstance()->GetContainerInstance(PREFERENCES_ID);

	if (!bc) {
		GetWorldContainerInstance()->SetContainer(PREFERENCES_ID, BaseContainer());
		bc = GetWorldContainerInstance()->GetContainerInstance(PREFERENCES_ID);
	}

	if (action == "Render") {
		arglist.Add(new NSI::PointerArg("errorhandler", (void*) eh));

	} else if (action == "Export") {
		String file = settings->GetFilename(DL_FOLDER_OUTPUT).GetString();
		std::string exported = file.GetCStringCopy();
		const char* output = exported.c_str();
		arglist.Add(new NSI::StringArg("streamfilename", output));
	}

	BaseDocument* renderdoc =
		(BaseDocument*) doc->GetClone(COPYFLAGS::DOCUMENT, nullptr);
	SceneParser* sp = new SceneParser(renderdoc);
	sp->GetContext().Begin(arglist);
	sp->SetRenderMode(mode);
	// Render scene
	bool RenderOK = sp->InitScene(true, frame);
	sp->SampleFrameMotion(frame);
	BaseDocument::Free(renderdoc);
	int live_pixel = bc->GetInt32(DL_LIVE_RENDER_COARSENESS);
	int scanning = bc->GetInt32(DL_SCANNING);
	std::string bucket_order = "";

	switch (scanning) {
	case 0:
		bucket_order = "circle";
		break;

	case 1:
		bucket_order = "spiral";
		break;

	case 2:
		bucket_order = "horizontal";
		break;

	case 3:
		bucket_order = "vertical";
		break;

	case 4:
		bucket_order = "zigzag";
		break;

	default:
		bucket_order = "horizontal";
		break;
	}

	if (bucket_order == "") {
		bucket_order = "circle";
	}

	sp->GetContext().SetAttribute(
		NSI_SCENE_GLOBAL,
		(NSI::IntegerArg("renderatlowpriority", 1),
		 NSI::StringArg("bucketorder", bucket_order),
		 NSI::IntegerArg("live.pixelsize", live_pixel)));
	sp->GetContext().RenderControl(
		(NSI::StringArg("action", "start"),
		 NSI::IntegerArg("progressive", 0),
		 NSI::PointerArg("stoppedcallback", (void*) &FrameStoppedCallbck),
		 NSI::PointerArg("stoppedcallbackdata", (void*) sp)));
	return RenderOK;
}

/**
        Function to hide a specific element from the RenderSettings GUI
*/
Bool RenderSettings::ShowDescriptionElement(GeListNode* i_node,
		Description* i_descr,
		Int32 i_MyDescID,
		Bool i_show)
{
	AutoAlloc<AtomArray> ar;
	ar->Append(static_cast<C4DAtom*>(i_node));
	BaseContainer* bc = i_descr->GetParameterI(DescLevel(i_MyDescID), ar);

	if (!bc) {
		return FALSE;
	}

	bc->SetBool(DESC_HIDE, !i_show);
	return TRUE;
}

/**
        Gets the currently used parameter description of the entity
        which in this case is the plugin with Id ID_RENDERSETTINGS.
*/
Bool RenderSettings::GetDDescription(GeListNode* i_node,
									 Description* i_description,
									 DESCFLAGS_DESC& i_flags)
{
	i_description->LoadDescription(ID_RENDERSETTINGS);
	// BaseContainer names;
	// names.SetString(0, "Render"_s);
	// names.SetString(1, "Live Render (IPR)"_s);
	// names.SetString(2, "Export to NSI File..."_s);
	// AddCycleButton(i_description, RENDER_CYCLEBUTTON, DescID(RENDER), "",
	// names); AddButton(i_description, ABORT_IPR, DescID(RENDER));
	// AddEmpty(i_description, RENDER_CYCLEBUTTON + 1, DescID(RENDER));
	// AddEmpty(i_description, RENDER_CYCLEBUTTON + 2, DescID(RENDER));
	BaseContainer* bc = ((BaseVideoPost*) i_node)->GetDataInstance();
	Bool OPEN_EXR_FORMAT =
		(bc->GetInt32(DL_DEFAULT_IMAGE_FORMAT) == DL_FORMAT_OPEN_EXR ||
		 bc->GetInt32(DL_DEFAULT_IMAGE_FORMAT) == DL_FORMAT_OPEN_EXR_DEEP ||
		 bc->GetInt32(DL_DEFAULT_IMAGE_FORMAT) == DL_FORMAT_OPEN_EXR_DWAA);
	Bool TIFF_FORMAT = bc->GetInt32(DL_DEFAULT_IMAGE_FORMAT) == DL_FORMAT_TIFF;
	Bool PNG_FORMAT = bc->GetInt32(DL_DEFAULT_IMAGE_FORMAT) == DL_FORMAT_PNG;

	/**
	            Based on the selected Image format we use ShowDescriptionElement
	   function which is implemented above to hide the dropdowns that does not
	   correspond with the selected format, and show just the one which
	   represents the selected image format. A description contains information
	   about the parameters of a node object.
	    */
	if (OPEN_EXR_FORMAT) {
		ShowDescriptionElement(
			i_node, i_description, DL_DEFAULT_IMAGE_OUTPUT, OPEN_EXR_FORMAT);
		ShowDescriptionElement(
			i_node, i_description, DL_DEFAULT_IMAGE_OUTPUT_TIFF, !OPEN_EXR_FORMAT);
		ShowDescriptionElement(
			i_node, i_description, DL_DEFAULT_IMAGE_OUTPUT_PNG, !OPEN_EXR_FORMAT);
		ShowDescriptionElement(
			i_node, i_description, DL_DEFAULT_IMAGE_OUTPUT_JPG, !OPEN_EXR_FORMAT);
		bc->SetInt32(DL_DEFAULT_IMAGE_BITDEPTH,
					 bc->GetInt32(DL_DEFAULT_IMAGE_OUTPUT));

	} else if (TIFF_FORMAT) {
		ShowDescriptionElement(
			i_node, i_description, DL_DEFAULT_IMAGE_OUTPUT, !TIFF_FORMAT);
		ShowDescriptionElement(
			i_node, i_description, DL_DEFAULT_IMAGE_OUTPUT_TIFF, TIFF_FORMAT);
		ShowDescriptionElement(
			i_node, i_description, DL_DEFAULT_IMAGE_OUTPUT_PNG, !TIFF_FORMAT);
		ShowDescriptionElement(
			i_node, i_description, DL_DEFAULT_IMAGE_OUTPUT_JPG, !TIFF_FORMAT);
		bc->SetInt32(DL_DEFAULT_IMAGE_BITDEPTH,
					 bc->GetInt32(DL_DEFAULT_IMAGE_OUTPUT_TIFF));

	}
	else if (PNG_FORMAT) {
		ShowDescriptionElement(
			i_node, i_description, DL_DEFAULT_IMAGE_OUTPUT, !PNG_FORMAT);
		ShowDescriptionElement(
			i_node, i_description, DL_DEFAULT_IMAGE_OUTPUT_TIFF, !PNG_FORMAT);
		ShowDescriptionElement(
			i_node, i_description, DL_DEFAULT_IMAGE_OUTPUT_PNG, PNG_FORMAT);
		ShowDescriptionElement(
			i_node, i_description, DL_DEFAULT_IMAGE_OUTPUT_JPG, !PNG_FORMAT);
		bc->SetInt32(DL_DEFAULT_IMAGE_BITDEPTH,
					 bc->GetInt32(DL_DEFAULT_IMAGE_OUTPUT_PNG));

	} else if (bc->GetString(DL_ISCLICKED) == "AbortRender") {
		ShowDescriptionElement(i_node, i_description, DL_RENDER_BUTTON, true);
		ShowDescriptionElement(
			i_node, i_description, DL_ABORT_RENDER_BUTTON, false);
	}

	if (bc->GetString(DL_ISCLICKED) == "IPRRender") {
		ShowDescriptionElement(i_node, i_description, DL_START_IPR, false);
		ShowDescriptionElement(i_node, i_description, DL_ABORT_IPR, true);

	} else if (bc->GetString(DL_ISCLICKED) == "AbortIPRRender") {
		ShowDescriptionElement(i_node, i_description, DL_START_IPR, true);
		ShowDescriptionElement(i_node, i_description, DL_ABORT_IPR, false);
	}

	if (bc->GetString(DL_ISCLICKED) == "SeqRender") {
		ShowDescriptionElement(i_node, i_description, DL_RENDER_SEQ, false);
		ShowDescriptionElement(i_node, i_description, DL_ABORT_RENDER_SEQ, true);

	} else if (bc->GetString(DL_ISCLICKED) == "AbortSeqRender") {
		ShowDescriptionElement(i_node, i_description, DL_RENDER_SEQ, true);
		ShowDescriptionElement(i_node, i_description, DL_ABORT_RENDER_SEQ, false);
	}

	// Check for output group toggle.
	if (bc->GetString(DL_OUTPUT_CHECK) == "Display") {
		bc->SetBool(DISPLAY_RENDERED_IMAGES, true);
		bc->SetBool(SAVE_RENDERED_IMAGES, false);
		bc->SetBool(SAVE_AND_DISPLAY, false);
		bc->SetBool(OUTPUT_NSI_FILES, false);

	} else if (bc->GetString(DL_OUTPUT_CHECK) == "Save") {
		bc->SetBool(SAVE_RENDERED_IMAGES, true);
		bc->SetBool(DISPLAY_RENDERED_IMAGES, false);
		bc->SetBool(SAVE_AND_DISPLAY, false);
		bc->SetBool(OUTPUT_NSI_FILES, false);

	} else if (bc->GetString(DL_OUTPUT_CHECK) == "Save_Display") {
		bc->SetBool(SAVE_AND_DISPLAY, true);
		bc->SetBool(DISPLAY_RENDERED_IMAGES, false);
		bc->SetBool(SAVE_RENDERED_IMAGES, false);
		bc->SetBool(OUTPUT_NSI_FILES, false);

	} else if (bc->GetString(DL_OUTPUT_CHECK) == "OutputNSI") {
		bc->SetBool(OUTPUT_NSI_FILES, true);
		bc->SetBool(DISPLAY_RENDERED_IMAGES, false);
		bc->SetBool(SAVE_RENDERED_IMAGES, false);
		bc->SetBool(SAVE_AND_DISPLAY, false);
	}

	i_flags |= DESCFLAGS_DESC::LOADED;
	/*if (bc->GetString(DL_ISCLICKED) == "IPR")
	    {
	            ShowDescriptionElement(i_node, i_description,
	   RENDER_CYCLEBUTTON, false); ShowDescriptionElement(i_node, i_description,
	   ABORT_IPR, true);
	    }
	    else if(bc->GetString(DL_ISCLICKED) =="ABORT_IPR")
	    {
	            ShowDescriptionElement(i_node, i_description,
	   RENDER_CYCLEBUTTON, true); ShowDescriptionElement(i_node, i_description,
	   ABORT_IPR, false);
	    }*/
	return SUPER::GetDDescription(i_node, i_description, i_flags);
}

Bool RenderSettings::SetDParameter(GeListNode* node,
								   const DescID& id,
								   const GeData& t_data,
								   DESCFLAGS_SET& flags)
{
	BaseContainer* bc = ((BaseVideoPost*) node)->GetDataInstance();
	String clicked_button = bc->GetString(DL_ISCLICKED);
	bool display = false;
	bool image = false;
	bool display_image = false;
	bool nsi = false;

	switch (id[0].id) {
	case DISPLAY_RENDERED_IMAGES: {
		bc->SetString(DL_OUTPUT_CHECK, "Display"_s);
		break;
	}

	case SAVE_RENDERED_IMAGES: {
		bc->SetString(DL_OUTPUT_CHECK, "Save"_s);
		break;
	}

	case SAVE_AND_DISPLAY: {
		bc->SetString(DL_OUTPUT_CHECK, "Save_Display"_s);
		break;
	}

	case OUTPUT_NSI_FILES: {
		bc->SetString(DL_OUTPUT_CHECK, "OutputNSI"_s);
		break;
	}
	}

	return true;
}

/**
        Overriden function which is called to decide which description
   parameters should be enabled or disabled after a specific action.
*/
Bool RenderSettings::GetDEnabling(GeListNode* i_node,
								  const DescID& i_id,
								  const GeData& i_data,
								  DESCFLAGS_ENABLE i_flags,
								  const BaseContainer* i_itemdesc)
{
	BaseContainer* bc = ((BaseVideoPost*) i_node)->GetDataInstance();
	String clicked_button = bc->GetString(DL_ISCLICKED);

	switch (i_id[0].id) {
	case DL_START_IPR:
		return (clicked_button != "Render" && clicked_button != "SeqRender");

	case DL_RENDER_BUTTON:
		return (clicked_button != "IPRRender" && clicked_button != "SeqRender");

	case DL_RENDER_SEQ:
		return (clicked_button != "IPRRender" && clicked_button != "Render");
	}

	if (i_id[0].id == DL_MULTILAYER_FILE) {
		BaseList2D* settings = (BaseList2D*) i_node;
		BaseContainer* data = settings->GetDataInstance();
		Int32 fileformat = data->GetInt32(DL_DEFAULT_IMAGE_FORMAT);

		if (fileformat == DL_FORMAT_PNG) { // Only output multilayer file if the
			// fileformat supports this
			return false;
		}
	}

	return true;
}

Bool RenderSettings::Message(GeListNode* i_node, Int32 i_type, void* i_data)
{
	switch (i_type) {
	case MSG_DESCRIPTION_COMMAND: {
		DescriptionCommand* dc = (DescriptionCommand*) i_data;
		BaseContainer* dldata = ((BaseVideoPost*) i_node)->GetDataInstance();
		BaseDocument* doc = GetActiveDocument();
		BaseTime t = doc->GetTime();
		/*
			        if (dc->_descId[0].id == RENDER_CYCLEBUTTON)
			        {
			                Int32 action = dldata->GetInt32(RENDER_CYCLEBUTTON);
			                long frame = t.GetFrame(doc->GetFps());
			                if (action == 0) //If render button is clicked
			                {
			                        dldata->SetString(DL_ISCLICKED, "Render"_s);
			                        Render3Dl(doc, frame, DL_INTERACTIVE, true,
			   dldata);
			                }
			                //If Export to NSI File button is clicked
			                else if (action == 2)
			                {
			                        dldata->SetString(DL_ISCLICKED, "Export"_s);
			                        Filename folder;
			                        String directory =
			   GeGetStartupApplication().GetString() + "/NSI";
			                        folder.SetDirectory(directory);
			                        folder.SetSuffix("nsi"_s);
			                        folder.FileSelect(FILESELECTTYPE::ANYTHING,
			   FILESELECT::SAVE, "Select Folder"_s);
			                        dldata->SetFilename(DL_FOLDER_OUTPUT, folder);
			                        Render3Dl(doc, frame, DL_INTERACTIVE, true,
			   dldata);
			                }
			                else if (action == 1)
			                {
			                        dldata->SetString(DL_ISCLICKED, "IPR"_s);
			                        CallCommand(ID_INTERACTIVE_RENDERING_START);
			                }
			        }

			        else if (dc->_descId[0].id == ABORT_IPR)
			        {
			                dldata->SetString(DL_ISCLICKED, "ABORT_IPR"_s);
			                CallCommand(ID_INTERACTIVE_RENDERING_STOP);
			        }
			        */
		long frame = t.GetFrame(doc->GetFps());
		BaseDocument* renderdoc =
			(BaseDocument*) doc->GetClone(COPYFLAGS::DOCUMENT, nullptr);
		SceneParser* parser = new SceneParser(renderdoc);
		AutoAlloc<Description> desc;

		switch (dc->_descId[0].id) {
		case DL_RENDER_BUTTON:
			dldata->SetString(DL_ISCLICKED, "Render"_s);
			SpecialEventAdd(DL_START_RENDER);
			break;

		case DL_START_IPR:
			dldata->SetString(DL_ISCLICKED, "IPRRender"_s);
			CallCommand(ID_INTERACTIVE_RENDERING_START);
			break;

		case DL_RENDER_SEQ:
			dldata->SetString(DL_ISCLICKED, "SeqRender"_s);
			break;

		case DL_ABORT_RENDER_BUTTON:
			dldata->SetString(DL_ISCLICKED, "AbortRender"_s);
			SpecialEventAdd(DL_STOP_RENDERING);
			break;

		case DL_ABORT_IPR:
			dldata->SetString(DL_ISCLICKED, "AbortIPRRender"_s);
			CallCommand(ID_INTERACTIVE_RENDERING_STOP);
			break;

		case DL_ABORT_RENDER_SEQ:
			dldata->SetString(DL_ISCLICKED, "AbortSeqRender"_s);
			break;

		default:
			break;
		}

		break;
	}
	}

	return true;
}

Bool RenderSettings::RenderEngineCheck(BaseVideoPost* node, Int32 id)
{
	switch (id) {
	case RENDERSETTING_STATICTAB_MULTIPASS:
	case RENDERSETTING_STATICTAB_ANTIALIASING:
	case RENDERSETTING_STATICTAB_OPTIONS:
	case RENDERSETTING_STATICTAB_STEREO:
	case RENDERSETTING_STATICTAB_OVERRIDEMAT:
		return false;
	}

	return true;
}

Bool RenderSettings::Init(GeListNode* i_node)
{
	BaseContainer* dldata = ((BaseVideoPost*) i_node)->GetDataInstance();
	dldata->SetBool(DL_TEXT_CHECKER, false);
	dldata->SetBool(DISPLAY_RENDERED_IMAGES, true);
	dldata->SetBool(SAVE_RENDERED_IMAGES, false);
	dldata->SetBool(SAVE_ADDITIONAL_JPEG, false);
	dldata->SetBool(OUTPUT_NSI_FILES, false);
	dldata->SetInt32(DL_SHADING_SAMPLES, 64);
	dldata->SetInt32(DL_PIXEL_SAMPLES, 8);
	dldata->SetInt32(DL_VOLUME_SAMPLES, 16);
	dldata->SetInt32(DL_PIXEL_FILTER, DL_BLACKMAN_HARRIS);
	dldata->SetFloat(DL_FILTER_WIDTH, 3.000);
	dldata->SetBool(DL_MOTION_BLUR, true);
	dldata->SetBool(DL_DEPTH_OF_FIELD, false);
	dldata->SetInt32(DL_MAX_DIFFUSE_DEPTH, 2);
	dldata->SetInt32(DL_MAX_REFLECTION_DEPTH, 2);
	dldata->SetInt32(DL_MAX_REFRACTION_DEPTH, 4);
	dldata->SetInt32(DL_MAX_HAIR_DEPTH, 5);
	dldata->SetFloat(DL_MAX_DISTANCE, 1000);
	dldata->SetInt32(DL_DEFAULT_IMAGE_FORMAT, DL_FORMAT_OPEN_EXR);
	dldata->SetInt32(DL_DEFAULT_IMAGE_OUTPUT, DL_SIXTEEN_BIT_FLOAT);
	dldata->SetInt32(DL_DEFAULT_IMAGE_BITDEPTH, DL_SIXTEEN_BIT_FLOAT);
	dldata->SetInt32(DL_DEFAULT_IMAGE_OUTPUT_TIFF, DL_THIRTYTWO_BIT);
	dldata->SetInt32(DL_DEFAULT_IMAGE_OUTPUT_JPG, DL_EIGHT_BIT);
	dldata->SetInt32(DL_DEFAULT_IMAGE_OUTPUT_PNG, DL_EIGHT_BIT);
	dldata->SetBool(DL_MULTILAYER_FILE, true);
	// Filename file = GeGetPluginPath() + Filename("NSI");
	// Filename file("Untitled");
	// dldata->SetFilename(DL_DEFAULT_IMAGE_FILENAME, file);
	dldata->SetInt32(DL_RESOLUTION, DL_HALF);
	dldata->SetInt32(DL_SAMPLING, DL_TEN_PERCENT);
	dldata->SetFloat(DL_PIXEL_ASPECT_RATIO, 0);
	dldata->SetFloat(DL_RENDER_MULTILIGHT, DL_RENDER_MULTILIGHT_OFF);
	return TRUE;
}

RENDERRESULT
RenderSettings::Execute(BaseVideoPost* node, VideoPostStruct* vps)
{
	if (vps == nullptr) {
		return RENDERRESULT::USERBREAK;

	} else if (vps->thread && vps->thread->TestBreak()) {
		return RENDERRESULT::USERBREAK;
	}

	switch (vps->vp) {
	case (VIDEOPOSTCALL::INNER): {
		if (vps->open && *vps->error == RENDERRESULT::OK) {
			RENDERFLAGS flags = vps->renderflags;
			bool external_render = false;

			if (flags & RENDERFLAGS::EXTERNAL) {
				external_render = true;
			}

			VPBuffer* colorBuf = vps->render->GetBuffer(VPBUFFER_RGBA, NOTOK);
			maxon::Int32 const xres = colorBuf->GetInfo(VPGETINFO::XRESOLUTION);
			maxon::Int32 const yres = colorBuf->GetInfo(VPGETINFO::YRESOLUTION);
			Int32 bit_depth = colorBuf->GetInfo(VPGETINFO::BITDEPTH);
			Int32 ColorPerPixel = colorBuf->GetInfo(VPGETINFO::CPP);
			iferr_scope_handler {
				return RENDERRESULT::OUTOFMEMORY;
			};
			// Dl rendering starts
			BaseDocument* doc = vps->doc;
			BaseDocument* renderdoc =
				(BaseDocument*) doc->GetClone(COPYFLAGS::DOCUMENT, nullptr);
			BaseTime t = renderdoc->GetTime();
			long frame = t.GetFrame(renderdoc->GetFps());
			SceneParser* sp = new SceneParser(renderdoc);
			sp->SetRenderMode(C4D_PREVIEW);
			sp->SetCustomRenderResolution(xres, yres);
			NSI::Context& ctx(sp->GetContext());
			// NSIErrorHandler_t eh = NSIErrorHandlerC4D;
			ctx.Begin((NSI::StringArg("type", "render")
					   // NSI::PointerArg("errorhandler", eh)
					  ));
			// Render scene
			bool RenderOK = sp->InitScene(false, frame);
			sp->SampleFrameMotion(frame);
			// Add display driver
			std::string driver_handle = "c4d_displayDriver";
			ctx.Create(driver_handle, "outputdriver");
			ctx.SetAttribute(driver_handle,
							 (NSI::StringArg("drivername", "C4D_bitmap"),
							  NSI::StringArg("imagefilename", "bitmap_output"),
							  NSI::PointerArg("BaseBitmap", colorBuf)));
			std::string layer_handle("c4d_outputlayer");
			ctx.Create(layer_handle, "outputlayer");
			// TODO: Get color profile from c4d render settings
			ctx.SetAttribute(layer_handle,
							 (NSI::StringArg("variablename", "Ci"),
							  NSI::StringArg("colorprofile", "srgb"),
							  NSI::StringArg("layername", "preview"),
							  NSI::StringArg("layertype", "color"),
							  NSI::StringArg("variablesource", "shader"),
							  NSI::StringArg("scalarformat", "float"),
							  NSI::IntegerArg("withalpha", 1),
							  NSI::StringArg("filter", "gaussian")));
			ctx.Connect(driver_handle, "", layer_handle, "outputdrivers");
			ctx.Connect(
				layer_handle, "", "3dlfc4d::scene_camera_screen", "outputlayers");
			BaseDocument::Free(renderdoc);
			ctx.SetAttribute(NSI_SCENE_GLOBAL,
							 (NSI::IntegerArg("renderatlowpriority", 1)));
			int progressive = 0;
			bool finished = false;
			ctx.RenderControl(
				(NSI::StringArg("action", "start"),
				 NSI::IntegerArg("progressive", progressive),
				 NSI::PointerArg("stoppedcallback", (void*) &FrameStopped),
				 NSI::PointerArg("stoppedcallbackdata", (void*) &finished)));

			while (!finished) {
				GeSleep(200); // Check periodically if rendering is finished or

				// has stopped
				// Note: The GeSleep() function is marked as deprecated in the
				// c4d API, so this code should eventually be changed to use
				// "condition variables"
				if (vps->thread->TestBreak()) {
					ctx.RenderControl((NSI::StringArg("action", "stop")));
					return RENDERRESULT::USERBREAK;
				}
			}

			delete sp;
			//			DL Rendering ends
		}

		break;
	}

	case (VIDEOPOSTCALL::RENDER): {
		if (vps->vd && vps->open) {
			vps->vd->SkipRenderProcess();
		}

		break;
	}
	}

	return RENDERRESULT::OK;
}

Bool Register3DelightPlugin(void)
{
	// When we register the plugin we also register the icons for the aov layers
	RegisterIcon(DL_DISPLAY_ON, GeGetPluginResourcePath() + "display-on.png");
	RegisterIcon(DL_DISPLAY_OFF, GeGetPluginResourcePath() + "display.png");
	RegisterIcon(DL_FOLDER_ON, GeGetPluginResourcePath() + "folder-on.png");
	RegisterIcon(DL_FOLDER_OFF, GeGetPluginResourcePath() + "folder.png");
	RegisterIcon(DL_JPG_ON, GeGetPluginResourcePath() + "jpg-on.png");
	RegisterIcon(DL_JPG_OFF, GeGetPluginResourcePath() + "jpg.png");
	return RegisterVideoPostPlugin(ID_RENDERSETTINGS,
								   "3Delight"_s,
								   PLUGINFLAG_VIDEOPOST_ISRENDERER,
								   RenderSettings::Alloc,
								   "dlrendersettings"_s,
								   0,
								   0);
}
