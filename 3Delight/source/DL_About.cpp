#include "DL_About.h"
#include <string>
#include "IDs.h"
#include "NSIAPI.h"
#include "delight.h"
#include "nsi.hpp"
#include "RegisterPrototypes.h"

Bool AboutDialog::CreateLayout()
{
	NSI::DynamicAPI& api = NSIAPI::Instance().API();
	decltype(&DlGetVersionString) get_version = nullptr;
	api.LoadFunction(get_version, "DlGetVersionString");
	decltype(&DlIsFreeLibrary) is_free = nullptr;
	api.LoadFunction(is_free, "DlIsFreeLibrary");
	decltype(&DlGetCopyrightString) get_copyright = nullptr;
	api.LoadFunction(get_copyright, "DlGetCopyrightString");
	// Cut string at first space to keep only version number.
	std::string version = get_version();
	version = version.substr(0, version.find(' '));
	std::string BuildDate = get_version();
	BuildDate = BuildDate.substr(BuildDate.find("(") + 1);
	BuildDate = BuildDate.substr(0, BuildDate.find(","));
	Bool IsFree = is_free();
	String CopyRight = get_copyright();
	SetTitle("About 3Delight"_s);
	GroupBegin(41231, BFH_SCALE | BFH_CENTER, 1, 6, ""_s, 0);
	GroupBorderSpace(0, 20, 0, 0);
	AddStaticText(3213, BFH_CENTER, 0, 0, "3Delight For Cinema4D"_s, BORDER_NONE);
	GroupEnd();
	GroupBegin(41232, BFH_SCALE | BFH_CENTER, 1, 6, ""_s, 0);
	GroupBorderSpace(0, 10, 0, 0);
	AddStaticText(
		3214, BFH_CENTER, 0, 0, "Version " + (String) version.c_str(), BORDER_NONE);
	AddStaticText(3215,
				  BFH_CENTER,
				  0,
				  0,
				  "Built on " + (String) BuildDate.c_str(),
				  BORDER_NONE);
	GroupEnd();
	GroupBegin(41234, BFH_SCALE | BFH_CENTER, 1, 6, ""_s, 0);

	if (IsFree) {
		GroupBorderSpace(0, 13, 0, 0);
		AddStaticText(
			3216, BFH_CENTER, 0, 0, "Using Free 3Delight License"_s, BORDER_NONE);

	} else {
		GroupBorderSpace(0, 35, 0, 0);
	}

	GroupBegin(41235, BFH_SCALE | BFH_CENTER, 1, 6, ""_s, 0);
	GroupBorderSpace(0, 13, 0, 0);
	AddStaticText(3217, BFH_CENTER, 0, 0, CopyRight, BORDER_NONE);
	AddStaticText(3218, BFH_CENTER, 0, 0, "All rights reserved."_s, BORDER_NONE);
	GroupEnd();
	return true;
}

Bool DL_About::Execute(BaseDocument* doc, GeDialog* parentManager)
{
	NSI::DynamicAPI& api = NSIAPI::Instance().API();

	if (dialog.IsOpen() == false) {
		dialog.Open(DLG_TYPE::ASYNC, 334455, -1, -1, 350, 200);

	} else {
		dialog.Close();
	}

	return true;
}

Bool RegisterAbout(void)
{
	return RegisterCommandPlugin(ABOUT_ID,
								 "About..."_s,
								 PLUGINFLAG_HIDEPLUGINMENU,
								 0,
								 String(),
								 NewObjClear(DL_About));
}
