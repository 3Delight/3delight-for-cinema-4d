#include "ExpandFilename.h"
#include <iomanip>
#include <sstream>
#include "DL_Utilities.h"

using namespace std;

void ExpandFilename(std::string& filename,
					long frame,
					std::string document_path,
					std::string document_name,
					std::string aov)
{
	ostringstream ss;
	ss << frame;
	string framenumber = ss.str();
	ostringstream ss2;
	ss2 << std::setw(4) << std::setfill('0') << frame;
	string framenumber_padded = ss2.str();
	bool found_framenumber = false;
	Replace(filename, "<aov>", aov);
	Replace(filename, "<project>", document_path);
	Replace(filename, "<scene>", document_name);

	if (Replace(filename, "@", framenumber)) {
		found_framenumber = true;
	}

	if (Replace(filename, "#", framenumber_padded)) {
		found_framenumber = true;
	}

	if (!found_framenumber) {
		filename = filename + framenumber_padded;
	}
}
