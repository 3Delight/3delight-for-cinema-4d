#include "DL_StandIn_Translator.h"
#include "DL_TypeConversions.h"
#include "dl_standin.h"
#include "nsi.hpp"

void StandInNode_Translator::CreateNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	std::string standin_handle = (std::string) Handle;
	ctx.Create(standin_handle, "procedural");
	std::string transform_handle = "X0_" + (std::string) Handle;
	ctx.Connect(standin_handle, "", transform_handle, "objects");
}

void StandInNode_Translator::ConnectNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	return;
}

void StandInNode_Translator::SampleAttributes(DL_SampleInfo* info,
		const char* Handle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	std::string standin_handle = (std::string) Handle;
	BaseObject* standin_obj = (BaseObject*) C4DNode;
	BaseContainer* data = standin_obj->GetDataInstance();
	std::string texturename = "";
	Filename texturefile = data->GetFilename(DL_STANDIN_FILENAME);

	if (texturefile.IsPopulated()) {
		texturename = StringToStdString(texturefile.GetString());
	}

	ctx.SetAttribute(standin_handle,
					 (NSI::CStringPArg("type", "apistream"),
					  NSI::CStringPArg("filename", texturename.c_str())));
}
