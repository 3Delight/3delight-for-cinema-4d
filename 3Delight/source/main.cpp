#include "main.h"

Bool PluginStart(void)
{
	// if (!RegisterCustomGUITest()) return FALSE;
	if (!RegisterPreferences()) {
		return FALSE;
	}

	if (!RegisterCustomListView()) {
		return FALSE;
	}

	if (!RegisterCustomMultiLight()) {
		return FALSE;
	}

	if (!RegisterInteractiveRenderingStart()) {
		return FALSE;
	}

	if (!RegisterInteractiveRenderingStop()) {
		return FALSE;
	}

	if (!Register3DelightPlugin()) {
		return FALSE;
	}

	// if (!Register3DelightCloudPlugin()) return FALSE;
	if (!RegisterInteractiveRenderManager()) {
		return FALSE;
	}

	if (!RegisterRenderManager()) {
		return FALSE;
	}

	if (!Register3DelightCommand()) {
		return FALSE;
	}

	if (!Register3DelightBatchRenderCommand()) {
		return FALSE;
	}

	if (!RegisterNSIExportCommand()) {
		return FALSE;
	}

	if (!RegisterCreateShelf()) {
		return FALSE;
	}

	if (!RegisterAbout()) {
		return FALSE;
	}

	if (!RegisterHelp()) {
		return FALSE;
	}

	// if (!RegisterPreferences()) return FALSE;
	
	if (!RegisterDL_CameraTag()) {
		return FALSE;
	}

	if (!RegisterDL_MotionBlurTag()) {
		return FALSE;
	}

	if (!RegisterDL_VisibilityTag()) {
		return FALSE;
	}

	if (!RegisterDL_SubdivisionSurfaceTag()) {
		return FALSE;
	}

	if (!RegisterStandinNode()) {
		return FALSE;
	}

	if (!RegisterStandInExport()) {
		return FALSE;
	}

	// Register 3Delight display driver for rendering to C4D bitmaps
	NSI::DynamicAPI& api = NSIAPI::Instance().API();
	decltype(&DspyRegisterDriverTable) PDspyRegisterDriverTable = nullptr;
	api.LoadFunction(PDspyRegisterDriverTable, "DspyRegisterDriverTable");

	if (PDspyRegisterDriverTable == nullptr) {
		CriticalOutput("The 3Delight dynamic library could not be found. "
					   "The 3Delight for Cinema4D plug-in failed to initialize.");
		return false;
	}

	PtDspyDriverFunctionTable table;
	memset(&table, 0, sizeof(table));
	table.Version = 1;
	table.pOpen = &BmpDspyImageOpen;
	table.pQuery = &BmpDspyImageQuery;
	table.pWrite = &BmpDspyImageData;
	table.pClose = &BmpDspyImageClose;
	PDspyRegisterDriverTable("C4D_bitmap", &table);
	return true;
}

void PluginEnd(void)
{
	NSIAPI::DeleteInstance();
}

Bool PluginMessage(Int32 id, void* data)
{
	switch (id) {
	case C4DPL_INIT_SYS:
		if (!g_resource.Init()) {
			return false; // don't start plugin without resource
		}

		return true;

	case C4DPL_STARTACTIVITY: {
		// Send message to load translators from other plugins
		GePluginMessage(DL_LOAD_PLUGINS, (void*) &PM);
		break;
	}

	case DL_LOAD_PLUGINS: {
		DL_PluginManager* pm = (DL_PluginManager*) data;
		pm->RegisterHook(AllocateHook<CameraHook>);
		pm->RegisterHook(AllocateHook<RenderOptionsHook>);
		pm->RegisterTranslator(ID_DL_VISIBILITYTAG,
							   AllocateTranslator<VisibilityTagTranslator>);
		pm->RegisterTranslator(STAND_IN_NODE_CREATE,
							   AllocateTranslator<StandInNode_Translator>);
		// pm->RegisterTranslator(,AllocateTranslator<QualityValuesParser>);
		break;
	}
	}

	return false;
}
