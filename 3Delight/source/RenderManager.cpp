#include "RenderManager.h"
#include "ErrorHandler.h"
#include "IDs.h"
#include "nsi.hpp"
#include "dlrendersettings.h"
#include "DL_TypeConversions.h"
#include "dl_preferences.h"
#include "RegisterPrototypes.h"
#include "c4d_messageplugin.h"
#include <nsi_dynamic.hpp>

namespace
{
	NSI::DynamicAPI& GetNSIAPI()
	{
		static NSI::DynamicAPI api;
		return api;
	}
}

const std::string k_stdout = "stdout";

static
void RenderManagerFrameStoppedCallback(void* stoppedcallbackdata,
									   NSIContext_t ctx,
									   int status)
{
	// End context when rendering is completed (or stopped)
	if (status != NSIRenderAborted && status != NSIRenderCompleted)
		return;

	SceneParser* sp = (SceneParser*)stoppedcallbackdata;
	delete sp;
	sp = nullptr;
	SpecialEventAdd(DL_FRAME_STOPPED);
}

RenderManager::RenderManager()
	: parser(0x0) {}

RenderManager::~RenderManager()
{
	delete (parser);
}

void RenderManager::FillFrames()
{
	BaseDocument* doc = GetActiveDocument();
	RenderData* rd = doc->GetActiveRenderData();
	BaseContainer render_data = rd->GetData();
	BaseTime t = doc->GetTime();

	Int32 FrameSequence = render_data.GetInt32(RDATA_FRAMESEQUENCE);

	if (FrameSequence == RDATA_FRAMESEQUENCE_MANUAL) {
		BaseTime t1 = render_data.GetTime(RDATA_FRAMEFROM);
		BaseTime t2 = render_data.GetTime(RDATA_FRAMETO);
		m_start_time = t1.GetFrame(doc->GetFps());
		m_end_time = t2.GetFrame(doc->GetFps());

	} else if (FrameSequence == RDATA_FRAMESEQUENCE_CURRENTFRAME) {
		m_start_time = m_end_time = t.GetFrame(doc->GetFps());

	} else if (FrameSequence == RDATA_FRAMESEQUENCE_ALLFRAMES) {
		BaseTime t1 = doc->GetMinTime();
		BaseTime t2 = doc->GetMaxTime();
		m_start_time = t1.GetFrame(doc->GetFps());
		m_end_time = t2.GetFrame(doc->GetFps());

	} else if (FrameSequence == RDATA_FRAMESEQUENCE_PREVIEWRANGE) {
		BaseTime t1 = doc->GetLoopMinTime();
		BaseTime t2 = doc->GetLoopMaxTime();
		m_start_time = t1.GetFrame(doc->GetFps());
		m_end_time = t2.GetFrame(doc->GetFps());
	}

	m_frame_step = render_data.GetInt32(RDATA_FRAMESTEP, 1);
}

void RenderManager::StopRender(NSI::Context& i_nsi)
{
	m_render_end_mutex.lock();

	if (parser)
	{
		if (m_sequence_context)
		{
			m_sequence_context->RenderControl(
				NSI::CStringPArg("action", "stop"));
			m_render_end_mutex.unlock();
		}
		else
		{
			// Unlock the mutex so the stopper callback can do its job.
			m_render_end_mutex.unlock();

			/*
				Notify the background rendering thread that it should stop (and,
				implicitly, wait for it to be finished, including the call to
				the stopper callback).
			*/
			i_nsi.RenderControl(NSI::CStringPArg("action", "stop"));
		}
	}
	else
	{
		m_render_end_mutex.unlock();
	}

	/*
		Wait for m_sequence_waiter to finish. This has to be done even if we
		didn't explicitly terminate the renderdl process (ie : if it has
		finished on its own), because we don't want	the thread hanging loose.
	*/
	if (m_sequence_waiter.joinable())
	{
		m_sequence_waiter.join();
	}
}

void RenderManager::Render(NSI::Context& i_nsi)
{
	for (Int32 frame = m_start_time; frame <= m_end_time; frame += m_frame_step)
	{
		if (m_batch)
		{
			parser->SetRenderMode(DL_BATCH);
			StatusSetText("Batch rendering frame " + String::IntToString(frame));
		}
		else
		{
			StatusSetText("Rendering frame " + String::IntToString(frame));
		}

		BaseDocument* doc = GetActiveDocument();
		BaseDocument* renderdoc = (BaseDocument*)doc->GetClone(COPYFLAGS::DOCUMENT, nullptr);
		RenderData* rd = renderdoc->GetActiveRenderData();
		BaseContainer* bc = GetWorldContainerInstance()->GetContainerInstance(PREFERENCES_ID);

		if (!bc)
		{
			GetWorldContainerInstance()->SetContainer(PREFERENCES_ID,BaseContainer());
			bc = GetWorldContainerInstance()->GetContainerInstance(PREFERENCES_ID);
		}

		bool output_nsi = false;
		bool has_vp = false;
		BaseVideoPost* vp = rd->GetFirstVideoPost();
		BaseContainer* data;

		while (vp != NULL && !has_vp)
		{
			// Look for rendersettings
			has_vp = (vp->GetType() == ID_RENDERSETTINGS);

			if (!has_vp)
			{
				vp = vp->GetNext();
			}
		}

		if (has_vp)
		{
			data = vp->GetDataInstance();
			output_nsi = data->GetBool(OUTPUT_NSI_FILES);
		}

		NSI::ArgumentList arglist;

		if (m_output_nsi)
		{
			std::string export_file = GetNSIExportFilename(data, frame);
			arglist.Add(new NSI::StringArg("type", "apistream"));
			arglist.Add(new NSI::StringArg("streamfilename", export_file));
			arglist.Add(new NSI::StringArg("streamformat", "nsi"));
		}

		else if (m_sequence_context)
		{
			/*
				When exporting a sequence, the frames are tied to a sequence context.
				3Delight takes care of storing, rendering, and deleting them.
			*/
			i_nsi.Begin(NSI::IntegerArg(
				"sequencecontext", m_sequence_context->Handle()));
		}

		else
		{
			NSI::ArgumentList argList;

			if (m_ipr)
				argList.Add(new NSI::CStringPArg("software", "CINEMA4D_LIVE"));
			else
				argList.Add(new NSI::CStringPArg("software", "CINEMA4D"));

			argList.Add(new NSI::IntegerArg("readpreferences", 1));

			// Render directly from the current process
			i_nsi.Begin(argList);
		}

		// Render scene
		parser->InitScene(true, frame);
		parser->SampleFrameMotion(frame);
		BaseDocument::Free(renderdoc);
		RENDER_MODE mode = parser->GetRenderMode();

		/** For non-batch renders, use lower priority */
		if (mode != DL_EXPORT_NSI && mode != DL_BATCH)
		{
			i_nsi.SetAttribute(
				NSI_SCENE_GLOBAL, NSI::IntegerArg("renderatlowpriority", 1));
		}

		if (BackgroundThreadRendering())
		{
			i_nsi.RenderControl(
				(
					NSI::CStringPArg("action", "start"),
					NSI::PointerArg("stoppedcallback", (const void*)RenderManagerFrameStoppedCallback),
					NSI::PointerArg("stoppedcallbackdata", (void*) parser)
				));
		}
		else
		{
			i_nsi.RenderControl(NSI::CStringPArg("action", "start"));
			i_nsi.End();
		}
	}
}

std::string RenderManager::GetNSIExportFilename(BaseContainer* data, long i_frame)const
{
	if (!m_output_nsi)
		return {};

	Filename outfile = data->GetFilename(OUTPUT_NSI_FILES_PATH);
	String filepart = outfile.GetFileString();
	outfile.SetFile(Filename(filepart + (String)std::to_string(i_frame).c_str()));
	outfile.SetSuffix(String("nsi"));
	std::string filename_string = StringToStdString(outfile.GetString());

	if (filename_string.length() == 0)
	{
		// When no file is specified, we output to standard output by default
		return k_stdout;
	}

	return filename_string;
}

Bool RenderManager::CoreMessage(Int32 id, const BaseContainer& bc)
{
	if (id == DL_START_RENDER || id == DL_START_BATCH_RENDER)
	{
		bool has_vp = false;

		BaseDocument* doc = GetActiveDocument();
		RenderData* rd = doc->GetActiveRenderData();
		BaseVideoPost* vp = rd->GetFirstVideoPost();
		BaseContainer* data;

		while (vp != NULL && !has_vp)
		{
			// Look for rendersettings
			has_vp = (vp->GetType() == ID_RENDERSETTINGS);

			if (!has_vp)
			{
				vp = vp->GetNext();
			}
		}

		if (has_vp)
		{
			data = vp->GetDataInstance();
			m_output_nsi = data->GetBool(OUTPUT_NSI_FILES);
		}
		if (parser)
		{
			StopRender(parser->GetContext());
		}

		id == DL_START_RENDER ? m_batch = false : m_batch = true;

		FillFrames();

		NSI::ArgumentList arglist;
		BaseDocument* renderdoc =
			(BaseDocument*)doc->GetClone(COPYFLAGS::DOCUMENT, nullptr);

		parser = new SceneParser(renderdoc);

		if (BackgroundProcessRendering())
		{
			m_sequence_context.reset(new NSI::Context(GetNSIAPI()));

			m_sequence_context->Begin((
				NSI::IntegerArg("createsequencecontext", 1),
				NSI::StringArg("software", "CINEMA4D"),
				NSI::IntegerArg("readpreferences", 1)));

			m_sequence_waiter =
				std::thread(
					[this]()
					{
						m_sequence_context->RenderControl(
							NSI::StringArg("action", "wait"));
						m_sequence_context->End();
						m_sequence_context.reset();

						m_render_end_mutex.lock();

						delete parser; parser = nullptr;

						m_render_end_mutex.unlock();
					});
			m_sequence_waiter.detach();
		}
		Render(parser->GetContext());
		StatusSetText(String(""));

		if (m_sequence_context)
		{
			m_sequence_context->RenderControl(
				NSI::StringArg("action", "endsequence"));
		}
	}

	else if (id == DL_FRAME_STOPPED)
	{
		StatusSetText(String(""));
		parser = 0x0;
	}
	else if (id == DL_STOP_RENDERING)
	{
		if (parser)
		{
			StopRender(parser->GetContext());
		}
	}
	return TRUE;
}

Bool RegisterRenderManager(void)
{
	return RegisterMessagePlugin(
			   DL_RENDER_MANAGER, String(), 0, NewObjClear(RenderManager));
}
