#include "NSIAPI.h"

static NSIAPI* sg_instance = 0x0;

NSIAPI::NSIAPI()
	: m_api() {}

NSIAPI::~NSIAPI() {}

NSIAPI&
NSIAPI::Instance()
{
	if (!sg_instance) {
		sg_instance = new NSIAPI;
	}

	return *sg_instance;
}

void NSIAPI::DeleteInstance()
{
	delete sg_instance;
	sg_instance = 0x0;
}
