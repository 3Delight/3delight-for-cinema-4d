#include "IDs.h"
#include "c4d.h"
#include "RegisterPrototypes.h"

extern bool interactive_rendering_active;

class InteractiveRenderingStop : public CommandData
{
public:
	virtual Bool Execute(BaseDocument* doc, GeDialog* parentManager);
};

Bool InteractiveRenderingStop::Execute(BaseDocument* doc, GeDialog* parentManager)
{
	SpecialEventAdd(DL_STOP_RENDERING);
	return true;
}

Bool RegisterInteractiveRenderingStop(void)
{
	return RegisterCommandPlugin(ID_INTERACTIVE_RENDERING_STOP,
								 "Abort Render"_s,
								 PLUGINFLAG_HIDEPLUGINMENU,
								 AutoBitmap("shelf_abortRender_200.png"_s),
								 String(),
								 NewObjClear(InteractiveRenderingStop));
}
