#ifndef NODE_H
#define NODE_H

#include <memory>
#include <vector>
#include "DL_Translator.h"
#include "c4d.h"

typedef std::shared_ptr<DL_Translator> DL_TranslatorPtr;

struct HierarchyData {
	bool isVisible;
	int cachePos; // Position in the cache hierarchy. Used to keep track of
	// virtual objects.
	int deformationSamples;
	int transformSamples;
	std::string parent_transform;
};

class Node
{
private:
	BaseList2D* c4d_node;
	DL_TranslatorPtr translator;

public:
	HierarchyData hdata;
	std::string handle;
	std::string cache_parent_handle;

	// Int64 dirty_checksum; //Used to determine if the node has been modified
	// and needs to be updated during interactive rendering

	Node(BaseList2D* listnode);

	DL_Translator* GetTranslator();
	BaseList2D* GetC4DNode();
};

#endif
