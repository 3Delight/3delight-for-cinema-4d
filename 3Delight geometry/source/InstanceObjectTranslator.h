#pragma once

#include "DL_API.h"
#include "c4d.h"

class InstanceObjectTranslator : public DL_TranslatorPlugin
{
public:
	virtual bool Accept(BaseList2D* C4DNode);
	virtual void CreateNSINodes(const char* Handle,
								const char* ParentTransformHandle,
								BaseList2D* C4DNode,
								BaseDocument* doc,
								DL_SceneParser* parser);
	virtual void ConnectNSINodes(const char* Handle,
								 const char* ParentTransformHandle,
								 BaseList2D* C4DNode,
								 BaseDocument* doc,
								 DL_SceneParser* parser);
	virtual void SampleAttributes(DL_SampleInfo* info,
								  const char* Handle,
								  BaseList2D* C4DNode,
								  BaseDocument* doc,
								  DL_SceneParser* parser);
};
