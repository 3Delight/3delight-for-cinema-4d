#include "InstanceObjectTranslator.h"
#include <vector>
#include "DL_TypeConversions.h"
#include "lib_instanceobject.h"
#include "nsi.hpp"
#include "DL_Utilities.h"

bool InstanceObjectTranslator::Accept(BaseList2D* C4DNode)
{
	BaseObject* obj = (BaseObject*) C4DNode;
	BaseContainer* data = obj->GetDataInstance();
	long mode = data->GetInt32(INSTANCEOBJECT_RENDERINSTANCE_MODE);

	if (mode == INSTANCEOBJECT_RENDERINSTANCE_MODE_SINGLEINSTANCE || mode == INSTANCEOBJECT_RENDERINSTANCE_MODE_MULTIINSTANCE) {
		return true;
	}

	return false;
}

void InstanceObjectTranslator::CreateNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	InstanceObject* Iobj = (InstanceObject*) C4DNode;
	BaseObject* Refobj = Iobj->GetReferenceObject(doc);
	if (!Refobj)
	{
		return;
	}

	BaseContainer* data = Iobj->GetDataInstance();
	long mode = data->GetInt32(INSTANCEOBJECT_RENDERINSTANCE_MODE);

	if (mode == INSTANCEOBJECT_RENDERINSTANCE_MODE_MULTIINSTANCE)
	{
		// Create instances
		NSI::Context& ctx(parser->GetContext());
		std::string ref_handle_name = parser->GetHandleName(Refobj);
		std::string instance_handle = ref_handle_name + "instance";
		std::string ref_handle = "X0_" + ref_handle_name;

		ctx.Create(instance_handle, "instances");
		ctx.Connect(instance_handle, "", ParentTransformHandle, "objects");
		ctx.Disconnect(ref_handle, "", NSI_ALL_NODES, "objects");
		ctx.Connect(ref_handle, "", instance_handle, "sourcemodels");
	}
}

void InstanceObjectTranslator::ConnectNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	InstanceObject* Iobj = (InstanceObject*)C4DNode;
	BaseObject* Refobj = Iobj->GetReferenceObject(doc);
	if (!Refobj)
	{
		return;
	}

	//Get the color of the reference object.
	ObjectColorProperties prop;
	Refobj->GetColorProperties(&prop);
	Vector refObj_color = prop.color;

	std::string ref_handle_name = parser->GetHandleName(Refobj);
	std::string instance_handle = ref_handle_name + "instance";
	std::string ref_handle = "X0_" + ref_handle_name;

	NSI::Context& ctx(parser->GetContext());
	BaseContainer* data = Iobj->GetDataInstance();
	long mode = data->GetInt32(INSTANCEOBJECT_RENDERINSTANCE_MODE);

	if (mode == INSTANCEOBJECT_RENDERINSTANCE_MODE_SINGLEINSTANCE) 
	{ 
		//single instance mode
		ctx.Connect(ref_handle, "", ParentTransformHandle, "objects");
	}

	else if (mode == INSTANCEOBJECT_RENDERINSTANCE_MODE_MULTIINSTANCE)
	{
		// multi instance mode
		UInt n_instances = Iobj->GetInstanceCount();

		/*
			Get instances color and render them using color attribute on dlAttributeRead shader.
			First insert the color for the reference object as it does not count as an instance.
		*/
		std::vector<float> colordata;
		colordata.reserve(3 * (n_instances+1));

		Vector color_linear = toLinear(Vector(refObj_color.x, refObj_color.y, refObj_color.z), doc);;
		colordata.emplace_back(color_linear.x);
		colordata.emplace_back(color_linear.y);
		colordata.emplace_back(color_linear.z);
		for (UInt i = 0; i < n_instances; i++)
		{
			maxon::Color64 color = Iobj->GetInstanceColor(i);
			color_linear = toLinear(Vector(color.r, color.g, color.b), doc);
			colordata.emplace_back(color_linear.x);
			colordata.emplace_back(color_linear.y);
			colordata.emplace_back(color_linear.z);
		}

		NSI::Argument arg_vc("color");
		arg_vc.SetType(NSITypeColor);
		arg_vc.SetCount(n_instances+1);
		arg_vc.SetValuePointer((void*)&colordata[0]);
		ctx.SetAttribute(instance_handle, (arg_vc));
	}
}

void InstanceObjectTranslator::SampleAttributes(DL_SampleInfo* info,
		const char* Handle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	InstanceObject* Iobj = (InstanceObject*)C4DNode;
	BaseContainer* data = Iobj->GetDataInstance();
	BaseObject* Refobj = Iobj->GetReferenceObject(doc);
	if (!Refobj) 
	{
		return;
	}

	std::string instance_handle = parser->GetHandleName(Refobj) + "instance";
	long mode = data->GetInt32(INSTANCEOBJECT_RENDERINSTANCE_MODE);
	
	if (mode == INSTANCEOBJECT_RENDERINSTANCE_MODE_MULTIINSTANCE)
	{
		NSI::Context& ctx(parser->GetContext());

		Matrix parent_transform = Iobj->GetMg();
		Matrix parent_transform_inverse = ~parent_transform;
		UInt n_instances = Iobj->GetInstanceCount();
		double* matrices = new double[n_instances * 16 + 16];
		double* it = matrices;

		// Insert the reference object first as it does not count as an instance in C4D.
		Matrix ref_trans = Refobj->GetMg();
		ref_trans = parent_transform_inverse * ref_trans;;
		memcpy(it, MatrixToNSIMatrix(ref_trans).data(), 16 * sizeof(double));
		it += 16;
		for (UInt i = 0; i < n_instances; i++)
		{
			Matrix instance_transform = Iobj->GetInstanceMatrix(i);
			/*
				Instance matrix is in global coordinates.
				Multiply by inverse of parent transform to compensate
			*/
			instance_transform = parent_transform_inverse * instance_transform;
			std::vector<double> matrix = MatrixToNSIMatrix(instance_transform);
			memcpy(it, matrix.data(), 16 * sizeof(double));
			it += 16;
		}

		NSI::Argument xform("transformationmatrices");
		xform.SetType(NSITypeDoubleMatrix);
		xform.SetCount(n_instances+1);
		xform.SetValuePointer(matrices);
		ctx.SetAttribute(instance_handle, (xform));

		delete[] matrices;
	}
}
