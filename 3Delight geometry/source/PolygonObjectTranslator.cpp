#include "PolygonObjectTranslator.h"
#include <map>
#include <vector>
#include "DL_TypeConversions.h"
#include "DL_Utilities.h"
#include "IDs.h"
#include "nsi.hpp"
#include "tsubdiv.h"

#ifdef __APPLE__
#	pragma clang diagnostic ignored "-Wimplicit-float-conversion"
#endif

using namespace std;

static
int GetUVIndex(int poly_index,
			   float u,
			   float v,
			   map<int, vector<int>>& conmap,
			   vector<float>& st,
			   int& maxindex)
{
	vector<int>& con = conmap[poly_index];
	bool found = false;
	int result = 0;
	int index;
	int con_size = (int) con.size();

	for (int i = 0; i < con_size && !found; i++) {
		index = con[i];

		if (!(abs(st[2 * index] - u) > 0) && !(abs(st[2 * index + 1] - v) > 0)) {
			result = index;
			found = true;
		}
	}

	if (!found) {
		result = maxindex;
		con.push_back(result);
		maxindex++;
		st.push_back(u);
		st.push_back(v);
	}

	return result;
}

void PolygonObjectTranslator::Init(BaseList2D* C4DNode,
								   BaseDocument* doc,
								   DL_SceneParser* parser)
{
	// Check for SDS
	is_subd = false;
	BaseObject* tmp = (BaseObject*) C4DNode;
	BaseTag* sds_tag;
	bool found_SDS_tag = false;

	while (tmp != NULL && !found_SDS_tag) {
		sds_tag = tmp->GetTag(ID_DL_SUBDIVISIONSURFACETAG);

		if (sds_tag != NULL) {
			found_SDS_tag = true;
			BaseContainer* data = sds_tag->GetDataInstance();
			is_subd = data->GetBool(RENDER_AS_SDS);
		}

		tmp = tmp->GetUp();
	}

	// Check for Phong tag
	BaseObject* object = (BaseObject*) C4DNode;
	BaseTag* phongtag = object->GetTag(Tphong);
	has_phong = false;

	if (phongtag != NULL) {
		has_phong = true;
	}
}

void PolygonObjectTranslator::CreateNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	PolygonObject* object = (PolygonObject*) C4DNode;
	int polycount = object->GetPolygonCount();
	if (polycount <= 0)
		return;
	const CPolygon* polys = object->GetPolygonR();
	// Create a mesh and connect it to the parent transform
	string handle = string(Handle);
	string transform_handle = string(ParentTransformHandle);
	ctx.Create(handle, "mesh");
	ctx.Connect(handle, "", transform_handle, "objects");
	vector<int> nvertices(polycount);
	vector<int> indices;
	indices.reserve(polycount * 4);

	for (int j = 0; j < polycount; j++) {
		indices.push_back(polys[j].a);
		indices.push_back(polys[j].b);
		indices.push_back(polys[j].c);
		nvertices[j] = 3;

		if (polys[j].c != polys[j].d) {
			nvertices[j] = 4;
			indices.push_back(polys[j].d);
		}
	}

	NSI::Argument arg_nvertices("nvertices");
	arg_nvertices.SetType(NSITypeInteger);
	arg_nvertices.SetCount(polycount);
	arg_nvertices.SetValuePointer((void*) &nvertices[0]);
	NSI::Argument arg_indices("P.indices");
	arg_indices.SetType(NSITypeInteger);
	arg_indices.SetCount(indices.size());
	arg_indices.SetValuePointer((void*) &indices[0]);
	vector<int> facevarying_indices;
	facevarying_indices.reserve(polycount * 4);
	int nverts;
	int index = 0;

	for (int j = 0; j < polycount; j++) {
		nverts = 3;

		if (polys[j].c != polys[j].d) {
			nverts = 4;
		}

		for (int i = 0; i < nverts; i++) {
			facevarying_indices.push_back(index);
			index++;
		}
	}

	int n_facevertices = (int) facevarying_indices.size();
	ctx.SetAttribute(handle, (arg_nvertices, arg_indices));

	if (is_subd) {
		ctx.SetAttribute(handle,
						 NSI::StringArg("subdivision.scheme", "catmull-clark"));
	}

	// Export first UV set as "st"
	UVWTag* UVtag = (UVWTag*) object->GetTag(Tuvw);

	if (UVtag != NULL) {
		vector<int> st_indices;
		st_indices.reserve(n_facevertices);
		map<int, vector<int>> UV_connectivity_map;
		// For every mesh vertex index (first int), build a vector of indices
		// (second int) into the UV coordinate vector
		vector<float> st;
		st.reserve(n_facevertices * 2);
		int maxindex = 0;
		int uv_index = 0;

		for (int j = 0; j < polycount; j++) {
			UVWStruct uv = UVtag->GetSlow(j);
			uv_index = GetUVIndex(polys[j].a,
								  (float) uv.a.x,
								  (float) uv.a.y,
								  UV_connectivity_map,
								  st,
								  maxindex);
			st_indices.push_back(uv_index);
			uv_index = GetUVIndex(polys[j].b,
								  (float) uv.b.x,
								  (float) uv.b.y,
								  UV_connectivity_map,
								  st,
								  maxindex);
			st_indices.push_back(uv_index);
			uv_index = GetUVIndex(polys[j].c,
								  (float) uv.c.x,
								  (float) uv.c.y,
								  UV_connectivity_map,
								  st,
								  maxindex);
			st_indices.push_back(uv_index);

			if (polys[j].c != polys[j].d) {
				uv_index = GetUVIndex(polys[j].d,
									  (float) uv.d.x,
									  (float) uv.d.y,
									  UV_connectivity_map,
									  st,
									  maxindex);
				st_indices.push_back(uv_index);
			}
		}

		int st_flags = (NSIParamPerVertex);
		/*if (UV_subdivision_mode == SDSOBJECT_SUBDIVIDE_UV_STANDARD)
			        {
			                st_flags = st_flags | NSIParamInterpolateLinear;
			        }*/
		NSI::Argument arg_st("st");
		arg_st.SetCount(st.size() / 2);
		arg_st.SetArrayType(NSITypeFloat, 2);
		arg_st.SetValuePointer((void*) &st[0]);
		arg_st.SetFlags(st_flags);
		NSI::Argument arg_st_indices("st.indices");
		arg_st_indices.SetType(NSITypeInteger);
		arg_st_indices.SetCount(st_indices.size());
		arg_st_indices.SetValuePointer((void*) &st_indices[0]);
		ctx.SetAttribute(handle, (arg_st, arg_st_indices));
	}

	if (has_phong && !is_subd) {
		NSI::Argument arg_N_indices("N.indices");
		arg_N_indices.SetType(NSITypeInteger);
		arg_N_indices.SetCount(n_facevertices);
		arg_N_indices.SetValuePointer((void*) &facevarying_indices[0]);
		ctx.SetAttribute(handle, (arg_N_indices));
	}

	// Export primvars (polygonselections, vertexmaps, vertexcolors)
	BaseTag* tag = object->GetFirstTag();
	string tagname;

	while (tag) {
		tagname = StringToStdString(tag->GetName());

		if (tag->GetType() == Tpolygonselection) {
			// Export polygon selection
			SelectionTag* sTag = (SelectionTag*) tag;
			BaseSelect* bs = sTag->GetBaseSelect();
			vector<float> selection(polycount, 0);
			Int32 seg = 0, a, b, i;

			while (bs->GetRange(seg++, LIMIT<Int32>::MAX, &a, &b)) {
				for (i = a; i <= b; ++i) {
					selection[i] = 1;
				}
			}

			NSI::Argument arg_selection(tagname);
			arg_selection.SetCount(polycount);
			arg_selection.SetType(NSITypeFloat);
			arg_selection.SetValuePointer((void*) &selection[0]);
			arg_selection.SetFlags(NSIParamPerFace);
			ctx.SetAttribute(
				Handle, (arg_selection, NSI::StringArg(tagname + ".type", "float")));

		} else if (tag->GetType() == Tvertexmap) {
			// We only set the indices here.
			// The values themselves are exorted in SampleAttributes, for
			// correct motion sampling
			NSI::Argument vmap_indices(tagname + ".indices");
			vmap_indices.SetType(NSITypeInteger);
			vmap_indices.SetCount(indices.size());
			vmap_indices.SetValuePointer((void*) &indices[0]);
			ctx.SetAttribute(
				Handle, (vmap_indices, NSI::StringArg(tagname + ".type", "float")));

		} else if (tag->GetType() == Tvertexcolor) {
			// We only set the indices here.
			// The values themselves are exorted in SampleAttributes, for
			// correct motion sampling
			ctx.SetAttribute(Handle, (NSI::StringArg(tagname + ".type", "color")));
			VertexColorTag* vcTag = (VertexColorTag*) tag;

			if (vcTag->IsPerPointColor()) {
				NSI::Argument vc_indices(tagname + ".indices");
				vc_indices.SetType(NSITypeInteger);
				vc_indices.SetCount(indices.size());
				vc_indices.SetValuePointer((void*) &indices[0]);
				ctx.SetAttribute(Handle, (vc_indices));
			}
		}

		tag = tag->GetNext();
	}
}

void PolygonObjectTranslator::SampleAttributes(DL_SampleInfo* info,
		const char* Handle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	PolygonObject* object = (PolygonObject*) C4DNode;
	// Can we skip motion blur sampling if deformed==NULL?
	// No, the mesh may still have PLA.
	const Vector* points = object->GetPointR();
	const CPolygon* polys = object->GetPolygonR();
	int pointcount = object->GetPointCount();
	int polycount = object->GetPolygonCount();
	// Vertex positions
	vector<float> P(pointcount * 3);

	for (int i = 0; i < pointcount; i++) {
		P[i * 3] = (float) points[i].x;
		P[i * 3 + 1] = (float) points[i].y;
		P[i * 3 + 2] = (float) points[i].z;
	}

	NSI::Argument arg_P("P");
	arg_P.SetType(NSITypePoint);
	arg_P.SetCount(pointcount);
	arg_P.SetValuePointer((void*) &P[0]);
	string handle = string(Handle);
	ctx.SetAttributeAtTime(handle, info->sample_time, (arg_P));

	// Phong normals
	if (has_phong && !is_subd) {
		vector<Float32> N;
		N.reserve(polycount * 4 * 3);
		Vector32* normals = object->CreatePhongNormals();
		Vector32 tmp;
		long nverts;

		for (int j = 0; j < polycount; j++) {
			nverts = 3;

			if (polys[j].c != polys[j].d) {
				nverts = 4;
			}

			for (int n = 0; n < nverts; n++) {
				tmp = normals[4 * j + n];
				N.push_back(tmp.x);
				N.push_back(tmp.y);
				N.push_back(tmp.z);
			}
		}

		DeleteMem(normals);
		NSI::Argument arg_N("N");
		arg_N.SetType(NSITypeNormal);
		arg_N.SetCount(N.size() / 3);
		arg_N.SetValuePointer((void*) &N[0]);
		arg_N.SetFlags(NSIParamPerVertex);
		ctx.SetAttributeAtTime(handle, info->sample_time, (arg_N));
	}

	// Export vertex maps and vertex colors
	BaseTag* tag = object->GetFirstTag();
	string tagname;

	while (tag) {
		tagname = StringToStdString(tag->GetName());

		if (tag->GetType() == Tvertexmap) {
			VertexMapTag* vmTag = (VertexMapTag*) tag;
			const Float32* data = vmTag->GetDataAddressR();
			NSI::Argument arg_vm(tagname);
			arg_vm.SetCount(pointcount);
			arg_vm.SetType(NSITypeFloat);
			arg_vm.SetValuePointer((void*) data);
			arg_vm.SetFlags(NSIParamPerVertex | NSIParamInterpolateLinear);
			ctx.SetAttributeAtTime(Handle, info->sample_time, (arg_vm));

		} else if (tag->GetType() == Tvertexcolor) {
			VertexColorTag* vcTag = (VertexColorTag*) tag;
			ConstVertexColorHandle tagdata = vcTag->GetDataAddressR();

			if (vcTag->IsPerPointColor()) { // per vertex
				vector<Float32> colordata;
				colordata.reserve(3 * pointcount);
				maxon::Color32 color;
				Vector color_linear;

				for (int i = 0; i < pointcount; i++) {
					color = vcTag->GetColor(tagdata, nullptr, nullptr, i);
					color_linear = toLinear(Vector(color.r, color.g, color.b), doc);
					colordata.push_back(color_linear.x);
					colordata.push_back(color_linear.y);
					colordata.push_back(color_linear.z);
				}

				NSI::Argument arg_vc(tagname);
				arg_vc.SetType(NSITypeColor);
				arg_vc.SetCount(pointcount);
				arg_vc.SetValuePointer((void*) &colordata[0]);
				arg_vc.SetFlags(NSIParamPerVertex);
				ctx.SetAttributeAtTime(Handle, info->sample_time, (arg_vc));

			} else { // per polygon vertex
			}
		}

		tag = tag->GetNext();
	}
}
