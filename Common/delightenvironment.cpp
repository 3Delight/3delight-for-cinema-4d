#ifndef DELIGHT_ENVIRONMENT
#define DELIGHT_ENVIRONMENT

#include "delightenvironment.h"
#include "DL_TranslatorPlugin.h"
#include "DL_Utilities.h"
#include "c4d.h"
#include "delight.h"
#include "nsi.hpp"
#include "nsi_dynamic.hpp"

const char*
DelightEnv::getDelightEnvironment()
{
	NSI::DynamicAPI m_api;
	decltype(&DlGetInstallRoot) get_install_root = nullptr;
	m_api.LoadFunction(get_install_root, "DlGetInstallRoot");
	const char* _3delight_installation_root = get_install_root();
	return _3delight_installation_root;
}
#endif
