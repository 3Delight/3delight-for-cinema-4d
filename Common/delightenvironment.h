#ifndef DELIGHT_ENVIRONMENT_H
#define DELIGHT_ENVIRONMENT_H

#include "DL_TranslatorPlugin.h"
#include "c4d.h"

class DelightEnv
{
public:
	static const char* getDelightEnvironment();
};

#endif
